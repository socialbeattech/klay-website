<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Klay Schools
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="NCI7rPOLdmDjAOQF1ayflyVtUje9SoYZRjy8lUBgpDI" />
	<!--<link rel="profile" href="https://gmpg.org/xfn/11">!-->
	
	<!--script type="text/javascript" src="https://app.referralyogi.com/assets/bootstrap_carousel.js"></script-->
	<style>
	#mega-menu-wrap-menu-1 {
		display: inline-block;
	}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 li.mega-menu-row.mega-offset-md-2.offset-md-2 {
		margin-left: 16.666667%;
	}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 li.mega-menu-row.mega-offset-md-3.offset-md-3 {
		margin-left: 25%;
	}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 li.mega-menu-column.mega-offset-md-1.offset-md-1 {
		margin-left: 8.333333%;
	}
		#mega-menu-menu-1 .image{width:auto;display:inline-block}
	#mega-menu-menu-1>li>ul.mega-sub-menu:after{content:'';position:absolute;top:100%;background:url('<?php echo get_template_directory_uri() ?>/images/sub-menu-after.png');width:100%;height:12px;display:block!important}
	.mega-sub-menu,.mega-sub-menu .menu{list-style:none;padding-left:0;margin:0}
	.mega-sub-menu .menu a{color:#333;font-family:'GothamRoundedBook';font-size:14px}
	.mega-block-title{font-family:'GothamRoundedMedium'}
	.centres-sub-menu .custom-html-widget a{color:#333;font-family:'GothamRoundedMedium';font-size:14px}
	.centres-sub-menu .custom-html-widget{margin-top:15px}
	#mega-menu-wrap-menu-1.mega-menu-wrap>#mega-menu-menu-1.mega-menu>li.mega-menu-item>a.mega-menu-link{position:relative}
	#mega-menu-wrap-menu-1.mega-menu-wrap>#mega-menu-menu-1.mega-menu>li.mega-menu-item>a.mega-menu-link:before{content:'';border-top:15px solid #14b9c5;border-left:15px solid transparent;border-right:15px solid transparent;position:absolute;top:100%;left:50%;transform:translateX(-50%);display:block;z-index:9999;opacity:0;visibility:hidden;transition:all 0.5s}
	#mega-menu-wrap-menu-1.mega-menu-wrap>#mega-menu-menu-1.mega-menu>li.mega-menu-item:hover>a.mega-menu-link:before{opacity:1;visibility:visible}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 .text-center ul.mega-sub-menu li{text-align:center;line-height:1}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 .text-center ul.mega-sub-menu li.widget_media_image{margin-top:15px;margin-bottom:10px}
	.black-color{color:#333}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 li.mega-menu-item-has-children > a.mega-menu-link:after, #mega-menu-wrap-menu-1 #mega-menu-menu-1 li.mega-menu-item-has-children > a.mega-menu-link span.mega-indicator:after{display:none}
	#mega-menu-wrap-menu-1 #mega-menu-menu-1 li.mega-menu-row.mega-border-top-80dde3{border-top:1px solid #e0e0e0;padding-top:30px;margin-top:30px}
	

.textwidget h4 {
    color: #333333 !important;
}
	</style>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5SNLRMS');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if(is_page_template('landingpages/template-webinar-2019.php')) { }else{ ?>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SNLRMS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-58903261-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-58903261-1');
</script>
<?php } ?>
	<header id="masthead" class="site-header blue-bg">
		<div class="container-fluid">
			<div class="row">
				<div class="col-5 col-sm-5 col-md-2" style="max-height:110px">
					<a href="<?php bloginfo('url') ?>" rel="home" class="linkhome">
						<img src="<?php the_field('site_logo','option'); ?>" alt="<?php bloginfo('title') ?>" class="site-logo d-none d-sm-none d-md-inline-block d-lg-inline-block" />
						<img src="<?php the_field('site_logo_mobile','option'); ?>" alt="<?php bloginfo('title') ?>" class="site-logo d-inline-block d-sm-inline-block d-md-none d-lg-none" />
					</a>
				</div>
				<div class="col-7 col-sm-7 col-md-10">
					<div class="d-none d-sm-none d-md-block d-lg-block">
						<div class="row">
							<div class="col-12 text-right">
								<ul class="list-unstyled secondary-menu mb-0 yellow-bg d-inline-block text-left gotham-rounded-medium">
								<?php while(have_rows('secondary_menu','option')): the_row(); ?>
									<li>
										<a href="<?php the_sub_field('link') ?>">
											<img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('title'); ?>" />
											<span><?php the_sub_field('title'); ?></span>
										</a>
									</li>
								<?php endwhile; ?>
								</ul>
							</div>
							<nav id="site-navigation" class="main-navigation col-12 text-right gotham-rounded-medium">
								<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
									'menu_class'     => 'main-menu list-unstyled mb-0 gotham-rounded-medium fs-18 clearfix d-inline-block mt-10',
								) );
								?>
							</nav>
						</div>
					</div>
					<div class="d-block d-sm-block d-md-none d-lg-none">
						<div class="row">
							<div class="col-12 text-right">
								<ul class="list-unstyled secondary-menu-mobile mb-0 mt-15 d-inline-block text-left gotham-rounded-medium white-color fs-14">
									<li>
									<?php while(have_rows('secondary_menu_mobile','option')): the_row(); ?>
										<a href="<?php the_sub_field('link') ?>">
											<img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('title'); ?>" />
											<span><?php the_sub_field('title'); ?></span>
										</a>
									<?php endwhile; ?>
									</li>
									<li>
										<?php
											wp_nav_menu( array(
												'theme_location' => 'menu-2',
												'menu_id'        => 'mobile-menu',
												'menu_class'     => 'mobile-menu list-unstyled mb-0 gotham-rounded-medium fs-18 clearfix d-inline-block',
											) );
										/* ?>
										<a href="javascript:void(0)" class="navigation-toggle">
											<i class="fa fa-bars"></i>
										</a> <?php */ ?>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<script>
jQuery('#myModalklay').on('hidden.bs.modal', function (e) {
  // do something...
  jQuery('#myModalklay iframe').attr("src", jQuery("#myModalklay  iframe").attr("src"));
});
</script>
	<div class="modal fade" id="myModalklay">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
					<div class="embed-responsive embed-responsive-16by9" id="yt-player">
						<iframe width="100%" height="315" src="https://www.youtube.com/embed/hyFPq3CJ6Y8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
      </div>
    </div>
  </div>
	<section class="important-menu-items yellow-bg d-block d-md-none fs-16 gotham-rounded-medium">
		<div class="container-fluid text-center">
			<div class="row">
				<div class="col">
					<a href="<?php bloginfo('url') ?>/cities/">Centres</a>
				</div>
				<div class="col">
					<a href="<?php bloginfo('url') ?>/programs/">Programs</a>
				</div>
				<div class="col">
					<a href="<?php bloginfo('url') ?>/admission-enquiry/">Admissions</a>
				</div>
			</div>
		</div>
	</section>
	<section class="banner">
		<?php if(is_front_page()){ ?>
		<div class="swiper-container" id="bannerSwiper">
			<div class="swiper-wrapper">
				<?php $maxSlides = 0;  while(have_rows('home_page_slider','option')){the_row(); ?>
				<div class="swiper-slide bannerid<?php echo $maxSlides; ?>">
					<img src="<?php the_sub_field('desktop_image') ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
					<img src="<?php the_sub_field('mobile_image') ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
					<div class="banner-caption-holder">
						<div class="container-fluid">
							<div class="row">
								<div class="col-6 col-md-7 fs-xs-22 fs-36">
									<div class="gotham-rounded-medium banner-caption white-color text-center text-md-left">
										<?php the_sub_field('content') ?>
										<div class="mt-30 mt-xs-15"><a href="<?php the_sub_field('button_link') ?>" class="btn-banner"><?php the_sub_field('button_text') ?></a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $maxSlides++; } ?>
			</div>
			<div class="swiper-pagination"></div>
			<div class="home-slide-prev"></div>
			<div class="home-slide-next"></div>
		</div>
		<div class="blue-bg text-center border-top-80dde3">
			<ul class="banner-swiper-pagination list-unstyled pt-30 pb-30 gotham-rounded-medium fs-18 clearfix d-inline-block">
				<?php $i=0; while(have_rows('home_page_slider_pagination','option')){the_row(); ?>
				<li id= "klayvideo<?php echo $i; ?>" <?php if($i==2){?> data-toggle="modal" data-target="#myModalklay" <?php } if(get_sub_field('mobile_hidden')==1){ ?>class="d-none d-sm-none d-md-inline-block"<?php } ?>><?php if($i !== 2){ ?><a data-slide="<?php echo $i; ?>" href="<?php the_sub_field('link') ?>"><?php } ?><img src="<?php the_sub_field('image') ?>" alt="" /><span><?php the_sub_field('title') ?></span>&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i><?php if($i !== 2){ ?></a><?php } ?></li>
				<?php $i++; } ?>
			</ul>
		</div>
		<?php }elseif(is_tax()|| is_singular('centres')){
			?>
			<?php   $term11 = get_queried_object(); ?>
			<?php   $deskbanner = get_field('desktop_banner',$term11);
					$mobilebanner = get_field('mobile_banner',$term11);
					if($deskbanner) { ?>
		            	<img src="<?php the_field('desktop_banner',$term11); ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
					<?php }else{ ?>
						<img src="<?php bloginfo('url') ?>/wp-content/uploads/2018/12/Untitled-1.jpg" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
					<?php }  if($deskbanner) {  ?>
						<img src="<?php the_field('mobile_banner',$term11); ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
					<?php }else { ?>
						<img src="<?php bloginfo('url') ?>/wp-content/uploads/2018/12/mobile_bann.jpg" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
					<?php } ?>
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<?php //$term11 = get_queried_object(); ?>
						<?php $terms = get_the_terms( $post->ID, 'centres_category' );
				if ( !empty( $terms ) ){
				// get the first term
				$termnew = array_shift( $terms );
				//echo $termnew->slug;
				} ?>
						<h1 class="deskshow fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color')==1){ ?> white-color<?php } ?>">KLAY in <?php echo $termnew->name; ?></h1>
					</div>
				</div>
			</div>
		</div>
		<?php } else if(is_home() && ! is_front_page()) { ?>
		<img src="<?php the_field('desktop_banner',1313) ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
		<img src="<?php the_field('mobile_banner',1313) ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color')==1){ ?> white-color<?php } ?>"><?php single_post_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
		<?php } else if(is_singular('post')) { ?>
		<img src="<?php the_field('desktop_banner',1313) ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
		<img src="<?php the_field('mobile_banner',1313) ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color')==1){ ?> white-color<?php } ?>">Blogs</h1>
					</div>
				</div>
			</div>
		</div>
		<?php } else if(is_category() || is_tag()) { ?>
			<img src="<?php the_field('desktop_banner',1313) ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
			<img src="<?php the_field('mobile_banner',1313) ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
			<div class="title-holder">
				<div class="container-fluid">
					<div class="row">
						<div class="col-6 col-md-7 fs-xs-22 fs-36">
							<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color')==1){ ?> white-color<?php } ?>"><?php printf( __( '%s', 'mala' ), single_cat_title( '', false ) ); ?></h1>
						</div>
					</div>
				</div>
			</div>

		<?php } else if(is_singular('medias')) { ?>
		<img src="<?php the_field('desktop_banner',828) ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
		<img src="<?php the_field('mobile_banner',828) ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color',828)==1){ ?> white-color<?php } ?>">Media</h1>
					</div>
				</div>
			</div>
		</div>
		<?php } else if(is_singular('parents_tale')) { ?>
		<img src="<?php the_field('desktop_banner',65028) ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
		<img src="<?php the_field('mobile_banner',65028) ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color',828)==1){ ?> white-color<?php } ?>">Parents Tales</h1>
					</div>
				</div>
			</div>
		</div>
		
		<?php } else if(is_page_template('page_thankyou_template.php')) { ?>
		<img src="<?php the_field('desktop_banner') ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
		<img src="<?php the_field('mobile_banner') ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color',828)==1){ ?> white-color<?php } ?>"></h1>
					</div>
				</div>
			</div>
		</div>
		
		<?php } else { ?>
		<img src="<?php the_field('desktop_banner') ?>" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
		<img src="<?php the_field('mobile_banner') ?>" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100" />
		<div class="title-holder">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6 col-md-7 fs-xs-22 fs-36">
						<h1 class="fs-40 fs-xs-20 mb-50 mt-0 gotham-rounded-book<?php if(get_field('white_color')==1){ ?> white-color<?php } ?>"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</section>
