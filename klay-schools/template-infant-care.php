<?php
/**
 * Template Name: Infant Care
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title">Celebrating Every Milestone of your Child!</h2>
				<p>One of our most sought after programs, the infant care, has developmental and age appropriate curriculum that invites exploration and celebrates the important milestones that act as transition from infancy towards the active and more independent world of toddlers.</p>
				<p>The infant space is exclusively designed with safety and security of the little ones in mind.</p>
			</div>
		</div>
		<div class="row mt-30">
			<div class="col-12 col-md-8">
				<div class="swiper-container" id="singleSlideSwiper">
					<div class="swiper-wrapper">
						<?php while(have_rows('image_carousel')){the_row(); ?>
						<div class="swiper-slide">
							<img src="<?php the_sub_field('image') ?>" alt="" class="w-100" />
							<span class="small-caption gotham-rounded-medium fs-15 white-color"><em><?php the_sub_field('title') ?></em></span>
						</div>
						<?php } ?>
					</div>
					<div class="single-slide-prev"></div>
					<div class="single-slide-next"></div>
				</div>
				<div class="gotham-rounded-book mt-30"><?php the_content(); ?></div>
			</div>
			<div class="col-12 col-md-4">
				<div class="enquire-now pt-15 pb-15 no-label-form">
				<?php //echo do_shortcode(get_field('form')); ?>
				<?php the_field('iframe_for_singe_form','option'); ?>
				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hgm?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content'); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title'); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation'); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link'); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)

</script>

<?php
endwhile;
get_footer();
