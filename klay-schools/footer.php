<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package Klay Schools

 */



?>

<section class="and-theres-more-at-klay pt-40 pb-40 gotham-rounded-book">

	<div class="container-fluid">

		<div class="row">

			<div class="col-12">

				<?php if(!is_singular('centres')){ ?><h2 class="mt-0 mb-30 section-title">More at KLAY</h2><?php } ?>

			</div>

			<div class="col-12 col-md-8 offset-md-2 fs-18 fs-xs-14 fs-sm-14">

				<div class="row">

					<?php if(is_front_page()){

				

					 $i=0; while(have_rows('and_theres_more_at_klay','option')){ the_row(); ?>

					<div class="col text-center wow <?php if($i%2==0){ ?>fadeInUp<?php } else { ?>fadeInDown<?php } ?>">

						<a href="<?php the_sub_field('link'); ?>"><img src="<?php the_sub_field('image'); ?>" alt="" /><br />

						<span class="mt-10 d-inline-block"><?php the_sub_field('title'); ?></span></a>

					</div>

					<?php $i++; } }else if(is_tax()){

			

						$term1 = get_queried_object();

						$i=0; while(have_rows('more_at_klay_links',$term1)){ the_row(); ?>

					<div class="col text-center wow <?php if($i%2==0){ ?>fadeInUp<?php } else { ?>fadeInDown<?php } ?>">

						<a href="<?php the_sub_field('url'); ?>"><img src="<?php the_sub_field('image'); ?>" alt="" /><br />

						<span class="mt-10 d-inline-block"><?php the_sub_field('title'); ?></span></a>

					</div>

					<?php $i++; }

					}else if(is_singular('centres')){}else if(is_blog() ){



							if(have_rows('more_at_klay_links',1313)){

						$i=0; while(have_rows('more_at_klay_links',1313)){ the_row(); ?>

					<div class="col text-center wow <?php if($i%2==0){ ?>fadeInUp<?php } else { ?>fadeInDown<?php } ?>">

						<a href="<?php the_sub_field('url'); ?>"><img style="width: 131px;" src="<?php the_sub_field('image'); ?>" alt="" /><br />

						<span class="mt-10 d-inline-block"><?php the_sub_field('title'); ?></span></a>

					</div>

					<?php $i++; } } else{$i=0;while(have_rows('and_theres_more_at_klay','option')){ the_row(); ?>

					<div class="col text-center wow <?php if($i%2==0){ ?>fadeInUp<?php } else { ?>fadeInDown<?php } ?>">

						<a href="<?php the_sub_field('url'); ?>"><img  class="img123" src="<?php the_sub_field('image'); ?>" alt="" /><br />

						<span class="mt-10 d-inline-block"><?php the_sub_field('title'); ?></span></a>

					</div>

					<?php $i++; }}





					}else{ 

						

						if(have_rows('more_at_klay_links')){

						$i=0; while(have_rows('more_at_klay_links')){ the_row(); ?>

					<div class="col text-center wow <?php if($i%2==0){ ?>fadeInUp<?php } else { ?>fadeInDown<?php } ?>">

						<a href="<?php the_sub_field('url'); ?>"><img style="width: 131px;" src="<?php the_sub_field('image'); ?>" alt="" /><br />

						<span class="mt-10 d-inline-block"><?php the_sub_field('title'); ?></span></a>

					</div>

					<?php $i++; } } else{$i=0;while(have_rows('and_theres_more_at_klay','option')){ the_row(); ?>

					<div class="col text-center wow <?php if($i%2==0){ ?>fadeInUp<?php } else { ?>fadeInDown<?php } ?>">

						<a href="<?php the_sub_field('link'); ?>"><img  class="img123" src="<?php the_sub_field('image'); ?>" alt="" /><br />

						<span class="mt-10 d-inline-block"><?php the_sub_field('title'); ?></span></a>

					</div>

					<?php $i++; }} } ?>

				</div>

			</div>

		</div>

	</div>

</section>

<section class="blue-bg pt-30 pb-30">

	<div class="container-fluid text-center">

			<span class="gotham-rounded-medium white-color lh-35 fs-20 fs-xs-18">Get to know us better!</span>

			<ul class="list-unstyled d-inline-block social-icons mb-0 clearfix">

				<li><a target="_blank" href="<?php the_field('fb_link','option'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/fb.png" alt="" /></a></li>

				<li><a target="_blank" href="<?php the_field('tw_link','option'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/tw.png" alt="" /></a></li>

				<li><a target="_blank" href="<?php the_field('you_link','option'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/you.png" alt="" /></a></li>

				<li><a target="_blank" href="<?php the_field('linkedin_link','option'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/lk.png" alt="" /></a></li>

			</ul>

		</div>

	</div>

</section>

<footer id="colophon" class="site-footer yellow-bg pt-50 pb-50">



	<div class="container-fluid">

		<div class="row">

			<div class="col-12 col-md-8 offset-md-2 padd_sub">

		<?php echo do_shortcode('[gravityform id="5" title="true" description="true" ajax="true"]'); ?>

	</div>

</div>

		<div class="row">

			<div class="col-12 col-md-8 border-right-black o-flow-hidden">

				<div class="row only-these-widgets">

					<div class="col-12 col-md-3 gotham-rounded-book text-center">

						<?php dynamic_sidebar('footer-1'); ?>

					</div>

					<div class="col-12 col-md-3 gotham-rounded-book text-center">

						<?php dynamic_sidebar('footer-2'); ?>

					</div>

					<div class="col-12 col-md-3 gotham-rounded-book text-center">

						<?php dynamic_sidebar('footer-3'); ?>

					</div>

					<div class="col-12 col-md-3 gotham-rounded-book text-center">

						<?php dynamic_sidebar('footer-4'); ?>

					</div>

				</div>

			</div>

			<div class="col-12 col-md-4 black-color gotham-rounded-book fs-18 fs-xs-16 text-center text-md-left mt-xs-30">

				<?php dynamic_sidebar('footer-5'); ?>

			</div>

		</div>

	</div>

</footer>

<footer class="copyrights blue-bg pt-30 pb-30 white-color">

	<div class="container-fluid">

		<div class="row">

			<div class="col-12 col-md-12 gotham-rounded-book text-center">				

				<h4 class="gotham-rounded-medium fs-18 fs-xs-16 mt-0 mt-xs-0 mt-sm-0 mb-0">Founding Years Learning Solutions</h4>

				<p class="mb-0 mt-30 mt-xs-15 fs-16 fs-xs-14">Survey No. 31/1, Seetaramapalya, K. R. Puram Hobli, Bangalore - 560048.<br /><em>Landmark: Next to Sumadhura Vasanatham Appt.</em></p>

			</div>

		</div>

	</div>

</footer>

<section class="important-menu-items blue-bg d-block d-md-none fs-16 gotham-rounded-medium stricky_menu">

		<div class="container-fluid text-center">

			<div class="row">

				<div class="col">

					<a href="tel:7676708888">CALL</a>

				</div>

				<div class="col">

					<a href="<?php bloginfo('url') ?>/admission-enquiry/">Admissions</a>

				</div>

				

			</div>

		</div>

	</section>


	<!-- <script type="text/javascript">
	// Parse the URL
	/*function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}*/
	// Give the URL parameters variable names
	//var source = getParameterByName('utm_source');
	//var medium = getParameterByName('utm_medium');
	//var campaign = getParameterByName('utm_campaign');
	
	//
	
	 
	// Put the variable names into the hidden fields in the form. selector should be "p.YOURFIELDNAME input"
	
	//document.querySelector("p.utm_medium input").value = medium;
	//document.querySelector("p.utm_campaign input").value = campaign;
	//
	function func1() {
	  var source = window.location.href;
	  document.querySelector(".source input").value = source;
	}
	window.onload=func1;
	</script> -->

<!-- <script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>  -->

<?php wp_footer(); ?>



</body>

</html>

