<?php
/**
 * Template Name: Webinar
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();

while(have_posts()): the_post();
?>


<script type="text/javascript">
piAId = '564842';
piCId = '87227';
piHostname = 'pi.pardot.com';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script>
<style type="text/css">

#more {display: none;}
	.social-icons-str{display:none;}
button#myBtn{
	background: url('<?php echo get_template_directory_uri() ?>/images/load-more.png');
	/*background-position: left top;*/
    width: 296px;
    height: 48px;
    color: #fff;
    border: none;
    cursor: pointer;
    font-family: 'GothamRoundedBook';
    display: inline-block;
    transition: color 0.3s;
    line-height: 44px;
}.social-icons-str {
    width: 100%;
    float: left;
    text-align: right;
    padding: 8px 0;
}
section.panel-experts {
    margin: 0x 0;
    padding: 35px 0;
}
span.parent-des.gotham-rounded-book img {
    width: 100%;
    height: auto;
}
.panel-experts-webinar1 {
    width: 100%;
    padding: 15px 30px;
}
.col-12.col-md-12.load-more-button {
    padding: 25px 0 0 0;
}
@media only screen and (max-width:767px){
span.parent-des.gotham-rounded-book img {
    width: 75%;
    height: auto;
    align-items: center;
    margin: 0 11%;
}.social-icons-str {
    width: 86%;
    float: left;
    text-align: right;
    padding: 8px 0;
}
p.gotham-rounded-medium {
    font-size: 18px !important;
}
.col-12.col-md-5.panel-experts-mehana {
    padding: 10px 10px;
}
.panel-experts-webinar1 {
    width: 100%;
    padding: 15px 0;
}
}
</style>



<section class="panel-experts">
	<div class="container-fluid">

		<div class="row">

			

			<div class="col-12 col-md-6 panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span></div>

					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-one'); ?></span>
				</div>
			</div>

			
			<!-- <div class="col-12 col-md-1"></div> -->

			<div class="col-12 col-md-6  panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-two'); ?></span>
				</div>
			</div>

			<!-- <div class="col-12 col-md-1"></div> -->

		</div>

		<div class="row">

			

			<div class="col-12 col-md-6 panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-three'); ?></span>
				</div>
			</div>

			

			<div class="col-12 col-md-6  panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-four'); ?></span>
				</div>
			</div>

			

		</div>


	<!-- 	<div class="row">

			

			<div class="col-12 col-md-6 panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-two'); ?></span>
				</div>
			</div>

			

			<div class="col-12 col-md-6  panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-two'); ?></span>
				</div>
			</div>

			

		</div> -->

		<div id="dots"></div>
		<div id="more">

		<div class="row">

			

			<div class="col-12 col-md-6 panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-five'); ?></span>
				</div>
			</div>

			

			<div class="col-12 col-md-6  panel-experts-mehana">
				<div class="panel-experts-webinar1">
					<div class="social-icons-str">
					<span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span></div>
					
					<span class="parent-des gotham-rounded-book"><?php the_field('webinar-six'); ?></span>
				</div>
			</div>

		

		</div>

		

	</div>

	<div class="row" style="text-align: center;">

		<div class="col-12 col-md-12 load-more-button">

		<button onclick="myFunction()" id="myBtn">Load More Webinars</button>

	</div>

	</div>


		

	
	</div>
</section>

<section class="panel-experts">
	<div class="container-fluid">
	<hr>
</div>

</section>
<script type="text/javascript">

function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Load More Webinars"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Load Less Webinars"; 
    moreText.style.display = "inline";
  }
}

</script>
<?php
endwhile;
get_footer();
