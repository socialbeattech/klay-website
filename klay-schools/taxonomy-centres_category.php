<?php


get_header();

?>


<section class="pt-50 pb-50 section_map">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 gotham-rounded-book">
				<?php echo $description = term_description(); ?>
				<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );?>
				<select class="form-control" id="dynamic_select">
					<option>Centres in <?php echo $term->name; ?> </option>
					<?php
							global $post; $i = 1;
							$args = array( 'numberposts' => -1, 'post_type' => 'centres', 'centres_category' =>$term->slug,'meta_query' => array( array( 'key'=> 'upcoming_events','compare'	=> '!=',
	        'value'		=>'1',
	    )
    )  );
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) { setup_postdata($post);
						?>
					<option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>
				<?php } ?>
				</select>
			</div>
			<div class="col-sm-6">
				<div class="embed-responsive embed-responsive-16by9 border-map">
					<?php $term = get_queried_object();
				echo $test_conent = get_field('map_iframe', $term); ?>

				</div>
			</div>
		</div>

	</div>
</section>
<section class="section_post">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">Centres in <?php echo $term->name; ?></h2>
			</div>
		</div>
		<div class="resultneww">
		<?php
		global $post; $i = 1;
		$args1 = array( 'posts_per_page' => '-1','paged' => 1, 'post_type' => 'centres', 'centres_category' =>$term->slug,'meta_query' => array( array( 'key'=> 'upcoming_events','compare'	=> '!=',
	        'value'		=>'1',
	    )
    )    );
		$args = array( 'posts_per_page' => '-1','paged' => 1, 'post_type' => 'centres', 'centres_category' =>$term->slug ,'meta_query' => array( array( 'key'=> 'upcoming_events','compare'	=> '!=',
	        'value'		=>'1',
	    )
    )   );
		$my_posts = new WP_Query( $args );
		$myposts = get_posts( $args1 );
		$count = count($myposts);
		if ( $my_posts->have_posts() ) :
		?>
		<div class="row post_pad my-posts">
			<?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
				<div class="col-6 col-sm-3">
					<a href="<?php the_permalink(); ?>">
						<div class="bordernew">
							<?php the_post_thumbnail( 'centre', array( 'class' => 'img-responsive center-block' ) );?>
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
			<?php endwhile ?>
		</div>
		<?php /* if($count >= 17){ ?>
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="image_center padd_btn">
						<span class="load_more load-btn" att="<?php echo $term->slug ?>">Load More Centres</span>
					</div>
				</div>
			</div>
<?php } */?>
</div>

		<?php endif ?>
</div>
		<h3 class="gotham-rounded-medium">
			<ul class="list-inline">
				<li class="first_li">Other cities:</li>
				<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;
				foreach($categories as $key => $value){	?>
					<li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name; ?></a></li>
					<li>|</li>
				<?php } ?>
			</ul>
		</h3>
	</div>
</section>
<section class="what_parent">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>

			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>
<div id='ry-review_embed'><script src=https://klayschools.referralyogi.com/review_embed/klayschools data-page='1' async defer></script></div>
<!-- <script id='load-ry-review' src=https://klayschools.referralyogi.com/review_notification data-page='1' async defer></script></script> -->
<?php
get_footer();
