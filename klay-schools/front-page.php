<?php
/**
 * Template Name: Front
 *
 * This is the template that displays home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Klay Schools
 */

get_header();
while ( have_posts() ) : the_post(); ?>
<section class="bubble-bg pt-40 pb-100 pb-xs-75" id="icon-box-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title"><?php the_field('section_1_title'); ?></h2>
				<h4 class="mt-0 mb-30 section-sub-title"><?php the_field('section_1_sub_title'); ?></h4>
			</div>
			<div class="col-12 pb-30  pb-xs-0">
				<div class="row">
					<?php $i=0; while(have_rows('trust_your_child_content')){the_row(); ?>
					<div class="col-6 col-md-3 mb-xs-30">
						<div class="icon-box pb-15 text-center white-color <?php the_sub_field('color') ?>-bg wow flipInY" data-wow-delay="0.<?php echo $i ?>s">
							<img src="<?php the_sub_field('icon') ?>" alt="" />
							<h5 class="mt-0 mb-15 asparagus fs-34 fs-xs-24 mb-xs-5"><?php the_sub_field('title') ?></h5>
							<div class="fs-17 gotham-rounded-book fs-xs-14"><?php the_sub_field('content') ?></div>
						</div>
					</div>
					<?php $i++; } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="yellow-bg pt-0 pb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3">
				<div class="row">
					<?php while(have_rows('infographics')){ the_row(); ?>
						<div class="col-4 col-md-4 text-center white-color">
							<img src="<?php the_sub_field('icon') ?>" class="half-top" alt="" />
							<h5 class="asparagus fs-35 fs-xs-24 mt-75 mt-xs-50 mb-0"><span class="count fs-30" data-refer="#icon-box-section" data-start="0" data-end="<?php the_sub_field('number') ?>"></span> +</h5>
							<p class="mb-0 gotham-rounded-book fs-xs-16 fs-18"><?php the_sub_field('title') ?></p>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="heart-bg pt-55 pt-xs-40">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title"><?php the_field('section_2_title'); ?></h2>
				<h4 class="mt-0 mb-30 section-sub-title"><?php the_field('section_2_sub_title'); ?></h4>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<div class="row">							
							<div class="col-12 col-md-5 order-md-last text-center text-md-left">
								<div class="speech-bubble pos-rel white-color gotham-rounded-book fs-22 fs-xs-18 wow zoomIn" data-wow-delay="1s">
									<img class="" src="<?php echo get_template_directory_uri() ?>/images/speech-bubble.png" alt="" />
									<div class="speech-bubble-alignment"><?php the_field('klay_for_parents_content'); ?></div>									
								</div>
								<div class="d-block text-center mt-30 wow slideInUp" data-wow-delay="1.2s">
									<a href="<?php the_field('klay_for_parents_link'); ?>" class="btn-long"><?php the_field('klay_for_parents_cta_text'); ?>&nbsp;<i class="fa fa-caret-right"></i></a>
								</div>
							</div>
							<div class="col-12 col-md-6 text-center text-md-right order-md-first mt-xs-30 wow zoomIn">
								<img src="<?php the_field('klay_for_parents_image'); ?>" alt="" />
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="blue-bg home_st text-center">
		<div class="container-fluid">
			<div class="row">
				<ul class="nav nav-tabs nav-fill gotham-rounded-medium col-12">
					<?php $i=0; while(have_rows('klay_for_parents')){ the_row(); ?>
					<li class="nav-item">
						<a class="nav-link" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?>&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i></a>
					</li>
					<?php $i++; } ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="star-bg pt-55 pt-xs-40" id="corporate-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title"><?php the_field('section_3_title'); ?></h2>
				<h4 class="mt-0 mb-30 section-sub-title"><?php the_field('section_3_sub_title'); ?></h4>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<div class="row">
							<div class="col-12 col-md-4 offset-md-1 text-center text-md-right">
								<div class="normal-bubble white-color gotham-rounded-light fs-20 fs-xs-18 pos-rel d-inline-block wow zoomIn" data-wow-delay="1s">
									<img class="" src="<?php echo get_template_directory_uri() ?>/images/nomal-bubble-pink.png" alt="" />
									<div class="normal-bubble-alignment text-center"><?php the_field('klay_for_corporates_content'); ?></div>
								</div>
								<div class="text-center mt-30 wow slideInUp" data-wow-delay="1.2s">
									<a href="<?php the_field('klay_for_corporates_link'); ?>" class="btn-medium"><?php the_field('klay_for_corporates_cta_text'); ?>&nbsp;<i class="fa fa-caret-right"></i></a>
								</div>
							</div>
							<div class="col-12 col-md-7 text-center text-md-right mt-xs-30 wow zoomIn">
								<img src="<?php the_field('klay_for_corporates_image'); ?>" alt="" />
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="blue-bg home_st text-center">
		<div class="container-fluid">
			<div class="row">
				<ul class="nav nav-tabs nav-fill gotham-rounded-medium col-12">
					<?php $i=0; while(have_rows('klay_for_corporates')){ the_row(); ?>
					<li class="nav-item">
						<a class="nav-link" href="<?php the_sub_field('Link'); ?>"><?php the_sub_field('title'); ?>&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i></a>
					</li>
					<?php $i++; } ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="cloudy-bg pt-40 pb-0 d-none d-sm-none d-md-block d-lg-block">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title">We are in <span class="count" data-start="0" data-end="7" data-refer="#corporate-section">7</span> Cities</h2>
			</div>
			<div class="col-12 col-md-10 offset-md-1 mt-50 mb-50 mb-xs-0 mb-sm-0 z-index-adjust">
				<div class="row">
					<div class="col-12 col-md-7 pb-40">
						<h3 class="fs-22 gotham-rounded-medium mb-20">KLAY is committed to caring for the world,<br />one family at a time.</h3>
						<div class="content"><?php the_content(); ?></div>
					</div>
					<div class="col-12 col-md-4 offset-md-1 pb-40 pb-xs-0 pb-sm-0">
						<div class="d-none d-sm-none d-md-block d-lg-block">
							<div class="location-content">
								<ul class="list-unstyled text-center papaya-sunrise locations fs-24 text-uppercase">
									<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;	
				foreach($categories as $key => $value){	?>
									<li><a href="<?php echo get_category_link($value->term_id); ?>"><span><?php echo $value->name; ?></span></a></li>
								<?php } ?>
									<!-- <li><a href=""><span>Chennai</span></a></li>
									<li><a href=""><span>Gurgaon</span></a></li>
									<li><a href=""><span>Hyderabad</span></a></li>
									<li><a href=""><span>Mumbai</span></a></li>
									<li><a href=""><span>noida</span></a></li>
									<li><a href=""><span>pune</span></a></li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="blue-bg pb-60 location-buses">
	</div>
</section>
<?php
endwhile; // End of the loop.
get_footer();
