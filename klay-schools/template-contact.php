<?php
/**
 * Template Name: Contact Us
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 image_center">
				<h2 class="mt-0 section-title">How can we help you?</h2>
			</div>
		</div>
		
	</div>	
</section>
<section class="contact_sec">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3 image_center">
				<!-- <img src="<?php echo get_template_directory_uri() ?>/images/contact/contact_back.png" alt="" /> -->
				<div class="contact-form1">
					<div class="contact-form11">
					<?php //echo do_shortcode('[gravityform id="2" title="true" description="true" ajax="true"]'); ?>
					<iframe src="https://go.pardot.com/l/563842/2019-02-26/4x3y5?Source_URL=<?php the_permalink(); ?>" width="100%" height="450" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
				</div>
				</div>
			</div>
		</div>
	</div>	
</section>
<section class="office_sec">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-12 image_center">
				<div class="office">
					<?php 
					$fff = get_field('contact'); $i=1;  $lastElement = count($fff); while(have_rows('contact')){the_row(); ?>

						<h1 class="gotham-rounded-medium"><?php the_sub_field('title') ?></h1>
						<p class="gotham-rounded-book"><?php the_sub_field('address') ?> <a href="mailto:<?php the_sub_field('email') ?>"><?php the_sub_field('email') ?></a></p>
						<p class="gotham-rounded-book"><?php the_sub_field('phone') ?></p>
                        <?php if($lastElement !== $i){ ?>
                        <hr>
                        <?php } $i++; ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>	
</section>
<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>
<?php
endwhile;
get_footer();
