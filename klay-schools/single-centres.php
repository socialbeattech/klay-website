<?php
/**

 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>

<section class="pt-50 pb-50 center-single">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				
				<h2 class="mt-0 mb-15 section-title text-xs-center"><span class="orange-color"><?php the_title(); ?></span></h2>
			</div>
		</div>
		<div class="row mt-50 mt-xs-30">
			<div class="col-12 col-md-8 hrmargin">
				<div class="swiper-container" id="singleSlideSwiper">
					<div class="swiper-wrapper">
						<?php while(have_rows('image_carousel')){the_row(); ?>
						<div class="swiper-slide">
							<?php // set the image url
					$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
							<img src="<?php the_sub_field('image') ?>" alt="<?php echo $image_alt; ?>" class="w-100" />
							<span class="small-caption gotham-rounded-medium fs-15 white-color"><em><?php the_sub_field('title') ?></em></span>
						</div>
						<?php } ?>
			
					</div>
					<div class="single-slide-prev"></div>
			<div class="single-slide-next"></div>
				</div>
				<div class="gotham-rounded-book mt-30"><?php the_content(); ?></div>
				<div class="mobile_show"><hr></div>
				<?php if(get_field('programs_and_timing')) { ?>
				<div class="pro_timie">
					<h2 class="gotham-rounded-medium">Programs and Timing</h2>
					<?php while(have_rows('programs_and_timing')){the_row(); ?>
					<h3 class="gotham-rounded-medium"><?php the_sub_field('title') ?></h3>
					<?php while(have_rows('toddler')){the_row(); ?>
					<p class="gotham-rounded-book"><?php the_sub_field('text') ?><span> <?php the_sub_field('value') ?></span></p>
					<?php } } ?>
				</div>

			<?php } ?>

			</div>
			<div class="col-12 col-md-4">
				<div class="enquire-now pt-15 pb-15 no-label-form">
				<?php //echo do_shortcode(get_field('form')); ?>
				
				<!-- <div class="gform_heading">
				                            <h3 style="font-family: 'GothamRoundedMedium';font-weight: 700; font-size: 1.25em;" class="gform_title">Enquire Now</h3>
				                            <span class="gform_description">Get in touch with us to schedule a visit or for any further information that you may require.</span>
				                        </div> -->
				<?php the_field('iframe_for_singe_form','option') ?>
				<?php $terms = get_the_terms( $post->ID, 'centres_category' );
				if ( !empty( $terms ) ){
				// get the first term
				$termnew = array_shift( $terms );
				//echo $termnew->slug;
				} ?>
				<?php $city_name =  $termnew->name; ?>
				<?php if($city_name == 'Bengaluru'){ ?>
					
<iframe src="http://go.pardot.com/l/563842/2019-04-04/53h8r?Source_URL=<?php the_permalink(); ?>" id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			<?php }elseif ($city_name == 'Chennai') { ?>

				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53h8t?Source_URL=<?php the_permalink(); ?>" id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>

			<?php }elseif ($city_name == 'Gurgaon') { ?>
				
<iframe src="http://go.pardot.com/l/563842/2019-04-04/53h8y?Source_URL=<?php the_permalink(); ?>" id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			<?php }elseif ($city_name == 'Hyderabad') { ?>
				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hc3?Source_URL=<?php the_permalink(); ?>" id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			<?php }elseif ($city_name == 'Mumbai') { ?>
				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hc7?Source_URL=<?php the_permalink(); ?>" id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			<?php }elseif ($city_name == 'Noida') { ?>
				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hc9?Source_URL=<?php the_permalink(); ?>" id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			<?php }else{ ?>

<!-- <iframe src="http://go.pardot.com/l/563842/2019-04-04/53hcc?"<?php// echo $_SERVER['QUERY_STRING'] ?> id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> -->
<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hcc?Source_URL=<?php the_permalink(); ?> id="pardot" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>

			<?php } 


			 ?>

				<div class="map_add">
					<?php $mapsrc= get_field('map_image'); if($mapsrc){ ?>
					<div class="image_center iframe_src"><iframe src="<?php the_field('map_image'); ?>" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div><?php } ?>
					<p class="address gotham-rounded-book"><?php the_field('address'); ?></p>
					<p class="phoneicon gotham-rounded-medium"><a href="tel:<?php the_field('phone'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png"><?php the_field('phone'); ?></a></p>

				</div>
				</div>
			</div>
			<?php $director_title = get_field('director_title');
if($director_title){ ?>
			<hr>
		<?php } ?>
		</div>
	</div>
</section>
<?php 
//$director_title = get_field('director_title');
if($director_title){ ?>
<section class="director_sec">
	<div class="container-fluid">
		<div class="pro_timie">
			<h2 class="gotham-rounded-medium">Meet our Centre Director</h2>
		</div>
		<div class="row director_area">
			<div class="col-sm-2 image_center">
<?php // set the image url
					$image_url = get_field('director_image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<img src="<?php the_field('director_image'); ?>" alt="<?php echo $image_alt; ?>">
			</div>
			<div class="col-sm-10">
				<div class="pro_timie"><h3 class="gotham-rounded-medium"><?php the_field('director_title'); ?></h3></div>
				<p><?php the_field('director_content'); ?></p>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php $Walkthrough= get_field('google_ads');
if($Walkthrough){ ?>
<section id="tech_village" class="tech_village">
	<div class="container-fluid hrmargin">
		<hr>
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title">Walkthrough our centre at <span class="orange-color"><?php the_title(); ?></span></h2>
				
				<iframe src="<?php the_field('google_ads'); ?>" width="100%" height="550" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" oallowfullscreen="true" msallowfullscreen="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<section class="section_post">
	<div class="container-fluid hrmargin">
		<hr>
		<div class="row">
			<div class="col-sm-12 text-center">
				<?php $terms = get_the_terms( $post->ID, 'centres_category' );
				if ( !empty( $terms ) ){
				// get the first term
				$termnew = array_shift( $terms );
				//echo $termnew->slug;
				} ?>
				<h2 class="section-title">Other Centres in <?php echo $termnew->name; ?></h2>
			</div>
		</div>
		
		<?php
		global $post; $i = 1;
		$args = array( 'posts_per_page' => '4','paged' => 1, 'post_type' => 'centres', 'centres_category' =>$termnew->slug);
		$my_posts = new WP_Query( $args );
		if ( $my_posts->have_posts() ) : 
		?>
		<div class="row post_pad my-posts">

		<?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
		<div class="col-6 col-sm-3">
		<a href="<?php the_permalink(); ?>"><div class="bordernew">
		<?php the_post_thumbnail( 'centre', array( 'class' => 'img-responsive center-block' ) );?>
		<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
		</div></a>
		</div>
		<?php endwhile ?>
		
		</div>
		<?php endif ?>

		<h3 class="gotham-rounded-medium blue-color" style="color: #14b9c5;"><a href="<?php bloginfo( 'url' );?>/daycare-preschool/<?php echo $termnew->slug; ?>">View all Centres in <?php echo $termnew->name; ?> ></a></h3>
		
		<h3 class="gotham-rounded-medium">
			<ul class="list-inline">
				<li class="first_li">Other cities:</li>
				<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;	
				foreach($categories as $key => $value){	?>
					<li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name; ?></a></li>
					<li>|</li>
				<?php } ?>
			</ul>
		</h3>
	</div>
</section>
<?php
				if($test_conent){ ?>

				 $term = get_queried_object();
				$test_conent = get_field('testimonial_content', $term);  ?>
<section class="what_parent">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>
<?php } ?>



<?php
endwhile; ?>
<?php if( have_posts() ){
                    while( have_posts() ){ the_post();?>

                    	<?php 

global $post;
//echo $post->ID;
$current_post_id = $post->ID; echo $review = get_field('google_review', $current_post_id); ?>

                    <?php } } ?>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>
<?php get_footer(); ?>
