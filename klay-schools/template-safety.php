<?php
/**
 * Template Name: Safety
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
	a.down_safety {
    font-weight: bold;
    font-size: 22px;
}
.gotham-rounded-book.read_child {
    font-size: 20px;
    padding-top: 25px;
    color: #006699
}
.gotham-rounded-book.read_child a{
 
    color: #006699;
}
.gotham-rounded-book.read_child a:hover {
    color: #FF4369 !important;
}
.gotham-rounded-book.read_child img {
    padding-left: 15px;
}
section.banner {
    margin-top: 7%;
}ul {
  list-style-image: url('<?php echo get_template_directory_uri();?>/images/safety-list.png');
}.row.safety-center {
    background: #ebebf1;
}span.safety-center-img img {
    width: 100%;
    height:auto;
}span.safety-center-content {
    width: 100%;
    float: left;
    padding: 40px 10px;
}li.gotham-rounded-book.slist.fs-18 {
    padding: 10px 5px;
}
span.safety-training-img img {
    width: 100%;
    padding:8% 5%;
}
hr {
    border-top: 1px solid #999999;
    width: 95%;
}.safety.col-12.col-md-7.pt-20 {
    padding-left: 35px;
}.social-icons-str {
    padding: 15px 0;
}
@media only screen and (max-width:767px){
section.banner {
    margin-top: 0;
}span.safety-center-img {
    width: 100%;
    float: left;
    padding: 10px 35px;
}.row.safety-center {
    background: #fff;
}
ul {
    padding: 0 15px;
}span.safety-center-content {
    width: 100%;
    float: left;
    padding: 0;
}.safety.col-12.col-md-7.pt-20 {
    padding-left: 15px;
}.section-title1 {
    font-size: 30px;
    font-family: 'AsparagusSprouts';
    text-align: center;
    line-height: 1;
}
}
</style>

<script type="text/javascript">
	jQuery(document).ready(function($){
	$('.read_child a').on('mouseover', function() {
		
  $(this).find('img').attr('src', 'https://www.klayschools.com/wp-content/uploads/2019/03/dloadico_hover.png');
});
		$('.read_child a').on('mouseleave', function() {
		
  $(this).find('img').attr('src', 'https://www.klayschools.com/wp-content/uploads/2019/03/dloadico.png');
});
	});
</script>


<section class="pt-50 pb-20 pb-xs-10">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 gotham-rounded-book mb-10 text-left fs-20 fs-xs-16">Keeping in mind the tender age of our young learners, child safety is a top priority at KLAY. Our <b><span class="italic gotham-rounded-bold"> Safety Officers in every centre </span></b>make sure that the centres are safety compliant and the stringent safety, health and hygiene standards that we follow are adhered to. </h2>


				<div class="gotham-rounded-book read_child"><a target="_blank" href="https://www.klayschools.com/wp-content/uploads/2019/03/Child-Safety-Policy-FYLS.pdf">Read more about our <span class="italic gotham-rounded-bold">Child Protection Policy</span><img src="https://www.klayschools.com/wp-content/uploads/2019/03/dloadico.png" alt="dloadico"></a></div>
			</div>
		</div>
<!--     <hr class="gray-line"> -->
    </div>    
</section>

<section class="pt-30 pb-50 pb-xs-10">
	<div class="container-fluid">
		<h2 class="mt-0 mb-15 section-title1 fs-xs-18">Safety at the Centres</h2>
		<div class="row safety-center">
			<div class="col-12 col-md-5" style="padding: 0;">
				<?php // set the image url
					$image_url = get_field('safety-centres-imgae');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<span class="safety-center-img"><img src="<?php the_field('safety-centres-imgae');?>" alt="<?php echo $image_alt; ?>"></span>
			</div>
			<div class="col-12 col-md-7">
				<span class="safety-center-content">
				 <?php if( have_rows('saftety-centres-content') ): ?> <ul> <?php while ( have_rows('saftety-centres-content') ) : the_row();?><li class="gotham-rounded-book slist fs-18"><?php the_sub_field('content'); ?></li><?php  endwhile; ?> </ul><?php else : endif;	?></span>
			</div>
		</div> 
	</div>   
</section>

<section class="pt-30 pb-50 pb-xs-30 pt-xs-10">
	<div class="container-fluid">
		<h2 class="mt-0 mb-15 section-title1 fs-xs-18">Safety Training at Klay</h2>
		<div class="row safety-trining" style="background: #ebebf1;">
			<div class="col-12 col-md-5" style="padding: 0;">
				<?php // set the image url
					$image_url = get_field('safety-training-image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<span class="safety-training-img"><img src="<?php the_field('safety-training-image');?>" alt="<?php echo $image_alt; ?>"></span>
			</div>
			<div class="col-12 col-md-7 pt-20">
				<p class="gotham-rounded-medium pt-30 fs-22">First Aid</p>
				<div class="gotham-rounded-book fs-18"><?php the_field('safety-training-content');?></div>
			</div>
			<hr class="gray-line"> 
		</div> 

		<div class="row safety-trining" style="background: #ebebf1;">
			<div class="col-12 col-md-5 d-block d-sm-block d-md-none" style="padding: 0;">
				<?php // set the image url
					$image_url = get_field('fire-safety-image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<span class="safety-training-img"><img src="<?php the_field('fire-safety-image');?>" alt="<?php echo $image_alt; ?>"></span>
			</div>
			<div class="safety col-12 col-md-7 pt-20">
				<p class="gotham-rounded-medium pt-30 fs-22 fs-xs-18">Fire Safety</p>
				<div class="gotham-rounded-book fs-18"><?php the_field('fire-safety-content');?></div>
			</div>
			<div class="col-12 col-md-5 d-none d-sm-none d-md-block" style="padding: 0;">
				<?php // set the image url
					$image_url = get_field('fire-safety-image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<span class="safety-training-img"><img src="<?php the_field('fire-safety-image');?>" alt="<?php echo $image_alt; ?>"></span>
			</div>
			
			<hr class="gray-line"> 
		</div> 

		<div class="row safety-trining" style="background: #ebebf1;">
			<div class="col-12 col-md-5" style="padding: 0;">
				<?php // set the image url
					$image_url = get_field('pocso-image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<span class="safety-training-img"><img src="<?php the_field('pocso-image');?>" alt="<?php echo $image_alt; ?>"></span>
			</div>
			<div class="col-12 col-md-7 pt-20">
				<p class="gotham-rounded-medium pt-30 fs-22">POCSO</p>
				<div class="gotham-rounded-book fs-18"><?php the_field('pocso-content');?></div>
			</div>
			
		</div> 

	</div>   
</section>

<section class="pt-30 pb-50 pb-xs-30">
	<div class="container-fluid">
		<h2 class="mt-0 mb-15 section-title1 fs-42">A Short Movie on Child Safety by Klay</h2>
		<div class="col-12 col-md-12 text-center" style="padding: 0;">
			<span class="safety-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/3T7Sox_h9Zs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></span>
			<div class="social-icons-str">
					<!-- <span class="gotham-rounded-medium">Share</span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png" alt="facebook"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png" alt="twitter"></a></span>
					<span class="social-icons"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.png" alt="linkedin"></a></span> -->
					<?php echo do_shortcode('[addtoany]'); ?></div>
					<a target="_blank" class="down_safety" href="<?php the_field('click_to_download_child_safety'); ?>">Click to Download child safety</a>
				</div>
		
		</div>
		<div class="col-12 col-md-12 text-right" style="padding: 0;">
		
	</div>   
</section>

<hr class="gray-line"> 

<?php
endwhile;
get_footer();
