<?php
/**
 * Template Name: Parent resources
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
.iframe_head h3.gform_title {
    letter-spacing: normal!important;
    margin: -4px 0 6px !important;
    font-family: 'AsparagusSprouts';
    font-size: 42px;
}
.menu {
    width: 100%;
}
	.parent-helpline.in_img img {
    width: 100%;
    height: 100%;
}
.row.parent-helpline {
    background: #e9e9e9;
    padding: 40px 35px 0 35px;
}
#login_submit,#logout_submit {
    margin: 0;
    background: url('<?php echo get_template_directory_uri() ?>/images/submit1.png');
    background-position: left top;
    width: 122px;
    height: 42px;
    color: #fff !important;
    border: none;
    cursor: pointer;
    font-family: 'GothamRoundedBook';
    display: inline-block;
    transition: color 0.3s;
    line-height: 44px;
    text-align: center;
}
input#login_submit::placeholder,input#logout_submit::placeholder {
    color: #fff !important;
}
#login_submit:hover, .btn-submit:hover,#logout_submit:hover {
    background-position: 0 -42px;
}
.parent-helpline.image_center {
    width: 100%;
}
input[type="text"],input[type="password"]{
    width: 100%;
    float: left;
    padding: 8px 6px;
    border-radius: 4px;
    border: 1px solid #808587;
}.col-12.col-md-4.form-field {
    width: 100%;
    float: left;
}.col-12.col-md-3.form-field {
    float: left;
    padding: 0;
    width: 100%;
   
}
.contact-form111.admin_form {
    background: #e9e9e9;
    padding: 1% 30%;
}.contact-form111.admin_form {
    background: #e9e9e9;
    padding: 3% 30%;
}
button#myBtn1{
	background: url('<?php echo get_template_directory_uri() ?>/images/load-more.png');
	/*background-position: left top;*/
    width: 296px;
    height: 48px;
    color: #fff;
    border: none;
    cursor: pointer;
    font-family: 'GothamRoundedBook';
    display: inline-block;
    transition: color 0.3s;
    line-height: 44px;
}
.free-parent-helpline a:hover{color:#ee5d73 !important;}
@media only screen and (max-width:767px){
.row.parent-helpline {
    background: #e9e9e9;
    padding: 15px 0px 0 0px;
}
.contact-form111.admin_form {
    background: #e9e9e9;
    padding: 3% 3%;
}
}
</style>
<section class="pt-50 pb-30 story_sec">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-12">
				<h2 class="mt-0 mb-0 section-title fs-42">Parenting Resources from your Trusted Parenting Partner </h2>
				<p class="gotham-rounded-book"><?php the_field('first-para'); ?></p>
			</div>
		</div>
		<div class="row parent-helpline">
			<div class="col-12 col-md-5">
                <?php // set the image url
                    $image_url = get_field('parent_helpline_img');
                    $image_id = pippin_get_image_id($image_url); 
                    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                ?>
				<div class="parent-helpline in_img"><img src="<?php echo the_field('parent_helpline_img');?>" alt="<?php echo $image_alt; ?>"></div>
			</div>
			<div class="col-12 col-md-7">
				<p class="gotham-rounded-medium free-parent-helpline fs-22">Free Parent Helpline</p>
				<hr>
				<p class="gotham-rounded-book free-parent-helpline fs-22"><?php the_field('parent-helpline-para'); ?></p>
				<p class="gotham-rounded-medium free-parent-helpline pt-15 fs-17"><a href="<?php bloginfo('url') ?>/parent-helpline/" target="_blank" style="color: #0099cc">Learn more about Parent Helpline ></a></p>
				<p class="gcolo gotham-rounded-medium free-parent-helpline pt-15 fs-17" id="showmenu" style="cursor: pointer;">Send us your Query Now ></p>
			</div>
			<div class="menu" style="display: none;">
					<div class="contact-form111 enquire-now pt-15 pb-15 ffset-md-3 no-label-form admin_form">
						<?php //echo do_shortcode('[gravityform id="3" title="true" description="true" ajax="true"]'); ?>
                        <div></div>
                        <div class="gform_heading iframe_head">
                            <h3 class="gform_title">Reach our Experts</h3>
                            <span class="gform_description"></span>
                        </div>
                    <iframe src="https://go.pardot.com/l/563842/2019-02-26/4x3yt" width="100%" height="300" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
					</div>
				</div>
		</div>
	</div>
</section>
<section class="pt-30 pb-30 story_sec">
	<div class="container-fluid">
		
		<div class="row parent-helpline">
			<div class="col-12 col-md-7">
				<p class="gotham-rounded-medium free-parent-helpline fs-22">Workshops/Webinars</p>
				<hr>
				<p class="gotham-rounded-book free-parent-helpline fs-22"><?php the_field('workshop-para'); ?></p>
				<p class="gotham-rounded-medium free-parent-helpline pt-15 fs-17"><a href="<?php bloginfo('url') ?>/webinars/" target="_blank" style="color: #0099cc">View All Webinars ></a></p>
				<p class="gcolo gotham-rounded-medium free-parent-helpline pt-15 fs-17" id="showmenu1">Upcoming Webinars ></p>
			</div>
			<div class="col-12 col-md-5">
                <?php // set the image url
                    $image_url = get_field('workshop-img');
                    $image_id = pippin_get_image_id($image_url); 
                    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                ?>
				<div class="parent-helpline in_img"><img src="<?php echo the_field('workshop-img');?>" alt="<?php echo $image_alt; ?>"></div>
			</div>
			
		</div>
		<div class="menu1" style="display: none;">
		<div class="row parent-helpline">
			
						<div class="col-12 col-md-6">
							<p class="gotham-rounded-book free-parent-helpline fs-22">March 14th, 11.30 am</p>
							<p class="gotham-rounded-medium free-parent-helpline pt-15 fs-17" style="color: #0099cc">March Women’s Day Bridge The Gap: Empowering Women to Chase their Dreams</p>
						</div>

						<div class="col-12 col-md-6">
							<p class="gotham-rounded-book free-parent-helpline fs-22">Nov 29, 11.30 am</p>
							<p class="gotham-rounded-medium free-parent-helpline pt-15 fs-17" style="color: #0099cc">Developing healthy children: Not an easy task!</p>
						</div>
			</div>
			<div class="row parent-helpline" style="text-align: left;">

		<div class="col-12 col-md-12 load-more-button pb-30">
<!-- 
		<button onclick="myFunction()" id="myBtn1">Register for the Webinars</button> -->

	</div>

	</div>
		</div>
	</div>
</section>
<section class="pt-50 pb-30 story_sec">
	<div class="container-fluid">
		<div class="row parent-helpline">
			<div class="col-12 col-md-5">
				<div class="parent-helpline in_img"><a href="<?php bloginfo('url') ?>/blog/put-your-own-mask-on-before-assisting-others/" target="_blank">
                    <?php // set the image url
                    $image_url = get_field('parent-blog-para1');
                    $image_id = pippin_get_image_id($image_url); 
                    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                ?>
                <img src="<?php echo the_field('parent-blog-para1');?>" alt="<?php echo $image_alt; ?>"></a></div>
			</div>
			<div class="col-12 col-md-7">
				<p class="gotham-rounded-medium free-parent-helpline fs-22">Parenting Blogs</p>
				<hr>
				<p class="gotham-rounded-book free-parent-helpline fs-22"><?php the_field('parent-blog-para'); ?></p>
				<p class="gotham-rounded-medium free-parent-helpline pt-15 fs-17"><a href="https://www.klayschools.com/blog/category/blog/" target="_blank" style="color: #0099cc">View Blogs ></a></p>
				
			</div>
		</div>
	</div>
</section>
<?php /* ?><section class="parent-helpline_sec" style="margin: 20px 0;">
	<div class="container-fluid">
		<div class="row">
			<div class="parent-helpline image_center">
				<form action="">
				<!-- <img src="<?php echo get_template_directory_uri() ?>/images/contact/contact_back.png" alt="" /> -->
				<div class="contact-form1" style="margin: 20px 0;">
					<h2 class="mt-0 mb-0 section-title fs-42">Get Involved</h2>
					<p class="gotham-rounded-book" style="text-align: center;">Contribute to the KLAY community. <br>
Login to your account and share a KLAY story, write a blog or a testimonial!</p>
					<div class="contact-form11 enquire-now pt-15 pb-15 ffset-md-3 no-label-form admin_form parent_login_new">
						<!-- <div class="col-12 col-md-4 form-field"><input type="text" name="username" placeholder="Username"></div>
						<div class="col-12 col-md-4 form-field"><input type="text" name="psw" placeholder="Password"></div>
						<div class="col-12 col-md-3 form-field"><input type="text" name="Login" placeholder="Login" id="get-involved"></div> -->

			     <?php
            if(is_user_logged_in())
            {
                ?>
                    <style>#login_form{display: none;}</style>
                <?php
            }
            else
            {
                ?>
                    <style>#logout_form,#logout_submit{display: none;}</style> 
                <?php
            }
        ?>

<form id="logout_form" method="post" action="<?php bloginfo('url') ?>/wp-admin/admin-ajax.php" onsubmit="event.preventDefault();">
<input type="hidden" name="action" value="logout" />
<input type="submit" id="logout_submit" value="Logout"/>
</form>          
<form id="login_form" method="post" action="<?php bloginfo('url') ?>/wp-admin/admin-ajax.php" onsubmit="event.preventDefault();">
<div class="col-12 col-md-4 form-field"><input type="text" name="username" id="username" placeholder="Username"/></div>
<div class="col-12 col-md-4 form-field"><input type="password" name="password" id="password" placeholder="Password"/>
</div>
<div class="col-12 col-md-3 form-field"><input type="hidden" name="action" value="login" />
<input type="submit" id="login_submit" value="Login" /></div> 
</form>
        
        
        <script>
            window.document.getElementById("logout_submit").addEventListener("click", function(e){
                var xhr = new XMLHttpRequest();
                xhr.open("GET", "<?php bloginfo('url') ?>/wp-admin/admin-ajax.php?action=logout&_wpnonce=df87b0006a", true);
                xhr.onload = function(){
                    location.reload();
                };
                xhr.send();
                xhr.setRequestHeader("X_REQUESTED_WITH","xmlhttprequest");

            }, false);
            
            window.document.getElementById("login_submit").addEventListener("click", function(e){
                var xhr = new XMLHttpRequest();
                var username = document.getElementById("username").value;
                var password = document.getElementById("password").value;
                xhr.open("GET", "<?php bloginfo('url') ?>/wp-admin/admin-ajax.php?action=login&username="+username+"&password="+password, true);
                xhr.onload = function(){
                    if(xhr.responseText == "TRUE")
                    {
                        location.reload();
                    }
                    else
                    {
                        alert("Please check username and password");
                    }
                };
                xhr.setRequestHeader("X_REQUESTED_WITH","xmlhttprequest");
                xhr.send();
            }, false);
        </script>
				
			</div>
		</div>
	</div>	
</section>
<?php */ ?>
<script type="text/javascript">
	  $(document).ready(function() {
        $('#showmenu').click(function() {
                $('.menu').slideToggle("fast");
        });
    });
</script>
<script type="text/javascript">
	  $(document).ready(function() {
        $('#showmenu1').click(function() {
                $('.menu1').slideToggle("fast");
        });
    });
</script>
<?php
endwhile;
get_footer();
