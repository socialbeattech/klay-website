<?php
/**
 * Template Name: Gallery
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title">KLAY - Video & Photo Galleries</h2>
			</div>
			<div class="col-12 gotham-rounded-book">
				<?php the_content(); ?>
			</div>


		</div>
	</div>
</section>
<section class="sec_gallery">
	<div class="container-fluid">
		<div class="row">
			<?php $i=1; while(have_rows('gallery')){the_row(); ?>
				<?php // set the image url
					$image_url = get_sub_field('single_image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
			<div class="col-12 col-md-6">
				<a href="#myModal<?php echo $i; ?>"  data-toggle="modal" data-target="#myModal<?php echo $i; ?>"><img class="imgwth" src="<?php the_sub_field('single_image') ?>" alt="<?php echo $image_alt; ?>"></a>
				<a href="#myModal<?php echo $i; ?>"  data-toggle="modal" data-target="#myModal<?php echo $i; ?>"><h3 class="gotham-rounded-book"><?php the_sub_field('title') ?><h3></a>
			</div>
			<div class="modal fade" id="myModal<?php echo $i; ?>">
<div class="modal-dialog modal-dialog-centered">
<div class="modal-content">
	
<span data-dismiss="modal">&times;</span>


<?php $gal_image = get_sub_field('image'); ?>

<div class="swipeslider_event">
			<div class="swiper-container swiper-container-gallery<?php echo $i; ?>">
			    <div class="swiper-wrapper">
						<?php foreach ($gal_image as $gal_image1) { ?>
							<?php // set the image url
					//$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($gal_image1['url']); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
						<div class="swiper-slide">
							<img src="<?php echo $gal_image1['url']; ?>" alt="<?php echo $image_alt; ?>"/>
							
						</div>
						<?php  } ?>
					</div>
					<div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
				</div>
			</div>



</div>
</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
	   var swiper<?php echo $i; ?> = new Swiper('.swiper-container-gallery<?php echo $i; ?>', {
	   	loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    $('#myModal<?php echo $i; ?>').on('shown.bs.modal',function(){
    	swiper<?php echo $i; ?>.update();
    });
    });
</script>
		<?php $i++;} ?>
		</div>
	</div>
</section>
<section class="sec_virtual" id="gallerytour">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title">Virtual Tour Gallery</h2>
			</div>
		</div>
		<div class="contact-form1 deskview" >
					<div class="contact-form11">
		<div class="row">
			<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC','exclude'=> array(19) ); 
			      $categories = get_terms( 'centres_category', $args);	
			      $i = 0;						
			      foreach($categories as $key => $value){	?>
			<div class="col-md-2">
			<?php
		global $post; $i = 1;
		$args = array( 'posts_per_page' => '8', 'post_type' => 'centres', 'centres_category' =>$value->slug ,'meta_query' => array(
		array('key' => 'google_ads','value' => '','compare' => '!=') ));
		$myposts = get_posts( $args ); ?>
		<h2 class="gotham-rounded-medium"><?php echo $value->name; ?></h2>
		<?php foreach( $myposts as $post ) { setup_postdata($post); 
		?>
			
			<a href="<?php the_permalink(); ?>/#tech_village"><h3 class="gotham-rounded-book"><?php the_title(); ?></h3></a>
		<?php $i++; } ?>
			
			</div>
		<?php } ?>
			
		</div>
	</div>
</div>
		<div class="contact-form1 mobileview" >
					<div class="contact-form11">
		<div class="row">
			<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC','exclude'=> array(19)); 
			      $categories = get_terms( 'centres_category', $args);	
			      $i = 0;						
			      foreach($categories as $key => $value){	?>
			<div class="col-md-12">
			<?php
		global $post; $i = 1;
		$args = array( 'posts_per_page' => '8', 'post_type' => 'centres', 'centres_category' =>$value->slug ,'meta_query' => array(
		array('key' => 'google_ads','value' => '','compare' => '!=') ));
		
		$myposts = get_posts( $args ); ?>
		<h2 class="gotham-rounded-medium accordion"><?php  echo $value->name; ?></h2>
		<div class="panel">
		<?php foreach( $myposts as $post ) { setup_postdata($post); 
		?>
			
			<a href="<?php the_permalink(); ?>/#tech_village"><h3 class="gotham-rounded-book"><?php  the_title(); ?></h3></a>
		<?php $i++; } ?>
			
			</div>
		</div>
		<?php } ?>
			</div>
		</div>
	</div>
</div>
	</div>
</section>
<?php
endwhile; ?>
<section class="section_event">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Upcoming Events</h2>
			</div>
		</div>
		<div class="swipeslider_event">
			<div class="swiper-container swiper-container-event">
			    <div class="swiper-wrapper">
					<?php 
					
					$today = date('Ymd');
						global $post; $i = 1;
						$args = array( 'posts_per_page' => '-1', 'post_type' => 'events','order' => 'ASC','meta_query' => array(
	     array(
	        'key'		=> 'date',
	        'compare'	=> '>=',
	        'value'		=> $today,
	    )
    ) );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) { setup_postdata($post); 
					?>
						<div class="swiper-slide image_center">
							<h2 class="gotham-rounded-book"><?php the_title(); ?></h2>
							<a href="<?php the_post_thumbnail_url(); ?>" data-rel="lightbox"><?php the_post_thumbnail('event-gal'); ?></a>
						</div>
			      	<?php } ?>
					<div class="swiper-button-next"></div>
				    <div class="swiper-button-prev"></div>
				</div>
			</div>
		</div>
		<div class="image_center padd_btn">
						<a href="<?php bloginfo('url') ?>/news-events/"><span class="load_more load-btn" att="Events">View all events</span></a>
					</div>
	</div>

</section>
<?php
while(have_posts()): the_post();
endwhile; 

get_footer(); ?>
