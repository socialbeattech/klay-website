<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Klay Schools
 */

get_header(); ?>

<section class="pt-50 pb-50 post_cat">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 cat_title_head">
				<h2 class="section-title">Child development and Parenting Blogs</h2>
				<p class="gotham-rounded-book">Tips, Advice, Fun Activities and more!</p>	
			</div>
		</div>
		<div class="row">	

			<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC'); 
					      $categories = get_terms( 'category', $args);	
					      $i = 0;
					      foreach($categories as $key => $value){ ?>
			<div class="col-12 col-md-4 mb-30 align-self-start">

				<?php $image_cat = get_field('category_image', $value->taxonomy . '_' . $value->term_id); ?>
				<?php // set the image url
					//$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($image_cat); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>


				<?php if(!empty($image_cat)){ ?>
					<a href="<?php echo get_category_link($value->term_id); ?>"><img src="<?php echo $image_cat; ?>" alt="<?php echo $image_alt; ?>" class="h-auto"></a>					
				<?php }else{ ?>
					<a href="<?php echo get_category_link($value->term_id); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/def-img.jpg" alt="" class="h-auto" alt="default_image"/></a>
				<?php } ?>
				<h4 class="mt-0 mb-0 pt-15 pb-15 gotham-rounded-medium fs-18"><a href="<?php echo get_category_link($value->term_id); ?>" class="black-color"><?php echo $value->name; ?></a></h4>
				
				
			</div>
			<?php } ?>		
		</div>
		<!-- <div class="row">
			<div class="col-sm-12 text-center pt-50 pb-0">
				<span class="load_more_blogs load-btn">Load More Blogs</span>
			</div>
		</div> -->
		<hr>
	</div>
</section>

<section class="latest_blog">
	<div class="container-fluid">
<div class="row">
			<div class="col-12 cat_title_head">
				<h2 class="section-title">Latest Blogs</h2>
		
			</div>
		</div>
		<div class="row">			
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="col-12 col-md-4 mb-30 align-self-start">
				<div class="image_re">
				<?php if(!empty(get_the_post_thumbnail())){ ?>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full',array('class'=>'w-100 h-auto')); ?></a>					
				<?php }else{ ?>
					<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/default-thumbnail.jpg" alt="" class="w-100 h-auto" /></a>
				<?php } ?>
				<h4 class="blue-bg mt-0 mb-0 pt-15 pb-15 gotham-rounded-medium fs-18 pos-abs-blog-title"><a href="<?php the_permalink(); ?>" class="white-color"><?php the_title() ?></a></h4>
			</div>
				<p class="gotham-rounded-book excerpt_hi"><?php excerpt('30'); ?></p>
				<p class="author gotham-rounded-book"><?php the_field('blogger_label'); ?>: <?php the_field('parent_blogger'); ?> <span class="star">*</span><br>
				<?php the_date('d/m/y'); ?></p>
				<a class="gotham-rounded-medium read_post" href="<?php the_permalink(); ?>">Read More ></a>
				
			</div>
			<?php endwhile; ?>		
		</div>
		<!-- <div class="row">
			<div class="col-sm-12 text-center pt-50 pb-0">
				<span class="load_more_blogs load-btn">Load More Blogs</span>
			</div>
		</div> -->
	</div>
</section>
<?php 
get_footer();