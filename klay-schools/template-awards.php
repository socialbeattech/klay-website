<?php
/**
 * Template Name: Awards
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
section.banner {
    margin-top: 7%;
}
@media only screen and (max-width:767px){
section.banner {
    margin-top: 0;
}h1.fs-40.fs-xs-20.mb-50.mt-0.gotham-rounded-book{
	font-size: 20px;
    width: 320px;
    text-align: center;
    margin-bottom: 0;
    position: relative;
    bottom: 0;
    top: 28px !important;
    font-weight: bold;
}
}
</style>
<!-- <section class="pt-50 pb-50 story_sec">
	<div class="container-fluid">
		<div class="row grey_back">
			<div class="col-12 col-md-6">
					<div class="pad_story">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12 col-md-6 story_img">
					<?php the_post_thumbnail('full', array( 'class' => 'img-responsive center-block imgwth' )); ?>
				</div>
			</div>
			
		</div>
</section> -->

<section class="pt-50 pb-50 pb-xs-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				
				<h2 class="mt-0 gotham-rounded-book mb-30 text-center fs-24 fs-xs-16"><?php the_field('top-awards-content'); ?></h2>
				<!-- <div class="popup-gallery"> --><p class="gotham-rounded-boo text-center mb-50 mt-50"><img class="" src="<?php the_field('awards-banner'); ?>" alt="awards"/></p><!-- </div> -->

				<?php if( have_rows('awards-content') ): while ( have_rows('awards-content') ) : the_row();?>
				 <p class="gotham-rounded-book text-center mb-30 mt-30 fs-18"><?php the_sub_field('awards-des'); ?><br>
				 <span class="italic gotham-rounded-bold text-center mb-30 mt-30 fs-18" style="color: #ff6600;font-weight: bold;"><?php the_sub_field('awards-by'); ?></span></p>
				<?php  endwhile; else : endif;	?>
			</div>
		</div>
    <hr class="gray-line">
    </div>    
</section>

<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>   




<!-- <script type="text/javascript">
	$(document).ready(function() {
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		
	});
});
</script> -->
<?php
endwhile;
get_footer();
