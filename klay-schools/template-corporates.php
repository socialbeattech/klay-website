<?php
/**
 * Template Name: Corporates
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */
get_header();
while(have_posts()): the_post();
?>
<style type="text/css">

.col-md-3.col-lg-3.col-sm-12.text-center.item {
    margin: 30px 0;
}
span.cor-content {
    width: 100%;
    float: left;
    padding: 6px 0;
    background: #eeeef2;
    font-weight: bold;
}
/*img.cor-image:hover {
    bottom: 5px;
    transition-duration: 0.5s;
    margin-bottom: -5px;
}*/
.bordernew1 img {
    width: 100%;
    height: 210px;
}.bordernew1 {
    width: 100%;
    float: left;
    padding: 15px 15px;
    border: 1px solid #fff;
}
.bordernew1:hover {
    width: 100%;
    float: left;
    padding: 15px 15px;
    border: 1px solid #000;
}
img.cor-image {
    position: relative;
    bottom: 0;
}
hr {
    border-top: 2px solid #eeedf5;
    width: 100%;
}
</style>
<section class="corporate mt-50">

	<div class="container-fluid">

		<h1 class="section-title" style="color: #ef7a45; text-align: center; padding-top: 50px">Corporates</h1>
		<hr>

	</div>

	<div class="container-fluid">

		<div class="row">

			<div class="col-md-3 col-lg-3 col-sm-12 text-center item">
				<div class="bordernew1">
				<span class="cor-img"><a href="http://thelittlecompany.co.in/our-centers/creche-daycare-in-mumbai/" target="_blank"><img class="cor-image" src="<?php echo get_template_directory_uri() ?>/images/cor-1.jpg" alt="mumbai"></a></span>
				
				<span class="cor-content">Mumbai</span>
				</div>
			</div>


			<div class="col-md-3 col-lg-3 col-sm-12 text-center item">
				<div class="bordernew1">
				<span class="cor-img"><a href="http://thelittlecompany.co.in/daycare-in-bangalore/" target="_blank"><img class="cor-image" src="<?php echo get_template_directory_uri() ?>/images/cor-2.jpg" alt="bangalore"></a></span>
				
				<span class="cor-content">Bangalore</span></div>
			</div>

			<div class="col-md-3 col-lg-3 col-sm-12 text-center item">
				<div class="bordernew1">
				<span class="cor-img"><a href="http://thelittlecompany.co.in/daycare-in-hyderabad/" target="_blank"><img class="cor-image" src="<?php echo get_template_directory_uri() ?>/images/cor-3.jpg" alt="hyderabad"></a></span>
				
				<span class="cor-content">Hyderabad</span></div>
			</div>

			<div class="col-md-3 col-lg-3 col-sm-12 text-center item">
				<div class="bordernew1">
				<span class="cor-img"><a href="http://thelittlecompany.co.in/daycare-in-guntur/" target="_blank"><img class="cor-image" src="<?php echo get_template_directory_uri() ?>/images/cor-4.jpg" alt="guntur"></a></span>
				
				<span class="cor-content">Guntur</span></div>
			</div>

			
			<div class="col-md-3 col-lg-3 col-sm-12 text-center item">
				<div class="bordernew1">
				<span class="cor-img"><a href="http://thelittlecompany.co.in/our-centers/preschool-in-gurgaon/" target="_blank"><img class="cor-image" src="<?php echo get_template_directory_uri() ?>/images/cor-5.jpg" alt="gurgaon"></a></span>
				
				<span class="cor-content">Gurgaon</span></div>
			</div>


	</div>



</section>

<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>
<?php
endwhile;
get_footer();
