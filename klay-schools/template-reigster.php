<?php  
/* 
Template Name: Register Page
*/
 

get_header();
?>
 

	<section class="pt-50 pb-50 aa_loginForm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3 image_center">

				<h2 class="mt-0 mb-15 section-title">Register with us</h2>
	
	<?php
	$error= '';
	$success = '';
 
	global $wpdb, $PasswordHash, $current_user, $user_ID;
 
	if(isset($_POST['task']) && $_POST['task'] == 'register' ) {
 
		
		$password1 = $wpdb->escape(trim($_POST['password1']));
		$password2 = $wpdb->escape(trim($_POST['password2']));
		//$first_name = $wpdb->escape(trim($_POST['first_name']));
		$location = $wpdb->escape(trim($_POST['location']));
		//$last_name = $wpdb->escape(trim($_POST['last_name']));
		$email = $wpdb->escape(trim($_POST['email']));
		$username = $wpdb->escape(trim($_POST['username']));
		
		if( $email == "" || $password1 == "" || $password2 == "" || $username == "") {
			$error= 'Please don\'t leave the required fields.';
		} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error= 'Invalid email address.';
		} else if(email_exists($email) ) {
			$error= 'Email already exist.';
		} else if($password1 <> $password2 ){
			$error= 'Password do not match.';		
		} else {
 
			$user_id = wp_insert_user( array ('first_name' => apply_filters('pre_user_first_name', $first_name), 'last_name' => apply_filters('pre_user_last_name', $last_name),'location' =>  $location , 'user_pass' => apply_filters('pre_user_user_pass', $password1), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => 'subscriber' ) );
			 update_user_meta( $user_id, 'location', $location);
			if( is_wp_error($user_id) ) {
				$error= 'Error on user creation.';
			} else {
				do_action('user_register', $user_id);
				
				$success = 'You\'re successfully register';
			}
			
		}
		
	}
	?>
 
        <!--display error/success message-->
	<div id="message">
		<?php 
			if(! empty($err) ) :
				echo '<p class="error">'.$err.'';
			endif;
		?>
		
		<?php 
			if(! empty($success) ) :
				echo '<p class="error">'.$success.'';
			endif;
		?>
	</div>
 <div class="contact-form1">
					<div class="contact-form11 register_form">
	<form method="post">
	<p><input type="text" value="" name="username" id="username" placeholder="Username"/></p>
	<p><input type="text" value="" name="email" id="email" placeholder="E-mail"/></p>
	<p><select id="location" name="location" class="location">
	<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
		  $categories = get_terms( 'centres_category', $args);
		  $i = 0;	
		  foreach($categories as $key => $value){	?>
			 <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>
		<?php } ?>
	</select></p>
	<p><input type="password" value="" name="password1" id="password1" placeholder="Password"/></p>
	<p><input type="password" value="" name="password2" id="password2" placeholder="Confirm Password"/></p>
	<button type="submit" name="btnregister" class="button" >Register Now</button>
	<input type="hidden" name="task" value="register" />
	<p><?php if($sucess != "") { echo $sucess; } ?> <?php if($error!= "") { echo $error; } ?></p>
</form>
</div>
</div>
</div>
</div>
 </div>
	</section>
<?php get_footer(); ?>