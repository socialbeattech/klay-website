function g_map(lat,lon,name,address,zoom,id)
{
	var myLatlng=new google.maps.LatLng(lat,lon);
	var mapOptions={zoom:zoom,center:myLatlng,mapTypeId:google.maps.MapTypeId.ROADMAP,scrollwheel:false};
	var map=new google.maps.Map(document.getElementById(id),mapOptions);
	var image = {
		url:siteurls.template_url+'/images/map-marker.png',
		scaledSize:new google.maps.Size(40,40)
		};
	var marker=new google.maps.Marker({
		position:myLatlng,
		map:map,
		title:""+name+"",
		animation:google.maps.Animation.DROP,icon:image
	});
	var infowindow=new google.maps.InfoWindow({
		content:""+address+""
	});
	google.maps.event.addListener(marker,'click',function(){
		infowindow.open(map,marker);
	});
	google.maps.event.addListenerOnce(map, 'idle', function() {
		google.maps.event.trigger(map, 'resize');
		map.setCenter(myLatlng);
	});
	return map;
}
jQuery(document).ready(function($){
/*        if(window.location.hash){
  var hash = window.location.hash.substr(1);
  jQuery('a[href="#'+hash+'"]').tab('show')
}*/
	$('.primary-menu a,.scroll').click(function(e){
		$('html,body').animate({scrollTop:$($(this).attr('href')).offset().top},1000);
		e.preventDefault();
	});
	$('.navigation-toggle').click(function(e){
		$('#site-navigation').slideToggle();
		e.preventDefault();
	});
	var options = {
		useEasing: true,
		useGrouping: true,
		separator: ',',
		decimal: '.',
	};
	$('.count').each(function(){
		var demo = new CountUp($(this)[0], parseInt($(this).data('start')), parseInt($(this).data('end')), 0, 5, options);
		var refer = $(this).data('refer');
		if(refer==''||refer==undefined)
		{
			refer = '#icon-box-section';
		}
		if (!demo.error){
			$(window).on('scroll',function(){
				if($(window).scrollTop() + $(window).height() > $(refer).offset().top)
				{
					demo.start();
				}
			});
		} else {
			console.error(demo.error);
		}
	});
	$('.only-these-widgets .widget-title').on('click',function(e){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('.only-these-widgets .menu').slideUp();
			$(this).next('div').children('.menu').slideUp();
		}
		else{
			$('.only-these-widgets .widget-title').removeClass('active');
			$(this).addClass('active');
			$('.only-these-widgets .menu').slideUp();
			$(this).next('div').children('.menu').slideDown();
		}
		e.preventDefault();
	});
	var bannerSwiper = new Swiper('#bannerSwiper', {
		speed: 1000,
    autoplay: {
        delay: 6000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },

		navigation: {
			nextEl: '#bannerSwiper .home-slide-next',
			prevEl: '#bannerSwiper .home-slide-prev',
		},
	});
	var singleSlideSwiper = new Swiper('#singleSlideSwiper', {
		speed: 400,
    loop: true,
		navigation: {
			nextEl: '#singleSlideSwiper .single-slide-next',
			prevEl: '#singleSlideSwiper .single-slide-prev',
		},
		autoplay: {
			delay: 7000,
			disableOnInteraction:false
		},
	});
	
	var preschool_lp = new Swiper('#preschool_lp', {
		speed: 400,
		loop: true,
		autoplay: {
			delay: 3000,
			disableOnInteraction:false
		},
	});
	
	$(window).on('resize',function(){
		if($('#bannerSwiper')[0])
		{
			bannerSwiper.update();
		}
		if($('#singleSlideSwiper')[0])
		{
			singleSlideSwiper.update();
		}
	});


      $('#dynamic_select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });



var urlvalue = $(".linkhome").attr("href");
			$('.load_more').click(function(){
				var datenew = $('.load_more').attr('att');
				$(".load_more").addClass("loading");

			$.ajax({
				type: 'POST',
                url: urlvalue+'/wp-content/themes/klay-schools/ajax.php',
                data:'mode=search&datenew='+datenew,
                success: function(resultnew)
                {
                    $('.resultneww').html(resultnew);
				}
            });
        });
    	$('.load_more_event').click(function(){
				var datenew = $('.load_more_event').attr('att');
				$(".load_more_event").addClass("loading");

			$.ajax({
				type: 'POST',
                url: urlvalue+'/wp-content/themes/klay-schools/ajax.php',
                data:'mode=event&datenew='+datenew,
                success: function(resultnew)
                {
                    $('.resultneww').html(resultnew);
				}
            });
        });


var callback2 = function() {

                    var location = $('.location_car').val();
                    var position = $('.position_car').val();


            $.ajax({
                type: 'POST',
                url: urlvalue+'/wp-content/themes/klay-schools/ajax.php',
                data:'mode=career&locationnew='+location+'&positionnew='+position,
                //data:'mode=search&datenew='+area1,
                success: function(resultnew)
                {
                    $('.searchresult').html(resultnew);
                }
            });
        };



        $('.location_car').change(callback2);
       $('.position_car').change(callback2);






$('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });


var swiper = new Swiper('#facilities-swiper', {
  slidesPerView: 1,
  spaceBetween: 30,
  slidesPerGroup: 1,
  loop: true,
  loopFillGroupWithBlank: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
var swiper = new Swiper('.swiper-container-event', {
      slidesPerView: 3,
      spaceBetween: 30,
      slidesPerGroup: 3,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.single-slide-prev',
        prevEl: '.single-slide-next',
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          slidesPerGroup: 1,

        },
    }
    });

    var swiper = new Swiper('.swiper-container-event1', {
      slidesPerView: 3,
      spaceBetween: 30,
      slidesPerGroup: 3,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
         nextEl: '.single-slide-next',
        prevEl: '.single-slide-prev',
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          slidesPerGroup: 1,

        },
    }
    });
        var swiper = new Swiper('.swiper-container-event2', {
      slidesPerView: 4,
      spaceBetween: 10,
      slidesPerGroup: 4,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.single-slide-next',
        prevEl: '.single-slide-prev',
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          slidesPerGroup: 1,

        },
    }
    });


    var Staffswiper = new Swiper('.swiper-container-staff', {
      slidesPerView: 2,
      spaceBetween: 30,
      slidesPerGroup: 2,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
          breakpoints: {
        767: {
          slidesPerView: 1,
          slidesPerGroup: 1,

        },
      }
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      Staffswiper.update();
    });



   var swiper = new Swiper('.swiper-container-gallery', {
   	loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    $('#myModal').on('shown.bs.modal',function(){
    	swiper.update();
    });

    var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

$(".career1").click(function(){
  $(".span8").removeClass("tabshow");
  $(".tabmain").addClass("tabshow");
  $(".carrer_tab_sub li .active1").addClass("active show");
  $("#tab21").addClass("active show");

});
$(".career2").click(function(){
  $(".span8").removeClass("tabshow");
  $(".tabmain").addClass("tabshow");
  $(".carrer_tab_sub li .active2").addClass("active show");
  $("#tab22").addClass("active show");

});
$(".career3").click(function(){

  $(".span8").removeClass("tabshow");
  $(".tabmain").addClass("tabshow");
  $(".carrer_tab_sub li .active3").addClass("active show");
  $("#tab23").addClass("active show");


});

$("#user_login").attr("placeholder", "User Name");
$("#user_pass").attr("placeholder", "Password");
$("#somfrp_user_info").attr("placeholder", "Enter Username or Email");


 		hash = window.location.hash;

    elements = $('a[href="' + hash + '"]');
    if (elements.length === 0) {
        $(".carrer_tab li a:first").addClass("active").show(); //Activate first tab
        $(".tab_content:first").show(); //Show first tab content
    } else {
        elements.click();
    }
		$('#mega-menu-item-23 a').on('click',function(e){
			if($("body").hasClass('page-template-template-career-php')){
				var str = $(this).attr('href');
				var newhash = str.split('#')[1];
				$('a[href="#' + newhash + '"]').tab('show');
				e.preventDefault();
			}
		});
});

new WOW().init();
