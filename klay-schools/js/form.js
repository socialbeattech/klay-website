var linktag = document.createElement("link");
linktag.rel = 'stylesheet';
linktag.href = 'https://www.klayschools.com/wp-content/themes/klay-schools/css/form.css';
document.head.appendChild(linktag);
jQuery(document).ready(function($){

	$( "<div class='amp-col-md-8 field_pd_1'></div>" ).insertBefore( ".Child_s_Name" );
	
	$( "<div class='field_pd_1'></div>" ).insertBefore( ".563842_50324pi_563842_50324" );
	$( "<div class='amp-row row_pd_1'></div>" ).insertBefore( ".field_pd_1" );
	$(".Child_s_Name").appendTo(".field_pd_1");
	$("#error_for_563842_49908pi_563842_49908").appendTo(".field_pd_1");
	$(".field_pd_1").appendTo(".row_pd_1");
	$(".Child_s_Name input").attr("placeholder", "Child's Name *");
	
	$( "<div class='amp-col-md-12 field_pd_8'></div>" ).insertBefore( ".note_city" );
	$(".note_city").appendTo(".field_pd_8");

	$( "<div class='amp-col-md-12 field_c_1'></div>" ).insertBefore( ".first_name" );
	$( "<div class='amp-col-md-12 field_c_1_new'></div>" ).insertBefore( ".Lead_Status" );
	$( "<div class='amp-col-md-12 field_c_2_new'></div>" ).insertBefore( ".Department_Categories" );
	$( "<div class='amp-col-md-12 field_c_3_new'></div>" ).insertBefore( ".Location" );
	$( "<div class='amp-row row_c_1'></div>" ).insertBefore( ".field_c_1" );
	$(".first_name").appendTo(".field_c_1");
	$(".Lead_Status").appendTo(".field_c_1_new");
	$(".Department_Categories").appendTo(".field_c_2_new");
	$(".Location").appendTo(".field_c_3_new");
	$("#error_for_563842_49908pi_563842_49908").appendTo(".field_c_1");
	$(".field_c_1").appendTo(".row_c_1");
	$(".field_c_1_new").appendTo(".row_c_1");
	$(".field_c_2_new").appendTo(".row_c_1");
	$(".field_c_3_new").appendTo(".row_c_1");
	$(".first_name input").attr("placeholder", "Name *");
	$("#563842_50292pi_563842_50292").attr("placeholder", "Phone Number *");
	$("#563842_53320pi_563842_53320").attr("placeholder", "Phone Number *");
	$("#563842_53384pi_563842_53384").attr("placeholder", "Phone Number *");
	



	$( "<div class='amp-col-md-12 field_c_2'></div>" ).insertBefore( ".job_title" );
	$( "<div class='amp-row row_c_2'></div>" ).insertBefore( ".field_c_2" );
	$(".job_title").appendTo(".field_c_2");
	$("#error_for_563842_49908pi_563842_49908").appendTo(".field_c_2");
	$(".field_c_2").appendTo(".row_c_2");
	
	

	$( "<div class='amp-col-md-4 field_pd_2'></div>" ).insertBefore( ".Child_s_Age" );
	$(".Child_s_Age").appendTo(".field_pd_2");
	$("#error_for_563842_49910pi_563842_49910").appendTo(".field_pd_2");
	$(".field_pd_2").appendTo(".row_pd_1");
	$(".Child_s_Age select").attr("placeholder", "Age *");

	$( "<div class='amp-col-md-12 field_pd_3'></div>" ).insertBefore( ".last_name" );
	$( "<div class='amp-row row_pd_2'></div>" ).insertBefore( ".field_pd_3" );
	$(".last_name").appendTo(".field_pd_3");
	$("#error_for_563842_49912pi_563842_49912").appendTo(".field_pd_3");
	$(".field_pd_3").appendTo(".row_pd_2");
	$(".last_name input").attr("placeholder", "Parent's Name *");	
	$("input[name='563842_50304pi_563842_50304']").parent('p').parent('.field_pd_3').removeClass('amp-col-md-12').addClass('amp-col-md-8');	
	$('select[name="563842_50306pi_563842_50306"]').parent('p').parent('.field_pd_2').appendTo(".row_pd_2");
	$( "<div class='amp-col-md-8 field_c_3'></div>" ).insertBefore( ".parent-helpline .last_name" );
	$( "<div class='amp-row row_c_3'></div>" ).insertBefore( ".field_c_3" );
	$(".parent-helpline .last_name").appendTo(".field_c_3");
	$("#error_for_563842_49912pi_563842_49912").appendTo(".field_c_3");
	$(".field_c_3").appendTo(".row_c_3");
	$(".parent-helpline .last_name input").attr("placeholder", "Parent's Name *");

	$( "<div class='amp-col-md-12 field_pd_4'></div>" ).insertBefore( ".phone" );
	$( "<div class='amp-row row_pd_3'></div>" ).insertBefore( ".field_pd_4" );
	$(".phone").appendTo(".field_pd_4");
	$("#error_for_563842_49914pi_563842_49914").appendTo(".field_pd_4");
	$(".field_pd_4").appendTo(".row_pd_3");
	$(".phone input").attr("placeholder", "Contact Number *");

	$( "<div class='amp-col-md-12 field_pd_5'></div>" ).insertBefore( ".email" );
	$( "<div class='amp-row row_pd_4'></div>" ).insertBefore( ".field_pd_5" );
	$(".email").appendTo(".field_pd_5");
	$("#error_for_563842_49916pi_563842_49916").appendTo(".field_pd_5");
	$(".field_pd_5").appendTo(".row_pd_4");
	$(".email input").attr("placeholder", "Email *");

	$( "<div class='amp-col-md-12 field_pd_6'></div>" ).insertBefore( ".Program" );
	$( "<div class='amp-row row_pd_5'></div>" ).insertBefore( ".field_pd_6" );
	$(".Program").appendTo(".field_pd_6");
	$("#error_for_563842_49918pi_563842_49918").appendTo(".field_pd_6");
	$(".field_pd_6").appendTo(".row_pd_5");
	$(".Program select").attr("placeholder", "Child's Name *");

	$( "<div class='amp-col-md-12 field_pd_7'></div>" ).insertBefore( ".city" );
	$( "<div class='amp-row row_pd_6'></div>" ).insertBefore( ".field_pd_7" );
	$(".city").appendTo(".field_pd_7");
	$("#error_for_563842_49920pi_563842_49920").appendTo(".field_pd_7");
	$(".field_pd_7").appendTo(".row_pd_6");
	$(".city input").attr("placeholder", "City *");

	$( "<div class='amp-col-md-12field_pd_8'></div>" ).insertBefore( ".Centre" );
	$(".Centre").appendTo(".field_pd_8");
	$("#error_for_563842_49922pi_563842_49922").appendTo(".field_pd_8");
	$(".field_pd_8").appendTo(".row_pd_6");
	$(".Centre select").attr("placeholder", "Child's Name *");

	$( "<div class='amp-col-md-12 field_pd_9'></div>" ).insertBefore( ".comments" );
	$( "<div class='amp-row row_pd_7'></div>" ).insertBefore( ".field_pd_9" );
	$(".comments").appendTo(".field_pd_9");
	$("#error_for_563842_49924pi_563842_49924").appendTo(".field_pd_9");
	$(".field_pd_9").appendTo(".row_pd_7");
	$(".comments #563842_49924pi_563842_49924").attr("placeholder", "Quick Note");
	$(".comments #563842_50310pi_563842_50310").attr("placeholder", "Type Your Query");
	$(".comments #563842_50312pi_563842_50312").attr("placeholder", "Type Your Message");
	$("#563842_50326pi_563842_50326").attr("placeholder", "Contact Number *");

	$('select[name=563842_49910pi_563842_49910] > option:first-child')
    .text('Age *');

    $('select[name=563842_49918pi_563842_49918] > option:first-child')
    .text('Program Interested In *');

    $('select[name=563842_49920pi_563842_49920] > option:first-child')
    .text('City *');


   $('select[name=563842_50250pi_563842_50250] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50274pi_563842_50274] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50276pi_563842_50276] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50278pi_563842_50278] > option:first-child')
    .text('Centre *'); 
    $('select[name=563842_50280pi_563842_50280] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50282pi_563842_50282] > option:first-child')
    .text('Centre *'); 
    $('select[name=563842_50284pi_563842_50284] > option:first-child')
    .text('Centre *');
$('select[name=563842_50296pi_563842_50296] > option:first-child')
    .text('City ');
    $('select[name=563842_50306pi_563842_50306] > option:first-child')
    .text('Child Age *');
	$("input[name='563842_50314pi_563842_50314']").parent('p').parent('.field_c_1').removeClass('amp-col-md-12').addClass('amp-col-md-6');			
	$( "<div class='amp-col-md-6 company_name'></div>" ).insertBefore( ".company" );			
	$(".company").appendTo(".company_name");			
	$(".company_name").appendTo(".row_c_1");			
	$("#563842_50316pi_563842_50316").attr("placeholder", "Company Name *");
	$('select[name=563842_50318pi_563842_50318] > option:first-child').text('Please select your location*');
	$("select[name='563842_50318pi_563842_50318']").parent('.Location').addClass('amp-col-md-12 locate_corporate');
	$( "<div class='amp-col-md-12 row locations_corp'></div>" ).insertBefore( ".locate_corporate" );
	$(".locate_corporate").appendTo(".locations_corp");
	$('select[name=563842_50390pi_563842_50390] > option:first-child').text('Please select*');
	$("#563842_50320pi_563842_50320").parent('.Lead_Mobile').addClass('amp-col-md-12');
	$( "<div class='amp-col-md-12  phone_email'></div>" ).insertBefore( ".Lead_Mobile" );
	$(".Lead_Mobile").appendTo(".phone_email");
	$( "<div class='amp-col-md-6 phone_email_inner'></div>" ).insertBefore( $('input[name="563842_50320pi_563842_50320"]').parent('.Lead_Mobile') );
	$( "<div class='amp-col-md-12 phone_email_inner'></div>" ).insertBefore( $('input[name="563842_49914pi_563842_49914"]').parent('.Lead_Mobile') );//
	$(".Lead_Mobile").appendTo(".phone_email_inner");
	$("#563842_50320pi_563842_50320").attr("placeholder", "Phone *");	
	$("input[name='563842_50322pi_563842_50322']").parent('p').parent('.field_pd_5').removeClass('amp-col-md-12').addClass('amp-col-md-6 corporate_email');
	$(".corporate_email").appendTo(".phone_email");		
	$("#563842_50330pi_563842_50330").attr("placeholder", "Type your message");
	$("#563842_49914pi_563842_49914").attr("placeholder", "Phone *");
	$("#563842_49924pi_563842_49924").attr("placeholder", "Quick Note");
	$("#563842_53342pi_563842_53342").attr("placeholder", "Quick Note");
	$("#563842_53406pi_563842_53406").attr("placeholder", "Quick Note");

	$('select[name=563842_50878pi_563842_50878] > option:first-child')
    .text('Purpose *');

	$('select[name=563842_50806pi_563842_50806] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50808pi_563842_50808] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50810pi_563842_50810] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50812pi_563842_50812] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50814pi_563842_50814] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50816pi_563842_50816] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50818pi_563842_50818] > option:first-child')
    .text('Centre *');
     $('select[name=563842_53324pi_563842_53324] > option:first-child')
    .text('Program Interested In * ');
    $('select[name=563842_53388pi_563842_53388] > option:first-child')
    .text('Program Interested In * ');
    $('select[name=563842_53326pi_563842_53326] > option:first-child')
    .text('City * '); 
    $('select[name=563842_53390pi_563842_53390] > option:first-child')
    .text('City * ');
    $('select[name=563842_53316pi_563842_53316] > option:first-child')
    .text('Age * ');
    $('select[name=563842_53380pi_563842_53380] > option:first-child')
    .text('Age * ');
    $('.form-field-slave select > option:first-child')
    .text('Centre * ');

$('.age_city select > option:first-child')
    .text('Age * ');
    $('.program_city select > option:first-child')
    .text('Program Interested In *');
     $('.city_city select > option:first-child')
    .text('City *');
     $('.centres_city select > option:first-child')
    .text('Centre *');
    $(".contact_city input").attr("placeholder", "Phone *");
    $(".note_city textarea").attr("placeholder", "Quick Note");
    $("#563842_50312pi_563842_50312").attr("placeholder", "Quick Note");
    $("#563842_50310pi_563842_50310").attr("placeholder", "Type your Query");
    $('select[name=563842_55983pi_563842_55983] > option:first-child').text('City *');

	
	
	$("input[name='563842_51396pi_563842_51396']").parent('p').parent('.field_c_1').removeClass('amp-col-md-12').addClass('amp-col-md-3');	
	 //alert(window.parent.doc)
// Parse the URL


});


/*window.addEventListener("message", function(message){ 

alert(message);

}, false);


$( window ).on( "load", function() {


	//alert('ghh');

	  parent.reload_cart();



//reload_cart("https://www.klayschool.com/");
 
	});
*/


/*
function func1() {
  var source = window.location.href;
  document.querySelector(".source input").value = source;
}
window.onload=func1;*/
