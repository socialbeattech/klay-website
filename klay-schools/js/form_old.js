var linktag = document.createElement("link");
linktag.rel = 'stylesheet';
linktag.href = 'https://www.lifeonline.co/klay-schools/wp-content/themes/klay-schools/css/form.css';
document.head.appendChild(linktag);
jQuery(document).ready(function($){

	$( "<div class='amp-col-md-8 field_pd_1'></div>" ).insertBefore( ".Child_s_Name" );
	$( "<div class='amp-row row_pd_1'></div>" ).insertBefore( ".field_pd_1" );
	$(".Child_s_Name").appendTo(".field_pd_1");
	$("#error_for_563842_49908pi_563842_49908").appendTo(".field_pd_1");
	$(".field_pd_1").appendTo(".row_pd_1");
	$(".Child_s_Name input").attr("placeholder", "Child's Name *");

	$( "<div class='amp-col-md-12 field_c_1'></div>" ).insertBefore( ".first_name" );
	$( "<div class='amp-row row_c_1'></div>" ).insertBefore( ".field_c_1" );
	$(".first_name").appendTo(".field_c_1");
	$("#error_for_563842_49908pi_563842_49908").appendTo(".field_c_1");
	$(".field_c_1").appendTo(".row_c_1");
	$(".first_name input").attr("placeholder", "Name *");



	$( "<div class='amp-col-md-12 field_c_2'></div>" ).insertBefore( ".job_title" );
	$( "<div class='amp-row row_c_2'></div>" ).insertBefore( ".field_c_2" );
	$(".job_title").appendTo(".field_c_2");
	$("#error_for_563842_49908pi_563842_49908").appendTo(".field_c_2");
	$(".field_c_2").appendTo(".row_c_2");
	
	

	$( "<div class='amp-col-md-4 field_pd_2'></div>" ).insertBefore( ".Child_s_Age" );
	$(".Child_s_Age").appendTo(".field_pd_2");
	$("#error_for_563842_49910pi_563842_49910").appendTo(".field_pd_2");
	$(".field_pd_2").appendTo(".row_pd_1");
	$(".Child_s_Age select").attr("placeholder", "Age *");

	$( "<div class='amp-col-md-12 field_pd_3'></div>" ).insertBefore( ".last_name" );
	$( "<div class='amp-row row_pd_2'></div>" ).insertBefore( ".field_pd_3" );
	$(".last_name").appendTo(".field_pd_3");
	$("#error_for_563842_49912pi_563842_49912").appendTo(".field_pd_3");
	$(".field_pd_3").appendTo(".row_pd_2");
	$(".last_name input").attr("placeholder", "Parent's Name *");

	$("input[name='563842_50304pi_563842_50304']").parent('p').parent('.field_pd_3').removeClass('amp-col-md-12').addClass('amp-col-md-8');
	$('select[name="563842_50306pi_563842_50306"]').parent('p').parent('.field_pd_2').appendTo(".row_pd_2");

	$( "<div class='amp-col-md-8 field_c_3'></div>" ).insertBefore( ".parent-helpline .last_name" );
	$( "<div class='amp-row row_c_3'></div>" ).insertBefore( ".field_c_3" );
	$(".parent-helpline .last_name").appendTo(".field_c_3");
	$("#error_for_563842_49912pi_563842_49912").appendTo(".field_c_3");
	$(".field_c_3").appendTo(".row_c_3");
	$(".parent-helpline .last_name input").attr("placeholder", "Parent's Name *");

	$( "<div class='amp-col-md-12 field_pd_4'></div>" ).insertBefore( ".phone" );
	$( "<div class='amp-row row_pd_3'></div>" ).insertBefore( ".field_pd_4" );
	$(".phone").appendTo(".field_pd_4");
	$("#error_for_563842_49914pi_563842_49914").appendTo(".field_pd_4");
	$(".field_pd_4").appendTo(".row_pd_3");
	$(".phone input").attr("placeholder", "Contact Number *");

	$( "<div class='amp-col-md-12 field_pd_5'></div>" ).insertBefore( ".email" );
	$( "<div class='amp-row row_pd_4'></div>" ).insertBefore( ".field_pd_5" );
	$(".email").appendTo(".field_pd_5");
	$("#error_for_563842_49916pi_563842_49916").appendTo(".field_pd_5");
	$(".field_pd_5").appendTo(".row_pd_4");
	$(".email input").attr("placeholder", "Email *");

	$( "<div class='amp-col-md-12 field_pd_6'></div>" ).insertBefore( ".Program" );
	$( "<div class='amp-row row_pd_5'></div>" ).insertBefore( ".field_pd_6" );
	$(".Program").appendTo(".field_pd_6");
	$("#error_for_563842_49918pi_563842_49918").appendTo(".field_pd_6");
	$(".field_pd_6").appendTo(".row_pd_5");
	$(".Program select").attr("placeholder", "Child's Name *");

	$( "<div class='amp-col-md-12 field_pd_7'></div>" ).insertBefore( ".city" );
	$( "<div class='amp-row row_pd_6'></div>" ).insertBefore( ".field_pd_7" );
	$(".city").appendTo(".field_pd_7");
	$("#error_for_563842_49920pi_563842_49920").appendTo(".field_pd_7");
	$(".field_pd_7").appendTo(".row_pd_6");
	$(".city input").attr("placeholder", "City *");

	$( "<div class='amp-col-md-12field_pd_8'></div>" ).insertBefore( ".Centre" );
	$(".Centre").appendTo(".field_pd_8");
	$("#error_for_563842_49922pi_563842_49922").appendTo(".field_pd_8");
	$(".field_pd_8").appendTo(".row_pd_6");
	$(".Centre select").attr("placeholder", "Child's Name *");

	$( "<div class='amp-col-md-12 field_pd_9'></div>" ).insertBefore( ".comments" );
	$( "<div class='amp-row row_pd_7'></div>" ).insertBefore( ".field_pd_9" );
	$(".comments").appendTo(".field_pd_9");
	$("#error_for_563842_49924pi_563842_49924").appendTo(".field_pd_9");
	$(".field_pd_9").appendTo(".row_pd_7");
	$(".comments #563842_49924pi_563842_49924").attr("placeholder", "Quick Note");
	$(".comments #563842_50310pi_563842_50310").attr("placeholder", "Type Your Query");
	$(".comments #563842_50312pi_563842_50312").attr("placeholder", "Type Your Message");

	$('select[name=563842_49910pi_563842_49910] > option:first-child')
    .text('Age *');

    $('select[name=563842_49918pi_563842_49918] > option:first-child')
    .text('Program Interested In *');

    $('select[name=563842_49920pi_563842_49920] > option:first-child')
    .text('City *');


   $('select[name=563842_50250pi_563842_50250] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50274pi_563842_50274] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50276pi_563842_50276] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50278pi_563842_50278] > option:first-child')
    .text('Centre *'); 
    $('select[name=563842_50280pi_563842_50280] > option:first-child')
    .text('Centre *');
    $('select[name=563842_50282pi_563842_50282] > option:first-child')
    .text('Centre *'); 
    $('select[name=563842_50284pi_563842_50284] > option:first-child')
    .text('Centre *');
$('select[name=563842_50296pi_563842_50296] > option:first-child')
    .text('Who do you want to reach ? *');
    $('select[name=563842_50306pi_563842_50306] > option:first-child')
    .text('Child Age *');



});
