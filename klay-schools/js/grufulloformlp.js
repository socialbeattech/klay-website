var linktag = document.createElement("link");
linktag.rel = 'stylesheet';
linktag.href = 'https://www.klayschools.com/wp-content/themes/klay-schools/css/grufulloformlp.css';
document.head.appendChild(linktag);
jQuery(document).ready(function($){
	
	$( "<div class='amp-col-md-3 field_pd_1'></div>" ).insertBefore( ".Child_s_Name" );
	$( "<div class='amp-row row_pd_1'></div>" ).insertBefore( ".field_pd_1" );
	$(".Child_s_Name").appendTo(".field_pd_1");
	$("#error_for_563842_51396pi_563842_51396").appendTo(".field_pd_1");
	$(".field_pd_1").appendTo(".row_pd_1");
	$(".Child_s_Name input").attr("placeholder", "Child's Name *");
	
	$( "<div class='amp-col-md-3 field_pd_2'></div>" ).insertBefore( ".Child_s_Age" );
	$(".Child_s_Age").appendTo(".field_pd_2");
	$("#error_for_563842_51398pi_563842_51398").appendTo(".field_pd_2");
	$(".field_pd_2").appendTo(".row_pd_1");
	$(".Child_s_Age input").attr("placeholder", "Child's Age *");
	
	$( "<div class='amp-col-md-6 field_pd_3'></div>" ).insertBefore( ".last_name" );
	$(".last_name").appendTo(".field_pd_3");
	$("#error_for_563842_51400pi_563842_51400").appendTo(".field_pd_3");
	$(".field_pd_3").appendTo(".row_pd_1");
	$(".last_name input").attr("placeholder", "Parent's Name *");
	
	$( "<div class='amp-col-md-3 field_pd_4'></div>" ).insertBefore( ".email" );
	$( "<div class='amp-row row_pd_2'></div>" ).insertBefore( ".field_pd_4" );
	$(".email").appendTo(".field_pd_4");
	$("#error_for_563842_51402pi_563842_51402").appendTo(".field_pd_4");
	$(".field_pd_4").appendTo(".row_pd_2");
	$(".email input").attr("placeholder", "Email ID *");
	
	$( "<div class='amp-col-md-3 field_pd_5'></div>" ).insertBefore( ".Lead_Mobile" );
	$(".Lead_Mobile").appendTo(".field_pd_5");
	$("#error_for_563842_51404pi_563842_51404").appendTo(".field_pd_5");
	$(".field_pd_5").appendTo(".row_pd_2");
	$(".Lead_Mobile input").attr("placeholder", "Contact Number *");
	
	$( "<div class='amp-col-md-3 field_pd_6'></div>" ).insertBefore( ".Location" );
	$(".Location").appendTo(".field_pd_6");
	$("#error_for_563842_51406pi_563842_51406").appendTo(".field_pd_6");
	$(".field_pd_6").appendTo(".row_pd_2");
	$(".Location input").attr("placeholder", "City *");
	
	$( "<div class='amp-col-md-3 field_pd_7'></div>" ).insertBefore( ".Bangalore_Centres" );
	$(".Bangalore_Centres").appendTo(".field_pd_7");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_7");
	$(".field_pd_7").appendTo(".row_pd_2");
	$(".Bangalore_Centres input").attr("placeholder", "Centre *");

	$( "<div class='amp-col-md-3 field_pd_8'></div>" ).insertBefore( ".Noida_centres" );
	$(".Noida_centres").appendTo(".field_pd_8");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_8");
	$(".field_pd_8").appendTo(".row_pd_2");
	$(".Noida_centres input").attr("placeholder", "Centre *");

	$( "<div class='amp-col-md-3 field_pd_9'></div>" ).insertBefore( ".Gurgaon_centres" );
	$(".Gurgaon_centres").appendTo(".field_pd_9");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_9");
	$(".field_pd_9").appendTo(".row_pd_2");
	$(".Gurgaon_centres input").attr("placeholder", "Centre *");

	$( "<div class='amp-col-md-3 field_pd_10'></div>" ).insertBefore( ".Hyderabad_centres" );
	$(".Hyderabad_centres").appendTo(".field_pd_10");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_10");
	$(".field_pd_10").appendTo(".row_pd_2");
	$(".Hyderabad_centres input").attr("placeholder", "Centre *");

	$( "<div class='amp-col-md-3 field_pd_11'></div>" ).insertBefore( ".Mumbai_centres" );
	$(".Mumbai_centres").appendTo(".field_pd_11");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_11");
	$(".field_pd_11").appendTo(".row_pd_2");
	$(".Mumbai_centres input").attr("placeholder", "Centre *");

	$( "<div class='amp-col-md-3 field_pd_12'></div>" ).insertBefore( ".Pune_centres" );
	$(".Pune_centres").appendTo(".field_pd_12");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_12");
	$(".field_pd_12").appendTo(".row_pd_2");
	$(".Pune_centres input").attr("placeholder", "Centre *");

	$( "<div class='amp-col-md-3 field_pd_13'></div>" ).insertBefore( ".Chennai_Centres" );
	$(".Chennai_Centres").appendTo(".field_pd_13");
	$("#error_for_563842_51408pi_563842_51408").appendTo(".field_pd_13");
	$(".field_pd_13").appendTo(".row_pd_2");
	$(".Chennai_Centres input").attr("placeholder", "Centre *");

	$('select[name=563842_51406pi_563842_51406] > option:first-child').text('City *');
	$('select[name=563842_56183pi_563842_56183] > option:first-child').text('City *');
	$('.field_pd_7 select > option:first-child').text('Centre *');
	$('.field_pd_8 select > option:first-child').text('Centre *');
	$('.field_pd_9 select > option:first-child').text('Centre *');
	$('.field_pd_10 select > option:first-child').text('Centre *');
	$('.field_pd_11 select > option:first-child').text('Centre *');
	$('.field_pd_12 select > option:first-child').text('Centre *');
	$('.field_pd_13 select > option:first-child').text('Centre *');
	$('.field_pd_13 select > option:first-child').text('Centre *');
	$('select[name=563842_55983pi_563842_55983] > option:first-child').text('City *');
	
	
	
	$("#pardot-form").addClass("grufullo_form");
	
	});