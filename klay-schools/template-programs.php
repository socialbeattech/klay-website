<?php
/**
 * Template Name: Programs
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-50 pb-xs-45 pt-xs-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-15 section-title">KLAY Maintains Global Quality Standards of <span class="orange-color">Safety, Hygiene, Infrastructure &amp; Care</span></h2>
			</div>
		</div>
		<div class="row mt-15">
			<div class="col-12 col-md-8">
				<div class="swiper-container" id="singleSlideSwiper">
					<div class="swiper-wrapper">
						<?php while(have_rows('image_carousel')){the_row(); ?>
						<div class="swiper-slide">
							<img src="<?php the_sub_field('image') ?>" alt="" class="w-100" />
							<?php if(get_sub_field('title')){ ?><span class="small-caption gotham-rounded-medium fs-15 white-color"><em><?php the_sub_field('title') ?></em></span><?php } ?>
						</div>
						<?php } ?>
					</div>
					<div class="single-slide-prev"></div>
					<div class="single-slide-next"></div>
				</div>
				<div class="gotham-rounded-book mt-30"><?php the_content(); ?></div>
			</div>
			<div class="col-12 col-md-4">
				<div class="enquire-now pt-15 pb-15 no-label-form">
				<?php //echo do_shortcode(get_field('form')); ?>
				<?php //the_field('iframe_for_singe_form','option'); ?>
				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hfr?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)

ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="program-details pb-50 pb-xs-45">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 text-center tab_day">
				<div class="d-none d-md-block">
					<img src="<?php echo get_template_directory_uri() ?>/images/light-green-bg.png" alt="" class="w-100" />
				</div>
				<div class="light-green-bg">				
					<h2 class="mt-0 mb-20 mb-xs-10 section-title pt-30 pt-xs-15">Program Details</h2>
					<ul class="nav nav-tabs d-md-inline-block clearfix" id="programDetailsTab" role="tablist">
						<?php $i=0; while(have_rows('program_details_content')){ the_row(); ?>
						<li class="nav-item text-center d-md-inline-block gotham-rounded-medium">
							<a class="fs-xs-16 nav-link<?php if($i==0){ ?> active<?php } ?>" id="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>-tab" data-toggle="tab" href="#<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>" role="tab" aria-controls="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>" <?php if($i==0){ ?>aria-selected="true"<?php }else{ ?>aria-selected="false"<?php } ?>><img src="<?php the_sub_field('icon') ?>" alt="" /><br /><?php the_sub_field('title') ?></a>
						</li>
						<?php $i++; } ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="col-12">
					<div class="tab-content tab-content-green-border" id="programDetailsContent">
						<?php $i=0; while(have_rows('program_details_content')){ the_row(); ?>
						<div class="tab-pane fade<?php if($i==0){ ?> show active<?php } ?> gotham-rounded-book" id="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>" role="tabpanel" aria-labelledby="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>-tab"><?php the_sub_field('content') ?></div>
						<?php $i++; } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="klay-standards pb-50 pb-xs-45">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="d-none d-md-block">
					<img src="<?php echo get_template_directory_uri() ?>/images/light-yellow-bg.png" alt="" class="w-100" />
				</div>
				<div class="light-yellow-bg">
					<div class="light-yellow-content">
						<div class="col-12">
							<div class="col-12">
								<h2 class="mt-0 mb-40 mb-xs-10 section-title">A Day at KLAY Daycare</h2>
							</div>
						</div>
						<div class="col-12">
							<div class="row">
								<div class="col-12 col-md-6 mb-xs-15">
									<img src="<?php the_field('section_3_image'); ?>" alt="" class="w-100 h-100" />
								</div>
								<div class="col-12 col-md-6">
									<div class="white-bg border-radius-10 h-100 pl-15 pr-15">
										<?php the_field('section_3_content') ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section class="day-at-klay pb-50 pb-xs-45">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="d-none d-md-block">
					<img src="<?php echo get_template_directory_uri() ?>/images/light-blue-bg.png" alt="" class="w-100" />
				</div>		
				<div class="light-blue-bg">
					<div class="light-blue-content">
						<div class="row">
							<div class="col-12">
								<h2 class="mt-0 mb-20 mb-xs-10 section-title">KLAY Standards</h2>
								<p class="text-center text-xs-left gotham-rounded-book fs-20 fs-xs-16">KLAY Schools are compliant with the National Association for the Education of Young Children<br />(NAEYC) standards. We adhere to the following KLAY standards in all our centers across India.</p>						
							</div>
						</div>
						<div class="row">
							<div class="col-12 text-center">
								<ul class="nav nav-tabs d-inline-block gotham-rounded-medium" id="klayStandardTab" role="tablist">
									<?php $i=0; while(have_rows('section_4_content')){ the_row(); ?>
									<li class="nav-item text-center d-inline-block">
										<a class="nav-link<?php if($i==0){ ?> active<?php } ?>" id="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>-tab" data-toggle="tab" href="#<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>" role="tab" aria-controls="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>" <?php if($i==0){ ?>aria-selected="true"<?php }else{ ?>aria-selected="false"<?php } ?>><?php the_sub_field('title') ?></a>
									</li>
									<?php $i++; } ?>
								</ul>
							</div>
							<div class="col-12">
								<div class="tab-content text-center" id="klayStandardContent">
									<?php $i=0; while(have_rows('section_4_content')){ the_row(); ?>
									<div class="tab-pane fade<?php if($i==0){ ?>  show active<?php } ?> gotham-rounded-book" id="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>" role="tabpanel" aria-labelledby="<?php echo strtolower(str_replace(' ','-',get_sub_field('title'))); ?>-tab"><?php the_sub_field('content') ?></div>
									<?php $i++; } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>	
</section>
<section class="what_parent pb-50 pb-xs-45">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>

<?php
endwhile;
get_footer();
