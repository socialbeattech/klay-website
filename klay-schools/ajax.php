<?php 
require_once '../../../wp-load.php';

if($_POST['mode']=="search"){
	$datea=$_POST['datenew']; ?>

<div class="row post_pad my-posts">
<?php 
		global $post; $i = 1;
		$args = array( 'posts_per_page' => '-1', 'post_type' => 'centres', 'centres_category' =>$datea,'offset'=>'1'  );
					
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) { setup_postdata($post); 
					
		?>
		

		
		<div class="col-6 col-sm-3">
		<a href="<?php the_permalink(); ?>"><div class="bordernew">
		<?php the_post_thumbnail( 'centre', array( 'class' => 'img-responsive center-block' ) );?>
		<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
		</div></a>
		</div>
		<?php } ?>
		</div>

<?php }

if($_POST['mode']=="event"){ ?>



	<div class="row">
				<?php 
							global $post; $i = 1;
							$args = array( 'posts_per_page' => '-1', 'post_type' => 'medias' );
							$args1 = array( 'posts_per_page' => '-1','post_type' => 'medias' );
							$myposts = get_posts( $args );
							$myposts1 = get_posts( $args1 );
							$count = count($myposts1);
							foreach( $myposts as $post ) { setup_postdata($post); 
						?>
					<div class="col-12 col-md-4 image_center media_image">
						
						<?php the_post_thumbnail('media-slider'); ?>
						<div class="gotham-rounded-medium media_content"><p><a href="<?php the_permalink(); ?>" style="color: #fff"><?php the_title(); ?></a></p></div>

						<h2 class="gotham-rounded-book"><?php //echo wp_trim_words( get_the_content(), 8, '...' );
						the_field('newspaper_name'); ?></h2>
					</div>
				<?php } ?>

			</div>
<?php }

if($_POST['mode']=="career"){
	$location =$_POST['locationnew'];
	$position =$_POST['positionnew'];
		global $post; $i = 1;
		$args = array( 'posts_per_page' => '-1', 'post_type' => 'career', 'location_category' =>$location,'position_category'=> $position  );
		$myposts = get_posts( $args );
		foreach( $myposts as $post ) { setup_postdata($post); 
		if($location || $position )	{
	?>
<div class="row">
	<div class="col-md-12">
		<?php $location_title = get_term_by('slug', $location, 'location_category');
		$position_title = get_term_by('slug', $position, 'position_category'); ?>
		<div class="gotham-rounded-book label_career"><span class="gotham-rounded-medium"><?php echo $position_title->name; ?></span>,   <?php echo $location_title->name; ?></div>
		<?php $competencies = get_field_object('competencies'); ?>
		<div class="gotham-rounded-medium label_career"><?php echo $competencies['label'] ?></div>
		<div class="gotham-rounded-book value_career"><?php echo $competencies['value'] ?></div>
	</div>
	<div class="col-md-12">
		<?php $main_responsibilities = get_field_object('main_responsibilities'); ?>
		<div class="gotham-rounded-medium label_career"><?php echo $main_responsibilities['label'] ?></div>
		<div class="gotham-rounded-book value_career"><?php echo $main_responsibilities['value'] ?></div>
	</div>
	<div class="col-md-12">
		<?php $timings = get_field_object('timings'); ?>
		<div class="gotham-rounded-medium label_career"><?php echo $timings['label'] ?></div>
		<div class="gotham-rounded-book value_career"><?php echo $timings['value'] ?></div>
	</div>
	<div class="col-md-12">
		<?php $qualifications = get_field_object('qualifications'); ?>
		<div class="gotham-rounded-medium label_career"><?php echo $qualifications['label'] ?></div>
		<div class="gotham-rounded-book value_career"><?php echo $qualifications['value'] ?></div>
	</div>
	<div class="col-md-12">
		<?php $experience = get_field_object('experience'); ?>
		<div class="gotham-rounded-medium label_career"><?php echo $experience['label'] ?></div>
		<div class="gotham-rounded-book value_career"><?php echo $experience['value'] ?></div>
	</div>
	<div class="col-md-12 apply_btn">
		<a class="btn-submit" href="javascript:void(0)">Apply Now</a>
	</div>
</div>


<?php } ?>
<script type="text/javascript">
	$(document).ready(function(){
	$(".btn-submit").click(function(){
    $(".active2").removeClass("active show");
  $(".active3").addClass("active show");
   $("#tab22").removeClass("active show");
  $("#tab23").addClass("active show");
    $("#input_4_5").filter(function() { 
    return ("Chennai" == "Chennai"); //To select Blue
}).prop('selected', true);

});
});
</script>

 <?php } } ?>