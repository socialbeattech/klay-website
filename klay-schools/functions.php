<?php
/**
 * Klay Schools functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Klay Schools
 */

if ( ! function_exists( 'klay_schools_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function klay_schools_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'klay-schools' ),
			'menu-2' => esc_html__( 'Mobile Menu', 'klay-schools' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'klay_schools_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'klay_schools_setup' );
add_image_size( 'centre', 207, 203, true );
add_image_size( 'blogsingle', 136, 101, true );
add_image_size( 'blog', 410, 309, true );

add_image_size( 'event-slider', 380, 680, true );
add_image_size( 'event-gal', 318, 449, true );

add_image_size( 'media-slider', 653, 350, true );
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function klay_schools_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'klay-schools' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'klay-schools' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 1', 'specsmakers' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'specsmakers' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title fs-14 gotham-rounded-medium mt-0 mb-0 d-block d-md-none white-color">',
		'after_title'   => '&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i></h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 2', 'specsmakers' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'specsmakers' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title fs-14 gotham-rounded-medium mt-0 mb-0 d-block d-md-none white-color">',
		'after_title'   => '&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i></h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 3', 'specsmakers' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'specsmakers' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title fs-14 gotham-rounded-medium mt-0 mb-0 d-block d-md-none white-color">',
		'after_title'   => '&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i></h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 4', 'specsmakers' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'specsmakers' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title fs-14 gotham-rounded-medium mt-0 mb-0 d-block d-md-none white-color">',
		'after_title'   => '&nbsp;<i class="d-inline-block d-sm-inline-block d-md-none d-lg-none fa fa-caret-right"></i></h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 5', 'specsmakers' ),
		'id'            => 'footer-5',
		'description'   => esc_html__( 'Add widgets here.', 'specsmakers' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title fs-22 mt-0 mb-0 text-uppercase">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'klay_schools_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function klay_schools_scripts(){
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', array(), '4.1.3' );
	wp_enqueue_style( 'padd-mar', get_template_directory_uri().'/css/padd-mar.css', array(), '1.0' );
	wp_enqueue_style( 'swiper', get_template_directory_uri().'/css/swiper.min.css', array(), '4.4.5' );
	wp_enqueue_style( 'animate', get_template_directory_uri().'/css/animate.min.css', array(), '3.7.0' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri().'/css/magnific-popup.css', array(), '1.1.0' );
	wp_enqueue_style( 'klay-schools-style', get_stylesheet_uri(), array(), '1.0' );
	
	wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/js/jquery.min.js', array(), '3.3.1' );
	wp_enqueue_script( 'bootstrap-carousel', get_template_directory_uri() . '/js/bootstrap-carousel.js', array(), '4.1.3', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array(), '4.1.3', true );
	wp_enqueue_script( 'swiper', get_template_directory_uri() . '/js/swiper.min.js', array(), '4.4.5', true );
	wp_enqueue_script( 'countUp', get_template_directory_uri() . '/js/countUp.min.js', array(), '1.0', true );
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array(), '1.3.0', true );
	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '1.1.0', true );
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0', true );
	$urlarrays = array('site_url' => get_site_url(), 'template_url' => get_template_directory_uri());
	wp_localize_script( 'custom', 'siteurls', $urlarrays );	
}
add_action( 'wp_enqueue_scripts', 'klay_schools_scripts' );

if(function_exists('acf_add_options_page')){
	acf_add_options_page();	
}


//CENTRES

$labels = array( 'name' => 'Centres', 'singular_name' => 'Centre', 'add_new' => 'Add New', 'add_new_item' => 'Add New Centre', 'edit_item' => 'Edit Centre', 'new_item' => 'New Centre', 'all_items' => 'All Centres', 'view_item' => 'View Centre', 'search_items' => 'Search Centres', 'not_found' =>  'No Centres found', 'not_found_in_trash' => 'No Centres found in Trash', 'parent_item_colon' => '', 'menu_name' => 'Centres' );



$args = array( 'labels' => $labels, 'public' => true, 'publicly_queryable' => true, 'show_ui' => true,  'show_in_menu' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'daycare-playschool', 'with_front' => false ), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false,'menu_position' => null,'supports' => array( 'title', 'thumbnail','editor' ), 'menu_icon' => get_bloginfo( 'template_url').'/inc/images/Centre.png','show_in_rest'=> true  ); 

register_post_type( 'centres', $args );

	$args = array(
'hierarchical'          => true,
'show_ui'               => true,
'labels'                => array('name' => 'Categories' , 'menu_name' => 'Categories' ),
'show_admin_column'     => true,
'update_count_callback' => '_update_post_term_count',
'query_var'             => true,
'rewrite'               => array( 'slug' => 'daycare-preschool', 'with_front' => false ),
);
register_taxonomy( 'centres_category', 'centres', $args );



//EventS

$labels = array( 'name' => 'Events', 'singular_name' => 'Event', 'add_new' => 'Add New', 'add_new_item' => 'Add New Event', 'edit_item' => 'Edit Event', 'new_item' => 'New Event', 'all_items' => 'All Events', 'view_item' => 'View Event', 'search_items' => 'Search Events', 'not_found' =>  'No Events found', 'not_found_in_trash' => 'No Events found in Trash', 'parent_item_colon' => '', 'menu_name' => 'Events' );



$args = array( 'labels' => $labels, 'public' => true, 'publicly_queryable' => true, 'show_ui' => true,  'show_in_menu' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'event', 'with_front' => false ), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false,'menu_position' => null,'supports' => array( 'title', 'thumbnail','editor' ), 'menu_icon' => get_bloginfo( 'template_url').'/inc/images/Event.png','show_in_rest'=> true  ); 

register_post_type( 'events', $args );


//MediaS

$labels = array( 'name' => 'Media', 'singular_name' => 'Media', 'add_new' => 'Add New', 'add_new_item' => 'Add New Media', 'edit_item' => 'Edit Media', 'new_item' => 'New Media', 'all_items' => 'All Medias', 'view_item' => 'View Media', 'search_items' => 'Search Medias', 'not_found' =>  'No Medias found', 'not_found_in_trash' => 'No Medias found in Trash', 'parent_item_colon' => '', 'menu_name' => 'Medias' );



$args = array( 'labels' => $labels, 'public' => true, 'publicly_queryable' => true, 'show_ui' => true,  'show_in_menu' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'media', 'with_front' => false ), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false,'menu_position' => null,'supports' => array( 'title', 'thumbnail','editor' ), 'menu_icon' => get_bloginfo( 'template_url').'/inc/images/Media.png','show_in_rest'=> true  ); 

register_post_type( 'medias', $args );


//Career

$labels = array( 'name' => 'Career', 'singular_name' => 'Career', 'add_new' => 'Add New', 'add_new_item' => 'Add New Career', 'edit_item' => 'Edit Career', 'new_item' => 'New Career', 'all_items' => 'All Careers', 'view_item' => 'View Career', 'search_items' => 'Search Careers', 'not_found' =>  'No Careers found', 'not_found_in_trash' => 'No Careers found in Trash', 'parent_item_colon' => '', 'menu_name' => 'Careers' );



$args = array( 'labels' => $labels, 'public' => true, 'publicly_queryable' => true, 'show_ui' => true,  'show_in_menu' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'career', 'with_front' => false ), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false,'menu_position' => null,'supports' => array( 'title', 'thumbnail','editor' ), 'menu_icon' => get_bloginfo( 'template_url').'/inc/images/Career.png','show_in_rest'=> true  ); 

register_post_type( 'career', $args );


//Parents Tale

$labels = array( 'name' => 'Parents Tale', 'singular_name' => 'Parents Tale', 'add_new' => 'Add New', 'add_new_item' => 'Add New Parents Tale', 'edit_item' => 'Edit Parents Tale', 'new_item' => 'New Parents Tale', 'all_items' => 'All Parents Tales', 'view_item' => 'View Parents Tale', 'search_items' => 'Search Parents Tales', 'not_found' =>  'No Parents Tales found', 'not_found_in_trash' => 'No Parents Tales found in Trash', 'parent_item_colon' => '', 'menu_name' => 'Parents Tales' );



$args = array( 'labels' => $labels, 'public' => true, 'publicly_queryable' => true, 'show_ui' => true,  'show_in_menu' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'parents-tale', 'with_front' => false ), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false,'menu_position' => null,'supports' => array( 'title', 'thumbnail','editor' ), 'menu_icon' => get_bloginfo( 'template_url').'/inc/images/Parents_tale.png','show_in_rest'=> true  ); 

register_post_type( 'parents_tale', $args );

//Family day engagement events
$labels = array( 'name' => 'Family Day', 'singular_name' => 'Family', 'add_new' => 'Add New', 'add_new_item' => 'Add New Family', 'edit_item' => 'Edit Family', 'new_item' => 'New Family', 'all_items' => 'All Family', 'view_item' => 'View Family', 'search_items' => 'Search Family', 'not_found' =>  'No Family found', 'not_found_in_trash' => 'No Family found in Trash', 'parent_item_colon' => '', 'menu_name' => 'Family Day' );



$args = array( 'labels' => $labels, 'public' => true, 'publicly_queryable' => true, 'show_ui' => true,  'show_in_menu' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'family', 'with_front' => false ), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false,'menu_position' => null,'supports' => array( 'title', 'thumbnail','editor' ), 'menu_icon' => get_bloginfo( 'template_url').'/inc/images/Event.png','show_in_rest'=> true  ); 

register_post_type( 'Family Day', $args );

	$args = array(
'hierarchical'          => true,
'show_ui'               => true,
'labels'                => array('name' => 'Location' , 'menu_name' => 'Location' ),
'show_admin_column'     => true,
'update_count_callback' => '_update_post_term_count',
'query_var'             => true,
'rewrite'               => array( 'slug' => 'location_category', 'with_front' => false ),
);
register_taxonomy( 'location_category', 'career', $args );

	$args = array(
'hierarchical'          => true,
'show_ui'               => true,
'labels'                => array('name' => 'Position' , 'menu_name' => 'Position' ),
'show_admin_column'     => true,
'update_count_callback' => '_update_post_term_count',
'query_var'             => true,
'rewrite'               => array( 'slug' => 'position_category', 'with_front' => false ),
);
register_taxonomy( 'position_category', 'career', $args );

function excerpt( $num=50 )

{

	$limit = $num+1;

	$excerpt = explode(' ', get_the_excerpt(), $limit);

	array_pop($excerpt);

	$excerpt = implode(" ",$excerpt);

	echo $excerpt;

}


function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');


function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}




add_action( 'wp_login_failed', 'aa_login_failed' ); // hook failed login

function aa_login_failed( $user ) {
// check what page the login attempt is coming from
$referrer = $_SERVER['HTTP_REFERER'];

// check that were not on the default login page
if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
// make sure we don’t already have a failed login attempt
if ( !strstr($referrer, '?login=failed' )) {
// Redirect to the login page and append a querystring of login failed
wp_redirect( $referrer . '?login=failed');
} else {
wp_redirect( $referrer );
}

exit;
}
}

/*add_action( 'wp_authenticate', '_catch_empty_user', 1, 2 );
$redirect_to_whereever_you_want = 'https://www.lifeonline.co/klay-schools/login/';
$referrer = $_SERVER['HTTP_REFERER'];
function _catch_empty_user( $username, $pwd ) {
  if ( empty( $username ) ) { ?>
    <script type="text/javascript">location.href = 'https://www.lifeonline.co/klay-schools/login/';</script>

    <?php echo "Valid User Name and Password"; exit();
  }
}*/

 function theme_add_user_zip_code_column( $columns ) {

	 $columns['location'] = __( 'Location', 'theme' );
	 return $columns;

 } 
 add_filter( 'manage_users_columns', 'theme_add_user_zip_code_column' );
 
 function theme_show_user_zip_code_data( $value, $column_name, $user_id ) {

	 if( 'location' == $column_name ) {
		 return get_user_meta( $user_id, 'location', true );
	 } // end if

 } // end theme_show_user_zip_code_data
 add_action( 'manage_users_custom_column', 'theme_show_user_zip_code_data', 10, 3 );


 function is_blog () {
	if ( (is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag()) ) {
		return true;
	}
	else {
		return false; 
	}
}
add_action( 'gform_after_submission_2', 'post_to_third_party_contact', 10, 2 );
function post_to_third_party_contact( $entry, $form ) {
 
    $post_url = 'http://go.pardot.com/l/563842/2019-01-18/4fhbl';
    $body = array(
        'input_1' => rgar( $entry, '1' ), //Name
        'input_7' => rgar( $entry, '7' ), //Contact number
        'input_4' => rgar( $entry, '4' ), //Email
        'input_5' => rgar( $entry, '5' ), //Who do you want to reach
        'input_6' => rgar( $entry, '6' ) //Type your message
    );
    //GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}

add_action( 'gform_after_submission_3', 'post_to_third_party_parent_helpline', 10, 2 );
function post_to_third_party_parent_helpline( $entry, $form ) {
 
    $post_url = 'https://go.pardot.com/l/563842/2019-02-05/4pb42';
    $body = array(
        'input_1' => rgar( $entry, '1' ), //Last Name
        'input_2' => rgar( $entry, '2' ), //Age
        'input_11' => rgar( $entry, '1' ), //Type your query
        'input_6' => rgar( $entry, '6' ) //Email
    );
    //GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}


add_action( 'gform_after_submission_1', 'post_to_third_party_enquire', 10, 2 );
function post_to_third_party_enquire( $entry, $form ) {
 
    $post_url = 'http://go.pardot.com/l/563842/2019-01-22/4gbhx';
    $body = array(
        'input_1' => rgar( $entry, '1' ), //Last Name
        'input_2' => rgar( $entry, '2' ), //Age
        'input_12' => rgar( $entry, '12' ), //Name
        'input_5' => rgar( $entry, '5' ), //Phone
		'input_6' => rgar( $entry, '6' ), //Email
        'input_7' => rgar( $entry, '7' ), //Age
        'input_9' => rgar( $entry, '9' ), //city
        'input_10' => rgar( $entry, '10' ), //center
        'input_11' => rgar( $entry, '11' ) //note
    );
    //GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}

add_action( 'gform_after_submission_4', 'post_to_third_party_career', 10, 2 );
function post_to_third_party_career( $entry, $form ) {
 
    $post_url = 'http://go.pardot.com/l/563842/2019-02-07/4pvq3';
    $body = array(
        'input_5' => rgar( $entry, '5' ), //Location
        'input_10' => rgar( $entry, '10' ), //Position
        'input_1' => rgar( $entry, '1' ), //Name
        'input_11' => rgar( $entry, '11' ), //Age
		'input_12' => rgar( $entry, '12' ), //Gender
        'input_4' => rgar( $entry, '4' ), //Email
        'input_7' => rgar( $entry, '7' ), //city
        'input_13' => rgar( $entry, '13' ), //Address
        'input_6' => rgar( $entry, '6' ), //Cover letter
        'input_15' => rgar( $entry, '15' ), //Resume paste
        'input_14' => rgar( $entry, '14' ) //Resume
    );
    //GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}

/*add_action( 'gform_after_submission_2', 'post_to_third_party_contact', 10, 2 );
function post_to_third_party_contact( $entry, $form ) {
 
    $post_url = 'http://go.pardot.com/l/563842/2019-01-18/4fhbl';
    $body = array(
        'input_1' => rgar( $entry, '1' ), //Name
        'input_7' => rgar( $entry, '7' ), //Phone
        'input_4' => rgar( $entry, '4' ), //Email
        'input_5' => rgar( $entry, '5' ), //Want to reach
        'input_6' => rgar( $entry, '6' ), //Message
	);
    //GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}*/

add_action( 'gform_after_submission_5', 'post_to_third_party_subscribe', 10, 2 );
function post_to_third_party_subscribe( $entry, $form ) {
 
    $post_url = 'http://go.pardot.com/l/563842/2019-02-07/4pvrm';
    $body = array(
        'input_1' => rgar( $entry, '1' ) //Email
    
	);
    //GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}

function login()
{
    $creds = array();
    
    $username = null;
    $password = null;
    
    if($_SERVER['REQUEST_METHOD'] == "POST")
    {
        $username = $_POST["username"];
        $password = $_POST["password"];
    }
    else
    {
        $username = $_GET["username"];
        $password = $_GET["password"];
    }
    
    $creds['user_login'] = $username;
    $creds['user_password'] = $password;
    $creds['remember'] = true;
    $user = wp_signon( $creds, false );
    if (is_wp_error($user))
    {
        echo "FALSE";   
    }
    else
    {
        echo "TRUE";
    }
    
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
    {
        die();
    }
    else
    {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
}

add_action("wp_ajax_login", "login");
add_action("wp_ajax_nopriv_login", "login");

function logout()
{
    wp_logout();
    
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
    {
        die();
    }
    else
    {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
}

add_action("wp_ajax_logout", "logout");
add_action("wp_ajax_nopriv_logout", "logout");


add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

add_filter("gform_field_validation_1_12", "custom_validation54", 10, 4);
function custom_validation54($result, $value, $form, $field){
// $first  = rgar( $value );
 if (!preg_match('~^[a-zA-Z\s]+$~', $value))
 {
    $result["is_valid"] = false;
    $result["message"] = "Please enter a parent name without digits and special characters";
 }
 return $result;
}

add_filter("gform_field_validation_1_5", "new_phone_validation", 10, 4);

function new_phone_validation($result, $value, $form, $field){

    if(!preg_match('~^\d{10}$~', $value)){
        $result["is_valid"] = false;
        $result["message"] = "Please enter a valid Phone number";
    }

    return $result;
}



add_filter("gform_field_validation_2_1", "custom_validation55", 10, 4);
function custom_validation55($result, $value, $form, $field){
// $first  = rgar( $value );
 if (!preg_match('~^[a-zA-Z\s]+$~', $value))
 {
    $result["is_valid"] = false;
    $result["message"] = "Please enter a parent name without digits and special characters";
 }
 return $result;
}
add_filter("gform_field_validation_2_10", "new_phone_validation2", 10, 4);

function new_phone_validation2($result, $value, $form, $field){

    if(!preg_match('~^\d{10}$~', $value)){
        $result["is_valid"] = false;
        $result["message"] = "Please enter a valid Phone number";
    }

    return $result;
}

add_filter("gform_field_validation_4_7", "new_phone_validation4", 10, 4);

function new_phone_validation4($result, $value, $form, $field){

    if(!preg_match('~^\d{10}$~', $value)){
        $result["is_valid"] = false;
        $result["message"] = "Please enter a valid Phone number";
    }

    return $result;
}

add_filter("gform_field_validation_4_1", "custom_validation41", 10, 4);
function custom_validation41($result, $value, $form, $field){
// $first  = rgar( $value );
 if (!preg_match('~^[a-zA-Z\s]+$~', $value))
 {
    $result["is_valid"] = false;
    $result["message"] = "Please enter a parent name without digits and special characters";
 }
 return $result;
}



function idpe_lp_gravity_to_sfdc( $entry, $form ) {

	$parsed_site_url = parse_url(rgar( $entry, '6' ));
	$parsed_site_url_arg=array();
	parse_str($parsed_site_url['query'], $parsed_site_url_arg);	
	$utmsource = $parsed_site_url_arg['utm_source'];
	$keyword = $parsed_site_url_arg['utm_term'];
	$campaign = $parsed_site_url_arg['utm_campaign'];
	$medium = $parsed_site_url_arg['utm_medium'];
	$display = $parsed_site_url_arg['utm_display'];
	$content = $parsed_site_url_arg['utm_content'];
	$location = $parsed_site_url_arg['utm_location'];
	
	
    $post_url = 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
    $body = array(
        'oid' => '00D7F000007CGCl',
		'retURL' => 'https://klayschools.com/thankyou/',
		'00N7F00000PwENL' => rgar( $entry, '1' ),/*childsname */
        '00N7F00000Pvpp3' => rgar( $entry, '7' ),/*Childage */
        'last_name' => rgar( $entry, '1' ),/*Parent's name - last name */
        'mobile' => rgar( $entry, '4' ),/*mobile */
        'email' => rgar( $entry, '3' ),/*email */
        'location' => rgar( $entry, '9' ),/*city */
        '00N7F00000QQ7Q3' => rgar( $entry, '6' ),/*Source URL*/
		'00N7F00000Pvwxy' => 'Website/Digital',
		'company' => 'None'        
	);
	
	
	
	if($location)
	{
		$body['00N7F00000Pvpqa'] = $location;
	}
	if($campaign)
	{
		$body['00N7F00000PvpqB'] = $campaign;
	}
	if($utmsource)
	{
		$body['00N7F00000PvpqL'] = $utmsource;
	}
	
    GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}
add_action( 'gform_after_submission_6', 'idpe_lp_gravity_to_sfdc', 10, 2 );

// retrieves the attachment ID from the file URL
function pippin_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}



?>