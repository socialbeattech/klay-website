<?php
/**
 * Template Name: Escalations
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 image_center">
				<h2 class="mt-0 mb-15 gotham-rounded-book esc_size fs-xs-17">Your concerns are important! <br>
Please reach out to us and we will respond within 24 hours.

</h2>
			</div>
		</div>
		
	</div>	
</section>
<section class="contact_sec">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3 image_center">
				<!-- <img src="<?php echo get_template_directory_uri() ?>/images/contact/contact_back.png" alt="" /> -->
				<div class="contact-form1">
					<div class="contact-form11">
					<?php //echo do_shortcode('[gravityform id="2" title="true" description="true" ajax="true"]'); ?>
					<iframe src="https://go.pardot.com/l/563842/2019-02-26/4x3y5" width="100%" height="430" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
				</div>
				</div>
			</div>
		</div>
	</div>	
</section>
<section class="office_sec">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-12 image_center">
				<div class="office office_st">
					<?php 
					$i=1; $fff = get_field('contact');  $lastElement = end($fff); while(have_rows('contact')){the_row(); ?>

						<h1 class="gotham-rounded-medium"><?php the_sub_field('title') ?></h1>
						<p class="gotham-rounded-book"><?php the_sub_field('address') ?> <a href="mailto:<?php the_sub_field('email') ?>"><span style="color:#003366;font-weight:bold;"><?php the_sub_field('email') ?></p></span></a>
						<p class="gotham-rounded-book"><?php the_sub_field('phone') ?></p>
<div class="border_line<?php echo $i; ?>"><hr> </div>
					<?php $i++;} ?>
				</div>
			</div>
		</div>
	</div>	
</section>
<?php
endwhile;
get_footer();
