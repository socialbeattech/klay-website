<?php
/**
 * Template Name: Parent Helpline
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
.iframe_head h3.gform_title {
    letter-spacing: normal!important;
    margin: -4px 0 6px !important;
    font-family: 'AsparagusSprouts';
    font-size: 42px;
}
.parent-helpline .contact-form11.admin_form {
    background: #a2e5e9;
    padding: 0% 30%;
}
body {
    font-size: 17px;
    font-family: 'GothamRoundedBook';
    font-weight: normal;
    font-style: normal;
    color: #333333;
}
body .gform_wrapper .gform_footer input.button, body .gform_wrapper .gform_footer input[type=submit], body .gform_wrapper .gform_page_footer input.button, body .gform_wrapper .gform_page_footer input[type=submit], .btn-submit {
    margin: 0;
    background: url('<?php echo get_template_directory_uri() ?>/images/submit1.png');
    background-position: left top;
    width: 122px;
    height: 42px;
    color: #fff;
    border: none;
    cursor: pointer;
    font-family: 'GothamRoundedBook';
    display: inline-block;
    transition: color 0.3s;
    line-height: 44px;
}
body .gform_wrapper .gform_footer input.button:hover, body .gform_wrapper .gform_footer input[type=submit]:hover, body .gform_wrapper .gform_page_footer input.button:hover, body .gform_wrapper .gform_page_footer input[type=submit]:hover, .btn-submit:hover {
    background-position: 0 -42px;
}
.parent-helpline .gform_wrapper h3.gform_title {
    letter-spacing: normal!important;
    margin: 10px 0 6px;
    font-family: 'AsparagusSprouts';
    font-size: 42px;
    font-weight: 500 !important;
}

.panel-title{letter-spacing: normal!important;
    margin: 10px 0 6px;
    font-family: 'AsparagusSprouts';
    font-size: 42px;}
.panel-experts-mehana1 {
    background: #f6f6f6;
}
.mehana
{
    text-align: center;
    width: 100%;
    float: left;
    padding: 30px 0;
}
.parent-helpline .gform_wrapper h3.gform_title {
    letter-spacing: normal!important;
    margin: -4px 0 6px !important;
    font-family: 'AsparagusSprouts';
    font-size: 42px;
}section.parent-helpline_sec {
    overflow-x: hidden;
}
@media only screen and (max-width:767px){
.parent-helpline .contact-form11.admin_form {
    background: #a2e5e9;
    padding: 3% 2%;
}.contact-form1 {
    border: 21px solid transparent;
    -webkit-border-image: url(https://www.lifeonline.co/klay-schools/wp-content/uploads/2019/01/contact_back_back-2.png) 30 fill stretch;
    -o-border-image: url(https://www.lifeonline.co/klay-schools/wp-content/uploads/2019/01/contact_back_back-2.png) 30 fill stretch;
    border-image: url(https://www.lifeonline.co/klay-schools/wp-content/uploads/2019/01/contact_back_back-2.png) 30 fill stretch;
}
span.mehana img {
    width: 80%;
}	
li#field_3_2 {
    padding: 0 18px 0 0;
}
.col-12.col-md-12.parent-helpline.image_center {
    padding-right: 0;
    padding-left: 0;
}

}
</style>
<!-- <section class="pt-50 pb-50 story_sec">
	<div class="container-fluid">
		<div class="row grey_back">
			<div class="col-12 col-md-6">
					<div class="pad_story">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12 col-md-6 story_img">
					<?php the_post_thumbnail('full', array( 'class' => 'img-responsive center-block imgwth' )); ?>
				</div>
			</div>
			
		</div>
</section> -->

<section class="parent_sec"  style="margin: 30px 0;">
		<div class="container-fluid">
			<p class="gotham-rounded-book"><?php the_field('parent-helpline-para'); ?></p>
		</div>

</section>

<section class="parent-helpline_sec" style="margin: 20px 0;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-12 parent-helpline image_center">
				<!-- <img src="<?php echo get_template_directory_uri() ?>/images/contact/contact_back.png" alt="" /> -->
				<div class="contact-form1" style="margin: 20px 0;">
					<div class="contact-form11 enquire-now pt-15 pb-15 ffset-md-3 no-label-form admin_form">
					<?php //echo do_shortcode('[gravityform id="3" title="true" description="true" ajax="true"]'); ?>
					<div class="gform_heading iframe_head">
                            <h3 class="gform_title">Reach our Experts</h3>
                            <span class="gform_description"></span>
                        </div>
					<iframe src="https://go.pardot.com/l/563842/2019-02-26/4x3yt?Source_URL=<?php the_permalink(); ?>" width="100%" height="300" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
				</div>
				</div>
				
			</div>
		</div>
	</div>	
</section>


<section class="panel-experts">
	<div class="container-fluid">

		<div class="row">

			<span class="panel-title" style="text-align: center; width: 100%; float: left;">Our Panel of Experts</span>

		</div>

		<div class="row">

			<div class="col-12 col-md-1  panel-experts-mehana">
			</div>

			<div class="col-12 col-md-5  panel-experts-mehana">
				<div class="panel-experts-mehana1" style="padding: 3px 15px;">
					<?php // set the image url
					$image_url = get_field('mehana-pic');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
					<span class="mehana" style="text-align: center;"><img src="<?php echo the_field('mehana-pic'); ?>" alt="<?php echo $image_alt; ?>"></span>
					<span class="parent-des gotham-rounded-book"><?php the_field('mehana-des'); ?></span>
				</div>
			</div>

			<div class="col-12 col-md-5 panel-experts-mehana">
				<div class="panel-experts-mehana1" style="padding: 3px 15px;">
					<?php // set the image url
					$image_url = get_field('meera-pic');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
					<span class="mehana" style="text-align: center;"><img src="<?php echo the_field('meera-pic'); ?>" alt="<?php echo $image_alt; ?>"></span>
					<span class="parent-des gotham-rounded-book"><?php the_field('meera-des'); ?></span>
				</div>
				
			</div>

			<div class="col-12 col-md-1  panel-experts-mehana">
			</div>
	</div>
	</div>

</section>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<?php
endwhile;
get_footer();
