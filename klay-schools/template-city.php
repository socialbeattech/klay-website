<?php
/**
 * Template Name: City
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
?>

<section class="section_post">
	<div class="container-fluid">
		
		<div class="resultneww">
		<div class="row post_pad my-posts">
			<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;	
				$term = get_queried_object();
				foreach($categories as $key => $value){	?>
				<div class="col-6 col-sm-3">
					<a href="<?php echo get_category_link($value->term_id); ?>">
						<div class="bordernew">
							<?php $image = get_field('category_image', $value->taxonomy . '_' . $value->term_id ); //print_r($image);?>
							<?php // set the image url
					//$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($image); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
							<img class="img-responsive center-block" src="<?php echo $image;  ?>" alt="<?php echo $image_alt; ?>">
							<h3 class="gotham-rounded-medium"><?php echo $value->name; ?></h3>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
</section>
<!-- <section class="what_parent">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section> -->

<?php
get_footer();
