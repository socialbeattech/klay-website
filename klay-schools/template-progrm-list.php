<?php
/**
 * Template Name: Program List
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
?>

<section class="section_post">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">Programs</h2>
			</div>
		</div>
		<div class="resultneww">
		<div class="row post_pad my-posts">
				<div class="col-6 col-sm-3">
					<?php $post=  get_post('1034'); ?>
					<a href="<?php the_permalink(); ?>">
						<div class="bordernew">

							<img class="img-responsive center-block" src="<?php the_post_thumbnail_url();  ?>" alt="th_preschool">
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
				<div class="col-6 col-sm-3">
					<?php $post=  get_post('158'); ?>
					<a href="<?php the_permalink(); ?>">
						<div class="bordernew">

							<img class="img-responsive center-block" src="<?php the_post_thumbnail_url();  ?>" alt="th_daycare">
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
				<div class="col-6 col-sm-3">
					<?php $post=  get_post('347'); ?>
					<a href="<?php the_permalink(); ?>">
						<div class="bordernew">

							<img class="img-responsive center-block" src="<?php the_post_thumbnail_url();  ?>" alt="th_infant">
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
				<div class="col-6 col-sm-3">
					<?php $post=  get_post('1000'); ?>
					<a href="<?php the_permalink(); ?>">
						<div class="bordernew">

							<img class="img-responsive center-block" src="<?php the_post_thumbnail_url();  ?>" alt="th_enrichment">
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
				<div class="col-6 col-sm-3">
					<?php $post=  get_post('264'); ?>
					<a href="<?php the_permalink(); ?>">
						<div class="bordernew">

							<img class="img-responsive center-block" src="<?php the_post_thumbnail_url();  ?>" alt="th_specialneeds">
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
			
		</div>
	</div>
</div>
</section>


<?php
get_footer();
