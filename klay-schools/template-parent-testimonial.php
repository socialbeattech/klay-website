<?php
/**
 * Template Name: Parent Testimonials
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
#more {display: none;}
	iframe {
    width: 95% !important;
    height: 317px !important;
}
img.quotes {
    position: relative;
    top: -7px;
    padding: 0 7px;
}.col-12.col-md-6.gotham-rounded-book.parent-testimonial-set-one {
    padding:2px 5px;
}.parent-testimonials img {
    width: 100%;
    height: 100%;
    padding: 5px 45px;
}


button#myBtn{
    margin: 0;
    background: url('<?php echo get_template_directory_uri() ?>/images/view-videos.png');
    background-position: left top;
    width: 236px;
    height: 46px;
    color: #fff;
    border: none;
    cursor: pointer;
    font-family: 'GothamRoundedBook';
    display: inline-block;
    transition: color 0.3s;
    line-height: 44px;
}
button#myBtn:hover, .btn-submit:hover {
    background-position: 0 -47px;
}
button#myBtn1{
	background: url('<?php echo get_template_directory_uri() ?>/images/load-more.png');
	/*background-position: left top;*/
    width: 296px;
    height: 48px;
    color: #fff;
    border: none;
    cursor: pointer;
    font-family: 'GothamRoundedBook';
    display: inline-block;
    transition: color 0.3s;
    line-height: 44px;
}
.gotham-rounded-book, .section-sub-title-font {
    font-family: 'GothamRoundedBook';
    font-style: initial;
}
.parent-testimonials {
    background: #eeeeee;
    padding: 30px 30px;
    margin: 10px 5px;
    height: auto;
}
.row.parent-testimonials-row{
	margin: 0 50px;
}
#open-popup {padding:20px}
.white-popup {
  position: relative;
  background: #FFF;
  padding: 40px;
  width: auto;
  max-width: 200px;
  margin: 20px auto;
  text-align: center;
}
.slide {
    padding: 40px;
    background: #FFF;
    margin: 0 auto;
    position: relative;
    /* max-width: 100px; */
    width: 55%;
    height: auto;
}
.slide iframe {
    width: 100%;
    height: 450px !important;
}h1.fs-40.fs-xs-20.mb-50.mt-0.gotham-rounded-book {
    color: #fff;
}
@media only screen and (max-width:767px){
.row.parent-testimonials-row{
	margin: 0 0px;
}.load-more-videos.pt-20 {
    text-align: center;
}
.parent-testimonials p {
    font-size: 12px;
}
button.mfp-close, button.mfp-arrow {
    overflow: visible;
    cursor: pointer;
    background: transparent;
    border: 0;
    -webkit-appearance: none;
    display: block;
    outline: none;
    padding: 0;
    z-index: 1046;
    box-shadow: none;
    touch-action: manipulation;
    right: 3%;
}
.gotham-rounded-medium.parents-des.fs-24 {
    font-size: 20px;
}
.gotham-rounded-book-italic.parents-des.pt-15.fs-17 {
    font-size: 16px;
}
.slide {
    padding: 38px 0;
    background: #FFF;
    margin: 0 auto;
    position: relative;
    /* max-width: 100px; */
    width: auto;
    height: auto;
}
.slide iframe {
    width: 100%;
    height: auto !important;
}
iframe {
    width: 100% !important;
    height: auto !important;
}
.gotham-rounded-medium.parents-des.fs-24 {
    text-align: center;
}
.parents-video-des.gotham-rounded-book {
    padding: 0 40px;
}
}
</style>

<section class="people_sec">
	<div class="container-fluid">
		<h2 class="mt-0 mb-0 section-title fs-42">What our Parents Say</h2>
		<p class="gotham-rounded-book  text-center">Learn more about KLAY from our Champions - our Parents!</p>
		<div class="row" style="margin:40px 0;">
			<div class="col-12 col-md-6">
				<div class="parents-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/gCw2DPiU7Yo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
			</div>
			<div class="col-12 col-md-6 pt-35">
				<div class="gotham-rounded-medium parents-des fs-24"><img class="quotes" src="<?php echo get_template_directory_uri(); ?>/images/before-quote.png" alt="before-quote"><span class="quotes-des" style="color: #01bac6">The fact that they give CCTV coverage speaks volumes about the confidence they have in the care they are giving to the child in the institution.</span><img class="quotes" src="<?php echo get_template_directory_uri(); ?>/images/after-quotes.png" alt="before-quote"> </div>
				<div class="gotham-rounded-book-italic parents-des pt-15 fs-17"><i>Sonvi Khanna, Mother of Avdhan<br>
					KLAY-TLC Bandra, Mumbai<i></div>
				<div class="load-more-videos pt-20">
					<button onclick="myFunction()" id="myBtn" class="open-popup"><a href="#gallery1" class="open-gallery-link" style="color: #fff">Watch More Video</a></button>
				</div>
			</div>
		</div>

	</div>
</section>

<section class="people_sec_video">
	<div class="container-fluid">
	
		<div class="row parent-testimonials-row">
				
				<?php if( have_rows('parent-testimonial-set1') ):?>
					
					<?php while ( have_rows('parent-testimonial-set1') ) : the_row(); ?>
							<div class="col-12 col-md-6 gotham-rounded-book parent-testimonial-set-one">
								<div class="parent-testimonials"><?php  the_sub_field('parent-testimonial-set-one'); ?></div>
					   		</div>
						<?php  endwhile; else : endif; ?>

		</div>

		<div id="dots"></div>
		<div id="more">
				<div class="row parent-testimonials-row">
					<?php if( have_rows('parent-testimonial-set2') ):?>
							<?php while ( have_rows('parent-testimonial-set2') ) : the_row(); ?>
								<div class="col-12 col-md-6 gotham-rounded-book parent-testimonial-set-two">
									<div class="parent-testimonials"><?php  the_sub_field('parent-testimonial-set-two'); ?></div>
						   		</div>
							<?php endwhile;  else : endif; ?>
				</div>

		</div>

		<div class="load-more-videos pt-20 text-center">
			<button onclick="myFunction()" id="myBtn1" style="text-align: center;">Load More Testimonials</button>
		</div>

		<div id="gallery1" class="mfp-hide">
			 <div class="slide">
			    <div class="parents-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/9SAAFKDkLUA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
				    <div class="parents-video-des gotham-rounded-book"><p style="color: #ff6600; font-weight: bold;">Shikha Mittal, Mother of Arjun, KLAY-TLC Bandra, Mumbai.</p>
				    <p>I’ve seen him transform from an extremely shy boy to a confident young boy. I think this is the best thing I could have done for Arjun.</p>
					</div>
			 </div>
			 <div class="slide">
			    <div class="parents-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/bsrS9kjVPB0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
			    <div class="parents-video-des gotham-rounded-book"><p style="color: #ff6600; font-weight: bold;">Kinjal Darukhanawala, Mother of Ariana, KLAY-TLC Bandra, Mumbai</p>
				    <p>I think people are still warming up to the idea of sending kids to day care in India. But I would just say to moms in general, you don’t need to put your careers on hold!</p>
					</div>
			 </div>
			 <div class="slide">
			    <div class="parents-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/GbM1-rI2uvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
			    <div class="parents-video-des gotham-rounded-book"><p style="color: #ff6600; font-weight: bold;">Tahira Nath, Mother of Veda, KLAY-TLC Bandra, Mumbai</p>
				    <p>Veda joined KLAY one day after she turned 1 and has been a happy preschooler ever since. This place (KLAY) has become Veda’s home away from home. She always </p>
					</div>
			 </div>
			 <div class="slide">
			    <div class="parents-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/bHnKW7sKJNI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
			    <div class="parents-video-des gotham-rounded-book"><p style="color: #ff6600; font-weight: bold;">Rooki Shetty Mother of Tia, KLAY-TLC Bandra, Mumbai.</p>
				    <p>I don’t feel guilty when I’m at work. I don’t feel bad that somebody else is raising my daughter. I don’t feel bad at all, because I know that they’re doing a good job! I’ve had good experiences.

					Tia’s learning curve has improved so much in terms of how she looks at things and how responsible she is. Daycare, I feel, is a better option than keeping a nanny at home.</p>
					</div>
			 </div>
		</div>


  </div>

</section>

<div class="container-fluid"><hr></div>
<script type="text/javascript">

function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn1");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Load More Testimonials"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Load Less Testimonials"; 
    moreText.style.display = "inline";
  }
}

</script>
<script type="text/javascript">

$('.open-gallery-link').click(function() {
  
  var items = [];
  $( $(this).attr('href') ).find('.slide').each(function() {
    items.push( {
      src: $(this) 
    } );
  });
  
  $.magnificPopup.open({
    items:items,
    gallery: {
      enabled: true 
    }
  });
});
</script>
<?php
endwhile;
get_footer();
