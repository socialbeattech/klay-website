<?php
/**
 * Template Name: Our STory
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-50 story_sec">
	<div class="container-fluid">
		<div class="row grey_back">
			<div class="col-12 col-md-6">
					<div class="pad_story">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12 col-md-6 story_img">
					<?php the_post_thumbnail('full', array( 'class' => 'img-responsive center-block imgwth' )); ?>
				</div>
			</div>
			
		</div>
</section>
<div class="container-fluid"><hr></div>
<section class="people_sec">
	<div class="container-fluid">
		<h2 class="mt-0 mb-15 section-title1">The People</h2>
		<div class="row">
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row">
					<div class="col-12 col-md-6">
				<div class="blue_back">
					<h3 class="gotham-rounded-medium image_center">Leadership</h3>
					<div class="image_center"><img src="<?php echo get_template_directory_uri(); ?>/images/head_line.png" alt="head_line"></div>
					<p class="gotham-rounded-book"><?php the_field('leadership_content'); ?></p>
					<div class="image_center"><a class="btn-banner_our" href="<?php the_field('leadership_link'); ?>">Meet the TEAM</a></div>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="blue_back1">
					<h3 class="gotham-rounded-medium image_center">Investors</h3>
					<div class="image_center in_img"><img src="<?php echo get_template_directory_uri(); ?>/images/head_line.png" alt="head_line"></div>
					<?php while(have_rows('investors_image')){the_row(); ?>
						<?php // set the image url
					$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
					<div class="image_center"><img class="in_img" src="<?php the_sub_field('image'); ?>" alt="<?php echo $image_alt; ?>"></div>
				<?php } ?>
				</div>
			</div>
		</div>
		</div>
	</div>

</div>
</section>
<div class="container-fluid"><hr></div>
<section class="journey_sec">
	<div class="container-fluid">
		<h2 class="mt-0 mb-15 section-title1">The Journey</h2>
		<div class="image_center"><h3 class="gotham-rounded-book">From one centre, to being <span class="orange-colornew gotham-rounded-medium">India’s Most Trusted Parenting Partner!</span></h3></div>
		<div class="row">
			<div class="col-12">
				<?php // set the image url
					$image_url = get_field('journey_image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
				<div class="image_center"><img class="in_img" src="<?php the_field('journey_image'); ?>" alt="<?php echo $image_alt; ?>"></div>
			</div>
		</div>
	</div>
</section>
<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>
<?php
endwhile;
get_footer();
