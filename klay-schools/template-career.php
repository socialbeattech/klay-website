<?php
/**
 * Template Name: Carrer
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
//while(have_posts()): the_post();
?>
<style type="text/css">
	.gfield_description.validation_message {
    font-size: 12px;
}.gform_wrapper div.validation_error {
    color: #790000;
    font-size: 1em;
    font-weight: 700;
    margin-bottom: 25px;
    border-top: 2px solid #790000;
    border-bottom: 2px solid #790000;
    padding: 16px 0;
    clear: both;
    width: 100%;
    text-align: center;
    font-size: 13px;
}
</style>
<section class="pt-50 pb-50">
	<div class="container-fluid">
	<!-- 	<div class="row">
			<div class="col-12"> -->

				 <div class="tabbable">
				    <ul class="nav carrer_tab">
				      <li><a href="#tab1" class="show active" data-toggle="tab"><h3 class="gotham-rounded-medium">Join Us Now!</h3></a></li>
				      <li class="pad_tab">|</li>
				       <li><a href="#tab4" data-toggle="tab"><h3 class="gotham-rounded-medium">Life at KLAY</h3></a></li>
				       <li class="pad_tab">|</li>
				      <li><a href="#tab2" data-toggle="tab"><h3 class="gotham-rounded-medium">Benefits</h3></a></li>
				      <li class="pad_tab">|</li>
				      <li><a href="#tab3" data-toggle="tab"><h3 class="gotham-rounded-medium">Career Path</h3></a></li>
				      
				     
				    </ul><h3 class="gotham-rounded-medium">
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<div class="tabmain">
								<p class="gotham-rounded-book">At KLAY, we value your unique qualities and talents. We're dedicated to creating a work environment where you can develop your potential to pursue a meaningful & satisfying career.</p>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-4 career1">
										<h2>WALK IN</h2>
										<img class="imgwth" src="<?php echo get_template_directory_uri(); ?>/images/career/carrer1.jpg" alt="carrer1">
									</div>
									<div class="col-md-4 career21">
										<a target="_blank" href="https://aa111.taleo.net/careersection/kly_cs_external/jobsearch.ftl?lang=en&portal=8116760768"><h2>APPLY NOW</h2>
										<img class="imgwth" src="<?php echo get_template_directory_uri(); ?>/images/career/carrer2.jpg" alt="carrer2"></a>
									</div>
									<div class="col-md-2"></div>
							<!--	<div class="col-md-4 career3">
										<h2>APPLY NOW</h2>
										<img class="imgwth" src="<?php //echo get_template_directory_uri(); ?>/images/career/carrer3.jpg" alt="carrer3">
									</div> -->
								</div>
							 </div>

							<div class="span8 tabshow">
								<div class="tabbable">
									<ul class="nav carrer_tab_sub">
										<li><a href="#tab21" class="active1" data-toggle="tab"><h4 class="gotham-rounded-book">Walk In</h4></a></li>
										<li><a href="#tab22" class="active2" data-toggle="tab"><h4 class="gotham-rounded-book">Apply Now</h4></a></li>
									<!-- <li><a href="#tab23" class="active3" data-toggle="tab"><h4 class="gotham-rounded-book">Apply Now</h4></a></li> -->
									</ul>
									<div class="tab-content sub_tab_content">
										<div class="tab-pane" id="tab21">
											<div class="walk_in">
												<h2 class="section-title">Walk In</h2>
												<p class="gotham-rounded-book"><strong>Ever Friday</strong> of the month at the following Venues!</p>
											</div>
											<div class="row">
												<div class="col-md-12 loc_pad_new">
													<div class="carrer_loc">
														<h3 class="gotham-rounded-medium">Bangalore <span class="gotham-rounded-book"><strong>Time:</strong> 11.00 am - 3.00 pm</span></h3>
														<hr>
														<h4 class="gotham-rounded-medium"><strong>Whitefield</strong></h4>
														<p class="gotham-rounded-book">Founding Years Survey No. 31/1, Seetaramapalya, K. R. Puram Hobli, Bangalore - 560048. 
														Landmark: Next to Sumadhura Vasanatham Appt.</p>
														<h4 class="gotham-rounded-medium"><strong>Rajajinagar</strong></h4>
														<p class="gotham-rounded-book">Klay Prep School & Daycare, Ground Floor, Galaxy Club, Brigade Gateway, Dr. Rajkumar Road, Rajajinagar, 
Beside World Trade Centre, Bengaluru, Karnataka 560055</p>
													</div>
												</div>
												<div class="col-md-12 loc_pad_new">
													<div class="carrer_loc">
														<h3 class="gotham-rounded-medium">Hyderabad <span class="gotham-rounded-book"><strong>Time:</strong> 11.00 am - 3.00 pm</span></h3>
														<hr>
														<h4 class="gotham-rounded-medium"><strong>Kondapur</strong></h4>
														<p class="gotham-rounded-book">Plot 20-29, Legend Platinum Building, Off. Hitec City Road, Kondapur, Hyderabad, Telangana 500081</p>
													</div>
												</div>
												<div class="col-md-12 loc_pad_new">
													<div class="carrer_loc">
														<h3 class="gotham-rounded-medium">Gurgaon <span class="gotham-rounded-book"><strong>Time:</strong> 11.00 am - 3.00 pm</span></h3>
														<hr>
														<h4 class="gotham-rounded-medium"><strong>DLF Phase 4</strong></h4>
														<p class="gotham-rounded-book">Nursery Site No. 4106, DLF Phase - 4, Ashok Marg, Near Galleria Market, Gurugram, Haryana </p>
													</div>
												</div>
												
											</div>
										</div>
										<div class="tab-pane" id="tab22">
											<h2 class="section-title">Open Positions</h2>
											<!--<div class="form4">
												<div class="row">
													<div class="col-md-12 image_center">
														<p class="gotham-rounded-book image_center">Select your professional area of interest and location to see current opportunities.</p>
													</div>
												</div>
												<div class="row image_center">
													
													<div class="col-md-4 offset-md-2 image_center">
														<select class="location_car">
															<option value="">Select Location</option>
															<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
															$categories = get_terms( 'location_category', $args);
															$i = 0;	
															foreach($categories as $key => $value){	?>
															<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
														<?php } ?>
														</select>
													</div>
													<div class="col-md-4">
														<select class="position_car">
															<option value="">Select Job Function</option>
															<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
															$categories = get_terms( 'position_category', $args);
															$i = 0;	
															foreach($categories as $key => $value){	?>
															<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
														<?php } ?>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-md-10 offset-md-2 searchresult">
														
													</div>
												</div>
											</div> -->
											
											<div class="col-md-12 apply_btn"><a class="btn-submit" target="_blank" href="https://aa111.taleo.net/careersection/kly_cs_external/jobsearch.ftl?lang=en&portal=8116760768">Know More</a></div>
											<style type="text/css">
												.col-md-12.apply_btn {text-align: center;}
											</style>
										</div>
										<div class="tab-pane" id="tab23">
											<h2 class="section-title">Apply Now</h2>
											<p class="gotham-rounded-book">We are always on the look out for talented people to be part of our team. If there are no current positions that fit your 
skill set, send us your resume. You can also email us at careers@klayschools.com. We will get in touch when we have a requirement. </p>
<div class="row form4">
			<div class="col-12 col-md-6 offset-md-3 image_center form4">
				<div class="enquire-now">
<?php echo do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]'); ?>
</div>
</div>
</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab2">
							<?php $post=  get_post('63036'); ?> 
							<h2 class="section-title mob_res"><?php the_title(); ?></h2>
							<div class="gotham-rounded-book ob_res1"><?php echo $content = apply_filters('the_content', $post->post_content); ?></div>
							<div class="row">
								<div class="col-12">
							<div class="contact-form1">
								<div class="contact-form11">
									<h3 class="gotham-rounded-medium">The perks of working at KLAY</h3>
									<ul class="nav carrer_tab">
										<?php $i=1; while(have_rows('benefits_tab')){the_row(); ?>
				      <li><a href="#tabnew<?php echo $i; ?>" class="<?php if($i==1){ ?>active<?php } ?>" data-toggle="tab"><h4 class="gotham-rounded-medium"><?php the_sub_field('title'); ?></h4></a></li>
				  <?php $i++; } ?>
				    </ul>
				    <div class="tab-content">
				    	<?php $i=1; while(have_rows('benefits_tab')){the_row(); ?>
						<div class="tab-pane <?php if($i==1){ ?>active<?php } ?>" id="tabnew<?php echo $i; ?>">
							<p class="gotham-rounded-light "><?php the_sub_field('content'); ?></p>
						</div>
				<?php $i++; } ?>
					</div>
								</div>
							</div>
						</div>
					</div>
							<h2 class="section-title teacher_clay">Why Teachers Love KLAY</h2>
							<div class="sliderwidth clearfix pt-50 pb-50"><?php echo do_shortcode('[wonderplugin_slider id=3]'); ?>
								


							</div>
							<div class="car_pa"></div>
						</div>
						<div class="tab-pane" id="tab3">
							<?php $post=  get_post('62963'); ?>
							<div class="careerpath">
								<h2 class="section-title"><?php the_title(); ?></h2>
								<?php echo $content = apply_filters('the_content', $post->post_content); ?>
							</div>
							<div class="careerpath">
								<h2 class="section-title">We Provide</h2>
							</div>
							<div class="row row_pad_career">
								<div class="col-6 col-md-4 nopadding careerpath1_bk">
									<span><?php the_field('career_path_title1'); ?></span>
								</div>
								<div class="col-6 col-md-4 nopadding">
									<img src="<?php the_field('career_image1'); ?>" class="imgwth" alt="career_path1">
								</div>
								<div class="col-6 col-md-4 nopadding careerpath2_bk">
									<span><?php the_field('career_path_title2'); ?></span>
								</div>
								<div class="col-6 col-md-4 nopadding">
									<img src="<?php the_field('career_image2'); ?>" class="imgwth" alt="career_path2">
								</div>
								<div class="col-6 col-md-4 nopadding careerpath3_bk">
									<span><?php the_field('career_path_title3'); ?></span>
								</div>
								<div class="col-6 col-md-4 nopadding">
									<img src="<?php the_field('career_image3'); ?>" class="imgwth" alt="career_path3">
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab4">
							<?php $post=  get_post('64460'); ?>
							<div class="careerpath">
								<h2 class="section-title"><?php the_title(); ?></h2>
								<?php echo $content = apply_filters('the_content', $post->post_content); ?>
							</div>
							<div class="careerpath">
								<h2 class="section-title">Our Staff - The KLAY Champions</h2>
							</div>
<?php //ob_start(); ?>
	
			<div class="swiper-container swiper-container-staff">
			    <div class="swiper-wrapper">
						<?php $i=0; while(have_rows('our_staff')){the_row(); ?>
						<div class="swiper-slide">
							<div class="staff_image"><img src="<?php the_sub_field('image') ?>" alt="" class="w-100" /></div>
							<div class="staff_content gotham-rounded-light"><?php the_sub_field('content') ?></div>
							<div class="staff_name gotham-rounded-medium"><?php the_sub_field('name') ?></div>
							<?php $myvalue = get_sub_field('name'); $arr = explode(' ',trim($myvalue)); ?>
							<div class="staff_link gotham-rounded-medium"><a data-toggle="modal" data-target="#myModal<?php echo $i; ?>" href="#">Read <?php echo $arr[0]; ?>’s Story</a></div>
						</div>




						
						<?php $i++;} ?>
			</div>
			 <div class="swiper-pagination"></div>
					</div>
					<hr>

					<div class="careerpath">
								<h2 class="section-title"><?php the_field('staff_title'); ?></h2>
								<p><?php the_field('staff_content'); ?></p>
							</div>
							<div class="row support_wo">
								<div class="col-md-8">
									
									<?php the_field('staff_iframe'); ?>
								</div>
								<div class="col-md-4">
									<div class="staff_content gotham-rounded-light">“<?php the_field('iframe_content'); ?>”</div>

<div class="staff_name gotham-rounded-medium"><?php the_field('iframe_name'); ?></div>
								</div>
								
							</div>
							<br>
							<hr>
							<div class="walk_in">
												<h2 class="section-title">Life at KLAY!</h2>
												<p class="gotham-rounded-book" style="text-align: center;">Watch us having Fun at Work!</p>
											</div>

											<div class="sliderwidth clearfix pb-50"><?php echo do_shortcode('[wonderplugin_slider id=2]'); ?>
								


							</div>
			
			</div>
			<?php //ob_end_clean(); ?>
					</div>
				</div>
			<!-- </div>
		</div> -->
<style type="text/css">
	.swiper-slide{background: #f6f6f6;padding: 5%;}
	.swiper-wrapper{padding-bottom: 5%;}
	.swiper-pagination-bullet { width: 12px; height: 12px;}
	.swiper-pagination-bullet-active {opacity: 1;background: #01bac6;}
	.swiper-container {width: 85%;}
</style>

	</div>
</section>
	<div class="container-fluid">
	<hr>
	</div>



<?php $i=0; while(have_rows('our_staff')){the_row(); ?>
						  <div class="modal" id="myModal<?php echo $i; ?>">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><?php the_sub_field('name') ?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="staff_image"><img src="<?php the_sub_field('image') ?>" alt="" class="w-100" /></div>
							<div class="staff_content gotham-rounded-light"><?php the_sub_field('full_content') ?></div>
							
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        
        </div>
        
      </div>
    </div>
  </div>
<?php $i++; } ?>
<?php
while(have_posts()): the_post();
endwhile;
get_footer();
