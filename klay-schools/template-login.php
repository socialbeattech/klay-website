<?php  
/** 
* Template Name: login page 
*/  
get_header(); ?>

<!-- section -->
<section class="pt-50 pb-50 aa_loginForm">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 image_center">

                <h2 class="mt-0 mb-15 section-title">Parent Login</h2>
                <!-- <img src="https://www.lifeonline.co/klay-schools/wp-content/themes/klay-schools/images/contact/contact_back.png" alt="" /> -->
                <div class="contact-form1">
                    <div class="contact-form11">
                    <?php 
                    global $user_login;
                    // In case of a login error.
                    if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
                    <div class="aa_error">
                    <p><?php _e( 'FAILED: Try again!', 'AA' ); ?></p>
                    </div>
                    <?php 
                    endif;
                    // If user is already logged in.
                    if ( is_user_logged_in() ) : ?>

                    <div class="aa_logout"> 

                    <?php 
                    _e( 'Hello', 'AA' ); 
                    echo $user_login; 
                    ?>

                    </br>

                    <?php _e( 'You are already logged in.', 'AA' ); ?>

                    </div>

                    <a id="wp-submit" href="<?php echo wp_logout_url(); ?>" title="Logout">
                    <?php _e( 'Logout', 'AA' ); ?>
                    </a>

                    <?php 
                    // If user is not logged in.
                    else: 

                    // Login form arguments.
                    $args = array(
                    'echo'           => true,
                    'redirect'       => admin_url(), 
                    'form_id'        => 'loginform',
                    'label_username' => __( 'Username' ),
                    'label_password' => __( 'Password' ),
                    'label_remember' => __( 'Remember Me' ),
                    'label_log_in'   => __( 'Log In' ),
                    'id_username'    => 'user_login',
                    'id_password'    => 'user_pass',
                    'id_remember'    => 'rememberme',
                    'id_submit'      => 'wp-submit',
                    'remember'       => true,
                    'value_username' => NULL,
                    'value_remember' => true
                    ); 

                    // Calling the login form.
                    wp_login_form( $args );
                    endif;
                    ?> 
                    </div>
                </div>
            </div>
        </div>
        <?php if ( !is_user_logged_in() ) { ?>
        <ul class="list-inline custom_reg gotham-rounded-book">

        <li>
        <a class="aa_forgot" href="https://www.lifeonline.co/klay-schools/register/">Register Now </a>
        </li>
        <li>|</li>
        <li>
        <a class="aa_forgot" href="https://www.lifeonline.co/klay-schools/password-reset/">Forgot Password ?</a>
        </li>
        </ul>
        <?php } ?>
        <hr>
    </div>
</section>
<!-- /section -->

<?php get_footer(); ?>