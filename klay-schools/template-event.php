<?php
/**
 * Template Name: Event
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();

?>


<section class="pt-50 pb-50 section_event section_new_event">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Upcoming Events</h2>
				<hr>
			</div>
		</div>
		<div class="swipeslider_event">
			<div class="swiper-container swiper-container-event1">
			    <div class="swiper-wrapper">
					<?php 
					
					$today = date('Ymd');
						global $post; $i = 1;
						$args = array( 'posts_per_page' => '-1', 'post_type' => 'events','order' => 'ASC','meta_query' => array(
	     array(
	        'key'		=> 'date',
	        'compare'	=> '>=',
	        'value'		=> $today,
	    )
    ) );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) { setup_postdata($post); 
					?>
						<div class="swiper-slide image_center">
							
							<h2 class="gotham-rounded-medium"><?php the_field('date'); ?></h2>
							<?php the_content(); ?>
							<a href="<?php the_post_thumbnail_url(); ?>" data-rel="lightbox"><?php the_post_thumbnail('event-gal'); ?></a>
						</div>
			      	<?php } ?>
					
				  
				</div>
				  <div class="single-slide-prev"></div>
			<div class="single-slide-next"></div>
			</div>
		</div>
	</div>

</section>
<section class="section_event section_new_archive">
	<div class="container-fluid">
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Event Archive</h2>
				
			</div>
		</div>
		<div class="swipeslider_event">
			<div class="swiper-container swiper-container-event2">
			    <div class="swiper-wrapper">
					<?php 
					
					$today = date('Ymd');
						global $post; $i = 1;
						$args = array( 'posts_per_page' => '-1', 'post_type' => 'events','order' => 'DESC','meta_query' => array(
	     array(
	        'key'		=> 'date',
	        'compare'	=> '<',
	        'value'		=> $today,
	    )
    ) );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) { setup_postdata($post); 
					?>
						<div class="swiper-slide image_center">

							
							<h2 class="gotham-rounded-medium"><?php //the_field('date'); ?></h2>
							<?php //the_content(); ?>
							<div class="image_overlay">
							<a href="<?php the_post_thumbnail_url(); ?>" data-rel="lightbox"><?php the_post_thumbnail('event-gal',array('class'=>'image')); ?>

							 <div class="middle">
							    <div class="text"> <i class="fa fa-search" aria-hidden="true"></i> </div>
							  </div></a>
							</div>
						</div>
			      	<?php } ?>
					
				   
				</div>
				<div class="single-slide-next"></div>
				 <div class="single-slide-prev"></div>
			
			</div>
		
		</div>
	<div class="hr"><hr></div>
	</div>

</section>
<section class="section_event section_new_archive">
	<div class="container-fluid">
	
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				<h2 class="section-title">Celebrity Mom Stories</h2><br>
				<div class="single_content"><p>Motherhood is a beautiful journey, filled with its own share of challenges and ups and downs interlaced with those eureka moments that make it all worth it.

For the first ever, KLAY in association with SheThePeople brings to you a series of short, relatable and inspiring videos that showcase mom stars – celebrity moms who have all come together to talk about their motherhood journeys and how they have so beautifully managed to strike the right balance between their careers and family. </p></div>
			<br><br>
			<p></p>	
				<iframe width="560" height="315" src="https://www.youtube.com/embed/iPiX3b3fNUA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				
			</div>
		</div>

	<div class="hr"><hr></div>
</div>
</section>
<?php while(have_posts()): the_post();
endwhile;
get_footer();
