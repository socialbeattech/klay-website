<?php
/**
 * Template Name: Family Day
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KCZKSZT');</script>
<!-- End Google Tag Manager -->
<style type="text/css">
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
</style>
<section class="header">
	<div class="fullcols">
		<img src="<?php echo get_template_directory_uri();?>/images/familyday-banner.jpg" alt="lp" class="img-fluid d-none d-sm-none d-md-block d-lg-block" width="1366" height="650" />
		<img src="<?php echo get_template_directory_uri();?>/images/familyday-mobile.jpg" alt="lp" class="img-fluid d-block d-sm-block d-md-none d-lg-none" width="480" height="460" />
	</div>
	<div class="fullcols family_head"> 
		<div class="container">
			<h1 class="GothamRoundedMedium">The most meaningful moments</br> in your life are ones you share</br> with your family</h1>
			<h2 class="GothamRoundedMedium">Share your family fun day</h2>
		</div>
	</div>
</section>
<section class="fullcols section_new_archive familyday_events">
	<div class="container">
		<h1 class="GothamRoundedMedium title text-center col-12 col-sm-12">Our Families that bonded over some fun activity</h1>
		<div class="row">			
			<?php
				$args = array('post_type'   => 'Family Day', 'post_status' => 'publish');  
				$familyday = new WP_Query( $args );
				if( $familyday->have_posts() ) : ?>
						<?php while( $familyday->have_posts() ) : $familyday->the_post(); ?>					
							<div class="familyday_holder col-12 col-sm-12 col-md-4 pb-5">							
								<?php $familypic = get_field('family_picture');if( $familypic ): ?><h4 class="col-12 col-sm-12 float-left text-center p-0 m-0"><img src="<?php echo $familypic['url']; ?>" alt="events" class="img-fluid text-center" width="<?php echo $familypic['width']; ?>" height="<?php echo $familypic['height']; ?>" /></h4><?php endif; ?>
								<?php if( get_field('family_title') ): ?><h1 class="col-12 col-sm-12 text-center px-5 mx-0"><?php the_field('family_title'); ?></h1><?php endif; ?>
								<?php if( get_field('parents_name') ): ?><h2 class="col-12 col-sm-12 text-center px-5 mx-0"><?php the_field('parents_name'); ?></h2><?php endif; ?>
								<?php if( get_field('child_name') ): ?><h3 class="col-12 col-sm-12 text-center px-5 mx-0"><?php the_field('child_name'); ?></h3><?php endif; ?>
								<?php if( get_field('brief_descriptions') ): ?><h5 class="col-12 col-sm-12 text-center px-5 mx-0"><?php the_field('brief_descriptions'); ?></h5><?php endif; ?>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>					
			<?php endif; ?>
		</div>
	</div>
</section>
<section class="better">
	<div class="fullcols">
		<div class="container px-0">
			<div class="float-left col-12 col-sm-12 pt-3 pb-2 px-0">
				<ul class="col-12 col-sm-12 float-left text-center m-0 p-0">
					<li class="get_title GothamRoundedBook fs-xs-14 fs-20">Get to know us better!</li>
					<li><a href="https://www.facebook.com/klayschools/"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/fb-fd.png" alt="fb" width="39" height="35"></a></li>
					<li><a href="https://twitter.com/KLAYSchools"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/tweet-fd.png" alt="tweet" width="39" height="35"></a></li>
					<li><a href="https://www.youtube.com/user/KlaySchools"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/yutbe-fd.png" alt="yutbe" width="39" height="35"></a></li>
					<li><a href="https://in.linkedin.com/company/klay-prep-schools-and-daycare"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/linkid-fd.png" alt="linkid" width="39" height="35"></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KCZKSZT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<style>
.GothamRoundedMedium { font-family: 'GothamRoundedMedium'; }

.section_new_archive { background : #303327 url("<?php echo get_template_directory_uri();?>/images/activitybg.jpg") repeat 0 0; }
.section_new_archive h1.title  { color : #fff; font-size : 34px; padding: 1em 0 0; } 
.familyday_holder { }
.familyday_holder h1 { }
.familyday_holder h1, .familyday_holder h2, .familyday_holder h3, .familyday_holder h4  { font-family: 'GothamRoundedMedium'; color : #fff; font-size : 18px;}
.familyday_holder h5 { font-family: 'GothamRoundedBook'; color : #fff; font-size : 12px;}

.better ul {list-style-type: none;}
.better ul li { color: #000;  display: inline-block;  float: none;  list-style-type: none;  padding: 0 3px;    text-align: center;    width: auto;}
.get_title { font-family: 'GothamRoundedBook'; }

@media only screen and (min-width:1024px)
{
.family_head { position: absolute; top: 14em; left: 5em; }
.family_head h1 { color : #fff; font-size : 34px; }
.family_head h2 { color : #08b1c2; font-size : 34px; }
}
@media only screen and (max-width:767px)
{
.family_head { left: 0; position: absolute; top: 4.5em; }
.family_head h1 { color : #fff; font-size : 15px; }
.family_head h2 { color : #08b1c2; font-size : 15px; }
.family_head { display : none; }
.header img { width : 100%; height : auto; }	
}

@media only screen and (min-width:768px) and (max-width:1024px)
{
.family_head { left: 2em; position: absolute;  top: 10em; }	
.family_head h1 { color : #fff; font-size : 25px; }
.family_head h2 { color : #08b1c2; font-size : 25px; }
}
@media only screen and (min-width:1367px)
{
.header img { width : 100%; height : auto; }	
}
</style>

<?php
endwhile;
get_footer();