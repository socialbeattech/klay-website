<?php  
/** 
 * Template Name: Forget page 
 */  
 get_header(); ?>

	<!-- section -->
	<section class="pt-50 pb-50 aa_loginForm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3 image_center">

				<h2 class="mt-0 mb-15 section-title">Forget Password</h2>
				<p>Please enter your username or email address
				<br>You will receive a link to create new password via email</p>
				<?php
        global $wpdb;
        
        $error = '';
        $success = '';
        
        // check if we're in reset form
        if( isset( $_POST['action'] ) && 'reset' == $_POST['action'] ) 
        {
            $email = trim($_POST['user_login']);
            
            if( empty( $email ) ) {
                $error = 'Enter a username or e-mail address..';
            } else if( ! is_email( $email )) {
                $error = 'Invalid username or e-mail address.';
            } else if( ! email_exists( $email ) ) {
                $error = 'There is no user registered with that email address.';
            } else {
                
                $random_password = wp_generate_password( 12, false );
                $user = get_user_by( 'email', $email );
                
                $update_user = wp_update_user( array (
                        'ID' => $user->ID, 
                        'user_pass' => $random_password
                    )
                );
                
                // if  update user return true then lets send user an email containing the new password
                if( $update_user ) {
                    $to = $email;
                    $subject = 'Your new password';
                    $sender = get_option('name');
                    
                    $message = 'Your new password is: '.$random_password;
                    
                    $headers[] = 'MIME-Version: 1.0' . "\r\n";
                    $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers[] = "X-Mailer: PHP \r\n";
                    $headers[] = 'From: '.$sender.' < '.$email.'>' . "\r\n";
                    
                    $mail = wp_mail( $to, $subject, $message, $headers );
                    if( $mail )
                        $success = 'Check your email address for you new password.';
                        
                } else {
                    $error = 'Oops something went wrong updaing your account.';
                }
                
            }
            
            if( ! empty( $error ) )
                echo '<div class="message"><p class="error"><strong>ERROR:</strong> '. $error .'</p></div>';
            
            if( ! empty( $success ) )
                echo '<div class="error_login"><p class="success">'. $success .'</p></div>';
        }
    ?>

 <div class="contact-form1">
					<div class="contact-form11 register_form">
    
    <!--    <form method="post">
            
                <p>
                    <?php $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : ''; ?>
                    <input type="text" placeholder="Enter Username or Email" name="user_login" id="user_login1" value="<?php echo $user_login; ?>" /></p>
                <p>
                    <input type="hidden" name="action" value="reset" />
                    <input type="submit" value="Reset my password" class="button1" id="submit" />
                </p>
            
        </form> -->
        <?php echo do_shortcode('[reset_password]'); ?>
	</div>
</div>	
		
</div>
	</section>
	<?php get_footer(); ?>