<?php
/**
 * Template Name: common centers
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();

?>
<section class="pt-50 pb-50 section_map">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-12 text-center gotham-rounded-book">
					<div class="row">
						<div class="col-md-3 offset-md-3 view_up">
						View upcoming Centres in</li>
				 </div>
				 <div class="col-md-4">
				<select class="form-control up_events" id="dynamic_select">
					<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;	
				foreach($categories as $key => $value){	 ?>
					<option value="<?php bloginfo('url') ?>/common-centers/#<?php echo $value->slug; ?>"><?php echo $value->name; ?> </option>
				<?php } ?>
				</select>
			</div>
		</div>
			</div>
			<!-- <div class="col-sm-6">
				<div class="embed-responsive embed-responsive-16by9 border-map">
					<?php $term1 = array('posts_per_page' => '-1','paged' => 1, 'post_type' => 'centres', 'centres_category' =>$term->slug  ); 
					$term1 = get_queried_object($term1);
				echo $test_conent = get_field('map_iframe', $term); ?>

				</div>
			</div> -->
		</div>

	</div>
</section>


<section class="section_post">
	<div class="container-fluid">
	
		<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;	
				foreach($categories as $key => $value){	

					$slugcen = $value->slug; ?>
					

			


<?php 
						global $post; $i = 1;
						$args = array( 'posts_per_page' => '-1', 'post_type' => 'centres','order' => 'ASC','centres_category' =>$slugcen,'meta_query' => array( array( 'key'=> 'upcoming_events','compare'	=> '=',
	        'value'		=>'1',
	    )
    ) ); 


						$myposts = get_posts( $args );
						$count = count($myposts);
if($count != 0 ){						
?>

    			<div class="row">
			<div id= "<?php echo $value->slug; ?>" class="col-sm-12 text-center">
				<h2 class="section-title">Upcoming Centres in <?php echo $value->name; ?></h2>
			</div>
		</div>


						<?php
					} ?>
<div class="row <?php if($count != 0 ){						
?>post_pad <?php } ?>my-posts">
					<?php
						foreach( $myposts as $post ) { setup_postdata($post); 
							//$jj = get_field('upcoming_events');
							/*if($jj == '1' ){*/
					?>

	
				<div class="col-6 col-sm-3">
					<?php if($value->name == 'Bengaluru'): ?>
					 <a href="<?php the_permalink(); ?>">
					<?php endif; ?>
						<div class="bordernew">
							<?php the_post_thumbnail( 'centre', array( 'class' => 'img-responsive center-block' ) );?>
							<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
						</div>
					<?php if($value->name == 'Bengaluru'): ?></a><?php endif; ?>
				</div>
			
		
				<?php 
				 } ?>

</div>
<?php if($count != 0 ){ ?><div class="row">
					<div class="col-12"><a href="<?php echo get_category_link($value->term_id); ?>"><p style="text-align: center;" class="gcolo gotham-rounded-medium free-parent-helpline pt-15 fs-17" id="showmenu1">View all Centres in <?php echo $value->name; ?> &gt;</p></a> </div>
				</div> <hr> <?php } ?>
<?php } ?>
</div>
		<h3 class="gotham-rounded-medium">
			<ul class="list-inline">
				<li class="first_li">Other cities:</li>
				<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
				$categories = get_terms( 'centres_category', $args);
				$i = 0;
				foreach($categories as $key => $value){	?>
					<li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name; ?></a></li>
					<li>|</li>
				<?php } ?>
			</ul>
		</h3>
	</div>
</section>

<div id='ry-review_embed'><script src=https://klayschools.referralyogi.com/review_embed/klayschools data-page='1' async defer></script></div>
<script id='load-ry-review' src=https://klayschools.referralyogi.com/review_notification data-page='1' async defer></script></script>
<?php
get_footer();
