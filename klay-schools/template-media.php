<?php
/**
 * Template Name: Media
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();

?>

<section class="pt-50 pb-50 section_medi">
	<div class="container-fluid">
		<div class="resultneww">
			<div class="row">
				<?php 
							global $post; $i = 1;
							$args = array( 'posts_per_page' => '3', 'post_type' => 'medias' );
							$args1 = array( 'posts_per_page' => '-1','post_type' => 'medias' );
							$myposts = get_posts( $args );
							$myposts1 = get_posts( $args1 );
							$count = count($myposts1);
							foreach( $myposts as $post ) { setup_postdata($post); 
						?>
					<div class="col-12 col-md-4 image_center media_image">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('media-slider'); ?></a>
						<div class="gotham-rounded-medium media_content"><p><a href="<?php the_permalink(); ?>" style="color: #fff"><?php the_title(); ?></a></p></div>

						<h2 class="gotham-rounded-book"><?php //echo wp_trim_words( get_the_content(), 8, '...' ); 
						the_field('newspaper_name');?></h2>
					</div>
				<?php } ?>

			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="image_center padd_btn">
						<span class="load_more_event load-btn" att="<?php the_title(); ?>">Load More News</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- <section class="section_event">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Upcoming Events</h2>
			</div>
		</div>
		<div class="swipeslider_event">
			<div class="swiper-container swiper-container-event1">
			    <div class="swiper-wrapper">
					<?php 
					$today = date('Ymd');
						global $post; $i = 1;
										$args = array( 'posts_per_page' => '-1', 'post_type' => 'events','order' => 'ASC','meta_query' => array(
	     array(
	        'key'		=> 'date',
	        'compare'	=> '>=',
	        'value'		=> $today,
	    )
    ) );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) { setup_postdata($post); 
					?>
						<div class="swiper-slide image_center">
							<h2 class="gotham-rounded-book"><?php the_title(); ?></h2>
							<?php the_post_thumbnail('event-slider'); ?>
						</div>
			      	<?php } ?>
				
				</div>
					<div class="single-slide-prev"></div>
			<div class="single-slide-next"></div>
			</div>
		</div>
	</div>

</section> -->

<?php while(have_posts()): the_post();
endwhile;
get_footer();
