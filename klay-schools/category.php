<?php 

get_header();?>

<script type="text/javascript">
piAId = '564842';
piCId = '87225';
piHostname = 'pi.pardot.com';

(function() {
  function async_load(){
    var s = document.createElement('script'); s.type = 'text/javascript';
    s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
    var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
  }
  if(window.attachEvent) { window.attachEvent('onload', async_load); }
  else { window.addEventListener('load', async_load, false); }
})();
</script>

<section class="pt-50 pb-50">

  <div class="container-fluid">

<!--     <div class="row">

      <div class="col-12 ">

        <h2 class="section-title single_cat_title"><?php printf( __( '%s', 'mala' ), single_cat_title( '', false ) ); ?></h2>

      </div>

    </div> -->

        <div class="row">     



      <?php 

    
 /*     $category = get_cat_ID('blog');

      $wp_query = new WP_Query(array(
          'post_type' => 'post',
          'posts_per_page' => 1,
          'cat' => $category,
          'ignore_sticky_posts' => 1,
          'post__in'  => get_option( 'sticky_posts' )
      ));
*/

      while ( have_posts() ) : the_post(); ?>

      <div class="col-12 col-md-4 mb-30 align-self-start">

        <div class="image_re">

        <?php if(!empty(get_the_post_thumbnail())){ ?>

          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large',array('class'=>'w-100 h-auto')); ?></a>          

        <?php }else{ ?>

          <a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/default-thumbnail.jpg" alt="" class="w-100 h-auto" /></a>

        <?php } ?>

        <h4 class="blue-bg mt-0 mb-0 pt-15 pb-15 gotham-rounded-medium fs-18 pos-abs-blog-title"><a href="<?php the_permalink(); ?>" class="white-color"><?php the_title() ?></a></h4>

      </div>

        <p class="gotham-rounded-book excerpt_hi"><?php excerpt('30'); ?></p>

        <p class="author gotham-rounded-book"><?php the_field('blogger_label'); ?> <strong><span class="orange-color"><?php the_field('parent_blogger'); ?></span></strong> <span class="star"></span><br>

        <?php the_date('d/m/y'); ?></p>

        <a class="gotham-rounded-medium read_post" href="<?php the_permalink(); ?>">Read More ></a>

        

      </div>

      <?php

      wp_reset_query();
 
  endwhile; ?> 



    </div>
        <div class="row">
     <div class="col-12">
      <?php wp_pagenavi(); ?>
      </div>
    </div>  

  </div>

</section>

                                  

                                       



<?php get_footer();?>