<?php
/**
 * Template Name: Famjam
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();

while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KL496DT');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KL496DT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<style type="text/css">
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
</style>

<section class="header">
	<div class="fullcols">
		<img src="<?php echo get_template_directory_uri();?>/images/famjam-lp-banner.jpg" alt="LP" width="1520px" height="auto" />
	</div>
	<div class="fullcols anniversary_form" id="anniversary_form"> 
		<div class="container">
			<iframe src="http://go.pardot.com/l/563842/2019-07-18/5tc3x?Source_URL=<?php the_permalink(); ?>" width="100%" height="260" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
		</div>
	</div>
</section>
<section class="second_part">
	<div class="fullcols">
		<div class="container">
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center cities">
				<h2>Bangalore | Hyderabad | NCR</h2>
				<h1>27th July 2019 | 10.30am to 1.00pm</h1>
				<h2>Harlur</h2>
				<h1>3rd August 2019 | 10.30am to 1.00pm</h1>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 weekend">
				<p>Join us for an unforgettable weekend where you can dance along to<br> your favourite tunes,experiment with clay and make new memories<br> with your little ones.</p>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 bg_bck text-center">
				<div class="row">
					<div class="col-12 col-md-8 col-sm-8 col-lg-8">
						<h3><strong class="book">Book your tickets now!</strong><br>Chargeable at Bangalore @ Rs 200 </h3>
					</div>
					<div class="col-12 col-md-2 col-sm-2 col-lg-2 p-0">
						<img src="<?php echo get_template_directory_uri();?>/images/text-peppa.png" alt="text-peppa.png" width="200" height="77" style="margin: 3% 0;"/>
					</div>
					<div class="col-12 col-md-2 col-sm-2 col-lg-2 p-0">
						<p class="giveway">giveaways<br>for all!</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 pt-5">
				<div class="row">
					<div class="col-12 col-md-4 col-sm-4 col-lg-4 text-center art_img py-5">
						<img src="<?php echo get_template_directory_uri();?>/images/corner.png" alt="corner" width="139" height="200" />
						
					</div>
					<div class="col-12 col-md-4 col-sm-4 col-lg-4 text-center music_img py-5">
						<img src="<?php echo get_template_directory_uri();?>/images/musicmovement.png" alt="music" width="237" height="200" />
						
					</div>
					<div class="col-12 col-md-4 col-sm-4 col-lg-4 text-center art_img py-5">
						<img src="<?php echo get_template_directory_uri();?>/images/potry.png" alt="potry.png" width="195" height="200" />
						
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 pt-1">
				<div class="row">
					<div class="col-12 col-md-4 col-sm-4 col-lg-4 text-center music_img  py-5">
						<img src="<?php echo get_template_directory_uri();?>/images/fireless.png" alt="fireless" width="281" height="200" />
						
					</div>
					<div class="col-12 col-md-4 col-sm-4 col-lg-4 text-center art_img py-5">
						<img src="<?php echo get_template_directory_uri();?>/images/mom&dad.png" alt="mom&dad" width="329" height="200" />

					</div>
					<div class="col-12 col-md-4 col-sm-4 col-lg-4 text-center music_img  py-5">
						<img src="<?php echo get_template_directory_uri();?>/images/photoboot.png" alt="photoboot" width="181" height="200" />
						
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center happening py-3">
				<h1>Happening at a centre near you!</h1>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 py-4">
				<div class="row">
					<div class="col-12 col-sm-4 col-lg-4 col-md-4 city_name">
						<h2>Bangalore</h2>
						<p><strong>KLAY Ecity 2,</strong> #5003/25/26/105,<br> Bettadasanapura Main Rd,<br> Doddathogur Village, Neeladri Road,<br> Near SNN Raj Neeladri Apartment,<br>ElectronicCity,Bengaluru, Karnataka-560100</p>
						<p><strong>KLAY Harlur,</strong> Ambalipura,66, Harlur Main Rd, Amblipura, PWD Quarters, 1st Sector, Harlur, Bengaluru,Karnataka-560068</p>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-md-4 city_name">
						<h2>NCR</h2>
						<p><strong>KLAY Sector 51 ,</strong>M Block, Mayfield Garden, Adjacent to Artemis Hospital, Gurugram, Haryana 122001</p>
						<p><strong>KLAY Sector 22,</strong> Old Delhi – Gurgaon Road, Near Krishna Chowk, Sector 19, Dundahera, Gurugram</p>
						<p><strong>KLAY DLF Phase IV</strong> Nursery Site No. 4106,DLF Phase - 4, Ashok Marg, Near Galleria Market, Gurugram, Haryana</p>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-md-4 city_name">
						<h2>Hyderabad</h2>
						<p><strong>KLAY Madhapur</strong> Building opposite to Westin Hotel, Raheja Mindspace , Madhapur, Hyderabad – 500081</p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="tollfree">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center">
			<h3>Talk to us on <a href="tel:+7676708888">7676708888</a> to know more!</h3>
		</div>
	</div>
</section>
<section class="celebarting">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center">
			<h1>KLAY – CELEBRATING FAMILIES</h1>
			<p>KLAY, India’s largest and most awarded parenting network celebrates families – families where both parents are equal partners in the journey of parenthood and families that stand by each other through all their ups and downs.</p>
			<p>Let us celebrate our families every single day of the year – not just those we are born into but also those we make along the way.</p>
			<p>Let KLAY be your parenting partner as we give your little ones the best start!</p>
		</div>
	</div>
</section>
<section class="last_part">
<div class="last-icons">
	<ul>
		<li><img src="<?php echo get_template_directory_uri();?>/images/happy-parents.png" width="71" height="69" alt="Parents"></li>
		<li><p class="GothamRounded-Bold fs-33" style="color:#423a34"><strong>17000+</strong><br><span class="GothamRoundedBook fs-20" style="color:#423a34">Happy Parents</span></p></li>
	</ul>
	<ul>
		<li>
		<img src="<?php echo get_template_directory_uri();?>/images/happy-children.png" width="71" height="69" alt="Children"></li>
		<li><p class="GothamRounded-Bold fs-33" style="color:#423a34"><strong>8500+</strong><br><span class="GothamRoundedBook fs-20" style="color:#423a34">Happy Children</span></p></li>
	</ul>
	<ul>
		<li>
		<img src="<?php echo get_template_directory_uri();?>/images/familycenters.png" width="71" height="69" alt="Centres"></li><li><p class="GothamRounded-Bold fs-33" style="color:#423a34"><strong>150+</strong><br><span class="GothamRoundedBook fs-20" style="color:#423a34">Centres</span></p></li>
	</ul>
	<ul>
		<li>
		<img src="<?php echo get_template_directory_uri();?>/images/familycities.png" width="71" height="69" alt="Cities"></li><li><p class="GothamRounded-Bold fs-33" style="color:#423a34;"><strong>7</strong><br><span class="GothamRoundedBook fs-20" style="color:#423a34">Cities</span></p></li>
	</ul>
</div>
</section>

<section class="yellow-bg eventslp_sticky d-block d-md-none fs-18 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">Call</a></div>
			<div class="col"> <a href="#anniversary_form">Enquire Now</a></div>
		</div>
	</div>
</section>


<style type="text/css">
.cities h2{font-weight: bold;font-family: 'GothamRoundedBook';padding: 1% 0;}
.cities h1{font-weight: bold;font-family: 'GothamRoundedBook';color:#1cb6c3;padding: 0 0 2% 0;}
.weekend p{background:#f2f1ea;font-family: 'GothamRoundedBook';padding: 1% 1%;}
.bg_bck{background:#1cb6c3;}
.anniversary_form {  background : url("<?php echo get_template_directory_uri();?>/images/anniversary-form-bg.jpg") repeat }
.bg_bck h3 {color: white;padding: 11px 0 0 0;font-family: 'GothamRoundedBook';}
strong.book{font-family:GothamRoundedBold_0;}
p.giveway{color: white;margin: 3% 0;font-family: GothamRoundedBold_0;}
.art_img {display: inline-block;background: #ee7c20;border: 2px solid white;}
.music_img {display: inline-block;background: #f9c000;border: 2px solid white;}
.last-icons{float: left;width: 100%;padding: 0px 0px;margin: 0 auto;text-align: center;}
.last-icons ul {display: inline-block;padding: 20px;}
.last-icons ul li {display: inline-block;padding: 10px;}
.last-icons img {vertical-align: super !important;}
.last-icons ul li p {text-align: left;}
.last-icons p {float: left;margin: 10px 0 0;}
.last-icons p span {float: left;margin: -10px 0 0;}
.celebarting {background: #ececec;padding: 3% 0;}
.celebarting p{font-family:'GothamRoundedBook';}
.celebarting h1{font-family:AsparagusSprouts;}
.tollfree h3{font-family:GothamRoundedBold_0;display: inline-block;background: white;border: 2px solid #bdd138;border-radius: 15px;padding: 1% 3%;margin: 0 0 13px 0;}
.tollfree{margin: 0px 0 -45px 0;z-index: 99999;}
.happening h1{font-family:AsparagusSprouts;}
.city_name h2{font-family:GothamRoundedMedium;text-transform: uppercase;padding: 2% 0;}
.second_part {padding: 2% 0;}
.weekend {background: #f2f1ea;text-align: center;}

@media only screen and (min-width: 769px)
{
.cities h2{font-size: 35px;}
.cities h1{font-size: 36px;}
.weekend p{font-size:26px;}
.bg_bck h3 {font-size: 40px;}
p.giveway {font-size: 26px;}
.celebarting p {font-size:19px;}
.celebarting h1 { font-size:43px; }
.tollfree h3 { font-size:35px; }
.happening h1  { font-size:55px;}
.city_name h2	{ font-size:22px; }
}

@media only screen and (max-width: 767px)
{
.cities p{padding: 5% 1%;}
.celebarting{padding: 11% 0;} 
.art_img{margin: 5% 0;}
.celebarting h1{padding: 5% 0 0 0;}
.last-icons ul { float : left; width : 100%; padding :0 0 2em 0; margin : 0 auto; }
.last-icons ul li {  float: left;   padding: 10px 6%; }

.cities h2{font-size: 25px;}
.cities h1{font-size: 26px;}
.weekend p{font-size:16px;}
.bg_bck h3 {font-size: 30px;}
p.giveway {font-size: 16px;}
.celebarting p {font-size:13px;}
.celebarting h1 { font-size:33px; }
.tollfree h3 { font-size:25px; }
.happening h1  { font-size:35px;}
.city_name h2	{ font-size:15px; }

.anniversary_form iframe  { min-height : 400px; }
}
.stricky_menu {  bottom: 0;  position: fixed;  width: 100%;  z-index: 9999999; }
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a {  color: #333; display: inline-block; padding: 8px 0; }
.stricky_menu .col a {  width: 100%; }


</style>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<?php
endwhile;
get_footer();
