<?php
/**
 * Template Name: IDPE
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TVC77DH');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TVC77DH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<section class="idpe_header fullcols">
	<div class="container-fluid p-0 m-0">
		<div class="row p-0 m-0">
			<div class="show_desktop d-none d-sm-none d-md-block d-lg-block">
				<img src="<?php echo get_template_directory_uri();?>/images/IECED-Banner.jpg" class="img-fluid" alt="Banner" width="1366" height="650" />
			</div>
			<div class="show_mobile d-block d-sm-block d-md-none d-lg-none">
				<img src="<?php echo get_template_directory_uri();?>/images/idpe_mobile_banner.jpg" class="img-fluid" alt="Banner" width="480" height="768" />
			</div>			
		</div>		
	</div>
</section>

<section class="idpe_form fullcols"  id="register">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="form_holder py-5">
				<h3 class="float-left col-12 co-sm-12 col-md-12 text-center fs-30 fs-xs-20 gotham-rounded-medium">Get IDPE Certified!</h3>
				<p class="float-left col-12 co-sm-12 col-md-12 text-center fs-17 fs-xs-15 gotham-rounded-book">Get in touch with us for further details regarding the courses</p>
				<iframe src="http://go.pardot.com/l/563842/2019-06-26/5ph68?Source_URL=<?php the_permalink(); ?>" width="100%" height="200" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			</div>			
		</div>		
	</div>
</section>


<section class="idpe_prebook fullcols">
	<div class="container-fluid p-0 m-0">
		<div class="row p-0 m-0">
			<img src="<?php echo get_template_directory_uri();?>/images/IDPE-book_banner.jpg" class="img-fluid show_desktop d-none d-sm-none d-md-block d-lg-block" alt="Banner" width="1366" height="399" />	
			<img src="<?php echo get_template_directory_uri();?>/images/m_idpe_banner.jpg" class="img-fluid show_mobile d-block d-sm-block d-md-none d-lg-none" alt="Banner" width="480" height="459" />			
		</div>	
		<div class="row p-0 m-0 idpe_heads">
			<h3 class="float-left col-12 co-sm-12 text-center fs-30 fs-xs-20 gotham-rounded-medium">IDPE - International Diploma In Preschool Education</h3>
		</div>		
	</div>
</section>

<section class="idpe_intro fullcols">
	<div class="container">		
		<div class="row p-0 m-0">
			<div class="idpe_paras col-12 col-sm-12 col-md-12 mt-5">
				<h5 class="float-left col-12 co-sm-12 fs-19 fs-xs-17 pb-3 gotham-rounded-medium px-0 mx-0">Introduction<h5>

				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">The International Diploma in Preschool Education (IDPE) is conceived with the thought of providing quality understanding of Early Childhood Education(ECE) in India. This diploma not only aims at giving hands-on exposure to participants but is also designed to develop thought leadership in the field of ECE in India.</p>

				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">The diploma is launched by Institute of Early Childhood Education and Development (IECED) which comes under the umbrella of <strong>Founding Years Learning Solutions</strong> India Private Limited. This is the proud parent company of India’s Largest and Most Trusted Pre-School and Day Care Centres –<strong>KLAY prep schools and day care.</strong> With Launch of IDPE, the insatiate aims to provide qualified and trained staff with professional and technical understanding of early childhood education.  The course material and learning through theory and practicum will help individuals to develop their talents and abilities to function in early childhood settings and eventually will lead to their career progression as well.</p>

				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">The diploma course draws on research and insights from educators around the world who are implementing quality teaching and appropriate preschool practice, providing students with a solid understanding of different functional areas of preschool teaching.</p> 

				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">This course is suitable for individuals and teachers who wish to work in preschools and progress to leadership roles.</p>
			</div>
			
			<div class="idpe_paras col-12 col-sm-12 mt-4 col-md-12">
				<h5 class="float-left col-12 co-sm-12 fs-19 fs-xs-17 pb-3 gotham-rounded-medium px-0 mx-0">Course Overview<h5>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">The course is designed to be a <strong>25 weeks (250 hour)</strong> professional development program for preschool teachers. This <strong>six-month program</strong> has been divided into coursework and internship and is designed to equip students with fundamental knowledge of skills required to teach in Preschool.</p>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">Course participants are expected to devote <strong>10 hours</strong> every week where 6 hours require face to face interaction and 4 hours are for practical/home assignments. Each session will include classroom theory sessions, group activities, and reflection exercises which will reinforce the content taught during the session.</p>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">At the end of the course, students need to do a hands-on internship program of 4 weeks in KLAY centres across India. Course participants who successfully complete the course will be awarded Certificate of completion by IECED.</p>				
			</div>

			<div class="idpe_paras col-12 col-sm-12 mt-4 mb-4 col-md-12">
				<h5 class="float-left col-12 co-sm-12 fs-19 fs-xs-17 pb-3 gotham-rounded-medium px-0 mx-0">Admission Criteria and Procedure<h5>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">Candidates for admission should possess 12 years of formal education and should be fluent in English. Completed admission forms with certified true copies of certificates, transcripts and other relevant documents should be submitted before the closing date of each batch.</p>			
			</div>
			
		</div>		
	</div>
</section>

<section class="idpe_prebook fullcols">
	<div class="container-fluid p-0 m-0">
		<div class="row p-0 m-0 idpe_heads">
			<h3 class="float-left col-12 co-sm-12 text-center fs-30 fs-xs-20 gotham-rounded-medium">IECED - Institute of Early Childhood Education and Development</h3>
		</div>		
	</div>
</section>

<section class="idpe_whoweare fullcols">
	<div class="container">		
		<div class="row px-0 mx-0 py-5">
			<div class="col-12 col-sm-12 col-md-6 float-left">
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-medium px-0 mx-0">Who We Are</p>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">IECED is an institute that works with education providers, employers and government to help Early childhood Educators grow in their understanding of child development and enhancing skill sets to function in their best capacity in classrooms.</p>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0"><strong>Everything under this institute contributes to achieving our purpose:</strong>  helping educators and managers from the field of early childhood education to develop their skills for professional growth and understanding. This skill development will ultimately lead to providing quality care and services to every single child who walks in for professional care.</p>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">We not only provide professional and technical skills to professionals in early childhood sector but also carry forward all the mandatory trainings for staff like POCSO sensitization and CPR- first aid training. Together, these training programs support to develop confident and work-ready preschool educators, more capable and inspiring managers, more productive workforce, and, ultimately, ensures physical and emotional safety of staff and children.</p>
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">The institute has developed course materials for conducting on- job and entry level trainings for staff. Both, short-term (8 hours) and long-term (one year) courses are developed to cater to the need of qualified and skilled staff in Early Childhood Education. Various Train the Trainer programs are conducted at the corporate level to take the trainings on ground with teachers and caregivers.</p>
			</div>	
			<div class="col-12 col-sm-12 col-md-6 float-left">
				<div class="d-none d-sm-none d-md-block d-lg-block">
					<img src="<?php echo get_template_directory_uri();?>/images/who-we-are-full.jpg" class="img-fluid" alt="Who We Are" width="519" height="586" />	
				</div>
				<div class="d-block d-sm-block d-md-none d-lg-none text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/who-we-are-mbile-1.jpg" class="img-fluid" alt="Who We Are" width="519" height="300" />	
					<img src="<?php echo get_template_directory_uri();?>/images/who-we-are-mobile-2.jpg" class="img-fluid" alt="Who We Are" width="259" height="284" />	
					<img src="<?php echo get_template_directory_uri();?>/images/who-we-are-right-mobile-3.jpg" class="img-fluid" alt="Who We Are" width="262" height="284" />	
				</div>					
			</div>			
		</div>		
	</div>
</section>


<section class="all_cncs fullcols py-5">
	<div class="container">		
		<div class="row p-0 m-0">
			<div class="float-left col-12 col-sm-12">
				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">The institute comes under the umbrella of Founding Years Learning Solutions India Private Limited which is the proud parent company of India’s Largest and Most Trusted Pre-School and Day Care Centres – KLAY prep schools and day care.</p>

				<p class="float-left col-12 co-sm-12 fs-16 fs-xs-15 gotham-rounded-book px-0 mx-0">With Launch of the Institute of Training and Development for Early Childhood Education, we aim to provide qualified and trained staff with professional and technical understanding of early childhood education.</p>
			</div>		
		</div>		
	</div>
</section>

<section class="yellow-bg eventslp_sticky d-block d-md-none fs-16 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="#register">Enquire Now</a></div>
		</div>
	</div>
</section>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<style type="text/css">
.page-id-64657 #masthead, .page-id-64657 .banner, .page-id-64657 .title-holder, .page-id-64657 .and-theres-more-at-klay, .page-id-64657 #colophon, .page-id-64657 .copyrights, .page-id-64657 .blue-bg, .page-id-64657 .important-menu-items  { display : none !important; }
.page-id-64657 .fullcols { float : left; width : 100%; position : relative; paddding :0; margin : 0 auto;}
.page-id-64657 .idpe_heads { background : #379da4; }
.page-id-64657 .idpe_heads h3 {  color : #fff; padding : 1em 0; }
.idpe_paras h5 { text-decoration : underline; color : #000; }
.idpe_paras p { color : #000; text-align : justify; }
.page-id-64657 .all_locations { background : #fff;   }
.page-id-64657 .all_locations ul {   }
.page-id-64657 .all_locations ul li { background : url("<?php echo get_template_directory_uri();?>/images/location-icon.png") no-repeat center center; list-style-type : none; padding : 3em 1.5em 0 1.5em; margin : 0 auto; text-align : center; display : inline-block;  }
.page-id-64657 .all_locations ul li a { color : #0099cc; }
.page-id-64657 .all_locations h5 a { color : #ff6969 !important; }
.page-id-64657 .idpe_whoweare { background : #ffeb64; }
.page-id-64657 .all_cncs { border-bottom : 1px solid #000; }
.page-id-64657 .social_icons { background : #ddd; }
.page-id-64657 .social_icons ul {  }
.page-id-64657 .social_icons ul li { list-style-type : none; margin : 0 auto; text-align : center; display : inline-block; }
.page-id-64657 .idpe_form { background : #193544; }
.page-id-64657 .idpe_form h3, .page-id-64657 .idpe_form p { color : #fff; }
.page-id-64657 .form_holder {  float: left;   width: 100%; }
.page-id-64657 .form_holder ul {  }
.page-id-64657 .form_holder ul li {   clear: none !important; }
.page-id-64657 .form_holder ul li label { display : none !important;  }

.page-id-64657 #gform_submit_button_6 { background : #f9da31 !important; border-radius : 10px; float : right; margin : 0 16px 0 0; font-family: 'GothamRoundedMedium'; font-size : 18px; color : #000 !important; width: 22%; }
#gform_confirmation_wrapper_6 {   color: white;  float: left;  width: 100%;  text-align: center; }

@media only screen and (min-width: 767px){
.page-id-64657 .social_icons ul li { padding : 0em 1em 0; }	
.page-id-64657 .form_holder .gform_footer  {margin :  -3.7em 0 0 0 !important; }
}

@media only screen and (max-width: 767px){
.page-id-64657 .social_icons ul li {  padding: 0 10px 0 0; }
.page-id-64657 #gform_submit_button_6 { float : left !important; margin : 0 16px 0 0; font-family: 'GothamRoundedMedium'; font-size : 18px; color : #000 !important; width : 100% !important; }	
.page-id-64657 .form_holder ul li {  clear: both !important;  padding: 0 !important; }
.page-id-64657 .gform_wrapper input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .page-id-64657 .gform_wrapper select {  font-size: 14px !important; }
.page-id-64657 .all_locations ul li { width : 100%; }
.form_holder iframe { min-height : 320px; }
}
#gform_6 .validation_error { display : none; }
.eventslp_sticky .col { }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }

</style>
<?php
endwhile;
get_footer();
