<?php
/**
 * Template Name: IECED
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
<style type="text/css">
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TVC77DH');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TVC77DH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<section class="banner_holder fullcols d-none d-sm-none d-md-block">
	<img src="<?php echo get_template_directory_uri();?>/images/ieced-lp-v1.jpg" class="img-fluid" alt="banner" width="1366" height="648" />
	<div class="certified_logo">
		<span><img src="<?php echo get_template_directory_uri();?>/images/certified.png" class="img-fluid" alt="banner" width="360" height="515" /></span>
	</div>
</section>
<section class="banner_holder fullcols d-block d-sm-block d-md-none">
	<img src="<?php echo get_template_directory_uri();?>/images/iecedepmobile-banner.png" class="img-fluid" alt="banner" width="480" height="480" />
	<div class="container">
		<h1>Become an <span>Internationally Certified </br>Preschool Teacher</span> with KLAY!</h1>
		<h2>KLAY in association with the Institute of Early Childhood</br>Education and Development (IECED), brings to you the</br><span>International Diploma in Preschool Education (KLAY Preschool Teacher Training Program).</span></h2>
	</div>
</section>

<section class="form_container fullcols">   
	<div class="container">
		<div class="form_holder" id="enq_form"><iframe src="http://go.pardot.com/l/563842/2019-06-26/5ph68?Source_URL=<?php the_permalink(); ?>" width="100%" height="300" type="text/html" frameborder="0" allowTransparency="true" style="border: 0px none;"></iframe></div>
	</div>
</section>

<section class="placements fullcols py-5">
	<div class="container">
		<h1 class="col-12 col-sm-12 float-left AsparagusSprouts p-0 m-0 text-center">100% Placement At KLAY,</h1>
		<h2 class="col-12 col-sm-12 float-left AsparagusSprouts p-0 m-0 text-center">Upon Successful Completion Of The Diploma!</h2>
		<p class="col-12 col-sm-12 float-left GothamRoundedBook px-0 pt-5"><strong>KLAY Preschool Teacher Training Program</strong> provides quality teacher training of Early Childhood education in India. High quality early age learning has been shown to have long-term positive outcomes in the future of students leading to more healthy, confident and successful individuals. KLAY Preschool Teacher Training Program draws on the vast experience of KLAY as a leader in early childhood education. Accreditation with KLAY's Preschool Teacher Training Program means teachers are skilled and well equipped to provide a high level of quality care and education for our young ones. Families can be assured that their children are receiving high quality, research based education that sets the foundation for a successful life.</p>
	</div>
</section>

<section class="whyidpe fullcols">
	<div class="container">
		<div class="fullcols d-none d-sm-none d-md-block"><img src="<?php echo get_template_directory_uri();?>/images/whyidpe-banner.jpg" class="img-fluid" alt="banner" width="1167" height="553" /></div>
		<div class="fullcols d-block d-sm-block d-md-none"><img src="<?php echo get_template_directory_uri();?>/images/whyidpe-mobile-1.png" class="img-fluid" alt="banner" width="480" height="450" /></div>
		<div class="whyidpe_connect">
			<div class="container">
				<div class="col-12 col-sm-12 col-md-6 float-left"></div>
				<div class="col-12 col-sm-12 col-md-6 float-left float-sm-left float-md-right whyidpe_content">
					<div class="yidpe_lists">
						<div class="fullcols d-none d-lg-block d-md-none">
						<!--<img src="<?php echo get_template_directory_uri();?>/images/whyidpeicon.jpg" class="img-fluid" alt="banner" width="270" height="45" />!-->
						<h1 class="why_text">WHY<b class="blue-text">  KLAY Preschool Teacher Training Program</b>?</h1>
						</div>
						<ul>
							<li><span class="GothamRoundedBook">100% Placement with KLAY,</br>upon successful completion of the diploma.</span></li>
							<li><span class="GothamRoundedBook">International research based curriculum</span></li>
							<li><span class="GothamRoundedBook">Guaranteed 4-week Internship with KLAY</span></li>
							<li><span class="GothamRoundedBook">Designed by leading child psychologists</span></li>
						</ul>
						<h3><a href="tel:+917349737682">73497 37682</a></h3>
						<h4><a href="mailto:training.academy@klayschools.com">training.academy@klayschools.com</a></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="fullcols d-block d-sm-block d-md-none">
		<img src="<?php echo get_template_directory_uri();?>/images/whyidpe-mobile-2.png" class="img-fluid" alt="banner" width="480" height="24" />
		
		</div>
	</div>
</section>

<section class="admissions fullcols py-5">		
	<div class="container">		
		<h1 class="col-12 col-sm-12 float-left p-0 m-0 GothamRoundedBook">Admission FAQ’s</h1>
		<p class="col-12 col-sm-12 float-left px-0 pt-2 pb-0 m-0 first">2000+ Early Childhood Professionals spread across 100+ preschool and childcare centres in India are united in their mission to nurture every child’s gifts! Do you want to join them?</p>
		<p class="col-12 col-sm-12 float-left px-0 pt-4 pb-3 m-0 second">Why is KLAY Preschool Teacher Training Program the ideal Nursery teacher training course for you?  <span><a href="#enq_form">Enquire Now!</a></span></p>
		
		<div class="admissions_acco">
			<div class="bs-example">
				<div class="accordion" id="accordionExample">
				<div class="card">
					<div class="card-header" id="headingOne">
						<h2>
							<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne">  What is the Eligibility Criteria? <i class="fa fa-plus"></i></button>									
						</h2>
					</div>
					<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							<ul>
								<li>12 + years of formal education</li>
								<li>Fluent in English</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingTwo">
						<h2>
							<button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo">What is the Program Duration? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
						<div class="card-body">
							<ul>
								<li>6 months (25 weeks inclusive of 4 weeks mandatory internship with Klay)</li>
								<li>Participants need to devote approximately 10 hours per week (6 hours for classes + 4 hours for homework/ assignments)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingThree">
						<h2>
							<button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"> What is the Program Curriculum? <i class="fa fa-plus"></i></button>                     
						</h2>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
						<div class="card-body">
							<table>
								<tr><th>Weeks</th><th>Topic</th></tr>
								<tr><td>Week 1</td><td>Basics of Preschool Education</td></tr>
								<tr><td>Weeks 2 & 3</td><td>Brain Development, Milestones and Development</td></tr>
								<tr><td>Weeks 4 & 5</td><td>Curriculum Planning & Design</td></tr>
								<tr><td>Weeks 6 & 7</td><td>Curriculum Implementation & Evaluation</td></tr>
								<tr><td>Week 8</td><td>Best Teaching Strategies</td></tr>
								<tr><td>Weeks 9 & 10</td><td>Art & Music in Classroom</td></tr>								
								<tr><td>Weeks 11 & 12</td><td>Safety & Security</td></tr>
								<tr><td>Weeks 13 & 14</td><td>Health & Hygiene</td></tr>
								<tr><td>Week 15</td><td>Power of Play</td></tr>
								<tr><td>Week 16</td><td>Power of Love</td></tr>
								<tr><td>Week 17</td><td>Team Management</td></tr>
								<tr><td>Weeks 18 & 19</td><td>Family School Partnership</td></tr>
								<tr><td>Week 20</td><td>Communication</td></tr>
								<tr><td>Week 21</td><td>Professionalism</td></tr>
							</table>
							<p class="col-12 col-sm-12 float-left pt-2 m-0">Each session will include classroom theory sessions, group activities, and reflection exercises which will reinforce the content taught during the session. At the end of the course, students need to do a hands-on internship program of 4 weeks in KLAY centres across India.</p>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingFour">
						<h2>
							<button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour">How will I be assessed? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
						<div class="card-body">
							<ul>
								<li><strong>Evaluation Weightage:</strong></li>
								<li>Compulsory Attendance - 20% (174  classroom hours & 30 hours of Internship)</li>
								<li>Mid-term exam (to be conducted after 12 weeks) - 20%</li>
								<li>Internals (Home Assignments presented in class) - 20%</li>
								<li>Written exam (3 hours on last day of course) – 40% (MCQ, Fill ups, True or False, Short Answers)</li>								
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingFive">
						<h2>
							<button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive">What kind of certification will I receive upon completion of the program? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
						<div class="card-body">
							<p>Course participants who successfully complete the course will be awarded Certificate of completion by Institute of Early Childhood Education & Development affiliated with KLAY.</p>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingSix">
						<h2><button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix">Is Internship compulsory? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
						<div class="card-body">
							<p>Yes, the internship is mandatory as it provides hands on experience and the opportunity for you to put your learnings to the test in the real world</p>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingSeven">
						<h2><button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven">Will I be offered a full time role with KLAY? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
						<div class="card-body">
							<p>Yes, those who successfully complete the program and receive the diploma certificate will be offered full time roles with KLAY.</p>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingEight">
						<h2><button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight">What is the Program Fee? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
						<div class="card-body">
							<p>Total fee for the 6 month diploma program is Rs. 48,000/- We do have options of scholarships and EMI based payment options.</p>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingNine">
						<h2>
							<button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine">What are my career prospects upon completing this program? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
						<div class="card-body">
							<p>The program is designed to be a stepping stone into the field of early childhood education. Teachers who work with KLAY often go on to have successful careers in education management. This program opens doors not only to teaching, but curriculum planning and management, pre-school administration and education counselling.</p>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingTen">
						<h2>
							<button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen">What is it like to work as a teacher at KLAY? <i class="fa fa-plus"></i></button>
						</h2>
					</div>
					<div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
						<div class="card-body">
							<p>Working at KLAY brings new adventures everyday. Each child is unique and everyday is filled with new experiences and learnings for KLAY staff. Teachers starting with KLAY join as trainees and can progress up to head teachers. There are also opportunities for teachers to switch to management roles or join the curriculum planning team within KLAY.</p>
							<p>KLAY is the industry leader in early childhood education and follows a curriculum based on international research in the space of childhood development. Working with KLAY means you are trained in and gaining experience in the latest teaching methodologies being followed in countries such as USA, UK and Singapore.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="media-part">
	<div class="container">
	<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center">
		<h1>Media</h1>
	</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-md-4 col-sm-4 col-lg-4 news_topic">
					<h2>Ed Tech Review reaches to roughly 84,330 users and delivers about 185,550 pageviews each month.</h2>
					<img src="<?php echo get_template_directory_uri();?>/images/edtechreview.png" alt="banner" width="535" height="259" />
					<p>The program has been launched in response to overwhelming demand from teaching aspirants and parents for a world-class certificate program in early years teacher training that would prepare candidates for a career in this field. Ever since its inception, KLAY has already trained over 1700 of preschool teachers, administrators and caregivers who are the engine behind this pan-India prep school and day care network of over 150 centres.</p>
					<p><a href="https://edtechreview.in/news/3580-klay-schools-launches-the-preschool-teacher-training-program-for-educators" target="_blank">Read more</a></p>
				</div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4 news_topic">
					<h2>Education World reaches to roughly 52,770 users and delivers about 116,160 pageviews each month.</h2>
					<img src="<?php echo get_template_directory_uri();?>/images/eductaion.png" alt="banner" width="535" height="259" />
					<p>KLAY Prep Schools and Day Care announced launch of KLAY Pre-School Teacher Training Program for early educators. This certification aims at equipping early educators with the skills and knowledge they need for engaging and nurturing young minds and aims to certify over 600 teachers by 2019. Since its inception, KLAY has already trained over 1700 of preschool teachers, administrators and caregivers who are the engine behind this pan-India prep school and day care network of over 150 centres.</p>
					<p><a href="https://www.educationworld.in/klay-launches-preschool-teacher-training-program-2/" target="_blank">Read more</a></p>
				</div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4 news_topic">
					<h2>The Hindu, Chennai edition (Print) reaches to roughly 336,393 people and is one of the most widely read newspaper in India</h2>
					<img src="<?php echo get_template_directory_uri();?>/images/eductaion.png" alt="banner" width="535" height="259" />
					<p>With the curriculum across education boards undergoing change with every academic year, the spotlight is now on keeping learners engaged and having interactive classrooms which, in turn, has put the focus on teachers’ skills.The draft National Education Policy (NEP) has emphasised on how better pre-service as well as in-service training can contribute to raising the skills of the teachers..</p>
					<p><a href="https://www.thehindu.com/news/cities/chennai/teachers-skill-upgrade-is-part-of-syllabus/article28828225.ece" target="_blank">Read more</a></p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-md-4 col-sm-4 col-lg-4 news_topic">
					<h2>Mail Today, Delhi edition (print) reaches to roughly 209,136 people and is ranked amongst the top seven newspapers in Delhi</h2>
					<img src="<?php echo get_template_directory_uri();?>/images/mailtoday.jpg" alt="banner" width="535" height="259" />
				</div>
			</div>
		</div>
	</div>
</section>

<section class="enquiry_formbanner fullcols">
	<img src="<?php echo get_template_directory_uri();?>/images/enquireform-banner.jpg" alt="banner" width="1366" height="480" />
</section>


<section class="calander fullcols py-4 mt-5">
	<div class="container">
	<h1>Program Calendar</h1> 
	  <div class="calander_inner">
		<div class="col-12 col-sm-4 float-left bright">
		<h3>NCR</h3>
		 <table>
			<tr colspan="2"><th>Month</th><th>Center</th></tr>
			<tr colspan="2"><td>July</td><td>Sec 62 Noida</td></tr>
			<tr colspan="2"><td>July</td><td>DLF Ph 4 Gurgaon</td></tr>
			<tr colspan="2"><td>July</td><td>Sec 45 Gurgaon</td></tr>
			<tr colspan="2"><td>September</td><td>Sec 81 Gugaon</td></tr>
			<tr colspan="2"><td>September</td><td>Uppal Gurgaon</td></tr>
			<tr colspan="2"><td>October</td><td>Sec 62 Noida</td></tr>
		 </table>
		</div>
		<div class="col-12 col-sm-4 float-left">
			<div class="middlepart">
				<h3>Mumbai</h3>
				<table>
					<tr colspan="2"><th>Month</th><th>Center</th></tr>
					<tr colspan="2"><td>July</td><td>Lower Parel</td></tr>
					<tr colspan="2"><td>July</td><td>BKC</td></tr>
					<tr colspan="2"><td>August</td><td>Malad</td></tr>
					<tr colspan="2"><td>September</td><td>Bandra</td></tr>
					<tr colspan="2"><td>September</td><td>Thane</td></tr>
					<tr colspan="2"><td>December</td><td>Andheri</td></tr>
					<tr colspan="2"><td>December</td><td>Kalina</td></tr>
				</table>	
			</div>
		</div>
		<div class="col-12 col-sm-4 float-left bleft">
			<h3>Bangalore</h3>
			<table>
				<tr colspan="2"><th>Month</th><th>Center</th></tr>
				<tr colspan="2"><td>July</td><td>Bellandur</td></tr>
				<tr colspan="2"><td>August</td><td>Harlur</td></tr>
				<tr colspan="2"><td>August</td><td>HSR layout</td></tr>				
				<tr colspan="2"><td>August</td><td>Bellandur</td></tr>
				<tr colspan="2"><td>August</td><td>North Bangalore</td></tr>				
				<tr colspan="2"><td>October</td><td>Indira Nagar</td></tr>
				<tr colspan="2"><td>October</td><td>White Field</td></tr>
				<tr colspan="2"><td>October</td><td>Bellandur</td></tr>				
				<tr colspan="2"><td>November</td><td>Electronic City</td></tr>
				<tr colspan="2"><td>November</td><td>Yemlur</td></tr>
				<tr colspan="2"><td>November</td><td>Saket</td></tr>
				<tr colspan="2"><td>November</td><td>Hebbal</td></tr>				
				<tr colspan="2"><td>December</td><td>Sadashiv Nagar</td></tr>				
				<tr colspan="2"><td>December</td><td>Bellandur</td></tr>
				<tr colspan="2"><td>December</td><td>White Field</td></tr>
				<tr colspan="2"><td>December</td><td>ETV</td></tr>
			</table>	
		</div>
	  </div>
	</div>
</section>

<section class="whoweare fullcols py-4 mt-5">
	<div class="container">
		<h1 class="fullcols GothamRoundedBook col-12 col-sm-12 col-md-12 float-left text-center text-sm-center text-md-left p-0 mb-4">Who We Are</h1>
		<div class="whoweare_holder">
			<div class="col-12 col-sm-12 col-md-7 float-left p-0 m-0">
				<div class="col-12 col-sm-12 float-left p-0 m-0">
					<p>KLAY Preschool Teacher Training Program is brought to you by IECED (Institite of Early Childhood Education and Development). </p>
					<p>IECED comes under the umbrella of <strong>Founding Years Learning Solutions India Private Limited</strong> which is the proud parent company of India's Largest and Most Trusted Pre-School and Day Care Centres - <span><a href="htts://klayschools.com">KLAY Prep Schools and Daycare</a></span></p>
				</div>
				<div class="col-12 col-sm-12 float-left py-3 m-0">
					<ul>
						<li class="first1"><span><strong>5</strong></br>Cities</span></li>
						<li class="middle2"><span><strong>1252</strong></br>Trained Professionals</span></li>
						<li class="top3"><span><strong>30</strong></br>Placements</span></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-5 float-left whoweare_video">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/Nfs4oK0shtE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</section>

<section class="pre_teacher fullcols py-4 mt-5">
	<div class="container">
		<h1 class="fullcols GothamRoundedBook col-12 col-sm-12 col-md-12 float-left text-center text-sm-center text-md-left">Life As A Pre-School Teacher</h1>
		<div class="pre_teacher_holder">
			<div class="col-12 col-sm-12 col-md-10 float-left p-0 m-0">
				<div id="pre_teacher_vdo1"><iframe width="560" height="315" src="https://www.youtube.com/embed/MwJzvEdl7ds" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
				<div id="pre_teacher_vdo2"><iframe width="560" height="315" src="https://www.youtube.com/embed/wM57F9u438A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
				<div id="pre_teacher_vdo3"><iframe width="560" height="315" src="https://www.youtube.com/embed/97iCn3KCd0Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
				<div id="pre_teacher_vdo4"><iframe width="560" height="315" src="https://www.youtube.com/embed/jkI8Tr-tpH4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
				<div id="pre_teacher_vdo5"><iframe width="560" height="315" src="https://www.youtube.com/embed/lMrIE-v8bd8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
			</div>
			<div class="col-12 col-sm-12 col-md-2 float-left preteacher_video">
				<div id="rightvdo1" class="active"><a href="#pre_teacher_vdo1"><img src="<?php echo get_template_directory_uri();?>/images/youtube-lp-video-1.jpg" alt="banner" width="240" height="130" /></a></div>
				<div id="rightvdo2"><a href="#pre_teacher_vdo2"><img src="<?php echo get_template_directory_uri();?>/images/youtube-lp-video-2.jpg" alt="banner" width="240" height="130" /></a></div>
				<div id="rightvdo3"><a href="#pre_teacher_vdo3"><img src="<?php echo get_template_directory_uri();?>/images/youtube-lp-video-3.jpg" alt="banner" width="240" height="130" /></a></div>
				<div id="rightvdo4"><a href="#pre_teacher_vdo4"><img src="<?php echo get_template_directory_uri();?>/images/youtube-lp-video-4.jpg" alt="banner" width="240" height="130" /></a></div>
				<div id="rightvdo5"><a href="#pre_teacher_vdo5"><img src="<?php echo get_template_directory_uri();?>/images/youtube-lp-video-5.jpg" alt="banner" width="240" height="130" /></a></div>
			</div>
		</div>
	</div>
</section>


<section class="tech_testimonials fullcols py-4 mt-5">
	<div class="container">
		<h1 class="fullcols GothamRoundedBook col-12 col-sm-12 pt-0 pb-4 m-0 text-center text-sm-left text-md-left">Teacher Testimonials</h1>
		<div class="testi_holder fullcols p-4">
			<div class="col-12 col-sm-12 col-md-2 float-left text-center text-sm-left text-md-left">
				<img src="<?php echo get_template_directory_uri();?>/images/testi-pic.png" class="text-center text-sm-left text-md-left img-fluid" alt="banner" width="170" height="181" />
			</div>
			<div class="col-12 col-sm-12 col-md-10 float-left text-center text-sm-left text-md-left">
				<h2 class="col-12 col-sm-12 float-left pb-2 px-0 pt-0 m-0 GothamRoundedBold_0 text-center text-sm-left text-md-left">Swathi Jha</h2>
				<p class="col-12 col-sm-12 float-left p-0 m-0 GothamRoundedBook text-center text-sm-left text-md-left">Admission of my child at KLAY and the high quality day care facility has not only resolved my doubts of sending him to school, but the opportunity to work in KLAY as a teacher has given me an identity. The education and quality day care that KLAY provides is highly appreciable. Now, I can give my all to my job along with the firm belief that my child’s growth and care is in good hands. Thanks to KLAY for making me a successful working mom! </p>
			</div>
		</div>
	</div>
</section>


<footer class="fullcols py-2 mt-5">
	<div class="container">		
		<div class="float-left col-12 col-sm-12 py-3 m-0 better">
			<ul class="col-12 col-sm-12 float-left text-center m-0 p-0">
				<li class="get_title GothamRoundedBook fs-xs-14 fs-20">Get to know us better!</li>
				<li><a href="https://www.facebook.com/klayschools/"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/lp-fb.png" alt="fb" width="39" height="35"></a></li>
				<li><a href="https://twitter.com/KLAYSchools"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/lp-tweet.png" alt="tweet" width="39" height="35"></a></li>
				<li><a href="https://www.youtube.com/user/KlaySchools"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/lp-yutbe.png" alt="yutbe" width="39" height="35"></a></li>
				<li><a href="https://in.linkedin.com/company/klay-prep-schools-and-daycare"><img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/lp-linkid.png" alt="linkid" width="39" height="35"></a></li>
			</ul>
		</div>
	</div>
	<div class="foo_contacts fullcols py-2">
		<div class="container">			
			<p class="fullcols p-0 m-0 text-center"><a href="tel:+917349737682">(+91) 7349737682</a>  |  <a href="mailto:training.academy@klayschools.com">training.academy@klayschools.com</a>  | <a href="http://www.klayschools.com">www.klayschools.com</a>  |  <a href="http://www.early-childhood-institute.com">www.early-childhood-institute.com</a></p>
		</div>
	</div>
	<div class="foo_contacts2 fullcols p-0">
		<div class="container">
			<p class="col-12 col-sm-12 float-left text-center pt-4 pb-0 m-0 GothamRoundedBold_0">Institute of Early Childhood Education & Development</p>
			<p  class="col-12 col-sm-12 float-left text-center pt-3 pb-0 m-0 GothamRoundedBook">Founding Years Learning Solutions Private Limited,</p> 
			<p  class="col-12 col-sm-12 float-left text-center p-0 mx-0 mb-5 GothamRoundedBook">II floor, Saroj Bank Square, Above Axis Bank, Devarabeesanahalli, Bangalore 560103</p>
		</div>
	</div>
</footer>

<section class="yellow-bg eventslp_sticky d-block d-md-none fs-18 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">Call</a></div>
			<div class="col"> <a href="#enq_form">Enquire Now</a></div>
		</div>
	</div>
</section>



<style type="text/css">
.fullcols { float : left; width : 100%; position : relative; }
section { padding : 0; }
.GothamRoundedBold_0 { font-family: 'GothamRoundedBold_0'; }
.GothamRoundedBook { font-family: 'GothamRoundedBook'; }
.AsparagusSprouts { font-family: 'AsparagusSprouts'; }
.banner_holder { background: #333333; }
.banner_holder h1 { color: #fff; float: left; font-size: 20px; text-align: center; width: 100%; }
.banner_holder h1 span { color: #fcbb11; }
.banner_holder h2 { color: #fff;  float: left; font-family: 'GothamRoundedBook';  font-size: 17px; line-height: 24px; padding: 5% 0; text-align: center; width: 100%; }
.banner_holder h2 span { font-family: 'GothamRoundedBold_0'; }
.placements { }
.placements h1 { color : #333333; font-size : 42px; }
.placements h2 { color : #333333; font-size : 42px; }
.placements p { color : #333333; font-size : 18px; text-align : justify; }
.whyidpe_content {  float: left;  margin: 0 auto;  padding: 0; }
.whyidpe_content ul {  float: left; margin: 1em auto 0 1em;  padding: 0; width: auto; }
.whyidpe_content ul li { float : left; color : #0099cc;  }
.whyidpe_content ul li span { color : #333; font-size: 18px;  }
.whyidpe_connect h3:before { background: url("<?php echo get_template_directory_uri();?>/images/phoneicons.jpg") no-repeat scroll left 30px;  content: " "; display: inline-block; width: 20px; text-align: left; height: 46px; }
.whyidpe_connect h3 a, .whyidpe_connect h4 a { font-family: 'GothamRoundedMedium'; color : #030303; font-size : 18px;  }
.whyidpe_connect h4:before { background: url("<?php echo get_template_directory_uri();?>/images/mailicons.jpg") no-repeat scroll left 14px;  content: " "; display: inline-block;   width:35px;   text-align: left;   height:  31px; }
.whyidpe_connect h3, .whyidpe_connect h4 {  float: left;  margin: 0 auto;  padding: 0; clear : both;  }
.enquiry_formbanner { }
.calander {  }
.calander h1 { float : left; width :100%;  color : #333333;font-size : 35px; font-family: 'GothamRoundedBook'; padding :0 0 1% 0; margin : 0 auto; }
.calander_inner {  }
.calander_inner h3 { float : left; width :100%;  color : #333333; font-size : 18px; font-family: 'GothamRoundedBold_0';  margin: 0 0 1em; padding: 0; text-align : center; }
.calander_inner p { float : left; width :100%;  color : #333333; font-size : 16px; font-family: 'GothamRoundedBook'; margin : 0 auto; padding : 0; }
.calander_inner .middlepart { }
.calander_inner { float: left; background: rgb(108, 234, 242); width: 100%; border-radius: 10px; padding: 3% 0px; }
.whoweare { background: #ffe362; }
.whoweare h1 { border-bottom : 1px solid #ffffff;  color : #333333;  font-size : 35px;}
.whoweare_holder {}
.whoweare_holder p { font-family: 'GothamRoundedBook'; color : #333333; font-size : 20px; }
.whoweare_holder p span { color : #3399cc; }
.whoweare_holder ul {}
.whoweare_holder ul li {  color: #333;  display: inline-block; float: none; font-family: 'GothamRoundedBook'; font-size: 16px; list-style-type: none;  margin: 0 auto; text-align: center;  width: auto; }
.whoweare_holder ul li span {  float: left;  text-align: center; width: 100%; font-family: 'GothamRoundedMedium'; font-size: 16px; }
.whoweare_holder ul li span strong {  font-family: 'GothamRoundedBold_0'; font-size: 18px; }
.whoweare_holder ul li.first1:before {  border-radius: 5px;  content: " ";  display: inline-block; width: 35px; height: 60px;  padding: 35px;    background : #fff url("<?php echo get_template_directory_uri();?>/images/locations-map.png") no-repeat center center; }
.whoweare_holder ul li.middle2:before { border-radius: 5px;  content: " ";  display: inline-block; width: 70px; height: 60px;  padding: 35px;  background : #fff url("<?php echo get_template_directory_uri();?>/images/2hands.png") no-repeat center center; }
.whoweare_holder ul li.top3:before { border-radius: 5px;  content: " ";  display: inline-block; width: 65px; height: 60px;  padding: 35px; background : #fff url("<?php echo get_template_directory_uri();?>/images/bag.png") no-repeat center center; }
.whoweare_video { }
.whoweare_video iframe { float : left; width : 100%; max-width : 100%;  }
.pre_teacher { }
.pre_teacher h1 { font-size : 35px; border-bottom : 1px solid #ebebeb; }
.pre_teacher_holder { }
.pre_teacher_holder iframe { float : left; width : 100%; max-width : 100%; }
.preteacher_video { }
.preteacher_video div { }
.preteacher_video img { height : auto; margin : 0 auto 10px auto; }
.preteacher_video .active { opacity : 0.7; }
.preteacher_video .active img {  opacity : 0.7;}
footer { background : #ededed;  border-top: 2px dashed #5a5758; }
.foo_contacts { background : #d93e56; }
.foo_contacts p, .foo_contacts p a { color : #fff; font-size : 18px; font-family: 'GothamRoundedBook'; }
.foo_contacts2 {  }
.foo_contacts2 p { font-size : 16px; color : #333333;  }
.tech_testimonials { background : #f4f4f4; }
.tech_testimonials h1 { background : #f4f4f4; font-size : 35px; color : #333333;}
.testi_holder { background : #fff; }
.testi_holder img  { }
.testi_holder h2 { color : #000; font-size : 20px; }
.testi_holder p { color : #333; font-size : 18px; }
.admissions { }
.admissions h1 { color : #333; font-size : 35px; border-bottom : 1px solid #ebebeb; }
.admissions p.first { color : #333; font-size : 20px; font-family: 'GothamRoundedBook'; }
.admissions p.second { color : #333; font-size : 23px; font-family: 'GothamRoundedMedium';  }
.admissions p.second span, .admissions p.second span a { color : #3399cc; font-family: 'GothamRoundedMedium'; }
.admissions_acco { float: left; width: 100%; border: 0px none; padding: 0px; margin: 0px auto; }
.accordion { background: #fff; cursor: pointer; float: left; margin: 0 auto; padding: 0; width: 100%; }
.card { float: left; padding: 0 0 1em; margin: 0px auto; background: rgb(255, 255, 255) none repeat scroll 0% 0%; width: 100%; border: 0px none ! important; }
.card-header { float: left; background: rgb(255, 255, 255) none repeat scroll 0% 0%; padding: 0px ! important; margin: 0px ! important;  border: 0 none !important; }
.card h2 button { color : #333333; font-size : 18px; font-family: 'GothamRoundedBold_0'; padding: 0 !important;  white-space: normal !important;  text-align: left; }
.card h2 button i { color : #3399cc !important; }
.card-body { padding : 0 !important; }
.card ul { float : left; width : 100%; padding :0; list-style-type : none; }
.card ul li { color : #333333; font-size : 18px; font-family: 'GothamRoundedBook'; }
.card p { color : #333333; font-size : 18px; font-family: 'GothamRoundedBook'; float : left; width :100%; margin : 0 auto; padding : 5px 0 0 0; }
.card ul li  { padding : 10px 0 0 0; }
.card tr th { color : #333333; font-size : 18px; font-family: 'GothamRoundedBook'; background: #d93e56; }
.card tr th  { border : 1px solid #ddd; padding: 5px; background: #d93e56; color : #fff; font-family: 'GothamRoundedMedium'; }
.card tr td  {  border : 1px solid #ddd; padding: 5px;  font-family: 'GothamRoundedBook';   }
.enquiry_formbanner img { width : 100%; height : auto; }
.banner_holder { width : 100%; height : auto;}
.better ul li {  color: #000; display: inline-block;  float: none; list-style-type: none; padding: 0 3px; text-align: center; width: auto; }
.whoweare_holder ul { padding :0; }
.accordion::after { content : none !important; }
.form_container { background : #474747;  margin: -3px auto 0 0;} 
.stricky_menu {  bottom: 0;  position: fixed;  width: 100%;  z-index: 9999999; }
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a {  color: #333; display: inline-block; padding: 8px 0; }
.stricky_menu .col a {  width: 100%; }
.certified_logo { left: 20px; max-width: 360px; position: absolute; top: 3em; }
.certified_logo span { float : left; width :100%;   text-align: center; }
.certified_logo span:last-child { margin : 10px auto 0; }
.certified_logo img  { clear : both; width : auto !important; }
.calander_inner table { float : left; width : 100%;  }
.calander_inner table tr { }
.calander_inner table tr th { padding : 5px; font-family: 'GothamRoundedMedium';  }
.calander_inner table tr td { padding : 5px; font-family: 'GothamRoundedBook'; }

@media only screen and (min-width: 1200px)
{
.whyidpe_connect { position: absolute; width: auto; right: 0px; left: 0px; top: 3em; }
.banner_holder img { width : 100%; height : auto; }
.form_holder iframe {  height: auto;  min-height: 201px;  padding: 2em 0; }
}

@media only screen and (min-width: 1367px)
{

.banner_holder img { width : 100%; height : auto; }
}

@media only screen and (min-width: 767px)
{
.yidpe_lists { float:left; width : 100%;  padding: 0 0 0 18%; }
.yidpe_lists img { margin : 0 0 3% 0; }
.pre_teacher_holder iframe { min-height : 460px; }
.calander_inner .bleft { border-left : 1px solid #fff; }
.calander_inner .bright { border-right : 1px solid #fff; }
.calander_inner table { text-align : center; }
b.blue-text {
    color: #10a5ae;
    font-family: 'GothamRoundedLight';
    font-size: 28px;}
h1.why_text{font-family: 'AsparagusSprouts';
    font-size: 2em;
    font-weight: normal;}
}

@media only screen and (max-width: 767px)
{
.get_title { width : 100% !important; }	
.preteacher_video { float:left;  }	
.preteacher_video div {  float: left; margin: 2% auto 0;  width: 20%; }		
.whoweare_holder ul li { width : 30% !important; }
.calander_inner .middlepart { float: left; padding: 3em 0 !important;  width: 100% !important; border : 0px none !important; }
.img-fluid { width : 100%; height : auto; }
.banner_holder .enquire_mb { position: absolute; bottom: 10px; width: 100%; text-align: center; }
.banner_holder .enquire_mb img { width : 154px; }
.foo_contacts p, .foo_contacts p a {  font-size : 16px !important; }
.whyidpe_connect h3 a, .whyidpe_connect h4 a { font-size : 14px !important; }
.calander h1, .admissions h1, .whoweare h1, .pre_teacher h1, .tech_testimonials h1 { font-size : 30px !important; }
}

section.media-part {
    position: relative;
    width: 100%;
    float: left;padding: 0% 0 3% 0;
}
.news_topic h2{font-size:20px;text-align:center;padding: 2% 0;font-weight: bold;
    font-family: 'GothamRoundedBook';}
.news_topic p {
    padding: 2% 1%;
    font-size: 15px;text-align: justify;font-family: 'GothamRoundedBook';
}
.media-part h1 {
    font-family: 'AsparagusSprouts';
    font-size: 50px;
    text-transform: uppercase;
	padding: 3% 0;
}
.news_topic p a {
    color: #218eba !important;
    font-size: 18px;font-family: 'GothamRoundedBook';
}
</style>


<script type="text/javascript">
$j = jQuery.noConflict();
$j(document).ready(function(){
   $j("#pre_teacher_vdo1").addClass('active');  $j("#pre_teacher_vdo2").hide(); $j("#pre_teacher_vdo3").hide(); $j("#pre_teacher_vdo4").hide(); $j("#pre_teacher_vdo5").hide();
  $j("#rightvdo1").click(function(){
    $j("#pre_teacher_vdo1").show(); $j("#pre_teacher_vdo2").hide(); $j("#pre_teacher_vdo3").hide(); $j("#pre_teacher_vdo4").hide(); $j("#pre_teacher_vdo5").hide();
	$j("#rightvdo1").addClass('active'); $j("#rightvdo2").removeClass('active'); $j("#rightvdo3").removeClass('active'); $j("#rightvdo4").removeClass('active'); $j("#rightvdo5").removeClass('active');
  });
  
  $j("#rightvdo2").click(function(){
    $j("#pre_teacher_vdo2").show(); $j("#pre_teacher_vdo1").hide(); $j("#pre_teacher_vdo3").hide(); $j("#pre_teacher_vdo4").hide(); $j("#pre_teacher_vdo5").hide();
	$j("#rightvdo2").addClass('active'); $j("#rightvdo1").removeClass('active'); $j("#rightvdo3").removeClass('active'); $j("#rightvdo4").removeClass('active'); $j("#rightvdo5").removeClass('active');
  });
  
  $j("#rightvdo3").click(function(){
    $j("#pre_teacher_vdo3").show(); $j("#pre_teacher_vdo1").hide(); $j("#pre_teacher_vdo2").hide(); $j("#pre_teacher_vdo4").hide(); $j("#pre_teacher_vdo5").hide();
	$j("#rightvdo3").addClass('active'); $j("#rightvdo1").removeClass('active'); $j("#rightvdo2").removeClass('active'); $j("#rightvdo4").removeClass('active'); $j("#rightvdo5").removeClass('active');
  });

$j("#rightvdo4").click(function(){
    $j("#pre_teacher_vdo4").show(); $j("#pre_teacher_vdo1").hide(); $j("#pre_teacher_vdo2").hide(); $j("#pre_teacher_vdo3").hide(); $j("#pre_teacher_vdo5").hide();
	$j("#rightvdo4").addClass('active'); $j("#rightvdo1").removeClass('active'); $j("#rightvdo2").removeClass('active'); $j("#rightvdo3").removeClass('active'); $j("#rightvdo5").removeClass('active');
  });
  
  $j("#rightvdo5").click(function(){
    $j("#pre_teacher_vdo5").show(); $j("#pre_teacher_vdo4").hide(); $j("#pre_teacher_vdo3").hide(); $j("#pre_teacher_vdo2").hide(); $j("#pre_teacher_vdo1").hide(); 
	$j("#rightvdo5").addClass('active'); $j("#rightvdo1").removeClass('active'); $j("#rightvdo2").removeClass('active'); $j("#rightvdo3").removeClass('active'); $j("#rightvdo4").removeClass('active');
  });
  

    $j(".collapse.show").each(function(){
        	$j(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $j(".collapse").on('show.bs.collapse', function(){
        	$j(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
        	$j(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
		

});

</script>
<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<?php
endwhile;
get_footer();
