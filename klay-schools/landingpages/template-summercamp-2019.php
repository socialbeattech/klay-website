<?php
/**
 * Template Name: Summer Camp 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WM5LSXF');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WM5LSXF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> 

<section class="summer_headers fullcols">
	<div class="fullcols"  id="register">
		<img src="<?php echo get_template_directory_uri();?>/images/Scamp-banner-lp.jpg" class="img-fluid show_md" alt="Banner" width="1366" height="825" />	
		<img src="<?php echo get_template_directory_uri();?>/images/scamp-mobile.png" class="img-fluid show_xs" alt="Banner" width="480" height="459" />	
		<div class="summer_holder">				
			<div class="container">				
				<iframe src="https://go.pardot.com/l/563842/2019-03-14/4zf6c?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>				
			</div>					
		</div>					
	</div>	
</section>

<section class="timetable fullcols py-5">
		<div class="timetable_holder">
			<div class="timetables col-12 col-sm-12 col-md-2 p-0 m-0 float-left">
				<h2 class="col-12 col-sm-12 text-center p-0 m-0">BANGALORE</h2>
				<h3 class="col-12 col-sm-12 text-center p-0 m-0">8th Apr - 10th May</h3>
			</div>	
			<div class="timetables col-12 col-sm-12 col-md-2 p-0 m-0 float-left">
				<h2 class="col-12 col-sm-12 text-center p-0 m-0">MUMBAI</h2>
				<h3 class="col-12 col-sm-12 text-center p-0 m-0">11th Apr - 3rd May</h3>
			</div>
			<div class="timetables col-12 col-sm-12 col-md-2 p-0 m-0 float-left">
				<h2 class="col-12 col-sm-12 text-center p-0 m-0">NCR</h2>
				<h3 class="col-12 col-sm-12 text-center p-0 m-0">13th May - 14th Jun</h3>
			</div>	
			<div class="timetables col-12 col-sm-12 col-md-2 p-0 m-0 float-left">
				<h2 class="col-12 col-sm-12 text-center p-0 m-0">PUNE</h2>
				<h3 class="col-12 col-sm-12 text-center p-0 m-0">8th Apr - 10th May</h3>
			</div>
			<div class="timetables col-12 col-sm-12 col-md-2 p-0 m-0 float-left">
				<h2 class="col-12 col-sm-12 text-center p-0 m-0">HYDERABAD</h2>
				<h3 class="col-12 col-sm-12 text-center p-0 m-0">15th Apr - 15th May</h3>
			</div>	
			<div class="timetables col-12 col-sm-12 col-md-2 p-0 m-0 float-left">
				<h2 class="col-12 col-sm-12 text-center p-0 m-0">CHENNAI</h2>
				<h3 class="col-12 col-sm-12 text-center p-0 m-0">8th Apr - May 11th</h3>
			</div>				
		</div>					
</section>

<section class="summer_icons fullcols">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="icons_holder py-3">
				<ul>
					<li><img src="<?php echo get_template_directory_uri();?>/images/music-move.png" class="img-fluid" alt="Banner" width="105" height="158" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/tricks-spells.png" class="img-fluid" alt="Banner" width="110" height="158" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/get-sporty.png" class="img-fluid" alt="Banner" width="114" height="158" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/mind-gym.png" class="img-fluid" alt="Banner" width="110" height="158" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/Atrify.png" class="img-fluid" alt="Banner" width="105" height="158" /></li>
				</ul>

				<ul>
					<li><img src="<?php echo get_template_directory_uri();?>/images/Science-Lap.png" class="img-fluid" alt="Banner" width="115" height="141" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/cakes-bakes.png" class="img-fluid" alt="Banner" width="140" height="141" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/center-stage.png" class="img-fluid" alt="Banner" width="140" height="141" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/picture-perfect.png" class="img-fluid" alt="Banner" width="165" height="141" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/Marital-arts.png" class="img-fluid" alt="Banner" width="130" height="141" /></li>
				</ul>
			</div>			
		</div>		
	</div>
</section>


<section class="creativity fullcols">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="creativity_holder py-3">
				<h1 class="col-12 col-sm-12 float-left py-4 m-0 text-center text-sm-center text-md-left">KLAY – IGNITING   IMAGINATION AND CREATIVITY</h1>
				<p class="col-12 col-sm-12 float-left pb-3 m-0">As India's largest and most trusted parenting partner, KLAY, sets the standard for Early Childhood Education, KLAY, sets the standard for Early Childhood Education, engaging children between the ages of 5 months to 10 years in an exciting world packed with stimulating learning experiences that arm them for tomorrow’s world!</p>
				<p class="col-12 col-sm-12 float-left pb-4 m-0">With a network of 135 plus centres, spread across 7 cities, they encourage children to explore the world around them and give them the best start. </p>
			</div>			
		</div>		
	</div>
</section>

<section class="expsummer fullcols">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="expsummer_holder py-4 ">
				<h1 class="col-12 col-sm-12 float-left py-2 m-0 text-center text-sm-center text-md-left">EXPERIENCE SUMMER THE KLAY WAY!</h1>
				<div class="klay_way col-12 col-sm-12 float-left px-0 py-3">
					<div class="klayway_1 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>15000+</h2>
						<h3>Happy Parents</h3>
					</div>
					<div class="klayway_2 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>7500+</h2>
						<h3>Happy Children</h3>
					</div>
					<div class="klayway_3 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>135+</h2>
						<h3>Centres</h3>
					</div>
					<div class="klayway_4 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>7+</h2>
						<h3>Cities</h3>
					</div>
				</div>
			</div>			
		</div>		
	</div>
</section>

<section class="summer_footer fullcols pd_20">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="summer_footer_holder col-12 col-sm-12 col-md-12 py-0 pb-3 m-0 px-0">
				<div class="summer_foot1 col-12 col-sm-12 col-md-6 float-left p-0 m-0">
					<p class="webs text-center text-sm-center text-md-left col-12 col-sm-12 p-0 m-0"><img src="<?php echo get_template_directory_uri();?>/images/sumercamp-webs.png" class="img-fluid show_xs" alt="Banner" width="42" height="42" /><a href="https://www.klayschools.com">www.klayschools.com</a>  |  <a href="mailto:info@klayschools.com">info@klayschools.com</a></p>
				</div>
				<div class="summer_foot2 col-12 col-sm-12 col-md-6 float-left p-0 m-0">
					<p class="callings text-center text-sm-center text-md-left col-12 col-sm-12 p-0 m-0"><img src="<?php echo get_template_directory_uri();?>/images/call-summercamps.png" class="img-fluid show_xs" alt="Banner" width="42" height="42" /><a href="tel:+917676708888">76767 08888</a></p>
				</div>
			</div>			
		</div>		
	</div>
</section>


<section class="yellow-bg eventslp_sticky d-block d-md-none fs-16 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">CALL</a></div>
			<div class="col"> <a href="#register">Admissions</a></div>
		</div>
	</div>
</section>


<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<style type="text/css">
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.page-id-64715 #masthead, .page-id-64715 .banner, .page-id-64715 .title-holder, .page-id-64715 .and-theres-more-at-klay, .page-id-64715 #colophon, .page-id-64715 .copyrights, .page-id-64715 .blue-bg, .page-id-64715 .important-menu-items  { display : none !important; }
.page-id-64715 .fullcols { float : left; width : 100%; position : relative; paddding :0; margin : 0 auto; }
.page-id-64715 .summer_icons { }
.page-id-64715 .summer_icons ul { display: inline-block;  float: none;  margin: 3% auto;  padding: 0; text-align: center; width: 100%; }
.page-id-64715 .summer_icons ul li { display: inline-block;  float: none;  margin: 0 auto; padding: 0 2em; text-align: center; }

.page-id-64715 h1 { font-family: 'GothamRoundedBold_0'; font-weight : bold; font-size : 25px; color : #000007; }
.page-id-64715 .creativity_holder p { font-family: 'GothamRoundedBook';  font-size : 16px; color : #323335;  }
.page-id-64715 .expsummer h2 { font-family: 'GothamRoundedBold_0';  color: #333333;  font-size: 25px; margin: 0 auto;  padding: 15px 0 0 90px; width: auto; }
.page-id-64715 .expsummer h3 {  font-family: 'GothamRoundedMedium'; color: #333333;  font-size: 17px; margin: 0 auto;  padding: 15px 0 0 90px; width: auto; }


.page-id-64715 .klay_way { border-bottom : 1px solid #d6d6d6; }
.page-id-64715 .summer_foot1 p { float: left; line-height: 40px; margin: 0; }
.page-id-64715 .summer_foot1 p a { font-family: 'GothamRoundedBold_0';   color: #333333;  float: none; font-size: 17px;  padding: 0 0 0 6px;  width: auto; }


.page-id-64715 .summer_foot2 p { float: left; line-height: 40px; margin: 0; }
.page-id-64715 .summer_foot2 p a { font-family: 'GothamRoundedBold_0';   color: #333333;  float: none; font-size: 17px;  padding: 0 0 0 6px;  width: auto; }




.timetable { background : #f9c003; }
.timetable_holder { }


.timetable_holder h2 { font-family: 'GothamRoundedBook';  font-size : 16px; color : #000000; }
.timetable_holder h3 { font-family: 'GothamRoundedBook';  font-size : 16px; color : #000000; }


.summer_holder { width: 100%; height : auto;  background : #34bddf url("<?php echo get_template_directory_uri();?>/images/scamp-bg.jpg") center center no-repeat; width : 100%;}
.summer_headers img { width  :100%; height : auto; }


@media only screen and (min-width: 1024px)
{
.page-id-64715 .summer_icons ul li { width: 20%; }	
.page-id-64715 .summer_icons ul li:first-child { padding-left : 0; }	
.page-id-64715 .summer_icons ul li:last-child { padding-right : 0; }
.page-id-64715 .summer_icons ul { margin: 3% auto;}
.summer_holder iframe { min-height : 315px; height : auto; }
}

@media only screen and (max-width: 1023px)
{
.page-id-64715 .summer_icons ul { margin: 0 auto;}
.page-id-64715 .summer_icons ul li { width: 100%; margin : 5% auto;  }		
}

@media only screen and (min-width: 767px)
{


.page-id-64715 .klayway_1:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-parents.png") no-repeat left center; content: " ";  float: left; width: 84px;  height: 86px; }
.page-id-64715 .klayway_2:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-childrens.png") no-repeat left center; content: " ";  float: left; width: 76px;  height: 76px; }
.page-id-64715 .klayway_3:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-centers.png") no-repeat left center; content: " ";  float: left; width: 76px;  height: 86px; }
.page-id-64715 .klayway_4:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-locateus.png") no-repeat left center; content: " ";  float: left; width: 75px;  height: 81px; }
.page-id-64715 .summer_header { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-banner.jpg") no-repeat center center; }
.summer_holder {  float : left;  position: relative;  width: 100%;  }	
.timetables { background : url("<?php echo get_template_directory_uri();?>/images/timetable_divider.png") no-repeat right center; }
.timetables:last-child { background : none !important; }
.page-id-64715 .summer_foot1 p.webs:before { background : url("<?php echo get_template_directory_uri();?>/images/sumercamp-webs.png") no-repeat left center; content: " ";  float: left; width: 42px;  height: 42px;  }
.page-id-64715 .summer_foot2 p.callings:before { background : url("<?php echo get_template_directory_uri();?>/images/call-summercamps.png") no-repeat left center; content: " ";  float: left; width: 45px;  height: 45px;  }
.show_xs { display : none; }
.show_md { display : block; }
.show_header_xs { display : none; }
}

@media only screen and (max-width: 767px)
{
.pd_20{padding-bottom:10%}
.page-id-64715 .klayway_1:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-parents.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 86px; }
.page-id-64715 .klayway_2:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-childrens.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 76px; }
.page-id-64715 .klayway_3:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-centers.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 86px; }
.page-id-64715 .klayway_4:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-locateus.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 81px; }
.page-id-64715 .expsummer h2, .page-id-64715 .expsummer h3 {  padding: 5px 0 0 0 !important; text-align : center; }
.page-id-64715 .summer_header { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-banner.jpg") no-repeat center center; }	
.amp-col-md-3 { width : 100%; }
.summer_holder { background: #34bddf; } 
.show_xs { display: inline-block;  float: none; text-align: center; }
.show_header_xs { display: inline-block; text-align: center; width : 100%; }
.timetables:after { background : url("<?php echo get_template_directory_uri();?>/images/timetable_divider-mobile.png") no-repeat center center;  content: " ";  display: inline-block;  float: none;  height: 10px;  padding: 15% 0 0; text-align: center; width: 100%; }
.page-id-64657 .social_icons ul li { paddding : 0 0.2em 0 0; }
.show_md { display : none; }
}
</style>

<script type="text/javascript">
piAId = '564842';
piCId = '85528';
piHostname = 'pi.pardot.com';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script>

<?php
endwhile;
get_footer();
