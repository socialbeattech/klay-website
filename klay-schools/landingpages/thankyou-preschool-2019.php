<?php
/**
 * Template Name: Thankyou Preschools 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
				
<?php if(get_field('pty_gtm_header')): the_field('pty_gtm_header'); endif; ?>
<?php if(get_field('pty_gtm_footer')): the_field('pty_gtm_footer'); endif; ?>				
				
<section class="fullcols banner_holder p-0 m-0">
	<?php if( have_rows('pty_slider') ): ?>
	<div class="fullcols desktop_slider">
		<div id="preschool_lp" class="swiper-container">
			<div class="swiper-wrapper">
				<?php while ( have_rows('pty_slider') ) : the_row(); ?>
				<?php $d_banners =  get_sub_field('pty_sliding_banner'); ?>
					<div class="swiper-slide">
						<img src="<?php echo $d_banners['url']; ?>" alt="<?php echo $d_banners['alt']; ?>" width="<?php echo $d_banners['width']; ?>" height="<?php echo $d_banners['height']; ?>">
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="form_holder" id="register_plp">
		<h1 class="col-12 col-sm-12 text-left fs-20"><?php the_field('pty_thankyou_message'); ?></h1>	
	</div>
</section>

<style type="text/css">
section { padding : 5% 0 !important; }
.banner_holder img { width :100%; height : auto; }
.form_holder { background: rgba(40, 198, 225, 0.7) none repeat scroll 0 0; }
.form_holder h1 { color : #fff; }
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
.page-id-<?php echo $current_pageid; ?> .fullcols { float : left; width : 100%; position : relative; }
.GothamRoundedBold_0 { font-family: 'GothamRoundedBold_0'; }
.GothamRoundedBook { font-family: 'GothamRoundedBook'; }
@media only screen and (min-width: 1200px)
{
.banner_holder img { height : auto; }	
}
@media only screen and (min-width: 767px)
{
.form_holder { background: rgba(40, 198, 225, 0.7) none repeat scroll 0 0;  border-radius: 10px;  left: auto;   max-width: 450px;  opacity: 0.89;  padding: 16px;   position: absolute;  right: 30px;   top: 10%;  z-index: 999; width : 100%; }
.banner_holder { height : 920px; }
}
@media only screen and (max-width: 767px)
{
.tagslist { max-width: 100%;  }
.tagslist h2 { border-bottom: 2px solid #ffffff; }
.form_holder { background: rgba(40, 198, 225, 0.7) none repeat scroll 0 0;  border-radius: 10px; max-width: 100%;  opacity: 0.89;  padding: 16px;   position: relative; }
.social-icons ul {  padding: 0 0 10%; }
.swiper-wrapper { height : auto !important; } 
.swiper-slide { height : auto !important; }
}
</style>
<?php
endwhile;
get_footer();
