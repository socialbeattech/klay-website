<?php
/**
 * Template Name: Grufullo 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();

?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NK8KZ8Z');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NK8KZ8Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<section class="summer_headers fullcols">
	<div class="fullcols">
		<img src="<?php echo get_template_directory_uri();?>/images/Gruffalo-LPdesktop.png" class="img-fluid show_md" alt="Banner" width="1366" height="665" />	
		<img src="<?php echo get_template_directory_uri();?>/images/Grufallo-lp-xs-banner.jpg" class="img-fluid show_xs" alt="Banner" width="480" height="769" />	
		<div class="summer_holder" id="register">				
			<div class="container">				
				<iframe src="http://go.pardot.com/l/563842/2019-05-09/5bft8?Source_URL=<?php the_permalink(); ?>" class="grufullo_form" id="grufullo_form" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>				
			</div>					
		</div>					
	</div>	
</section>

<section class="summer_icons fullcols">
	<div class="container">
		<div class="row p-0 mt-4 mb-0 my-0">
			<h2 class="col-12 col-sm-12 float-left py-2 m-0 text-center text-sm-center text-md-left">Join us for a one of a kind birthday party packed with lots of exciting activities for one and all.</h2>
			<h3 class="col-12 col-sm-12 float-left py-2 m-0 text-center text-sm-center text-md-left">Come, let us celebrate Gruffalo’s birthday together!</h3>
			<div class="icons_holder py-3 col-12 col-sm-12 p-0 m-0">
				<ul>
					<li><img src="<?php echo get_template_directory_uri();?>/images/story-time-1.png" class="img-fluid" alt="Banner" width="127" height="138" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/dance-along-2.png" class="img-fluid" alt="Banner" width="143" height="138" /></li>
					<li class="worktext"><img src="<?php echo get_template_directory_uri();?>/images/toddles.png" class="img-fluid" alt="Banner" width="191" height="184" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/art-corner-4.png" class="img-fluid" alt="Banner" width="136" height="138" /></li>
					<li><img src="<?php echo get_template_directory_uri();?>/images/treasure-trail-5.png" class="img-fluid" alt="Banner" width="151" height="145" /></li>
				</ul>
			</div>			
		</div>		
	</div>
</section>

<section class="expsummer fullcols">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="col-12 col-sm-12 p-0 m-0 expsummer_holder py-3">
				<h1 class="col-12 col-sm-12 float-left py-2 m-0 text-center text-sm-center text-md-left"></h1>
				<div class="klay_way col-12 col-sm-12 float-left px-0 py-3">
					<div class="klayway_1 col-12 col-sm-6 col-md-4 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>17000+</h2>
						<h3>Happy Parents</h3>
					</div>
					<div class="klayway_2 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>8500+</h2>
						<h3>Happy Children</h3>
					</div>
					<div class="klayway_3 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>140+</h2>
						<h3>Centres</h3>
					</div>
					<div class="klayway_4 col-12 col-sm-6 col-md-2 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>7+</h2>
						<h3>Cities</h3>
					</div>
				</div>
			</div>			
		</div>		
	</div>
</section>

<section class="summer_footer fullcols pd_20">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="summer_footer_holder col-12 col-sm-12 col-md-12 py-0 pb-3 m-0 pl-4 pr-4">
				<div class="summer_foot1 col-12 col-sm-12 col-md-8 float-left text-center p-0 m-0">
					<p class="webs text-center col-12 col-sm-12 p-0 m-0"><img src="<?php echo get_template_directory_uri();?>/images/sumercamp-webs.png" class="img-fluid show_xs" alt="Banner" width="42" height="42" /><a href="https://www.klayschools.com">www.klayschools.com</a>  |  <a href="mailto:info@klayschools.com" class="ndhref">info@klayschools.com</a></p>
				</div>
				<div class="summer_foot2 col-12 col-sm-12 col-md-4 float-left p-0 m-0">
					<p class="callings text-center col-12 col-sm-12 p-0 m-0"><img src="<?php echo get_template_directory_uri();?>/images/call-summercamps.png" class="img-fluid show_xs" alt="Banner" width="42" height="42" /><a href="tel:+917676708888">76767 08888</a></p>
				</div>
			</div>			
		</div>		
	</div>
</section>


<section class="yellow-bg eventslp_sticky d-block d-md-none fs-16 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">CALL</a></div>
			<div class="col"> <a href="#grufullo_form">Admissions</a></div>
		</div>
	</div>
</section>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>
<style type="text/css">

.page-id-65941 #masthead, .page-id-65941 .banner, .page-id-65941 .title-holder, .page-id-65941 .and-theres-more-at-klay, .page-id-65941 #colophon, .page-id-65941 .copyrights, .page-id-65941 .blue-bg, .page-id-65941 .important-menu-items  { display : none !important; }
.page-id-65941 .fullcols { float : left; width : 100%; position : relative; }
.page-id-65941 .summer_icons.fullcols { border-top: 2px dotted #755f9a; margin-top: 5px !important; }

.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.page-id-65941 #masthead, .page-id-65941 .banner, .page-id-65941 .title-holder, .page-id-65941 .and-theres-more-at-klay, .page-id-65941 #colophon, .page-id-65941 .copyrights, .page-id-65941 .blue-bg, .page-id-65941 .important-menu-items  { display : none !important; }
.page-id-65941 .fullcols { float : left; width : 100%; position : relative; paddding :0; margin : 0 auto; }

.page-id-65941 .summer_icons ul { display: inline-block;  float: none;  margin: 0 auto 3% auto;  padding: 0; text-align: center; width: 100%; }
.page-id-65941 .summer_icons ul li { display: inline-block;  float: none;  margin: 0 auto; padding: 0 2em; text-align: center; }
.page-id-65941 .summer_icons ul li img { padding :0; }
.page-id-65941 .summer_icons ul li.worktext img:first-child { padding :2em 0 0 0; }
.page-id-65941 .summer_icons ul li.worktext img:last-child { }
.summer_icons h2 {  font-family: 'GothamRoundedMedium';   font-size : 20px; color : #666362; }
.summer_icons h3 { font-family: 'GothamRoundedBold_0'; font-weight : bold; font-size : 25px; color : #e9546e; }


.page-id-65941 h1 { font-family: 'GothamRoundedBold_0'; font-weight : bold; font-size : 25px; color : #000007; }

.page-id-65941 .expsummer h2 { font-family: 'GothamRoundedBold_0';  color: #333333;  font-size: 25px; margin: 0 auto;  padding: 15px 0 0 90px; width: auto; }
.page-id-65941 .expsummer h3 {  font-family: 'GothamRoundedMedium'; color: #333333;  font-size: 17px; margin: 0 auto;  padding: 15px 0 0 90px; width: auto; }


.page-id-65941 .klay_way { }
.page-id-65941 .summer_foot1 p { float: left; line-height: 40px; margin: 0; }
.page-id-65941 .summer_foot1 p a { font-family: 'GothamRoundedBold_0';   color: #333333;  float: left; font-size: 23px;  padding: 0 0 0 25px;  width: auto; text-align : center; }
.page-id-65941 .summer_foot1 p a.ndhref { float: none; padding: 0px 0px 0px 2em; }


.page-id-65941 .summer_foot2 p { float: left; line-height: 40px; margin: 0; }
.page-id-65941 .summer_foot2 p a { font-family: 'GothamRoundedBold_0';   color: #333333;  float: left; font-size: 23px;  padding: 0 0 0 6px;  width: auto; }

.summer_holder { width: 100%; height : auto;  background : url("<?php echo get_template_directory_uri();?>/images/Grufullo-lp-bg.jpg") repeat; width : 100%;}
.summer_headers img { width  :100%; height : auto; }


@media only screen and (min-width: 1024px)
{
.page-id-65941 .summer_icons ul li { }	
.page-id-65941 .summer_icons ul li:first-child { padding-left : 0; }	
.page-id-65941 .summer_icons ul li:last-child { padding-right : 0; }
.page-id-65941 .summer_icons ul { margin: 3% auto;}
.summer_holder iframe { min-height : 315px; height : auto; }
}


@media only screen and (max-width: 1023px)
{
.page-id-65941 .summer_icons ul { margin: 0 auto;}
.page-id-65941 .summer_icons ul li { width: 100%; margin : 5% auto;  }		
}

@media only screen and (min-width: 767px)
{


.page-id-65941 .klayway_1:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-parents.png") no-repeat left center; content: " ";  float: left; width: 84px;  height: 86px; }
.page-id-65941 .klayway_2:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-childrens.png") no-repeat left center; content: " ";  float: left; width: 76px;  height: 76px; }
.page-id-65941 .klayway_3:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-centers.png") no-repeat left center; content: " ";  float: left; width: 76px;  height: 86px; }
.page-id-65941 .klayway_4:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-locateus.png") no-repeat left center; content: " ";  float: left; width: 75px;  height: 81px; }
.page-id-65941 .summer_header { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-banner.jpg") no-repeat center center; }
.summer_holder {  float : left;  position: relative;  width: 100%;  }	

.page-id-65941 .summer_foot1 p.webs:before { background : url("<?php echo get_template_directory_uri();?>/images/sumercamp-webs.png") no-repeat left center; content: " ";  float: left; width: 42px;  height: 42px;  }
.page-id-65941 .summer_foot2 p.callings:before { background : url("<?php echo get_template_directory_uri();?>/images/call-summercamps.png") no-repeat left center; content: " ";  float: left; width: 45px;  height: 45px;  }
.show_xs { display : none; }
.show_md { display : block; }
.show_header_xs { display : none; }
}

@media only screen and (max-width: 767px)
{
.pd_20{padding-bottom:10%}
.page-id-65941 .klayway_1:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-parents.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 86px; }
.page-id-65941 .klayway_2:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-childrens.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 76px; }
.page-id-65941 .klayway_3:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-centers.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 86px; }
.page-id-65941 .klayway_4:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-locateus.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 81px; }
.page-id-65941 .expsummer h2, .page-id-65941 .expsummer h3 {  padding: 5px 0 0 0 !important; text-align : center; }
.page-id-65941 .summer_header { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-banner.jpg") no-repeat center center; }	
.amp-col-md-3 { width : 100%; }
.show_xs { display: inline-block;  float: none; text-align: center; }
.show_header_xs { display: inline-block; text-align: center; width : 100%; }
.page-id-64657 .social_icons ul li { paddding : 0 0.2em 0 0; }
.page-id-65941 .summer_footer_holder a {
    float: none !important;
}
.page-id-65941 .summer_foot1 p a {  font-size: 16px !important; padding: 0 0 0 10px !important; float : none !important; }
.page-id-65941 .summer_foot2 .callings a { float : none !important; }

.show_md { display : none; }

}


</style>

<script type="text/javascript">
piAId = '564842';
piCId = '85528';
piHostname = 'pi.pardot.com';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script>

<?php
endwhile;
get_footer();
