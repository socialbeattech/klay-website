<?php
/**
 * Template Name: Preschools 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();

?>
				
<?php if(get_field('gtm_head')): the_field('gtm_head'); endif; ?>
<?php if(get_field('gtm_footer')): the_field('gtm_footer'); endif; ?>				
				
<section class="fullcols banner_holder p-0 m-0">
	<?php if( have_rows('slider') ): ?>
	<div class="fullcols desktop_slider">
		<div id="preschool_lp" class="swiper-container">
			<div class="swiper-wrapper">
				<?php while ( have_rows('slider') ) : the_row(); ?>
				<?php $d_banners =  get_sub_field('sliding_banner'); ?>
					<div class="swiper-slide">
						<img src="<?php echo $d_banners['url']; ?>" alt="<?php echo $d_banners['alt']; ?>" width="<?php echo $d_banners['width']; ?>" height="<?php echo $d_banners['height']; ?>">
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="form_holder" id="register_plp">
		<h1 class="col-12 col-sm-12 text-left fs-20"><?php the_field('form_title'); ?></h1>	
		<?php if($current_pageid=='64989'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-23/51mny?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
		<?php if($current_pageid=='65406'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-26/51w1r?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
		<?php if($current_pageid=='65398'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-26/51w1w?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
		<?php if($current_pageid=='65396'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-26/51w21?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
		<?php if($current_pageid=='65404'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-26/51w23?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
		<?php if($current_pageid=='65400'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-26/51w2c?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
		<?php if($current_pageid=='65402'): ?> <iframe src="http://go.pardot.com/l/563842/2019-03-26/51w2h?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> <?php endif; ?>
	</div>
</section>

<section class="fullcols first_programs">
	<div class="container">
		<h1 class="col-12 col-sm-12 col-md-12 text-center fs-xs-30 fs-40 GothamRoundedBold_0 orange px-0 mx-0"><?php the_field('preschool_title'); ?></h1>
		<div class="tagslist">
			<div class="fullcols allprograms pt-3 mb-1">
				<div class="float-left col-12 col-sm-12 col-md-6 px-0 mx-0"><h2 class="GothamRoundedBold_0 fs-20 fs-xs-16"><?php the_field('preschool_caption1'); ?></h2></div>
				<div class="float-left col-12 col-sm-12 col-md-6 px-0 mx-0"><h2 class="GothamRoundedBold_0 fs-20 fs-xs-16"><?php the_field('preschool_caption2'); ?></h2></div>
			</div>
			<div class="fullcols allprograms">
				<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0"><h2 class="GothamRoundedBold_0 fs-20 fs-xs-16 black"><?php the_field('preschool_caption3'); ?></h2></div>
				<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0"><h2 class="GothamRoundedBold_0 fs-20 fs-xs-16 black"><?php the_field('preschool_caption4'); ?></h2></div>
				<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0"><h2 class="GothamRoundedBold_0 fs-20 fs-xs-16 black"><?php the_field('preschool_caption5'); ?></h2></div>
			</div>
			<?php $ppmd1 =  get_sub_field('pp_desktop_banner'); $ppxs1 =  get_sub_field('pp_mobile_banner1'); $ppxs2 =  get_sub_field('pp_mobile_banner2'); ?>
			<div class="brain_gamers fullcols py-3 d-none d-sm-none d-md-block text-center">				
				<img src="<?php echo $ppmd1['utl']; ?>" alt="<?php echo $ppmd1['alt']; ?>" class="img-fluid" width="<?php echo $ppmd1['width']; ?>" height="<?php echo $ppmd1['height']; ?>">
			</div>
			<div class="brain_gamers fullcols d-block d-sm-block d-md-none text-center">				
				<img src="<?php echo $ppxs1['utl']; ?>" alt="<?php echo $ppxs1['alt']; ?>" class="img-fluid" width="<?php echo $ppxs1['width']; ?>" height="<?php echo $ppxs1['height']; ?>">
			</div>
			<div class="brain_gamers fullcols d-block d-sm-block d-md-none text-center">	
				<img src="<?php echo $ppxs2['utl']; ?>" alt="<?php echo $ppxs2['alt']; ?>" class="img-fluid" width="<?php echo $ppxs2['width']; ?>" height="<?php echo $ppxs2['height']; ?>">
			</div>			
			<p class="col-12 col-sm-12 float-left p-0 m-0 GothamRoundedBook text-justify fs-16 black pt-2"><?php the_field('pp_paras'); ?></p>			
		</div>
			
	</div>
</section>

<section class="fullcols curriculum">
	<div class="container">
		<div class="curriculum_content">
			<h1 class="col-12 col-sm-12 col-md-12 text-center fs-xs-30 fs-40 GothamRoundedBold_0 orange px-0 mx-0"><?php the_field('curriculum_title'); ?></h1>	
			<p class="col-12 col-sm-12 float-left p-0 m-0 GothamRoundedBook text-justify fs-16 black pt-2"><?php the_field('curriculum_paras'); ?></p>				
			<?php if( have_rows('curriculum_innerpage') ): ?> 			
				<div class="fullcols curriculum_programs py-3">					
					<?php while ( have_rows('curriculum_innerpage') ) : the_row(); ?>
						<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0 text-center">
								<h3 class="float-left col-12 col-sm-12 col-md-12 p-0 m-0 GothamRoundedBook fs-18 text-center pt-3"><?php the_sub_field('learners_title'); ?></h3>
								<span class="float-left col-12 col-sm-12 col-md-12 p-0 m-0 GothamRoundedBook fs-13 text-center pb-3"><?php the_sub_field('learners_descs'); ?></span>
								<?php $curriculum_lb =  get_sub_field('learners_banner'); ?>
								<img src="<?php echo $curriculum_lb['url']; ?>" alt="<?php echo $curriculum_lb['alt']; ?>" class="img-fluid" width="<?php echo $curriculum_lb['width']; ?>" height="<?php echo $curriculum_lb['height']; ?>">
						</div>
					<?php endwhile; ?>			
				</div>	
			<?php endif; ?>	
		</div>				
	</div>
</section>






<section class="fullcols learning_centers">
	<div class="container">
		<h1 class="col-12 col-sm-12 col-md-12 text-center fs-xs-30 fs-40 GothamRoundedBold_0 orange px-0 mx-0"><?php the_field('learning_centers_title'); ?></h1>	
		<p class="col-12 col-sm-12 float-left p-0 m-0 GothamRoundedBook text-justify fs-16 black pt-2"><?php the_field('learning_centers_paras'); ?></p>
		<?php if( have_rows('learning_centers_innerpage') ): ?> 		
		<div class="fullcols learning_centers_1 py-3 mt-3">							
			<?php while ( have_rows('learning_centers_innerpage') ) : the_row(); ?>
			<?php $lcsb = get_sub_field('learners_center_banner'); ?>
				<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0 mb-2 pb-4 text-center">
					<h3><?php the_sub_field('learners_center_title'); ?></h3>
					<img src="<?php echo $lcsb['url']; ?>" alt="<?php echo $lcsb['alt']; ?>" class="img-fluid text-center" width="<?php echo $lcsb['width']; ?>" height="<?php echo $lcsb['height']; ?>">
				</div>
			<?php endwhile; ?>				
		</div>
		<?php endif; ?>	
	</div>
</section>




<section class="fullcols advantage">
	<div class="container">
		<div class="fullcols advantage_content py-3">			
			<h1 class="col-12 col-sm-12 col-md-12 text-center fs-xs-30 fs-40 GothamRoundedBold_0 orange px-0 mx-0"><?php the_field('klay_advs_title'); ?></h1>								
			<?php if( have_rows('klay_advs_innerpage') ): ?> 	
			<div class="fullcols advantage_programs text-center py-4">							
				<?php while ( have_rows('klay_advs_innerpage') ) : the_row(); ?>
				<?php $kdsb = get_sub_field('klay_advs_banner'); ?>
				<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0 pb-4 mb-3 text-center">
					<img src="<?php echo $kdsb['url']; ?>" alt="<?php echo $kdsb['alt']; ?>" class="img-fluid text-center" width="<?php echo $kdsb['width']; ?>" height="<?php echo $kdsb['height']; ?>" />
					<h3><?php the_sub_field('klay_advs_title'); ?></h3>					
				</div>
				<?php endwhile; ?>						
			</div>
			<?php endif; ?>				
		</div>
	</div>
</section>

<section class="fullcols comeover py-4">
	<div class="container">	
		<div class="fullcols advantage_content py-3">					
			<h1 class="col-12 col-sm-12 col-md-12 text-center fs-xs-30 fs-40 GothamRoundedBold_0 orange px-0 mx-0"><?php the_field('know_us_better_title'); ?></h1>								
			<?php if( have_rows('know_us_better_innerpage') ): ?> 	
			<div class="fullcols advantage_programs text-center py-4">							
				<?php while ( have_rows('know_us_better_innerpage') ) : the_row(); ?>
				<?php $kubb = get_sub_field('know_us_better_sub_banner'); ?>
				<div class="float-left col-12 col-sm-12 col-md-4 px-0 mx-0 pb-4 mb-3 text-center">
					<img src="<?php echo $kubb['url']; ?>" alt="<?php echo $kubb['alt']; ?>" class="img-fluid text-center" width="<?php echo $kubb['width']; ?>" height="<?php echo $kubb['height']; ?>" />
					<h3><?php the_sub_field('know_us_better_sub_title'); ?></h3>					
				</div>
				<?php endwhile; ?>						
			</div>
			<?php endif; ?>							
		</div>
	</div>
	<?php if( have_rows('social_media') ): ?>
		<div class="social-icons">
			<ul>
			  <?php while ( have_rows('social_media') ) : the_row();  ?>
			  <?php $social_icons = get_sub_field('social_icons'); ?>
				 <li><a href="<?php the_sub_field('social_icons_href');?>"><?php if(!empty($social_icons)): ?><img src="<?php echo $social_icons["url"];?>" alt="banner" width="60" height="60" class="img-fluid"  /><?php endif; ?></a></li>
				<?php endwhile; ?>
			</ul>		
		</div>
	<?php endif; ?>		
</section>


<section class="yellow-bg eventslp_sticky d-block d-md-none fs-18 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">Call</a></div>
			<div class="col"> <a href="#register_plp">Enquire Now</a></div>
		</div>
	</div>
</section>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<style type="text/css">
section { padding : 5% 0 !important; }
.banner_holder img { width :100%; height : auto; }
.form_holder { background: rgba(40, 198, 225, 0.7) none repeat scroll 0 0; }
.comeover {  background: rgb(248, 217, 39); }
.social-icons ul {  display: inline-block;  float: none; margin: 0 5px; list-style-type : none; }
.social-icons { float: left;    margin: 0 auto;    padding: 30px 0;    text-align: center;    width: 100%; }
.social-icons img { width : auto !important;}
.advantage { background: #01bac6; }
.advantage_content {  background: rgb(255, 255, 255);  border-radius: 10px;  float: left;  padding: 4%;  width: 100%; }
.advantage_programs { text-align: center;  width: 100%; }
.advantage_programs h3 { color: black;   float: left;   font-family: "GothamRoundedBook";  font-size: 18px;    margin: 0 auto;   padding: 20px 0 0;  text-align: center;  width: 100%; }
.learning_centers_1, .learning_centers_2 {  clear: both;  float: left;  margin: 0 auto;  padding: 1rem 0; width: 100%; }
.learning_centers div h3 { background: rgb(248, 217, 40);  color: rgb(64, 64, 65);  display: inline-block;  float: none;
  font-family: "GothamRoundedBold_0";  font-size: 14px;  height: auto; margin: 0 auto; max-width: 270px; padding: 5px 0; text-align: center;  width: 100%; }
.learning_centers img { text-align: center; }
.curriculum_content {  background: rgb(255, 255, 255);  border-radius: 10px; float: left;  padding: 4%; width: 100%; }
.curriculum_programs h3 { color: rgb(224, 113, 62);  margin: 0 auto;  padding: 20px 0 0;  }
.curriculum_programs span { color: rgb(224, 113, 62);  padding: 0 0 20px; }
.curriculum { background: rgb(248, 217, 39); }
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
.page-id-<?php echo $current_pageid; ?> .fullcols { float : left; width : 100%; position : relative; }
.GothamRoundedBold_0 { font-family: 'GothamRoundedBold_0'; }
.GothamRoundedBook { font-family: 'GothamRoundedBook'; }
.orange { color: rgb(238, 121, 69); }
.tagslist { float: none; margin: 0 auto;  width: 100%; }
.tagslist h2 { background: rgb(238, 238, 238); color: rgb(51, 51, 51);  float: left; margin: 0 auto;  padding: 10px 0;  text-align: center;  width: 99%; }
.tagslist span { color: rgb(51, 51, 51);  font-style: italic;  padding: 0 16px; }
.black { color : #333; }
.form_holder h1 { color: #fff; float: left;  font-family: 'GothamRoundedBold_0';  margin: 0 auto; padding: 5% 0 0 3%; width: 100%; }

@media only screen and (min-width: 1200px)
{
.banner_holder img { height : auto; }	
}

@media only screen and (min-width: 767px)
{
.tagslist {max-width: 950px; }
.form_holder { background: rgba(40, 198, 225, 0.7) none repeat scroll 0 0;  border-radius: 10px;  left: auto;   max-width: 450px;  opacity: 0.89;  padding: 16px;   position: absolute;  right: 30px;   top: 10%;  z-index: 999; width : 100%; }
.banner_holder { height : 920px; }
}

@media only screen and (max-width: 767px)
{
.tagslist { max-width: 100%;  }
.tagslist h2 { border-bottom: 2px solid #ffffff; }
.form_holder { background: rgba(40, 198, 225, 0.7) none repeat scroll 0 0;  border-radius: 10px; max-width: 100%;  opacity: 0.89;  padding: 16px;   position: relative; }
.social-icons ul {  padding: 0 0 10%; }
.swiper-wrapper { height : auto !important; } 
.swiper-slide { height : auto !important; }
}
</style>
<?php
endwhile;
get_footer();
