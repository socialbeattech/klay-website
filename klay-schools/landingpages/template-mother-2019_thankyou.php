<?php
/**
 * Template Name: Summer Mother thankyou 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>

<style type="text/css">
	.mother_sec h2{font-size: 33px;}
	.mother_sec h3{font-size: 33px;}
	.mother_sec p{font-size: 23px;}
	img.icon_mo1 {
    width: 17px;
    margin-top: 25px;
}
img.icon_mo2 {
    width: 30px;
    margin-top: 10px;
    margin-left: 10px;
}
img.icon_mo3 {
    width: 30px;
    margin-right: 35px;
}
img.icon_mo4 {
    width: 40px;
    margin-left: 15px;
    margin-top: -25px;
}
.back_black {
    background: #000;
    font-size: 16px;
    color: #fff;
    text-align: center;
    padding-right: 41px;
    border-radius: 15px;
}
img.icon_fe {
    width: 40px;
}
.border_dashed {
    border-bottom: 2px dashed #fff;
    margin-left: 2%;
    padding: 5% 0 3%;margin-right: 2%;
}
.border_dashed1 {
 
    padding: 3% 0 3%;
}

.price_summer1 {
    font-size: 24px;
    margin-top: 10%;
}
.price_summer {
    background: #ea516d;margin-top:-5%;
}
.price_summer:before {
    content: url(https://www.klayschools.com/wp-content/themes/klay-schools/images/mother/icon_before11.png);
    position: relative;
    z-index: 100000;
    left: -64.2%;
    top: -2px;
}
section.mother_sec1 {
    padding: 5% 0;
}
ul.city_mother li {
    display: inline-block;
    padding: 0px 50px;
    border-right: 1px solid;font-size:26px;
}
ul.city_mother li span{font-size:21px;
}
ul.city_mother {
    text-align: center;
}
h2.team_head {
    background: #c11728;
    position: absolute;
    top: 50px;
    left: 25%;
    color: #fff;
    font-size: 23px;
    padding: 4px 10px;
    border-radius: 5px;
}
.pb-4, .py-4 {
    width: 100%;
}
section.mother_sec {
    padding: 4% 0;
}
.summer_white {
    background: #f6f6fb;
    margin: 0px 15%;
    border-radius: 15px 15px 0px 0;    padding: 50px;
}

.white_back,body{background: #f6f6fb;}
img.icon_fe.icon_fe1 {
    padding-bottom: 10px;
}
img.icon_fe.icon_fe2 {
    padding-bottom: 25px;
}
img.icon_fe.icon_fe3 {
    padding-bottom: 17px;
}
section.yellow-bg.eventslp_sticky.d-block.d-md-none.fs-16.gotham-rounded-medium.stricky_menu {
    display: none !important;
}
</style>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WM5LSXF');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WM5LSXF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> 

<section class="summer_headers fullcols1">
	<div class="fullcols1"  id="register">
		<img src="<?php echo get_template_directory_uri();?>/images/mommy_lp.jpg" class="img-fluid show_md" alt="Banner" width="1366" height="825" />	
		<img src="<?php echo get_template_directory_uri();?>/images/mother/mother_mobile.jpg" class="img-fluid show_xs" alt="Banner" width="480" height="459" />	
		<div class="summer_holder1">
		<div class="summer_white">				
			<div class="container">				
				<h1 class="col-12 col-sm-12 text-left fs-20">Thank you for your interest! Our representative will get in touch with you shortly.</h1>
						
			</div>	
			</div>				
		</div>					
	</div>	
</section>

<section class="mother_sec white_back">
	<div class="container sec1_padd">
		<h2 class="gotham-rounded-medium text-center"><img  class="icon_mo1" src="<?php echo get_template_directory_uri();?>/images/mother/mo_icon1.png">This Mother’s Day,<img  class="icon_mo2" src="<?php echo get_template_directory_uri();?>/images/mother/mo_icon2.png"></h2>
		<h3 class="gotham-rounded-medium text-center"><img  class="icon_mo3" src="<?php echo get_template_directory_uri();?>/images/mother/mo_icon3.png">unwind, relax and let your hair down at KLAY.<img  class="icon_mo4" src="<?php echo get_template_directory_uri();?>/images/mother/mo_icon4.png"></h3>
		<p class="gotham-rounded-book text-center">Inviting all mommies, daddies and lil’ ones to an exciting fest that celebrates moms on their special day. <span class="gotham-rounded-medium">Join us with your little ones for a day of unlimited fun and joyful memories.</span></p>
	</div>
</section>
<section class="mother_sec1 white_back">
	<div class="container">
		<h2 class="team_head gotham-rounded-book">Team up with your little ones for</h2>
		<div class="back_black">
		<div class="row">
			<div class="col-md-9">
				<div class="row border_dashed">
					<div class="col-md-3 hand_font font_black">
						<img  class="icon_fe icon_fe1" src="<?php echo get_template_directory_uri();?>/images/mother/fe_icon1.png"><br>
						Mama Baby Dance
					</div>
					<div class="col-md-3 hand_font font_black">
						<img  class="icon_fe icon_fe2" src="<?php echo get_template_directory_uri();?>/images/mother/fe_icon2.png"><br>
						Pamper Time
					</div>
					<div class="col-md-3 hand_font font_black">
						<img  class="icon_fe icon_fe3" src="<?php echo get_template_directory_uri();?>/images/mother/fe_icon3.png"><br>
						Craft a Card
					</div>
					<div class="col-md-3 hand_font font_black">
						<img  class="icon_fe icon_fe4" src="<?php echo get_template_directory_uri();?>/images/mother/fe_icon4.png"><br>
						Plant a Tree
					</div>
				</div>
				<div class="row border_dashed1">
					<div class="col-md-4 hand_font font_black offset-md-2">
						<img  class="icon_fe" src="<?php echo get_template_directory_uri();?>/images/mother/fe_icon5.png"><br>
						Story time with Mom
					</div>
					<div class="col-md-4 hand_font font_black">
						<img  class="icon_fe" src="<?php echo get_template_directory_uri();?>/images/mother/fe_icon6.png"><br>
						Cookie Decorations
					</div>
				</div>
			</div>
			<div class="col-md-3 gotham-rounded-medium price_summer">
				<div class="price_summer1">
				KLAY Child:<br>
₹ 100<br><br>
Others: <br>
₹ 200
</div>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="city_sec white_back">
	<div class="container">
		<ul class="city_mother gotham-rounded-medium">
			<li>Mumbai <br><span class="gotham-rounded-book">25th May</span></li>
			<li>Pune <br><span class="gotham-rounded-book">25th May</span></li>
			<li>NCR <br><span class="gotham-rounded-book">18th May</span></li>
			<li>Chennai <br><span class="gotham-rounded-book">11th May</span></li>
		</ul>
	</div>
</section>



<section class="expsummer fullcols white_back">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="expsummer_holder py-4 ">
				
				<div class="klay_way col-12 col-sm-12 float-left px-0 py-3">
					<div class="klayway_1 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>17000+</h2>
						<h3>Happy Parents</h3>
					</div>
					<div class="klayway_2 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>8500+</h2>
						<h3>Happy Children</h3>
					</div>
					<div class="klayway_3 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>140+</h2>
						<h3>Centres</h3>
					</div>
					<div class="klayway_4 col-12 col-sm-6 col-md-3 float-left p-0 mx-0 mb-3 mb-sm-3 mb-md-0">
						<h2>7+</h2>
						<h3>Cities</h3>
					</div>
				</div>
			</div>			
		</div>		
	</div>
</section>

<section class="summer_footer fullcols pd_20 white_back">
	<div class="container">
		<div class="row p-0 m-0">
			<div class="summer_footer_holder col-12 col-sm-12 col-md-12 py-0 pb-3 m-0 px-0">
				<div class="summer_foot1 col-12 col-sm-12 col-md-6 float-left p-0 m-0">
					<p class="webs text-center text-sm-center text-md-left col-12 col-sm-12 p-0 m-0"><img src="<?php echo get_template_directory_uri();?>/images/sumercamp-webs.png" class="img-fluid show_xs" alt="Banner" width="42" height="42" /><a href="https://www.klayschools.com">www.klayschools.com</a>  |  <a href="mailto:info@klayschools.com">info@klayschools.com</a></p>
				</div>
				<div class="summer_foot2 col-12 col-sm-12 col-md-6 float-left p-0 m-0">
					<p class="callings text-center text-sm-center text-md-left col-12 col-sm-12 p-0 m-0"><img src="<?php echo get_template_directory_uri();?>/images/call-summercamps.png" class="img-fluid show_xs" alt="Banner" width="42" height="42" /><a href="tel:+917676708888">76767 08888</a></p>
				</div>
			</div>			
		</div>		
	</div>
</section>


<section class="yellow-bg eventslp_sticky d-block d-md-none fs-16 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">CALL</a></div>
			<div class="col"> <a href="#register">Admissions</a></div>
		</div>
	</div>
</section>


<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<style type="text/css">
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.page-id-65901 #masthead, .page-id-65901 .banner, .page-id-65901 .title-holder, .page-id-65901 .and-theres-more-at-klay, .page-id-65901 #colophon, .page-id-65901 .copyrights, .page-id-65901 .blue-bg, .page-id-65901 .important-menu-items  { display : none !important; }
.page-id-65901 .fullcols { float : left; width : 100%; position : relative; paddding :0; margin : 0 auto; }
.page-id-65901 .summer_icons { }
.page-id-65901 .summer_icons ul { display: inline-block;  float: none;  margin: 3% auto;  padding: 0; text-align: center; width: 100%; }
.page-id-65901 .summer_icons ul li { display: inline-block;  float: none;  margin: 0 auto; padding: 0 2em; text-align: center; }

.page-id-65901 h1 { font-family: 'GothamRoundedBold_0'; font-weight : bold; font-size : 25px; color : #000007; }
.page-id-65901 .creativity_holder p { font-family: 'GothamRoundedBook';  font-size : 16px; color : #323335;  }
.page-id-65901 .expsummer h2 { font-family: 'GothamRoundedBold_0';  color: #333333;  font-size: 25px; margin: 0 auto;  padding: 15px 0 0 90px; width: auto; }
.page-id-65901 .expsummer h3 {  font-family: 'GothamRoundedMedium'; color: #333333;  font-size: 17px; margin: 0 auto;  padding: 15px 0 0 90px; width: auto; }


.page-id-65901 .klay_way { border-bottom : 1px solid #d6d6d6; }
.page-id-65901 .summer_foot1 p { float: left; line-height: 40px; margin: 0; }
.page-id-65901 .summer_foot1 p a { font-family: 'GothamRoundedBold_0';   color: #333333;  float: none; font-size: 17px;  padding: 0 0 0 6px;  width: auto; }


.page-id-65901 .summer_foot2 p { float: left; line-height: 40px; margin: 0; }
.page-id-65901 .summer_foot2 p a { font-family: 'GothamRoundedBold_0';   color: #333333;  float: none; font-size: 17px;  padding: 0 0 0 6px;  width: auto; }




.timetable { background : #f9c003; }
.timetable_holder { }


.timetable_holder h2 { font-family: 'GothamRoundedBook';  font-size : 16px; color : #000000; }
.timetable_holder h3 { font-family: 'GothamRoundedBook';  font-size : 16px; color : #000000; }


.summer_holder1 { width: 100%; height : 200px;  background : #f7be19; width : 100%;}
.summer_headers img { width  :100%; height : auto; }


@media only screen and (min-width: 1024px)
{
.page-id-65901 .summer_icons ul li { width: 20%; }	
.page-id-65901 .summer_icons ul li:first-child { padding-left : 0; }	
.page-id-65901 .summer_icons ul li:last-child { padding-right : 0; }
.page-id-65901 .summer_icons ul { margin: 3% auto;}
.summer_holder iframe { min-height : 315px; height : auto; }
}

@media only screen and (max-width: 1023px)
{
.page-id-65901 .summer_icons ul { margin: 0 auto;}
.page-id-65901 .summer_icons ul li { width: 100%; margin : 5% auto;  }		
}

@media only screen and (min-width: 767px)
{


.page-id-65901 .klayway_1:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-parents.png") no-repeat left center; content: " ";  float: left; width: 84px;  height: 86px; }
.page-id-65901 .klayway_2:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-childrens.png") no-repeat left center; content: " ";  float: left; width: 76px;  height: 76px; }
.page-id-65901 .klayway_3:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-centers.png") no-repeat left center; content: " ";  float: left; width: 76px;  height: 86px; }
.page-id-65901 .klayway_4:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-locateus.png") no-repeat left center; content: " ";  float: left; width: 75px;  height: 81px; }
.page-id-65901 .summer_header { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-banner.jpg") no-repeat center center; }
.summer_holder {  float : left;  position: relative;  width: 100%;  }	
.timetables { background : url("<?php echo get_template_directory_uri();?>/images/timetable_divider.png") no-repeat right center; }
.timetables:last-child { background : none !important; }
.page-id-65901 .summer_foot1 p.webs:before { background : url("<?php echo get_template_directory_uri();?>/images/sumercamp-webs.png") no-repeat left center; content: " ";  float: left; width: 42px;  height: 42px;  }
.page-id-65901 .summer_foot2 p.callings:before { background : url("<?php echo get_template_directory_uri();?>/images/call-summercamps.png") no-repeat left center; content: " ";  float: left; width: 45px;  height: 45px;  }
.show_xs { display : none; }
.show_md { display : block; }
.show_header_xs { display : none; }
}

@media only screen and (max-width: 767px)
{

	.summer_holder1 {height : auto;}
	ul.city_mother li {
    display: block;
    padding: 0;
    border-right: 0px solid;
    font-size: 26px;
    padding-bottom: 25px;
}
ul.city_mother {
    text-align: center;
    padding: 0;
    margin: 0;
}
h2.team_head {
    background: #c11728;
    position: relative;
    top: 0;
    left: 0;
    color: #fff;
    font-size: 18px;
    padding: 4px 10px;
    border-radius: 5px;
    margin-bottom: 0;
    width: 88%;
    margin: 0 auto;
    text-align: center;
}
.back_black {
    padding-right: 0;
    
}
.font_black {
    padding-bottom: 40px;
}
img.icon_fe.icon_fe1 {
    padding-bottom: 0px;
}
img.icon_fe.icon_fe2 {
    padding-bottom: 0px;
}
img.icon_fe.icon_fe3 {
    padding-bottom: 0px;
}
.border_dashed {
    border-bottom: 0px dashed #fff;
    margin-left: 0%;
    padding: 0;
    margin-right: 0;
    padding-top: 7%;
}
.pd_20{padding-bottom:10%}
.page-id-65901 .klayway_1:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-parents.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 86px; }
.page-id-65901 .klayway_2:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-childrens.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 76px; }
.page-id-65901 .klayway_3:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-centers.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 86px; }
.page-id-65901 .klayway_4:before { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-locateus.png") no-repeat center center; content: " ";  display: inline-block;   float: none;  height: 86px;   text-align: center;  width: 100%;  height: 81px; }
.page-id-65901 .expsummer h2, .page-id-65901 .expsummer h3 {  padding: 5px 0 0 0 !important; text-align : center; }
.page-id-65901 .summer_header { background : url("<?php echo get_template_directory_uri();?>/images/summercamp-banner.jpg") no-repeat center center; }	
.amp-col-md-3 { width : 100%; }
.summer_holder { background: #34bddf; } 
.show_xs { display: inline-block;  float: none; text-align: center; }
.show_header_xs { display: inline-block; text-align: center; width : 100%; }
.timetables:after { background : url("<?php echo get_template_directory_uri();?>/images/timetable_divider-mobile.png") no-repeat center center;  content: " ";  display: inline-block;  float: none;  height: 10px;  padding: 15% 0 0; text-align: center; width: 100%; }
.page-id-64657 .social_icons ul li { paddding : 0 0.2em 0 0; }
.show_md { display : none; }
.price_summer {
    background: #ea516d;
    margin-top: -5%;
    width: 50%;
    margin: 0 auto;
    padding-bottom: 30px;
}
.price_summer:before{display: none;}
.summer_white {
   
    margin: 0 4%;
   
}
}

@media only screen and (max-width: 480px)
{
mother_sec h3 {
    font-size: 18px;
}
.mother_sec h2 {
    font-size: 22px;
}
.mother_sec p {
    font-size: 16px;
}
.price_summer1 {
    font-size: 18px;
    margin-top: 15%;
}
}
</style>

<script type="text/javascript">
piAId = '564842';
piCId = '85528';
piHostname = 'pi.pardot.com';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script>

<?php
endwhile;
get_footer();
