<?php
/**
 * Template Name: DayCare Mumbai 
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
<?php if(get_field('gtm_head')): the_field('gtm_head'); endif; ?>
<?php if(get_field('gtm_body')): the_field('gtm_body'); endif; ?>
<section class="fullcols banner_holder">
	<?php $lg_banner = get_field('large_banner'); $xs_banner = get_field('small_mobile_banner'); ?>
	<?php if(!empty($lg_banner)): ?><img src="<?php echo $lg_banner["url"];?>" alt="banner" width="<?php echo $lg_banner["width"];?>" height="<?php echo $lg_banner["height"];?>"  class="img-fluid d-none d-sm-none d-md-block"  /><?php endif; ?>
	<?php if(!empty($xs_banner)): ?><img src="<?php echo $xs_banner["url"];?>" alt="banner" width="<?php echo $xs_banner["width"];?>" height="<?php echo $xs_banner["height"];?>"  class="img-fluid d-block d-sm-block d-md-none"  /><?php endif; ?>
	<div class="form_holder" id="register">
		<h1 class="col-12 col-sm-12 text-left fs-20"><?php the_field('form_heading1');?></h1>
		<h2 class="col-12 col-sm-12 text-left fs-15"><?php the_field('form_heading2');?></h2>
		<h3 class="col-12 col-sm-12 text-left fs-14 mt-3"><?php the_field('form_heading3');?></h3>	
		<iframe src="https://go.pardot.com/l/563842/2019-03-26/51vvc?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
		<h4 class="col-12 col-sm-12 text-left fs-18"><?php the_field('form_last_desc');?></h4>
	</div>
</section>



<section class="fullcols titless_holder yellow_title py-4 headicons text-center">
	<div class="container">
		<?php $s1_icons = get_field('sections_1_icons'); ?>
		<?php if(!empty($s1_icons)): ?><img src="<?php echo $s1_icons["url"];?>" alt="banner" width="<?php echo $s1_icons["width"];?>" height="<?php echo $s1_icons["height"];?>"  class="img-fluid"  /><?php endif; ?><h1 class="fs-23 fs-xs-20 black"><?php the_field('sections_1_main_title');?></br><span class="fs-16 fs-xs-16 black"><?php the_field('sections_1_sub_title');?></span></h1>
	</div>
</section>

<section class="fullcols prgrams_holder py-4">
	<div class="container">
		<?php if( have_rows('programs') ): ?>
		<div class="fullcols prgrams_inner">
			<?php while ( have_rows('programs') ) : the_row(); ?>
			<?php $ph1 =  get_sub_field('programs_headings'); $ph2 =  get_sub_field('programs_sub_heading1'); $ph3 =  get_sub_field('programs_sub_heading2'); $phicons =  get_sub_field('programs_headings_icons'); $ph3 =  get_sub_field('programs_sub_heading3'); $ph4 =  get_sub_field('programs_desc'); ?>
				<div class="col-12 col-sm-12 col-md-6 float-left pt-0 pt-sm-0 pt-md-4">
					<?php if(!empty($ph1)): ?><h1 class="fs-20 fs-xs-18 p-0 m-0"><?php echo $ph1; ?></h1><?php endif; ?>
					<?php if(!empty($ph2)): ?><h2 class="fs-13 fs-xs-11 p-0 m-0"><?php echo $ph2; ?></h2><?php endif; ?>
					<?php if(!empty($phicons)): ?><img src="<?php echo $phicons['url']; ?>" class="img-fluid" alt="Banner" width="<?php echo $phicons['width']; ?>" height="<?php echo $phicons['height']; ?>" /><?php endif; ?>
					<?php if(!empty($ph3)): ?><h3 class="fs-13 fs-xs-11 p-0 m-0"><?php echo $ph3; ?></h3><?php endif; ?>
					<?php if(!empty($ph4)): ?><p class="float-left col-12 col-sm-12 text-left fs-13 p-0 m-0"><?php echo $ph4; ?></p><?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
		
		<?php if( have_rows('programs_s2') ): ?>
		<div class="fullcols prgrams_inner pt-4">
			<?php while ( have_rows('programs_s2') ) : the_row(); ?>
			<?php $ph1 =  get_sub_field('programs_headings_s2'); $ph2 =  get_sub_field('programs_sub_heading1_s2'); $ph3 =  get_sub_field('programs_sub_heading2_s2'); $phicons =  get_sub_field('programs_headings_icons_s2'); $ph3 =  get_sub_field('programs_sub_heading3_s2'); $ph4 =  get_sub_field('programs_desc_s2'); ?>
				<div class="col-12 col-sm-12 col-md-6 float-left pt-0 pt-sm-0 pt-md-4">
					<?php if(!empty($ph1)): ?><h1 class="fs-20 fs-xs-18 p-0 m-0"><?php echo $ph1; ?></h1><?php endif; ?>
					<?php if(!empty($ph2)): ?><h2 class="fs-13 fs-xs-11 p-0 m-0"><?php echo $ph2; ?></h2><?php endif; ?>
					<?php if(!empty($phicons)): ?><img src="<?php echo $phicons['url']; ?>" class="img-fluid" alt="Banner" width="<?php echo $phicons['width']; ?>" height="<?php echo $phicons['height']; ?>" /><?php endif; ?>
					<?php if(!empty($ph3)): ?><h3 class="fs-13 fs-xs-11 p-0 m-0"><?php echo $ph3; ?></h3><?php endif; ?>
					<?php if(!empty($ph4)): ?><p class="float-left col-12 col-sm-12 text-left fs-13 p-0 m-0"><?php echo $ph4; ?></p><?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>	
</section>

<section class="fullcols titless_holder blue_title py-4 headicons text-center">
	<div class="container">
		<h1 class="fs-23 fs-xs-20 white"><?php the_field('sections_2_main_title');?></br><span class="fs-16 fs-xs-16 white"><?php the_field('sections_2_sub_title');?></span></h1>
		<?php $s2_icons = get_field('sections_2_icons'); ?>
		<?php if(!empty($s2_icons)): ?><img src="<?php echo $s2_icons["url"];?>" alt="banner" width="<?php echo $s2_icons["width"];?>" height="<?php echo $s2_icons["height"];?>"  class="img-fluid"  /><?php endif; ?>
</section>


<section class="fullcols advantage_holder py-4">
	<div class="container">
		<div class="fullcols text-center">
			<?php if( have_rows('advantages_listings1') ):  while ( have_rows('advantages_listings1') ) : the_row(); ?>
			<div class="advantage_inner pb-4">
				<?php $adv_part1 = get_sub_field('advantages_icons_part1'); ?>
				<?php if(!empty($adv_part1)): ?><img src="<?php echo $adv_part1["url"];?>" alt="banner" width="<?php echo $adv_part1["width"];?>" height="<?php echo $adv_part1["height"];?>" class="img-fluid"  /><?php endif; ?>
				<h3 class="pt-2"><?php the_sub_field('advantages_main_title_part1'); ?></h3>
				<h4><?php the_sub_field('advantages_sub_title_part1'); ?></h4>
			</div>
			<?php endwhile; endif; ?>
		</div>
		<div class="fullcols text-center pt-0 pt-sm-0 pt-md-4 pb-4 pb-sm-4 pb-md-0">
			<?php if( have_rows('advantages_listings2') ):  while ( have_rows('advantages_listings2') ) : the_row(); ?>
			<div class="advantage_inner pb-4">
				<?php $adv_part2 = get_sub_field('advantages_icons_part2'); ?>
				<?php if(!empty($adv_part2)): ?><img src="<?php echo $adv_part2["url"];?>" alt="banner" width="<?php echo $adv_part2["width"];?>" height="<?php echo $adv_part2["height"];?>" class="img-fluid"  /><?php endif; ?>
				<h3 class="pt-2"><?php the_sub_field('advantages_main_title_part2'); ?></h3>
				<h4><?php the_sub_field('advantages_sub_title_part2'); ?></h4>
			</div>			
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>


<section class="fullcols titless_holder yellow_title py-4 headicons text-center">
	<div class="container">
		<?php $s3_icons = get_field('sections_3_icons'); ?>
		<?php if(!empty($s3_icons)): ?><img src="<?php echo $s3_icons["url"];?>" alt="banner" width="<?php echo $s3_icons["width"];?>" height="<?php echo $s3_icons["height"];?>"  class="img-fluid"  /><?php endif; ?><h1 class="fs-23 fs-xs-20 black"><?php the_field('sections_3_main_title'); ?></br><span class="fs-16 fs-xs-16 black"><?php the_field('sections_3_sub_title'); ?></span></h1>
	</div>
</section>

<section class="fullcols py-4 testimonials_centers">
	<div class="container">
		<div class="col-12 col-sm-12 col-md-6 float-left testimonials_daycares">
			<?php $testi_icons = get_field('parents_speak_testimonials_quotes_icons'); ?>
			<?php if(!empty($testi_icons)): ?><img src="<?php echo $testi_icons["url"];?>" alt="banner" width="<?php echo $testi_icons["width"];?>" height="<?php echo $testi_icons["height"];?>"  class="img-fluid"  /><?php endif; ?>
			<p class="fs-14 text-justify author_content"><?php the_field('parents_speak_testimonials_quotes'); ?></p>
			<p class="fs-11 text-justify author_name text-uppercase"><?php the_field('parents_speak_testimonials_quotes_author'); ?></p>
		</div>
		<div class="col-12 col-sm-12 col-md-6 float-left testimonials_centcout">
			<div class="col-12 col-sm-12 p-0 m-0 float-left testimonials_centcout">
				<div class="otz_count col-12 col-sm-12 col-md-4 float-left"><p class="fs-60 p-0 m-0"><?php the_field('parents_speak_centers_count'); ?></p></div>
				<div class="across_ind col-12 col-sm-12 col-md-8 mt-4 float-left"><h3 class="fs-13 text-uppercase"><?php the_field('parents_speak_centers_label'); ?></h3><h4 class="fs-13 text-uppercase">&nbsp;&nbsp;&nbsp;<?php the_field('parents_speak_centers_descs'); ?></h4></div>
			</div>
			<div class="col-12 col-sm-12 p-0 m-0 float-left col-md-6 testimonials_centcout">				
				<div class="across_ind col-12 col-sm-12 col-md-8 mt-4 float-left"><h3 class="fs-13 text-uppercase"><?php the_field('parents_speak_parents_label'); ?></h3><h4 class="fs-13 text-uppercase">&nbsp;&nbsp;&nbsp;<?php the_field('parents_speak_parents_descs'); ?></h4></div>
				<div class="otz_count  col-12 col-sm-12 col-md-4 float-left"><p class="fs-60 p-0 m-0 largecount"><?php the_field('parents_speak_parents_count'); ?></p></div>
			</div>
		</div>
	</div>
</section>

<section class="fullcols titless_holder blue_title py-4 headicons text-center">
	<div class="container">
		<h1 class="fs-23 fs-xs-20 white"><?php the_field('sections_4_main_title');?></br><span class="fs-16 fs-xs-16 white"><?php the_field('sections_4_sub_title');?></span></h1>
		<?php $s4_icons = get_field('sections_4_icons'); ?>
		<?php if(!empty($s4_icons)): ?><img src="<?php echo $s4_icons["url"];?>" alt="banner" width="<?php echo $s1_icons["width"];?>" height="<?php echo $s1_icons["height"];?>"  class="img-fluid"  /><?php endif; ?>
	</div>
</section>

<section class="fullcols titless_holder gray_title py-4 headicons text-center">
	<div class="container">
		<h1 class="fs-23 fs-xs-20 white"><?php the_field('sections_5_main_title');?></h1>
		<?php $s5_icons = get_field('sections_5_icons'); ?>
		<?php if(!empty($s5_icons)): ?><img src="<?php echo $s5_icons["url"];?>" alt="banner" width="<?php echo $s5_icons["width"];?>" height="<?php echo $s5_icons["height"];?>"  class="img-fluid"  /><?php endif; ?>
	</div>
</section>

<section class="fullcols titless_holder yellow_title py-4 headicons text-center">
	<div class="container">
		<?php $s6_icons = get_field('sections_6_icons'); ?>
		<?php if(!empty($s6_icons)): ?><img src="<?php echo $s6_icons["url"];?>" alt="banner" width="<?php echo $s6_icons["width"];?>" height="<?php echo $s6_icons["height"];?>"  class="img-fluid"  /><?php endif; ?>
		<h1 class="fs-23 fs-xs-20 black"><?php the_field('sections_5_main_title');?></br><span class="fs-16 fs-xs-16 black"><?php the_field('sections_1_sub_title');?></span></h1></h1>		
	</div>
</section>

<section class="fullcols text_holder py-4">
	<div class="container">	
		<?php if( have_rows('sections_6_descs') ): ?>		
			<div class="fullcols py-3">
				<?php while ( have_rows('sections_6_descs') ) : the_row(); ?>
					<div class="col-12 col-sm-12 col-md-4 float-left text-center"><p><?php the_sub_field('sections_6_paras');?></p></div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
		</div>
	</div>
</section>

<section class="fullcols py-4 text-center">
	<?php if( have_rows('social_media') ): ?>
	<div class="footer_socials text-center">		
		<ul>
			<?php while ( have_rows('social_media') ) : the_row();  ?>
			<?php $social_icons = get_sub_field('social_icons'); ?>
			<li><a href="<?php the_sub_field('social_icons_href');?>" target="_blank"><?php if(!empty($social_icons)): ?><img src="<?php echo $social_icons["url"];?>" alt="banner" width="<?php echo $social_icons["width"];?>" height="<?php echo $social_icons["height"];?>" class="img-fluid"  /><?php endif; ?></a></li>
			<?php endwhile; ?>
		</ul>		
	</div>
	<?php endif; ?>
</section>



<section class="yellow-bg eventslp_sticky d-block d-md-none fs-16 gotham-rounded-medium stricky_menu">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col"> <a href="tel:7676708888">Call</a></div>
			<div class="col"> <a href="#register">Enquire Now</a></div>
		</div>
	</div>
</section>

<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>

<style type="text/css">
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.testimonials_centers { background : #e7e9e9; }
.locate_holder { background : #e7e9e9; }
.otz_count { float : left; width : auto; }
.otz_count p { color : #e14769; font-family: 'GothamRoundedBold_0'; }
.across_ind  { float : left; width : auto; }
.across_ind h3 { font-family: 'GothamRoundedBold_0';  color : #3d3d3c; float : left; width :100%; }
.across_ind h4 { color : #3d3d3c; float : left; width :100%; padding : 0;  font-family: 'GothamRoundedBook'; }
.largecount { color : #00b3c4 !important; }
.testimonials_centers { }
.testimonials_centers img { }
.author_content { color : #000100; font-family: 'GothamRoundedBook'; }
.author_name { color : #3d3d3c; font-family: 'GothamRoundedBold_0';  }
.advantage_inner {   display: inline-block;   float: none;  text-align: center;  width: auto;}
.headicons { }
.headicons img { display : inline-block; text-align : center; float : none;   vertical-align: top;  }
.headicons h1 { display : inline-block; text-align : center; float : none; margin : 20px auto 0 10px; }
.headicons span { display : inline-block; text-align : center; float : none; }
.headicons h1 {  font-family: 'GothamRoundedBold_0';  text-transform: uppercase;  clear : both; }
.headicons h1 span {  font-family: 'GothamRoundedBook'; text-transform: uppercase;  clear : both; }
.black { color: #333; }
.white { color: #fff; }
.banner_holder img { width : 100%; height : auto; }
.form_holder h1 { font-family: 'GothamRoundedBold_0'; color : #fff; }
.form_holder h2 { font-family: 'GothamRoundedBold_0'; color : #fff; }
.form_holder h3 { font-family: 'GothamRoundedBook'; color : #fff; }
.form_holder h4 { font-family: 'GothamRoundedBook'; color : #fff; }
.footer_socials ul {  padding: 0; }
.locate_holder { }
.locate_holder p  {  float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBook'; }
.locate_holder p a  { color: #1d1d1b; font-size :20px; }
.text_holder p  {  float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBook'; }
.gray_title { background : #423a34; }
.advantage_holder img { text-align  :center; }
.advantage_holder h3 { color: #1d1d1b; float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBold_0'; margin: 0 auto; text-transform: uppercase;  clear : both; font-size : 16px; }
.advantage_holder h4 { color: #1d1d1b; float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBook'; margin: 0 auto; text-transform: uppercase;  clear : both; font-size : 16px; }
.eventslp_sticky .col { border-right: 1px solid #fff;    }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
.page-id-<?php echo $current_pageid; ?> .fullcols { float : left; width : 100%; position : relative; }
.yellow_title { background : #fdc100; }
.blue_title { background : #00b3c4; }
.prgrams_inner h1 { color: #1d1d1b;  font-family: 'GothamRoundedBold_0'; float: left; width  :100%; }
.prgrams_inner h2 { color: #1d1d1b;  font-family: 'GothamRoundedBold_0';   float: left; }
.prgrams_inner h3 { color: #1d1d1b;  font-family: 'GothamRoundedBold_0'; float: left; width  :100%;   }
.prgrams_inner p { color: #313131;  font-family: 'GothamRoundedBook';  }
.prgrams_inner img { float: left; margin: -20px 0px 0px 20px;; }
.prgrams_holder { background : #e7e9e9; }

@media only screen and (min-width: 767px)
{
.form_holder { background: #00b3c4;  position: absolute;  z-index: 999;  max-width: 450px;  left: 10px;  right: 0;  top: 32px;
  opacity: 0.89;   border-radius: 10px;   padding: 16px; }
.footer_socials ul li {  display: inline-block;   padding: 10px; }
.show_xs { display : none; }
.show_md { display : block; }
.show_header_xs { display : none; }
.testimonials_daycares { border-right : 2px dashed #e59ca2; }
}

@media only screen and (max-width: 767px)
{
.form_holder { background: #00b3c4;  position: relative;  z-index: 999; width :100%;  max-width: 100%; opacity: 0.89;   border-radius: 10px;   padding: 16px; }
.testimonials_daycares { border-bottom : 2px dashed #e59ca2; }
.footer_socials ul li {  display: inline-block;   padding: 10px 0; }	
.footer_socials ul li img { max-width : 80%; }	
.show_md { display : none; }
}
</style>
<?php
endwhile;
get_footer();
