<?php
/**
 * Template Name: Thankyou Webinar 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>

<section class="fullcols banner_holder">
	<?php $wdbanner = get_field('webinar_desktop_banner'); $wmbanner = get_field('webinar_mobile_banner'); ?>
	<?php if(!empty($wdbanner)): ?><img src="<?php echo $wdbanner["url"];?>" alt="banner" width="<?php echo $wdbanner["width"];?>" height="<?php echo $wdbanner["height"];?>"  class="img-fluid d-none d-sm-none d-md-block"  /><?php endif; ?>
	<?php if(!empty($wmbanner)): ?><img src="<?php echo $wmbanner["url"];?>" alt="banner" width="<?php echo $wmbanner["width"];?>" height="<?php echo $wmbanner["height"];?>"  class="img-fluid d-block d-sm-block d-md-none"  /><?php endif; ?>
	<div class="form_holder" id="register">
		<?php if(get_field('webinar_heading')):?><h1 class="col-12 col-sm-12 text-left fs-20"><?php the_field('webinar_heading');?></h1><?php endif; ?>
		<!--<h2 class="col-12 col-sm-12 text-left fs-15"></h2>
		<h3 class="col-12 col-sm-12 text-left fs-14 mt-3"></h3> -->			
	</div>
</section>

<style type="text/css">
.eventslp_sticky .col { border-right: 1px solid #fff; }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.testimonials_centers { background : #e7e9e9; }
.locate_holder { background : #e7e9e9; }
.otz_count { float : left; width : auto; }
.otz_count p { color : #e14769; font-family: 'GothamRoundedBold_0'; }
.across_ind  { float : left; width : auto; }
.across_ind h3 { font-family: 'GothamRoundedBold_0';  color : #3d3d3c; float : left; width :100%; }
.across_ind h4 { color : #3d3d3c; float : left; width :100%; padding : 0;  font-family: 'GothamRoundedBook'; }
.largecount { color : #00b3c4 !important; }
.testimonials_centers { }
.testimonials_centers img { }
.author_content { color : #000100; font-family: 'GothamRoundedBook'; }
.author_name { color : #3d3d3c; font-family: 'GothamRoundedBold_0';  }
.advantage_inner {   display: inline-block;   float: none;  text-align: center;  width: auto;}
.headicons { }
.headicons img { display : inline-block; text-align : center; float : none;   vertical-align: top;  }
.headicons h1 { display : inline-block; text-align : center; float : none; margin : 20px auto 0 10px; }
.headicons span { display : inline-block; text-align : center; float : none; }
.headicons h1 {  font-family: 'GothamRoundedBold_0';  text-transform: uppercase;  clear : both; }
.headicons h1 span {  font-family: 'GothamRoundedBook'; text-transform: uppercase;  clear : both; }
.black { color: #333; }
.white { color: #fff; }
.banner_holder img { width : 100%; height : auto; }
.form_holder h1 { font-family: 'GothamRoundedBold_0'; color : #fff; }
.form_holder h2 { font-family: 'GothamRoundedBold_0'; color : #fff; }
.form_holder h3 { font-family: 'GothamRoundedBook'; color : #fff; }
.form_holder h4 { font-family: 'GothamRoundedBook'; color : #fff; }
.footer_socials ul {  padding: 0; }
.locate_holder { }
.locate_holder p  {  float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBook'; }
.locate_holder p a  { color: #1d1d1b; font-size :20px; }
.text_holder p  {  float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBook'; }
.gray_title { background : #423a34; }
.advantage_holder img { text-align  :center; }
.advantage_holder h3 { color: #1d1d1b; float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBold_0'; margin: 0 auto; text-transform: uppercase;  clear : both; font-size : 16px; }
.advantage_holder h4 { color: #1d1d1b; float: left; width: 100%; text-align  :center; font-family: 'GothamRoundedBook'; margin: 0 auto; text-transform: uppercase;  clear : both; font-size : 16px; }
.eventslp_sticky .col { border-right: 1px solid #fff;    }
.eventslp_sticky .col a { padding: 8px 0;   color: #333; display: inline-block; }
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
.page-id-<?php echo $current_pageid; ?> .fullcols { float : left; width : 100%; position : relative; }
.yellow_title { background : #fdc100; }
.blue_title { background : #00b3c4; }
.prgrams_inner h1 { color: #1d1d1b;  font-family: 'GothamRoundedBold_0'; float: left; width  :100%; }
.prgrams_inner h2 { color: #1d1d1b;  font-family: 'GothamRoundedBold_0';   float: left; }
.prgrams_inner h3 { color: #1d1d1b;  font-family: 'GothamRoundedBold_0'; float: left; width  :100%;   }
.prgrams_inner p { color: #313131;  font-family: 'GothamRoundedBook';  }
.prgrams_inner img { float: left; margin: -20px 0px 0px 20px;; }
.prgrams_holder { background : #e7e9e9; }

@media only screen and (min-width: 767px)
{
.form_holder { background: #00b3c4;  position: absolute;  z-index: 999;  max-width: 450px;  left: 10px;  right: 0;  top: 32px;
  opacity: 0.89;   border-radius: 10px;   padding: 16px; }
.footer_socials ul li {  display: inline-block;   padding: 10px; }
.show_xs { display : none; }
.show_md { display : block; }
.show_header_xs { display : none; }
.testimonials_daycares { border-right : 2px dashed #e59ca2; }
body { background: #00b3c4 !important; }
}

@media only screen and (max-width: 767px)
{
.form_holder { background: #00b3c4;  position: relative;  z-index: 999; width :100%;  max-width: 100%; opacity: 0.89;   border-radius: 10px;   padding: 16px; }
.testimonials_daycares { border-bottom : 2px dashed #e59ca2; }
.footer_socials ul li {  display: inline-block;   padding: 10px 0; }	
.footer_socials ul li img { max-width : 80%; }	
.show_md { display : none; }
}
</style>
<?php
endwhile;
get_footer();
