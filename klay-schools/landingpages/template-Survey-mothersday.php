<?php
/**
 * Template Name: Survey mothersday result
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<section class="pt-50 pb-50 pb-xs-45 pt-xs-30">
	<div class="container-fluid">
		<div class="row mt-15">
			<div class="col-12 col-md-12">
				<div class="swiper-container" id="singleSlideSwiper">
					<div class="swiper-wrapper">
						<?php while(have_rows('image_carousel')){the_row(); ?>
						<div class="swiper-slide">
							<img src="<?php the_sub_field('image') ?>" alt="" class="w-100" />
							<?php if(get_sub_field('title')){ ?><span class="small-caption gotham-rounded-medium fs-15 white-color"><em><?php the_sub_field('title') ?></em></span><?php } ?>
						</div>
						<?php } ?>
					</div>
					<div class="single-slide-prev"></div>
					<div class="single-slide-next"></div>
				</div>
				<div class="gotham-rounded-book mt-30"><?php the_content(); ?></div>
			</div>
		
		</div>
	</div>
</section>

<?php
endwhile;
get_footer();
