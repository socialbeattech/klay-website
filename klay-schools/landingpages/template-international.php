<?php
/**
 * Template Name: International-summit
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */
get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KVL7RV4');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KVL7RV4');</script>
<!-- End Google Tag Manager -->
<style type="text/css">
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
</style>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

</head>
<body>
<section class="top_header">
	<div class="container-fluid">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 d-none d-sm-none d-md-block d-lg-block">
			<div class="row">
				<div class="col-12 col-md-3 col-sm-3 col-lg-3 ">
					<span class="logo">
						<img src="<?php echo get_template_directory_uri();?>/images/early-logo.png" class="img-fluid" alt="Banner" width="180" height="180" />
					</span>

				</div>
				<div class="col-12 col-md-9 col-sm-9 col-lg-9">
					<ul class="menu-bar">
						<li><a href="#About-ISEY">About ISEY</a></li>
						<li><a href="#Should-Attend">Who Should Attend</a></li>
						<li><a href="#event-schedule">Event Schedule</a></li>
						<li><a href="#speakers">Speakers</a></li>
						<li class="tick"><a href="#tickets">Tickets</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="banner-part">
	<div class="container-fluid p-0 m-0">
		<div class="sumit_desktop d-none d-sm-none d-md-block d-lg-block">
			<img src="<?php echo get_template_directory_uri();?>/images/insumit_desktop.png" class="img-fluid" alt="Banner" width="100%" height="665" />
		</div>
		<div class="sumit_mobile d-block d-sm-block d-md-none d-lg-none">
			<img src="<?php echo get_template_directory_uri();?>/images/insumit_mobile.png" class="img-fluid" alt="Banner" width="480" height="700" />
		</div>
	</div>
</section>

<div class="logo-mobile pt-3">
	<div class="container-fluid">
		<div class="col-6">
			<div class="row">	
				<div class="col-6 col-md-2 col-sm-2">
					<img src="<?php echo get_template_directory_uri();?>/images/sumit-logo.png" class="img-fluid" alt="Banner" width="100" height="100" />
				</div>
				<div class="col-6 col-md-2 col-sm-2">
					<p><a href="#tickets" class="mob_tick">Tickets</a></p>
				</div>
				<div class="col-6 col-md-2 col-sm-2">
					<menu-header>
	
  <button class="hamburger">&#9776;</button>
  <button class="cross">&#735;</button>
</menu-header>

<div class="menu">
  <ul>
    <a href="#About-ISEY"><li>About ISEY</li></a>
    <a href="#Should-Attend"><li>Who Should Attend</li></a>
    <a href="#event-schedule"><li>Event Schedule</li></a>
    <a href="#speakers"><li>Speakers</li></a>
    Vibha Krishnamurthy
  </ul>
</div> 
				</div>
			</div>
		</div>
	</div>
</div>


<section class="form" id="earlyform">
	
			<div class="padot_form">
				<h5>Please drop in your details below</h5>
				<iframe src="http://go.pardot.com/l/563842/2019-07-24/5v8vj?Source_URL=<?php the_permalink(); ?>" width="100%" height="350" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
			</div>
		
</section>
<section class="counts">
	<div class="container-fluid">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-md-7 col-sm-7 col-lg-7 date_time">
					<ul>
						<li>5th September ’19</li>
						<li>8:00 AM - 5:30 PM</li>
						<li class="bnglore">Bengaluru</li>
					</ul>
				</div>
				<div class="col-12 col-md-5 col-sm-5 col-lg-5 count_timer">
					<div class="counter" id="timer">
					 
					  <div class="counter" id="days"></div>
					  <div class="counter"  id="hours"></div>
					  <div class="counter" id="minutes"></div>
					  <div class="counter" id="seconds"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="early_years" id="About-ISEY">
	<div class="container">
		<div class="col-12 col-md-12 col-lg-12 col-sm-12 text-center py-5">
			<h1 class="black_text">About The International Summit on Early Years</h1>
			
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center summit_text ">
			<p><b>The International Summit on Early Years (ISEY)</b> is one of India's largest global summits aimed at addressing modern day issues of early childhood and charting future courses of action. As a platform to engage and network with academics, practitioners, and policymakers in the space of early childhood care and education, this summit will change the way we <b>learn, teach and grow.</b></p>
			<p>This year’s summit theme is: </p>
			<h2>Happy Adults=Happy Children</h2>
			<p class="well">The importance of the emotional wellbeing of practitioners working with young children</p>
		</div>
	</div>
</section>
<section class="attend" id="Should-Attend">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center py-5">
			<h1 class="white_text">Who should attend?</h1>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center sub_text ">
			<div class="row">
				<div class="col-12 col-md-4 col-sm-4 col-lg-4">
					<p>Early Childhood<br>Professionals</p>
				</div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4">					
					<p>Administrators of <br>Educational Institutations</p>
				</div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4">
				<p class="no_border">Head Teachers, Teachers<br>and Assistant Teachers</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center sub_text line_bottoms">
			<div class="row">
				<div class="col-12 col-md-4 col-sm-4 col-lg-4">
					<p>Directors of<br>Educational Institutions</p>
				</div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4">
					<p>Students of Education/<br>Psychology/Child Development</p>
				</div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4">
					<p class="no_border">Principals</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="Speaker" id="speakers">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center py-5">
			<h1 class="black_text">Speakers</h1>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center speaker_img py-3">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Peter14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Peter</b> Gray</h5>
					<p>Research Professor,<br>Boston College</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
				
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Know more</button>
					</div>
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Dr. Peter Gray is a research professor of psychology at Boston College who has conducted and published research in neuroendocrinology, developmental psychology, anthropology, and education. He is the author of an internationally acclaimed introductory psychology textbook (Psychology, Worth Publishers, now in its 8th edition), which views all of psychology from an evolutionary perspective. His recent research focuses on the role of play in human evolution and how children educate themselves, through play and exploration, when they are free to do so. He has expanded on these ideas in his book, Free to Learn: Why Unleashing the Instinct to Play Will Make Our Children Happier, More Self-Reliant, and Better Students for Life (Basic Books). He also authors a regular blog called Freedom to Learn, for Psychology Today magazine. He is a founding member and president of the non-profit Alliance for Self-Directed Education (ASDE), which is aimed at creating a world in which children’s natural ways of learning are facilitated rather than suppressed. He is also a founding board director of the non-profit Let Grow, the mission of which is to renew children’s freedom to play and explore outdoors, independently of adults. He earned his undergraduate degree at Columbia College and Ph.D. in biological sciences at the Rockefeller University many years ago. His own current play includes kayaking, long-distance bicycling, backwoods skiing, and vegetable gardening.</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
  
				
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Ashish14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Ashish</b> Karamchandani</h5>
					<p>Managing Director,<br>FSG</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Ashish">Know more</button>
					</div>
					<div class="modal fade" id="Ashish" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Ashish Karamchandani is the managing director at FSG, a mission-driven consulting firm that focuses on using market-based solutions to drive sustainable social change. His emphasis has been on multi-year programs such as low income housing. Through his work, Karamchandani helped to develop a housing market that has sold more than 100,000 homes and has more than 10 housing companies now offering 15-year mortgages to informal sector customers with no income documentation. He is currently co-leading a program to improve private preschool education for low income households in urban India.</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Jackie14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Jackie</b> Harland</h5>
					<p>Founder,<br>London Children's Practice</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Jackie">Know more</button>
					</div>
					<div class="modal fade" id="Jackie" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Jackie Harland is qualified as a Speech and Language Therapist over 30 years ago and has established four multidisciplinary clinics within the UK and internationally. She is a founder and Director of Services in the London Children’s Practice in central London and  is the co-founder of London Learning Centre in Delhi. She has a Masters in Early Childhood Education and has co-authored an early years framework and online screening tool that supports early years teachers in good practice within pre-schools. She has conducted research in early childhood education in the UK and internationally. </p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center speaker_img py-3">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr ">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Ramya14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Ramya</b> Venkataraman</h5>
					<p>Founder & CEO,<br>CENTA</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Ramya">Know more</button>
					</div>
					<div class="modal fade" id="Ramya" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Ramya Venkataraman is Founder & CEO of Centre for Teacher Accreditation (CENTA), an India-based teacher certification entity. Prior to this, Ramya was with McKinsey & Company for 16 years, including as Leader of the Education Practice (which she built). Ramya is known for several pioneering on-the-ground reform efforts in both school education and skills in India - driving large scale reform in public schools. Beyond hands-on experience in India, Ramya has played advisory roles in education in Bhutan, South East Asia, middle east and North Africa.

									Ramya was recently elected an Ashoka Fellow in recognition of her leadership and continued contributions to the education space. Ramya holds a B.Tech. from the Indian Institute of Technology Delhi and an MBA from the Indian Institute of Management Calcutta.
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Prachi14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Prachi</b> Windlass</h5>
					<p>Director,<br>India Education</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Prachi">Know more</button>
					</div>
					<div class="modal fade" id="Prachi" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Prachi is a director at Michael & Susan Dell Foundation India LLP and manages the foundation’s India investments. She is on the board of several for-profit education companies and advisory committees to state governments and education departments. She is passionate about the area of data driven education and has spearheaded the foundation’s work in multiple ways, including: incubating service providers, creating role model states for classroom based assessments and embedding student learning based accountability in all portfolio companies.</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/preethi_web.jpg" alt="petergrey" width="180" height="180" />
					<h5><b>Preethi Vickram</b></h5>
					<P>Founder,Director of LIFE <br>(Leadership Initiative For Education)</p> 
					<!--<h5><b>Dr. Swati</b> Popat Vats</h5>
					<p>President, Early Childhood<br>Association India</p>
					<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Swati">Know more</button>
					</div>
					<div class="modal fade" id="Swati" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<!--<p>Dr. Swati Popat Vats is the founder President of Early Childhood Association India. She is also President of Podar Education Network and leads over 290 preschools and Daycares as founder director of Podar Jumbo Kids. She is also the National representative for the World Forum Foundation.
									She is Nursery Director of Little Wonders Nursery (UAE) that has branches in Jumeirah and Sharjah.  She has received many accolades and awards for her contribution to Early Childhood Education and has been conferred a Fellowship of Honor from the New Zealand Tertiary College. She was a founder consultant for the Euro Kids preschool project in India and helped set up TATASKY’s children’s television activity channel- ACTVE WHIZKIDS. Swati has authored many books for parents and children and is a strong advocate of nature-based learning in the early years 
									 </p>!-->
									 <p>Preethi has been an educationist for over 15 years. She has been associated with Podar Education Network, providing sub franchisees to Karnataka, AP and Telangana and she is currently handling 56 centers. She has been the Territory head for ECA.  She is a Leadership mentor and Founder Director of LIFE, which is a dynamic initiative that focuses on educational leadership and organizational cultures. Preethi is a committed educator, passionate parenting coach, leadership mentor and a brand champion. She constantly thrives to make a difference in the lives of children and consequently, the world at large. </p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center speaker_img py-3">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr ">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/shamin mehrotra.jpg" alt="petergrey" width="180" height="180" />
					<h5><b>Shamin Mehrotra</b></h5>
					<p> Director, School Outreach <br>& Senior Counselor at Ummeed </p>
					<!--<h5><b>Dr. Vibha</b>Krishnamurthy</h5>
					<p>Founder & Executive <br>Director Ummeed</p>
					<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Vibha">Know more</button>
					</div>
					<div class="modal fade" id="Vibha" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<!--<p>Vibha founded Ummeed Child Development Center in 2001 and serves as its Executive Director. Vibha has grown Ummeed from a clinic providing integrated services to a center of excellence for clinical care, as well as a resource center in the field of developmental disabilities for professionals, families, and community organizations. Vibha’s vision to make inclusion of children with developmental disabilities a visible agenda for India.
									</p>!-->
									<p>Shamin is a senior counselor and feels passionately about working with children with disabilities (and their families), and providing them with ongoing mental health and counseling support to enable their overall journey in a positive direction. As an early and integral member of Ummeed’s Mental Health Team, she played a significant role in developing, designing and delivering the Mental Health Training Program. 
									Shamin has a Master’s degree (MA) in Applied Psychology from the University of Mumbai and a Master’s degree (MSED) in Psychological Services from the University of Pennsylvania. 
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Meghna14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Meghna</b> Yadav</h5>
					<p>Head of Training<br> at KLAY</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Meghna">Know more</button>
					</div>
					<div class="modal fade" id="Meghna" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>With Masters in Child Psychology (Summa cum Laude) from San Jose State University, California, Meghna is working in the field of Early Childhood Education for more than two decades. Experienced in counselling of families and pre-school functioning with renowned organisations in USA, Singapore and India, Meghna plays an integral role in parent partnership initiatives at KLAY.  </p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Priya14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Priya</b> Krishnan</h5>
					<p>Founder <br> CEO at KLAY</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Priya">Know more</button>
					</div>
					<div class="modal fade" id="Priya" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Priya Krishnan is the CEO of KLAY- the most prestigious and trustworthy prep school and day care brands in India. A business graduate from London Business School, and having worked in various leadership positions with companies like Anderson Consulting and PWC, Priya moved from UK to India with the desire of starting a chain of high quality and trustworthy day care centres in emerging urban hubs which would offer parents and children unparalleled education and care. Her passion to make a difference in the education world was also driven by her vision of enabling women to get back to work by creating learning spaces where they could leave their children on their way to work. 
									 </p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center speaker_img py-3">
			<div class="row">
				<div class="col-12 col-sm-2 col-md-2 col-lg-2"></div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr ">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Pooja14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Pooja </b>Goyaly</h5>
					<p>Chief Strategy Officer  <br> at KLAY</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Pooja">Know more</button>
					</div>
					<div class="modal fade" id="Pooja" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Pooja is a serial entrepreneur and an educator. She currently drives strategy and new businesses at KLAY as Chief Strategy Officer. Her most recent venture was Intellitots Learning which got acquired by KLAY in Dec 2017. Prior to that, she founded a PRM software company in Silicon Valley. An engineer from IIT Delhi, with an MBA from INSEAD, France, Pooja has worked in the USA, Europe and India, and has successfully paired her work experience at companies like Adobe Systems and Palm with her passion for education. She is an angel investor and advisor to many startups and has been awarded the ‘Woman of Substance’ award. Pooja speaks on the issues of entrepreneurship, leadership and changing education paradigms. Pooja is a voracious reader, finds joy in yoga and running, and is an avid traveler. 
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 col-12 profile-spkr">
					<img class="img-fuid" src="<?php echo get_template_directory_uri();?>/images/Fathima14-8-19.png" alt="petergrey" width="180" height="180" />
					<h5><b>Fathima </b>Khader</h5>
					<p>Founder EvolveED</p>
					<!--<p class="know"><a href="#" target="_blank">Know more</a></p>!-->
					<div class="know">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Fathima">Know more</button>
					</div>
					<div class="modal fade" id="Fathima" role="dialog">
						<div class="modal-dialog">	
							<div class="modal-content">
								<!--<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Modal Header</h4>
								</div>!-->
								<div class="modal-body">
									<p>Fathima brings with her close to 15 years of experience in the education sector working extensively with children, teachers and parents in areas of life skills training, personal development and child behaviour management.  Her passions lies in conducting workshops on personal safety, body awareness, human sexuality, CSA awareness and has extensively worked with adolescents on responsible behaviours.  She currently heads EvolveED, a Bangalore based training organisation, that primarily works in the space of childcare, focusing on healing, restoring and transforming human relationships. </p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
      
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-2 col-md-2 col-lg-2"></div>
			</div>
		</div>
	</div>
</section>
<section class="ticket" id="tickets">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center book">
			<h1 class="white_texts">BOOK YOUR TICKET!</h1>
			<p class="ticketed">ISEY is a ticketed event. Please book your tickets at the earliest to ensure you get a spot.<br> We are currently offering early bird discounts & group discounts.</p>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 ">
			<div class="row">
				<div class="col-12 col-md-3 col-sm-3 col-lg-3 regular_box text-center">
					<h3>Regular <br>Ticket </h3>
					<!--<p>Price: ₹5,000</p>!-->
					<p class="regi"><a href="#earlyform">REGISTER NOW</a></p>
				</div>
				<div class="col-12 col-md-1 col-sm-1 col-lg-1"></div>
				<div class="col-12 col-md-3 col-sm-3 col-lg-3 regular_box text-center">
					<h3>Early Bird<br>Discounted Ticket </h3>
					<!--<p>Price: ₹3,500</p>!-->
					<p class="regi"><a href="#earlyform">REGISTER NOW</a></p>
				</div>
				<div class="col-12 col-md-1 col-sm-1 col-lg-1"></div>
				<div class="col-12 col-md-3 col-sm-3 col-lg-3 regular_box text-center">
					<h3>Group<br>Discounted Ticket</h3>
					<!--<p>Price:₹3,500<br>(Min 5 tickets purchase required)</p>!-->
					<p class="regi"><a href="#earlyform">REGISTER NOW</a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<footer class="lastpart" id="event-schedule">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 text-center">
			<h1 class="black_texts">Event Schedule</h1>
		</div>
		
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 table-scroll ">
			<div class="dataTables_scrollBody" style="position: relative;overflow: auto;width: 100%;">
			<table id="example" class="display nowrap" style="width:100%">  
			  <tr class="sub-head">
				<th class="time">Time</th>
				<th class="event"> Event</th>
				<th class="speaker">Speaker</th>
				<th class="topic">Topic</th>
			  </tr>
			  <tr class="bottom_line">
				<td>8:00 - 9:00 am</td>
				<td>Registration</td>
				<td></td>
				<td class="fourth"></td>
			   
			  </tr>
			  <tr>
				<td>8:00 - 8:45 am</td>
				<td>Coffee and Networking</td>
				<td></td>
				<td class="fourth"></td>
				
			  </tr>
			  <tr>
				<td>9:00 – 9:20 am </td>
				<td>Welcome note</td>
				<td>Priya Krishnan, CEO KLAY</td>
				<td class="fourth"></td>
				
			  </tr>
			  <tr>
				<td>9:30 - 10:30 am</td>
				<td>Keynote Speaker Talk</td>
				<td>Dr. Peter Gray</td>
				<td class="fourth">Why Play is far more valuable<br> for children’s mental growth<br> than academic training</td>
			  </tr>
			  <tr>
				<td>All Day </td>
				<td>Poster Walk </td>
				<td></td>
				<td class="fourth"></td>
			  </tr>
			  <tr>
				<td>All Day </td>
				<td>Stalls </td>
				<td></td>
				<td class="fourth"></td>
			  </tr>
			  <tr>
				<td>All Day </td>
				<td>Klay classroom </td>
				<td></td>
				<td class="fourth"></td>
			  </tr>
			  <tr>
				<td>10:30 - 11:15 am</td>
				<td>Keynote Speaker Talk</td>
				<td>Ashish Karamchandani</td>
				<td class="fourth">Ground Realities of <br>ECE professionals</td>
			  </tr>
			  <tr>
				<td>11:15 - 11:45 am </td>
				<td>Tea and networking</td>
				<td></td>
				<td class="fourth"></td>
			  </tr>
			   <tr>
				<td>11:45 - 12:30 pm</td>
				<td>Keynote Speaker Talk</td>
				<td>Jackie Harland</td>
				<td class="fourth">Emotions are Contagious - Successful Strategies for Emotional Regulation</td>
			  </tr>
			   <tr>
				<td>12:30 - 1:30 pm</td>
				<td>Panel Discussion</td>
				<td>Preethi Vickram<br> 
		Early Childhood Association India<br>
		Ramya Venkataraman, CENTA <br>Prachi Windlass, India Education<br>
		Shamin Mehrotra, Ummeed<br>
		Meghna Yadav, KLAY</td>
				<td class="fourth">How do we sustain the cycle of<br>
		“Happy adults = Happy children”</td>
			  </tr>
			  <tr>
				<td>1:30 - 2:00 pm</td>
				<td>Book Signing by<br> Dr. Peter Gray</td>
				<td>Dr. Peter Gray</td>
				<td class="fourth">Free To Learn: Why Unleashing<br>
		the Instinct to Play Will Make<br>
		Our Children Happier, More<br>
		Self-Reliant, and Better Students
		for Life</td>
			  </tr>
				   <tr>
				<td>1:30 - 2:30 pm</td>
				<td>Lunch</td>
				<td></td>
				<td class="fourth"></td>
			  </tr>
			  <tr>
				<td>2:30 - 4:00 pm </td>
				<td>Workshop</td>
				<td>Dr. Peter Gray </td>
				<td class="fourth">What are the barriers to Play in<br> your center and how can you<br> overcome them.</td>
			  </tr>
			  <tr>
				<td rowspan="3">4:00 - 5:00 pm </td>
				<td>Workshop</td>
				<td>Jackie Harland</td>
				<td class="fourth">Zones of Regulation</td>
				
			  </tr>
			  <tr>
				<td>Workshop</td>
				<td>Preethi Vickram</td>
				<td class="fourth">Mindful adults build resilience in children</td>
				
			  </tr>
			  <tr>
				<td>Workshop</td>
				<td>Fathima Khader</td>
				<td class="fourth">Power of Mindfulness and Play </td>
				
			  </tr>
				<tr class="closing_line">
				<td>5:00 - 5:30 pm </td>
				<td>Closing</td>
				<td>Pooja Goyal</td>
				<td class="fourth"></td>
			  </tr>
			</table>
			</div>
		</div>
	</div>
</footer>

<section class="eventslp_sticky d-block d-md-none text-center">
<div class="now-en">
<a href="#earlyform">Enquire Now</a>
</div>		
</section>

<script type="text/javascript">


jQuery('ul.menu-bar li a').on( 'click', function(event) {

  var target  = jQuery( this );

  var element = target.attr('href'); 

  jQuery('ul.menu-bar li aa').removeClass('active')

  target.addClass('active');
  jQuery("body, html").animate({

    scrollTop: jQuery( element ).offset().top - 90 

  }, 800);

});


function makeTimer() {

	//		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
		var endTime = new Date("5 sep 2019 9:56:00 GMT+01:00");			
			endTime = (Date.parse(endTime) / 1000);

			var now = new Date();
			now = (Date.parse(now) / 1000);

			var timeLeft = endTime - now;

			var days = Math.floor(timeLeft / 86400); 
			var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
			var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
			var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
			if (hours < "10") { hours = "0" + hours; }
			if (minutes < "10") { minutes = "0" + minutes; }
			if (seconds < "10") { seconds = "0" + seconds; }

			$("#days").html(days + "<span>Days</span>");
			$("#hours").html(hours + "<span>Hours</span>");
			$("#minutes").html(minutes + "<span>Minutes</span>");
			$("#seconds").html(seconds + "<span>Seconds</span>");		

	}

	setInterval(function() { makeTimer(); }, 1000);


// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)



$( ".cross" ).hide();
$( ".menu" ).hide();
$( ".hamburger" ).click(function() {
$( ".menu" ).slideToggle( "slow", function() {
$( ".hamburger" ).hide();
$( ".cross" ).show();
});
});

$( ".cross" ).click(function() {
$( ".menu" ).slideToggle( "slow", function() {
$( ".cross" ).hide();
$( ".hamburger" ).show();
});
});





</script>
<style type="text/css">
@font-face {
    font-family: 'RalewayExtraBold';
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewayExtraBold.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewayExtraBold.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayExtraBold.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayExtraBold.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayExtraBold.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayExtraBold.svg#RalewayExtraBold') format('svg');
}


@font-face {
    font-family: 'RalewayMedium';
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewayMedium.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewayMedium.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayMedium.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayMedium.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayMedium.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayMedium.svg#RalewayMedium') format('svg');
}


@font-face {
    font-family: 'RalewayBold';
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewayBold.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewayBold.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayBold.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayBold.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayBold.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewayBold.svg#RalewayBold') format('svg');
}

@font-face {
    font-family: 'RalewaySemiBold';
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewaySemiBold.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/RalewaySemiBold.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewaySemiBold.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewaySemiBold.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewaySemiBold.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/RalewaySemiBold.svg#RalewaySemiBold') format('svg');
}

.important-menu-items  { display : none !important; }
.speaker_img h5 {font-size: 23px;padding: 4% 0 0 0;color: #95c13e; font-family: 'RalewayBold';}
.speaker_img p {font-size: 20px;color: #4d4d4f;font-family: 'RalewayMedium';}
p.know a {display: inline-block;background:#a1cb3b;color: white;text-decoration: none;padding: 2% 4%;border-radius: 17px;font-size:18px;}
h1.black_text::after{content: "";width: 85px;height: 26px;background:url("<?php echo get_template_directory_uri();?>/images/arrow.png") no-repeat;top: 72%;right:46%;position: absolute;display: inline-block;}
h1.black_texts::after{content: "";width: 85px;height: 26px;background:url("<?php echo get_template_directory_uri();?>/images/arrow.png") no-repeat;top: 118%;right:46%;position: absolute;display: inline-block;}
h1.white_text::after{content: "";width: 86px;height: 27px;background:url("<?php echo get_template_directory_uri();?>/images/white_arrow.png") no-repeat;top: 72%;right:46%;position: absolute;display: inline-block;}
h1.white_texts::after{content: "";width: 86px;height: 27px;background:url("<?php echo get_template_directory_uri();?>/images/white_arrow.png") no-repeat;top: 42%;right:46%;position: absolute;display: inline-block;}
.sub_text p{border-right:1px solid;font-family: 'RalewayMedium';font-weight: 600;}
p.no_border{border-right:none !important;}
section.attend {background: #ffcb05;color: #b55b13;font-size: 19px;padding: 2% 0 3% 0;}
h1.black_text,h1.black_texts {font-size: 50px; font-family: 'RalewayBold'; }
 {font-size: 50px; font-family: 'RalewayBold'; }
h1.white_text,h1.white_texts{font-size: 50px; color:white; font-family: 'RalewayBold'; }

footer.lastpart {padding: 12% 0;background: #ebeced;margin: -8% 0 0 0;}
.summit_text h2{color:#f0536d;font-size:28px;font-family: 'RalewayBold';}
p.well{color:#f0536d;font-size:15px;}
.ticket{background:url("<?php echo get_template_directory_uri();?>/images/bcakgrounf-img.png")no-repeat center;height:681px;background-size:cover;width:100%;}
.regular_box {background: #ee2348; opacity: 0.7;border-radius: 15px;padding: 3% 0%;margin: 5% 0 0 0;}
p.regi a {color: #ef4a68;background: white;font-size: 14px;display: inline-block;padding: 2% 2%;border-radius: 14px;}
.regular_box  p {color: #ffcd03;font-size: 16px;font-weight:bold;padding: 3% 0 0 0;}
.regular_box  h3 {color: white;font-size: 25px;padding: 5% 0 0 0;font-family: 'RalewayMedium';font-weight: 600;}
p.ticketed{color:white;font-size:20px;font-family: 'RalewayMedium';padding: 4% 0 0 0;}
section.ticket{padding: 3% 0 0% 0;}
.summit_text p {font-family: 'RalewaySemiBold';font-size:17px;}
.counts{background:black;padding: 1% 0;}
.date_time ul{list-style-type: none;color: white;padding: 0;margin: 0 auto;}

ul.menu-bar {list-style-type:none;padding:0;margin:0 auto;}
ul.menu-bar li a {float:left;padding:0 1% 0 3%;font-size:17px;font-family: 'RalewayMedium';color: black;text-decoration: none;}

li.tick {background: yellow;text-align: center;display: inline-block;border-radius: 7px;text-transform: uppercase;font-size: 17px;font-family: 'RalewayMedium';margin: 0 0 0 2%;}

.heading_table {padding: 5% 0 0 0;}
table, th, td {border-right: 3px solid #eceded;border-collapse: collapse;border-bottom: 3px solid #eceded;}
th, td {padding: 2% 0;text-align: center;    }
tr.title_head {text-align: center;border-radius: 5px;}
th {text-align: center !important;border-right: none;border-bottom: none !important;}
th.time {background: #1dccd5;border-radius: 15px;font-size: 18px;border-bottom: none !important;color: white;border-right: 4px solid #ebeced;}
th.event {background: #ef6176;border-radius: 15px;color: white;font-size: 20px;border-right: 4px solid #ebeced;}
th.speaker {background: green;border-radius: 15px;color: white;border-right: 4px solid #ebeced;}
th.topic {background: #ffd101;border-radius: 15px;color: white;border-right: 1px solid #ebeced;}
td.fourth {border-right: 1px solid #ebeced;}
td {background: #ffffff;}
tr.bottom_line {border-top: 9px solid #ebeced;}

@media only screen and (max-width: 767px){
.sub_text p{border-right:none;border-bottom: 1px solid #c69e08 !important;
padding: 0 0 6% 0;font-size: 18px;}
h1.white_text{font-size:27px;}
h1.white_texts{font-size:26px;padding: 2% 0 0 0;}
h1.black_text:after{top: 70%;right: 37%;}
h1.black_text {font-size: 37px;}
h1.black_texts {font-size: 30px;}
.ticket{height:auto;}
.regular_box{margin: 0 0 5% 0;}
p.ticketed{color:white;font-size:16px;}
.summit_text p {font-size:16px;}
.summit_text h2 {font-size:24px;}
h1.white_texts::after{top: 27%;right: 35%;}
p.ticketed{padding: 24% 0 0;}
.regular_box h3{font-size: 20px;}
p.regi a{font-size: 12px;}
.regular_box p{font-size: 13px;}
.table_chart {padding: 5% 0 0 0;}
h1.white_text::after{right: 38%;}
h1.black_text {font-size: 23px;}
h1.black_texts::after{right: 38%; top:134% !important;}
.padot_form h5 {font-size: 17px;padding: 4% 3%;color: #00bac6;font-family: 'RalewayMedium';text-align: center;}
.modal-dialog {max-width: 394px !important;}
.date_time {text-align: center;}
.counter{padding:0 !important;display:block;}
#days,#minutes,#seconds,#hours{font-size: 18px;}
span{font-size:12px;}
#days{padding:2px 9px !important;}
#minutes{padding:2px 4px !important;}
#hours{padding:2px 8px !important;}
#minutes{padding:2px 4px !important;}
#seconds{padding:2px 2px !important;}
.profile-spkr {padding: 5% 0;}
.logo-mobile {position: fixed;top: 0;background: white;width: 100%;padding: 1% 4%;z-index: 9999;}
.banner-part {padding:0;}
section.ticket {padding: 9% 0;}
section#speakers {padding: 0 0 7% 0;}
section#About-ISEY {padding: 0 0 7% 0;}
.dataTables_scrollBody{width: 19em !important;}
section.eventslp_sticky{position: fixed;bottom: 0;z-index: 9999;background: #1ca6d3;width: 100%;padding:2% 0;}
.now-en a{color: white !important;text-align: center !important;padding: 3% 0;}
.count_time {text-align: center;}
menu-header{width:100%; background:#ffffff; height:60px; line-height:60px; border-bottom:1px solid #dddddd; }
.hamburger{background:none; position:absolute; bottom:14px; left: 315%; line-height:45px; padding:5px 15px 0px 15px; color:#00b9c5; border:0; font-size:1.4em; font-weight:bold; cursor:pointer; outline:none; z-index:10000000000000; } 
.cross{background:none; position:absolute; bottom:-21px;left:335%; padding:7px 15px 0px 15px; color:#2ab8d1; border:0; font-size:3em; line-height:65px; font-weight:bold; cursor:pointer; outline:none; z-index:10000000000000; }
.menu{z-index:1000000; font-weight:bold; font-size:0.8em; width:371%; background:#f1f1f1;  position:absolute; text-align:center; font-size:12px;}
.menu ul {margin: 0; padding: 0; list-style-type: none; list-style-image: none;.width:100%;}
.menu li {display: block;   padding:15px 0 15px 0; border-bottom:#dddddd 1px solid;}
.menu li:hover{display: block;    background:#ffffff; padding:15px 0 15px 0; border-bottom:#dddddd 1px solid;}
.menu ul li a { text-decoration:none;  margin: 0px; color:#666;}
.menu ul li a:hover {  color: #666; text-decoration:none;}
.menu a{text-decoration:none; color:#666;}
.menu a:hover{text-decoration:none; color:#666;}
.glyphicon-home{color:white; font-size:1.5em; margin-top:5px; margin:0 auto; }
a.mob_tick{color: black;text-decoration: none;background: #fdbf0f;display: inline-block;margin: 0% 135%;border-radius: 9px;font-size: 15px;padding: 16% 28%;text-transform:uppercase;font-family: 'RalewayMedium';}
.table-scroll {padding: 24% 0 0 0;}
th.time,th.event,th.speaker,th.topic{font-size:15px;width:20%;padding: 5px 13px;}
td{font-size:10px;padding: 4% 1%;}
th{padding:0;}
.count_timer {text-align: center;}
#earlyform {background: #18b2c1;padding: 0 0 7% 4%;}
.padot_form {border: 2px solid #18b2c1;border-radius: 30px;background: white;width: 96%;padding: 0 -4%;}
}

@media only screen and (min-width: 768px){
section.banner-part {padding: 6% 0 0 0;}
.sub_text p{border-right:1px solid #c69e08;}
section.form {position: absolute;top: 28%;right: 8%;left: auto;
background: white;}
.padot_form h5 {font-size: 20px;padding: 4% 3%;color: #00bac6;text-align:center;font-family: 'RalewayMedium';font-weight:bold;}
.table-scroll {padding: 3% 0;}
section.top_header {padding: 2% 0 1% 0;position: fixed !important;z-index: 9999;width: 100%;background: white;}
ul.menu-bar li.tick a{font-size:16px;padding:1% 12% 0 10%;}
.modal-dialog {max-width: 1000px !important;}
.date_time ul li{float: left;padding: 1% 3%;font-size: 20px;font-family: 'RalewayBold';border-right: 2px solid;}
.logo-mobile {display: none;}
section#About-ISEY {padding: 1% 0 3% 0;}
.count_time{text-align:center;}
.line_bottoms{padding: 5% 0;}
}


span.logo {position: absolute;top: -0.3em;bottom: 0;right:68%;}
.counter {display: inline-block; line-height: 1; padding: 0px 0 0 31px; font-size: 18px;color:white; text-align: center;}
span {display: block; font-size: 14px; color: white;font-family: 'RalewayMedium'; }
#days {background: #33b3d9;padding: 2px 18px;margin: 0 auto;display: inline-block;border: 4px solid black;font-size: 25px;border-radius:12px;font-family: 'RalewayMedium';}
#minutes {background: #33b3d9;padding: 2px 8px;margin: 0 auto;display: inline-block;border: 4px solid black;font-size: 25px;border-radius:12px;font-family: 'RalewayMedium';}
#seconds {background: #33b3d9;padding: 2px 5px;margin: 0 auto;display: inline-block;border: 4px solid black;font-size: 25px;border-radius:12px;font-family: 'RalewayMedium';}
#hours {background: #33b3d9;padding: 2px 15px;margin: 0 auto;display: inline-block;border: 4px solid black;font-size: 25px;border-radius:12px;font-family: 'RalewayMedium';}
li.bnglore {border-right: none !important;}
.modal-content {margin-top: 20% !important;z-index: 9999;}
.modal-body p{font-size:16px;font-family: 'RalewayMedium';}
.btn-info {color: #fff;background-color: #a1cb3b;border-color: #a1cb3b;font-size: 16px !important;border-radius: 15px !important;font-family: 'RalewayMedium';}
.modal-footer{border-top: none !important;}
button.btn.btn-default {color: white !important;background: #19b5be !important;border: 2px solid #19b5be !important;}
</style>
<?php
endwhile;
get_footer();
	