<?php
/**
 * Template Name: Thankyou Day Care 2019
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
$current_pageid = get_the_ID();
?>
<?php if(get_field('dt_gtm_header')): the_field('dt_gtm_header'); endif; ?>
<?php if(get_field('dt_gtm_footer')): the_field('dt_gtm_footer'); endif; ?>
<section class="fullcols banner_holder">
	<?php $lg_banner = get_field('large_banner'); $xs_banner = get_field('small_mobile_banner'); ?>
	<?php if(!empty($lg_banner)): ?><img src="<?php echo $lg_banner["url"];?>" alt="banner" width="<?php echo $lg_banner["width"];?>" height="<?php echo $lg_banner["height"];?>"  class="img-fluid d-none d-sm-none d-md-block"  /><?php endif; ?>
	<?php if(!empty($xs_banner)): ?><img src="<?php echo $xs_banner["url"];?>" alt="banner" width="<?php echo $xs_banner["width"];?>" height="<?php echo $xs_banner["height"];?>"  class="img-fluid d-block d-sm-block d-md-none"  /><?php endif; ?>
	<div class="form_holder" id="register">
		<h1 class="col-12 col-sm-12 text-left fs-20"><?php the_field('thankyou_message');?></h1>
	</div>
</section>

<style type="text/css">
.page-id-<?php echo $current_pageid; ?> #masthead, .page-id-<?php echo $current_pageid; ?> .banner, .page-id-<?php echo $current_pageid; ?> .title-holder, .page-id-<?php echo $current_pageid; ?> .and-theres-more-at-klay, .page-id-<?php echo $current_pageid; ?> #colophon, .page-id-<?php echo $current_pageid; ?> .copyrights, .page-id-<?php echo $current_pageid; ?> .blue-bg, .page-id-<?php echo $current_pageid; ?> .important-menu-items  { display : none !important; }
.page-id-<?php echo $current_pageid; ?> .fullcols { float : left; width : 100%; position : relative; paddding :0; margin : 0 auto; }

.black { color: #333; }
.white { color: #fff; }

.banner_holder img { width : 100%; height : auto; }

.form_holder h1 { font-family: 'GothamRoundedBold_0'; color : #fff; }

@media only screen and (min-width: 767px)
{
.form_holder { background: #00b3c4;  position: absolute;  z-index: 999;  max-width: 400px;  left: 10px;  right: 0;  top: 32px;
  opacity: 0.89;   border-radius: 10px;   padding: 16px; }
.footer_socials ul li {  display: inline-block;   padding: 10px; }
.show_xs { display : none; }
.show_md { display : block; }
.show_header_xs { display : none; }
}

@media only screen and (max-width: 767px)
{
.form_holder { background: #00b3c4;  position: relative;  z-index: 999; width :100%;  max-width: 100%; opacity: 0.89;   border-radius: 10px;   padding: 16px; }	
.show_md { display : none; }
}
</style>
<?php
endwhile;
get_footer();
