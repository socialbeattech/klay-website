<?php
/**
 * Template Name: Team
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>

<section class="pt-50 pb-50 section_content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 image_center gotham-rounded-book">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>
<section class="section_team_ceo">
	<div class="container-fluid">
		<div class="team_width">
			<div class="team_box">
				<div class="row ceo">
					<div class="col-12 col-md-4 image_center gotham-rounded-book">
						<?php // set the image url
					$image_url = get_field('team_image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
						<img src="<?php the_field('team_image'); ?>" alt="<?php echo $image_alt; ?>">
					</div>
					<div class="col-12 col-md-8 ceo_margin">
						<h2 class="gotham-rounded-medium"><?php the_field('team_name'); ?></h2>
						<h3 class="gotham-rounded-book"><?php the_field('team_designation'); ?></h3>
						<p class="gotham-rounded-book"><?php the_field('team_content'); ?></p>
					</div>
				</div>
			</div>

			<div class="row">
				<?php $i=1; while(have_rows('team')){the_row(); ?>
					<div class="col-12 col-md-4">
						<div class="team_back team_mem">
							<div class="image_center">
								<?php // set the image url
					$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
								<img src="<?php the_sub_field('image') ?>" alt="<?php echo $image_alt; ?>">
							</div>
							<h2 class="gotham-rounded-medium image_center"><?php the_sub_field('title') ?></h2>
							<h3 class="gotham-rounded-book image_center color<?php echo $i; ?>"><?php the_sub_field('designation') ?></h3>
							<p class="gotham-rounded-book team_content"><?php the_sub_field('content') ?></p>
						</div>
					</div>
					<style type="text/css">
	.team_back.team_mem:hover .color<?php echo $i; ?> {
    border-bottom: 3px solid <?php the_sub_field('color') ?>;
}
.color<?php echo $i; ?> {
    border-bottom: 3px solid #fff;
}
@media only screen and (max-width: 767px){
	.color<?php echo $i; ?> {
    border-bottom: 0px solid #fff;
}
	.team_back.team_mem:hover .color<?php echo $i; ?> {
    border-bottom: 0px solid <?php the_sub_field('color') ?>;
}
	}
</style>
				<?php $i++; } ?>
			</div>

		</div>
	</div>
</section>

<?php
endwhile;
get_footer();
