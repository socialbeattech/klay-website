<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Klay Schools
 */

get_header();
while ( have_posts() ):	the_post();
	wpb_set_post_views(get_the_ID());
	wpb_get_post_views(get_the_ID()); 
	
?>
<section class="pt-50 pb-50 post_cat">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-8">
				<?php
			/*	if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs" class="fs-14">','</p>' );
				}*/
				?>
				<p id="breadcrumbs" class="fs-14"><span><span><a href="https://www.klayschools.com/">Home</a> » <span><a href="https://www.klayschools.com/parent-tales/">Parents Tale</a> » <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span></p>
				<div class="single_content">
					<h3 class="gotham-rounded-medium"><?php the_title(); ?></h3>
					<div class="deskview">
				<div class="row">
					<div class="col-8 gotham-rounded-book meta_content">
						<?php if(get_field('parent_blogger')){ ?><strong><?php the_field('blogger_label'); ?>: <span class="orange-color"><?php the_field('parent_blogger'); ?></span></strong> | <?php } echo get_the_date(); ?>  |  Read time: <?php echo do_shortcode('[rt_reading_time postfix="mins" postfix_singular="min"]'); ?>
					</div>
					<div class="col-4">
						<?php echo do_shortcode('[addtoany]'); ?>
						
					</div>
				</div>
			</div>
				<div class="mobileview">
				<div class="row">
					<div class="col-7 gotham-rounded-book meta_content">
						<?php if(get_field('parent_blogger')){ ?><strong><?php the_field('blogger_label'); ?>: <span class="orange-color"><?php the_field('parent_blogger'); ?></span></strong>  <?php } ?> <br> <?php echo get_the_date(); ?>  |  Read time: <?php the_field('read_time') ?>
					</div>
					<div class="col-5 mobile_size">
						<?php echo do_shortcode('[addtoany]'); ?>
						
					</div>
				</div>
				</div>
				
					<?php if(!empty(get_the_post_thumbnail())){ ?>
						<?php the_post_thumbnail('full',array('class'=>'w-100 h-auto')); ?>				
					<?php }else{ ?>
						<img src="<?php echo get_template_directory_uri(); ?>/images/default-thumbnail.jpg" alt="" class="w-100 h-auto" />
					<?php } ?>
					<br>	<br>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-12 col-md-4 sidebar_blog">
				<!--<div class="write_blog">
					<h3 class="gotham-rounded-medium">Want to write a Blog?</h3>
					<p class="gotham-rounded-book">There is a larger audience out there, looking for parenting tips and skills.</p>
					<p class="gotham-rounded-book">If you would like to share your parenting skills  and experience, please write to us at <span class="gotham-rounded-medium"><a href="mailto:username@klayschools.com">username@klayschools.com</a></span></p>
				</div>-->
				<div class="write_blog">
					<h3 class="gotham-rounded-medium">Latest Blogs</h3>
					<?php
							global $post; $i = 1;
							$args = array( 'numberposts' => 3, 'post_type' => 'post' );
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) { setup_postdata($post); 
						?>
					<div class="row latest_single">
						<div class="col-5">
							<?php if(!empty(get_the_post_thumbnail())){ ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blogsingle',array('class'=>'w-100 h-auto')); ?></a>	<?php }else{ ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/default-thumbnail.jpg" alt="" class="w-100 h-auto" /></a>
							<?php } ?>
						</div>
						<div class="col-7">
							<p class="gotham-rounded-book lat_title"><a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a></p>
							<p class="gotham-rounded-book lat_title"><?php the_field('parent_blogger'); ?><br><?php the_date('d/m/y'); ?></p>
							
						</div>
					</div>
				<?php } ?>
				<h3 class="gotham-rounded-medium popu_blog">Popular Blogs</h3>
				<?php
							global $post; $i = 1;
							$args = array( 'numberposts' => 1, 'post_type' => 'post','meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC' );
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) { setup_postdata($post); 
						?>
				<div class="row latest_single">
						<div class="col-12">
							<?php if(!empty(get_the_post_thumbnail())){ ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blogsingle',array('class'=>'w-100 h-auto')); ?></a>	<?php }else{ ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/default-thumbnail.jpg" alt="" class="w-100 h-auto" /></a>
							<?php } ?>
						</div>
						<div class="col-12">
							<p class="gotham-rounded-book lat_title Popular"><a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a></p>
							<p class="gotham-rounded-book lat_title_new"><?php the_field('parent_blogger'); ?><br><?php the_date('d/m/y'); ?></p>
							<?php echo do_shortcode('[post-views]'); ?><span class="gotham-rounded-book lat_title">times</span>
						</div>
					</div>
				<?php } ?>
				<div class="gform_heading">
                            <h3 style="font-family: 'GothamRoundedMedium';font-weight: 700; font-size: 1.25em;" class="gform_title">Enquire Now</h3>
                            <span class="gform_description">Get in touch with us to schedule a visit or for any further information that you may require.</span>
                        </div>
				<iframe src="https://go.pardot.com/l/563842/2019-02-18/4vmwc" width="100%" height="520" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
endwhile;
get_footer();
