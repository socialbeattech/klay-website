<?php
/**
 * Template Name: Admission
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>

<section class="pt-50 pb-50 sec_admission">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 image_center">
				<h2 class="mt-0 mb-15 section-title">Be a part of the KLAY Family!</h2>
				<!--<h3 class="gotham-rounded-book"><a href="#">Click here for Fee Details ></a></h3>-->
			</div>
		</div>
		
	</div>	
</section>
<section class="contact_sec">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3 image_center">
				<!-- <img src="<?php echo get_template_directory_uri() ?>/images/contact/contact_back.png" alt="" /> -->
				<div class="contact-form1">
					<div class="contact-form11 enquire-now pt-15 pb-15 no-label-form admin_form">
					<?php //echo do_shortcode('[gravityform id="1" title="true" description="true" ajax="true"]'); ?>
					<!-- <div class="gform_heading">
					                            <h3 style="font-family: 'GothamRoundedMedium';font-weight: 700; font-size: 1.25em;" class="gform_title">Enquire Now</h3>
					                            <span class="gform_description">Get in touch with us to schedule a visit or for any further information that you may require.</span>
					                        </div> -->
				
				<!-- <iframe src="http://go.pardot.com/l/563842/2019-02-18/4vmwc" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> -->
				<?php the_field('iframe_for_singe_form','option'); ?>
				<iframe src="http://go.pardot.com/l/563842/2019-04-04/53hfm?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
				</div>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="office_sec">
	<div class="container-fluid">
		<hr>
		<div class="row">
			<div class="col-12 image_center">
				<h2 class="mt-0 mb-15 section-title">Our Programs</h2>
			</div>
		</div>
		<div class="program_size">
		<div class="row">
			<?php $i=1; while(have_rows('admission_program')){ the_row();  ?>
				<?php // set the image url
					$image_url = get_sub_field('image');
					$image_id = pippin_get_image_id($image_url); 
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
				?>
			<div class="col-12 col-md-4 image_center">
				<div class="bordernew">
				<img src="<?php the_sub_field('image') ?>" alt="<?php echo $image_alt; ?>">
				<h3 class="gotham-rounded-medium"><?php the_sub_field('title') ?></h3>
				</div>
			</div>
		<?php $i++; } ?>
		</div>
	</div>
	</div>	
</section>
<section class="loc_sec">
	<div class="container-fluid">
	
		<div class="row">
			<div class="col-12 image_center">
				<h2 class="mt-0 mb-15 section-title">Our Locations</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-12 image_center">
				<div class="office">
					
					<ul class="list-inline image_center map_mobile">
						
						<?php $args = array( 'orderby'=> 'name', 'order' => 'ASC');
						$categories = get_terms( 'centres_category', $args);
						$i = 0;	
						foreach($categories as $key => $value){	?>
							
							<li class="gotham-rounded-medium"><img src="<?php echo get_template_directory_uri() ?>/images/admission/loc_icon.png" alt="" /><span class="br_of"><br></span><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name; ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</section>
<section class="loc_sec_off">
	<div class="container-fluid">
	
		<div class="row">
			<div class="col-12 image_center">
				<h2 class="mt-0 mb-15 section-title">Reach Us</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-12 image_center">
				<div class="office add_office">
					<div class="map_off">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.823980226118!2d77.70681851477079!3d12.983107490848328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1394cd62b5a3%3A0xa4acd85f7d997f23!2sKLAY+Prep+Schools+%3A+Corporate+Office!5e0!3m2!1sen!2sin!4v1546513830657" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
					<?php $main_field = get_field('contact',1058); ?>

						<h1 style="color:#0099cc;" class="gotham-rounded-medium"><?php echo $main_field[0]['title']; ?></h1>
						<p class="gotham-rounded-book"><?php echo $main_field[0]['address']; ?> <a style="color:#333333;" href="mailto:<?php echo $main_field[0]['email']; ?>"><span style="color:#333333;font-weight:bold;"><?php echo $main_field[0]['email']; ?></p></span></a>
						<p style="color:#333333;font-weight:bold;" class="gotham-rounded-book"><?php echo $main_field[0]['phone']; ?></p>

					<?php  ?>
				</div>
			</div>
		</div>
	</div>	
</section>
<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>
<?php
endwhile;
get_footer();
