<?php
/**
 * Template Name: FAQ
 *
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
	.accordion1 .card-header:after {
    font-family: 'FontAwesome';  
    content: "\f068";
    float: right; 
}
.accordion1 .card-header.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\f067"; 
}
</style>
<section class="pt-50 pb-50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				    <div id="accordion" class="accordion1">
        <div class="card mb-0">
        	<?php $i=1; while(have_rows('faq')){the_row(); ?>
            <div class="card-header collapsed" data-toggle="collapse" href="#collapse<?php echo $i; ?>">
                <a class="card-title">
               		<?php the_sub_field('title') ?>
                </a>
            </div>
            <div id="collapse<?php echo $i; ?>" class="card-body collapse" data-parent="#accordion" >
                <p><?php the_sub_field('content') ?>
                </p>
            </div>
        <?php $i++; } ?>

        </div>
    </div>
			</div>
		</div>
		
	</div>
</section>

<?php
endwhile;
get_footer();
