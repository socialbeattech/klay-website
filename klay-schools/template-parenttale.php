<?php
/**
 *  Template Name: Parent Tale page
 *
 * 
 */

get_header(); ?>


<section class="pt-50 pb-50 sec_admission">
	<div class="container-fluid">
		<div class="row">
		<h1 class="float-left col-12 col-sm-12 text-center gotham-rounded-medium fs-xs-24 fs-24 pb-4">Our Moms and Dads have stories too! Read on!</h1>
		
			<?php
			//$paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
			//$args = array('post_type' => 'parents_tale','post_status' => 'publish','posts_per_page' => -1,order);
			$args = array( 'numberposts' => -1, 'post_type' => 'parents_tale');
			$arr_posts = new WP_Query( $args );
			?>

			<?php if ( $arr_posts->have_posts() ) : ?>
				<?php while ( $arr_posts->have_posts() ) : ?>
					<?php $arr_posts->the_post(); ?>				
					<div class="float-left col-12 col-sm-12 col-md-3 blogs_list">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="resource_thumb p-0 m-0 float-left col-12 col-sm-12 float-left text-center">
								<a href="<?php the_permalink(); ?>">								
								<?php the_post_thumbnail(); ?>
								</a>
							</div>							
							<?php $catalog_title = get_the_category($id)[0]->name; ?></a>							
							<!-- <div class="resource_band"><a href="<?php the_permalink(); ?>">							
							<?php if($catalog_title == 'Blog'): ?>							
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog.png" class="img-fluid img-responsive" alt="girl" width="13" height="19" />
							<?php elseif($catalog_title == 'Case Study'): ?>
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/article.png" class="img-fluid img-responsive" alt="girl" width="18" height="17" />
							<?php elseif($catalog_title == 'Video'): ?>	
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/tube.png" class="img-fluid img-responsive" alt="girl" width="18" height="16" />
							<?php elseif($catalog_title == 'webinar'): ?>
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/webinar.png" class="img-fluid img-responsive" alt="girl" width="12" height="19" />
							<?php else: ?>	
								<img src="https://www.klayschools.com/corporate/wp-content/themes/klaymicro/images/blog.png" class="img-fluid img-responsive" alt="girl" width="13" height="19" />
							<?php endif; ?>							
							</div> -->
							<h2 class="resource_title p-0 mt-3 float-left col-12 col-sm-12 float-left text-center"><a href="<?php the_permalink(); ?>" class="gotham-rounded-book fs-xs-18 fsnew-18"><?php the_title(); ?></a></h2>
						</article>
					</div>					
				<?php endwhile; ?>
					
			<?php endif; ?>		
		</div><!-- .container -->
	</div><!-- .content-area -->
</section>
<?php get_footer();