<?php
/**
* Template Name: Curriculum
*
* This is the template that displays home page by default.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package Klay Schools
*/
get_header(); ?>
<style>
    .gray-line{border-top: 1px solid #999999;}
    .tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}
</style>
<section class="pt-50 pb-50">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-12 mb-40">
				<?php the_field('content'); ?>
			</div>
			<?php
            // check if the repeater field has rows of data
            if( have_rows('flower-images') ):
                // loop through the rows of data
                while ( have_rows('flower-images') ) : the_row(); ?>    
                <div class="col-lg-4 col-md-4 col-sm-12 text-center pt-30 pb-xs-30">
                    <p class="gotham-rounded-medium"><?php the_sub_field('text');?></p>
                    <img src="<?php the_sub_field('images');?>" alt="">
                </div>
            <?php endwhile;
            endif;
            ?>
		</div>
    </div>
</section>
<div class="container-fluid">
    <hr class="gray-line">
</div>    
<section class="mt-50">
   <div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-40 text-center">
			    <h2 class="mt-0 mb-30 section-title"><?php the_field('sec_heading'); ?></h2>
				<?php the_field('sec_content'); ?>
				<div class="container">
                    <div class="swiper-container" id="singleSlideSwiper">
                        <div class="swiper-wrapper"> 
                            <?php
                                if( have_rows('swiper') ):
                                    while ( have_rows('swiper') ) : the_row(); ?>
                                        <?php // set the image url
                    $image_url = get_sub_field('image');
                    $image_id = pippin_get_image_id($image_url); 
                    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                ?>
                                    <div class="swiper-slide">
                                      <img class="" style="width:100%;" src="<?php the_sub_field('image'); ?>" alt="<?php echo $image_alt; ?>"/>
                                      <!-- <img class="d-block d-sm-block d-md-none d-lg-none" style="width:100%;" src="<?//php the_sub_field('mobile-banner','options'); ?>" /> -->
                                    </div>
                                    <?php endwhile;
                                else :
                                endif;
                            ?>
                        </div>
                        <div class="single-slide-prev"></div>
                        <div class="single-slide-next"></div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</section>
<div class="container-fluid">
    <hr class="gray-line">
</div>
<section class="mt-50">
   <div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-40 text-center">
			    <h2 class="mt-0 mb-30 section-title"><?php the_field('third_title'); ?></h2>
				<?php the_field('third_content'); ?>
            </div>
        </div>
    </div>           
</section>
<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section> 
 
<?php get_footer();?>