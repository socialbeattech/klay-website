<?php
/**
 * Template Name: Enrichment Program
 *
 * This is the template that displays home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Klay Schools
 */
 get_header();
 while ( have_posts() ) : the_post(); ?>
<section class="pt-50 pb-50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<?php the_field('main_content'); ?>
                <div class="d-none d-md-block">
                    <img src="<?php echo get_template_directory_uri() ?>/images/enrichment-yellowbg.png" alt="" class="w-100" />
                </div>
                <div class="light-yellow-bg">
                    <div class="enchriment-yellow-content">
                       <div class="row">
                        <div class="col-12 col-md-6">
                            <img class="pt-xs-14" src="<?php the_field('image1'); ?>" alt="">
                        </div>
                        <div class="col-12 col-md-6"> 
                            <div class="row">
                                    <div class="col-8">
                                        <div class="br_align"></div>
                                  <h2 class="gotham-rounded-medium fs-24 fs-xs-20"><?php the_field('title1'); ?></h2>        
                            </div>
                            <div class="col-4">
                             
                                <img class="logo1" src="<?php the_field('logo1'); ?>" alt="">
                            </div>
                        </div>
                            <?php the_field('content1'); ?>

                        </div>
                         
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</section>
<section>
   <div class="container-fluid">
		<div class="row">
			<div class="col-12">
                <div class="d-none d-md-block">
                    <img src="<?php echo get_template_directory_uri() ?>/images/enrichment-yellowbg.png" alt="" class="w-100 h-100vh" />
                </div>
                <div class="light-yellow-bg">
                    <div class="enchriment-yellow-content2">
                       <div class="row">
                        <div class="col-12 col-md-6">
                            <img src="<?php the_field('image2'); ?>" alt="">
                        </div>
                        <!-- <div class="col-12 col-md-6 mt-40"> 
                            <?php //the_field('content2'); ?>
                        </div>
                        <img class="logo2" src="<?php //the_field('logo2'); ?>" alt="">   -->
                        <div class="col-12 col-md-6"> 
                            <div class="row">
                                    <div class="col-8">
                                        <div class="br_align"></div>
                                  <h2 class="gotham-rounded-medium fs-24 fs-xs-20"><?php the_field('title2'); ?></h2>        
                            </div>
                            <div class="col-4">
                             
                                <img class="logo1" src="<?php the_field('logo2'); ?>" alt="">
                            </div>
                        </div>
                            <?php the_field('content2'); ?>

                        </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</section>
<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>
<?php
endwhile; // End of the loop.
get_footer();