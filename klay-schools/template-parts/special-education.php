<?php
/**
 * Template Name: Special Education
 *
 * This is the template that displays home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Klay Schools
 */
 get_header();
 while ( have_posts() ) : the_post(); ?>
<style>
    ul li{font-family: 'GothamRoundedBook';}
</style>
 <section class="pt-50 pb-50">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 mb-30 section-title"><?php the_field('section_title'); ?></h2>
				<?php the_field('first_content'); ?>
			</div>
		</div>
    </div>
</section>
<section>
<div class="container">
    <?php if( get_field('image') ): ?>
        <?php // set the image url
                    $image_url = get_field('image');
                    $image_id = pippin_get_image_id($image_url); 
                    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                ?>
	    <img src="<?php the_field('image'); ?>" alt="<?php echo $image_alt; ?>"/>
    <?php endif; ?>
</div>
</section>
<section class="pt-50 pb-50">
	<div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2 class="mt-0 mb-30 fs-24"><strong class="gotham-rounded-medium"><?php the_field('second_title'); ?></strong></h2>
                <?php the_field('second_content'); ?>
            </div>
            <div class="col-lg-4 col-lg-4 col-sm-12">
                <div class="enquire-now pt-15 pb-15 no-label-form">
                    <?php //echo do_shortcode(get_field('form')); ?>
                    <div class="gform_heading">
                            <h3 style="font-family: 'GothamRoundedMedium';font-weight: 700; font-size: 1.25em;" class="gform_title">Enquire Now</h3>
                            <span class="gform_description">Get in touch with us to schedule a visit or for any further information that you may require.</span>
                        </div>
                        <?php //echo do_shortcode(get_field('form')); ?>
                        <iframe src="http://go.pardot.com/l/563842/2019-04-04/53hgp?Source_URL=<?php the_permalink(); ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
                <!-- <iframe src="https://go.pardot.com/l/563842/2019-02-18/4vmwc" width="100%" height="550" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe> -->
                </div>    
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="gray-bg">
                    <div class="col-12">   
                        <h2 class="mt-0 mb-30 section-title pt-30"><?php the_field('third_title'); ?></h2>
                        <?php the_field('third_content'); ?>
                    </div>
                </div>
            </div>  
        </div>  
    </div>
</section>
<section class="mt-30">
    <h2 class="mt-0 mb-20 section-title"><?php the_field('fourth_title'); ?></h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="lightgray-bg pt-30 pb-30">
                    <div class="col-12 text-justify">
                        <?php the_field('fourth_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <section class="what_parent">
    <span class="grey-foldable-border"></span>
</section> -->
<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
// Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var source1 = getParameterByName('utm_medium');
var source2 = getParameterByName('utm_campaign');
var ifr = document.querySelectorAll('iframe')[1];


//ifr.setAttribute('src', ifr.getAttribute('src')+'?source='+source)
ifr.setAttribute('src', ifr.getAttribute('src')+'&source='+source+'&Location_hidden='+source1+'&Campaign_Name='+source2)
</script>
<?php
endwhile; // End of the loop.
get_footer();