<?php
/**
 * Template Name: Facilities
 *
 * This is the template that displays home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Klay Schools
 */
 get_header();
 while ( have_posts() ) : the_post(); ?>
<style>
    .gray-line{border-top: 2px solid #999999;}
    .facilities-border:hover{border: 2px solid #999999;}
    
</style>
<section class="pt-50 pb-50 pb-xs-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="mt-0 section-title"><?php the_field('heading'); ?></h2>
				<h2 class="mt-0 gotham-rounded-book mb-30 text-center fs-24 fs-xs-16"><?php the_field('sub-heading'); ?></h2>
				<?php the_field('main-content'); ?>
			</div>
		</div>
    <hr class="gray-line">
    </div>    
</section>
<section>
   <div class="container text-center">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3">
				<h2 class="mt-0 section-title pb-30"><?php the_field('second-heading'); ?></h2>
                <div class="swiper-container" id="singleSlideSwiper">
                    <div class="swiper-wrapper"> 
                        <?php
                            if( have_rows('swiper') ):
                                while ( have_rows('swiper') ) : the_row(); ?>
                                    <?php // set the image url
                    $image_url = get_sub_field('swiper-image');
                    $image_id = pippin_get_image_id($image_url); 
                    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                ?>
                                <div class="swiper-slide">
                                    <img class="" src="<?php the_sub_field('swiper-image'); ?>" alt="<?php echo $image_alt; ?>"/>
                                    <div class="gray-bg sizing"><?php the_sub_field('text'); ?></div> 
                                </div>
                                <?php endwhile;
                            else :
                            endif;
                        ?>
                    </div>
                    <div class="single-slide-prev"></div>
                    <div class="single-slide-next"></div>
                </div>   
            </div>
       </div>
    </div>       				
</section>
<div class="container-fluid">
    <hr class="gray-line">
</div>
<div class="section">
    <div class="container-fluid">
      <h2 class="section-title pt-50 pb-50"><?php the_field('third-heading'); ?></h2>
       <div class="col-lg-12 col-md-12 colsm-12">
       <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12"></div>
        <div class="col-lg-2 col-md-2 col-sm-12 text-center facilities-border">
            <a class="" href="<?php bloginfo('url') ?>/gallery/" target="_blank"><img class="" style="padding-top:20px" src="<?php the_field('know1'); ?>" alt="gat1"/>
            <?php the_field('know1-text'); ?></a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 text-center facilities-border">
            <a href="<?php bloginfo('url') ?>/news-events/" target="_blank"><img class="" style="padding-top:20px" src="<?php the_field('know2'); ?>" alt="get2"/>
            <?php the_field('know2-text'); ?></a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 text-center facilities-border">
            <a href="<?php bloginfo('url') ?>/parent-resources/" target="_blank"><img class="" style="padding-top:20px" src="<?php the_field('know3'); ?>" alt="get3"/>
            <?php the_field('know3-text'); ?></a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12"></div>
        </div>
        </div>
    </div>
</div>
<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>       				
<?php
endwhile; // End of the loop.
get_footer();