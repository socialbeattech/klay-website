<?php
/**
 * Template Name: Placements
 * This is the template that displays day care layout.
 *
 * @package Klay Schools
 */

get_header();
while(have_posts()): the_post();
?>
<style type="text/css">
li.gotham-rounded-book.list {
    width: 50%;
    float: left;
    line-height: 2;
}.col-12.col-md-12.mb-20.branches {
    padding: 35px 25px;
    background: #f2f2f2;
}ul {
  list-style: none;
}

.list::before {content: "•"; color: #fb8220;
  display: inline-block; width: 1em;
  margin-left: -1em}
@media only screen and (max-width:767px){
h1.fs-40.fs-xs-20.mb-50.mt-0.gotham-rounded-book{
	font-size: 20px;
    width: 320px;
    text-align: center;
    margin-bottom: 0;
    position: relative;
    bottom: 0;
    top: 28px !important;
    font-weight: bold;
}.col-12.col-md-12.mb-20.branches {
    padding: 25px 0px;
    background: #f2f2f2;
}li.gotham-rounded-book.list {
    width: 100%;
    float: left;
    line-height: 2;
    font-size: 16px;
}
}
</style>


<section class="pt-50 pb-50 pb-xs-30">
	<div class="container">
		<div class="row">
			<div class="col-12">
				
				<p class="mt-0 gotham-rounded-book mb-30 text-center fs-24 fs-xs-16">Here is a list of schools that our children go to, after graduating from KLAY!</p>

				</div>
			</div>

			<div class="row mb-20">
				
				<?php if( have_rows('placements') ):?><?php while ( have_rows('placements') ) : the_row();?><div class="col-12 col-md-12 mb-20 branches">
				 <b><p class="mt-0 gotham-rounded-book fs-24 fs-xs-16" style="padding-left: 40px;margin-bottom: 10px"><?php the_sub_field('city'); ?></p></b>
				 <?php if( have_rows('branches') ): ?> <ul> <?php while ( have_rows('branches') ) : the_row();?><li class="gotham-rounded-book list fs-18"><?php the_sub_field('branch'); ?></li><?php  endwhile; ?> </ul><?php else : endif;	?></div>
				<?php  endwhile;?>  <?php else : endif;	?>
			</div>
			
		</div>

  
</section>
<section>
	<div class="container-fluid">
		<hr class="gray-line">
	</div>
</section>

<section class="what_parent mt-50">
	<span class="grey-foldable-border"></span>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">What Parents are Saying!</h2>
			</div>
			<div class="col-sm-12 gotham-rounded-light">
				<?php $term = get_queried_object();
				echo $test_conent = get_field('testimonial_content', $term); ?>

				<p class="text-center name_sec blue-color"><strong><?php echo $test_title = get_field('testimonial_title', $term); ?></strong><br>
				<?php echo $test_designation = get_field('testimonial_designation', $term); ?><br>
				<a class="btn-submit par_padd" href="<?php echo $test_link = get_field('testimonial_link', $term); ?>">Read More</a>
				</p>
			</div>
		</div>
	</div>
</section>   




<?php
endwhile;
get_footer();
