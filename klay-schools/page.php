<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Klay Schools
 */

get_header();
?>

<section class="pt-50 pb-50 story_sec">
	<div class="container-fluid">
		<div class="row grey_back">
			<div class="col-12 col-md-12">

		<?php
		while ( have_posts() ) :
			the_post(); ?>
<p class="gotham-rounded-medium"><?php the_content(); ?></p>
			<?php
	

		endwhile; // End of the loop.
		?>

		</div><!-- #main -->
	</div><!-- #primary -->
</div></section>

<?php
//get_sidebar();
get_footer();
